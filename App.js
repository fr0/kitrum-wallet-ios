import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  View,
  TouchableWithoutFeedback,
  Text,
  Alert
} from 'react-native';
import { createStackNavigator } from 'react-navigation';
import SplashScreen from 'react-native-splash-screen';

import LoginLayout from './app/layouts/LoginLayout';
import WalletLayout from './app/layouts/WalletLayout';
import AddCard from './app/screens/wallet/AddCard';
import Products from './app/screens/wallet/Products';
import SelectContact from './app/screens/wallet/SelectContact';
import BankAccount from './app/screens/BankAccount';
import BankAccountLogin from './app/screens/BankAccountLogin';
import BankAccountComplete from './app/screens/BankAccountComplete';
import BankAccountSuccess from './app/screens/BankAccountSuccess';
import ProfileLayout from './app/layouts/ProfileLayout';
import Web from './app/screens/Web';
import LockScreen from './app/screens/Lock';

import IbanCardIO from './app/common/CardIO';

import TouchEventUtils from 'fbjs/lib/TouchEventUtils';

import PinCode from './app/screens/signup/PinCode';
import KYC from './app/screens/signup/KYC';
import KYCTerms from './app/screens/signup/KYCTerms';
import FitnanceTerms from './app/screens/signup/FitnanceTerms';
import FitnanceSecurityTerms from './app/screens/signup/FitnanceSecurityTerms';
import WebLogin from './app/screens/WebLogin';

import AddIBANAccount from './app/screens/signup/AddIBANAccount';
import BankAccountLoginIBAN from './app/screens/signup/BankAccountLogin';
import BankAccountCompleteIBAN from './app/screens/signup/BankAccountComplete';
import BankAccountLoginIBANSMS from './app/screens/signup/BankAccountLoginSMS';
import BankAccountSuccessIBan from './app/screens/signup/BankAccountSuccess'

import SendMoney from './app/screens/send-money/Form';

import RiskQuestionnaire from './app/screens/RiskQuestionnaire';
import RiskQuestionnaireResult from './app/screens/RiskQuestionnaire/Result';
import SavingQuestionnaire from './app/screens/SavingQuestionnaire';
import SavingQuestionnaireIntro from './app/screens/SavingQuestionnaire/Intro';

import Savings1 from './app/screens/savings/Savings1'
import Savings2 from './app/screens/savings/Savings2'
import Savings3 from './app/screens/savings/Savings3'
import Savings4 from './app/screens/savings/Savings4'
import Savings5 from './app/screens/savings/Savings5'
import Savings6 from './app/screens/savings/Savings6'
import Savings7 from './app/screens/savings/Savings7'
import Savings8 from './app/screens/savings/Savings8'
import Savings9 from './app/screens/savings/Savings9'
import Savings10 from './app/screens/savings/Savings10'
import Savings11 from './app/screens/savings/Savings11'
import InvestorType from './app/screens/savings/Savings11/InvestorType';
import Savings12 from './app/screens/savings/Savings12'

import Marketplace from './app/screens/Marketplace'
import MarketplaceDrilldown from './app/screens/Marketplace/Drilldown'
import MarketplaceOpportunity from './app/screens/Marketplace/Opportunity'
import MarketplaceForm from './app/screens/Marketplace/Form'
import MarketplaceFormBankInfo from './app/screens/Marketplace/BankInfo'
import MarketplaceFormHistory from './app/screens/Marketplace/History'
import MarketplaceFormStory from './app/screens/Marketplace/Story'
import MarketplaceFilters from './app/screens/Marketplace/Filters'

import PortfolioAddBalance from './app/screens/Portfolio/AddBalance'

let Nav = createStackNavigator({
  Login: { screen: LoginLayout },
  WebLogin: { screen: WebLogin },

  AddCard: { screen: AddCard },
  Products: { screen: Products },
  Wallet: { screen: WalletLayout },
  BankAccount: { screen: BankAccount },
  BankAccountLogin: { screen: BankAccountLogin },
  Profile: { screen: ProfileLayout },
  BankAccountComplete: { screen: BankAccountComplete },
  BankAccountLogin: { screen: BankAccountLogin },
  BankAccount: { screen: BankAccount },
  BankAccountSuccess: { screen: BankAccountSuccess },
  BankAccount: { screen: BankAccount },

  SendMoney: { screen: SendMoney },

  IbanCardIO: { screen: IbanCardIO },
  Web: { screen: Web },
  SelectContact: { screen: SelectContact },

  PinCode: {screen: PinCode},
  KYC: { screen: KYC },
  KYCTerms: { screen: KYCTerms },
  FitnanceTerms: { screen: FitnanceTerms },
  FitnanceSecurityTerms: { screen: FitnanceSecurityTerms },

  AddIBANAccount: { screen: AddIBANAccount },
  BankAccountLoginIBAN: { screen: BankAccountLoginIBAN },
  BankAccountCompleteIBAN: { screen: BankAccountCompleteIBAN },
  BankAccountLoginIBANSMS: { screen: BankAccountLoginIBANSMS },
  BankAccountSuccessIBan: { screen: BankAccountSuccessIBan },

  RiskQuestionnaire: {screen: RiskQuestionnaire},
  RiskQuestionnaireResult: {screen: RiskQuestionnaireResult},
  SavingQuestionnaire: {screen: SavingQuestionnaire},
  SavingQuestionnaireIntro: {screen: SavingQuestionnaireIntro},

  Savings1: {screen: Savings1},
  Savings2: {screen: Savings2},
  Savings3: {screen: Savings3},
  Savings4: {screen: Savings4},
  Savings5: {screen: Savings5},
  Savings6: {screen: Savings6},
  Savings7: {screen: Savings7},
  Savings8: {screen: Savings8},
  Savings9: {screen: Savings9},
  Savings10: {screen: Savings10},
  Savings11: {screen: Savings11},
  InvestorType: {screen: InvestorType},
  Savings12: {screen: Savings12},

  Marketplace: {screen: Marketplace},
  MarketplaceDrilldown: {screen: MarketplaceDrilldown},
  MarketplaceOpportunity: {screen: MarketplaceOpportunity},
  MarketplaceForm: {screen: MarketplaceForm},
  MarketplaceFormBankInfo: {screen: MarketplaceFormBankInfo},
  MarketplaceFormHistory: {screen: MarketplaceFormHistory},
  MarketplaceFormStory: {screen: MarketplaceFormStory},
  MarketplaceFilters: {screen: MarketplaceFilters},

  PortfolioAddBalance: {screen: PortfolioAddBalance}
})

export default class App extends Component {

  constructor(props){
    super(props);

    this.state = {
      lockScreen: false,
      currentRoute: 'Login',
    }

    this.noLockRoutes = [
      'Login', 'PinCode'
    ]

    this.timeSinceLastInteraction = 0

    this._onPress = this._onPress.bind(this)
    this._onPinSuccess = this._onPinSuccess.bind(this)
  }

  componentDidMount(){

    SplashScreen.hide()

    setInterval(() => {
      let {currentRoute} = this.state

      if(this.timeSinceLastInteraction++ > 360 && this.state.lockScreen === false && !this.noLockRoutes.includes(currentRoute)){
        this.setState({lockScreen: true})
      }
    }, 1000)
  }

  _onPress(event){
    this.timeSinceLastInteraction = 0
  }

  _onPinSuccess(){
    this.setState({lockScreen: false})
  }

  _getCurrentRouteName(navState) {
    if (navState.hasOwnProperty('index')) {
      this._getCurrentRouteName(navState.routes[navState.index])
    } else {
      this.setState({
        currentRoute: navState.routeName
      })
    }
  }

  render() {
    let { lockScreen } = this.state


    return (
      <View
        style={{height: '100%'}}
        onTouchStart={this._onPress}
      >
        {
          lockScreen
          ?
          <LockScreen
            onSuccess={this._onPinSuccess}
          />
          :
          <Nav
            onNavigationStateChange={(prevState, newState) => this._getCurrentRouteName(newState)}
          />
        }
      </View>
    );
  }
}