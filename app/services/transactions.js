import {
  AsyncStorage
} from 'react-native';
import { baseUrl } from './variables'

export function create(transaction) {
  return new Promise((resolve, reject) => {
    AsyncStorage.getItem('@iBANStore:session_token').then((session_token) => {

      fetch(
        `${ baseUrl }/api/v1/transactions?session_token=${session_token}`,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(transaction)
        }
      )
        .then(response => {
          if(response.status === 200) {
            response = JSON.parse(response['_bodyText']);
            resolve(response)
          }else{
            reject(response)
          }
        })
        .catch((error) => {
          reject(error)
        });
    })
  });
}