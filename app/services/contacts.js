import {
  AsyncStorage
} from 'react-native';
import { baseUrl } from './variables'

export function list() {
  return new Promise((resolve, reject) => {
    AsyncStorage.getItem('@iBANStore:session_token').then((session_token) => {
      fetch(
        `${ baseUrl }/api/v1/contacts?session_token=${session_token}`,
        {
          method: 'GET'
        }
      )
        .then(response => {
          if(response.status === 200) {
            response = JSON.parse(response['_bodyText']);
            resolve(response['contacts'])
          }else{
            reject(response)
          }
        })
        .catch((error) => {
          reject(error)
        });
    })
  });
}

export function upsert(contact) {
  return new Promise((resolve, reject) => {
    AsyncStorage.getItem('@iBANStore:session_token').then((session_token) => {

      const data = new FormData();
      data.append('first_name', contact.familyName);
      data.append('last_name', contact.givenName);
      data.append('phone_number', contact.phoneNumbers[0].number);
      if(contact.hasThumbnail){
        data.append('avatar', {
          uri: contact.thumbnailPath,
          type: 'image/png',
          name: 'avatar'
        });
      }

      fetch(
        `${ baseUrl }/api/v1/contacts?session_token=${session_token}`,
        {
          method: 'POST',
          body: data
        }
      )
        .then(response => {
          if(response.status === 200) {
            response = JSON.parse(response['_bodyText']);
            resolve(response)
          }else{
            reject(response)
          }
        })
        .catch((error) => {
          reject(error)
        });
    })
  });
}