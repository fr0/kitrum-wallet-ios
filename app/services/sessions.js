import {
  AsyncStorage,
  Alert
} from 'react-native';
import { baseUrl } from './variables'
import Contacts from 'react-native-contacts'
import {handleRequestError} from './variables'

export function login(email, password, navigation) {
  return new Promise((resolve, reject) => {

    fetch(
      `${ baseUrl }/api/v1/login`,
      {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          email,
          password
        })
      }
    )
      .then(response => {
        let data = JSON.parse(response['_bodyText']);

        if(response.status === 200 || response.status === 203){
          AsyncStorage.multiSet(
            [
              ['@iBANStore:session_token', data.session_token],
              ['@iBANStore:current_user', JSON.stringify(data.user)],
            ]
          ).then(error => {

          });
        }

        handleRequestError(response, navigation)

        if(response.status === 200 || response.status === 203){
          resolve(response)
        }else{
          reject(response)
        }
      })
      .catch((error) => {
        reject(error)
      });
  })
}