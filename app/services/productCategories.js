import {
  AsyncStorage
} from 'react-native';
import { baseUrl } from './variables'

export function all() {
  return new Promise((resolve, reject) => {
    AsyncStorage.getItem('@iBANStore:session_token').then((session_token) => {
      fetch(
        `${ baseUrl }/api/v1/product_categories?session_token=${session_token}`,
        {
          method: 'GET'
        }
      )
        .then(response => {
          if(response.status === 200) {
            response = JSON.parse(response['_bodyText']);
            resolve(response['product_categories'])
          }else{
            reject(response)
          }
        })
        .catch((error) => {
          reject(error)
        });
    })
  });
}