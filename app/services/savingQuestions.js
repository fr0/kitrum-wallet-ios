import {
  AsyncStorage
} from 'react-native';
import { baseUrl, handleRequestError } from './variables'

function parse_response(resolve, reject, response, navigation){
  if(response.status === 200) {
    resolve(JSON.parse(response['_bodyText']))
  }else{
    if(navigation){
      handleRequestError(response, navigation)
    }
    reject(response)
  }
}

function withSessionToken(fn){
  return new Promise((resolve, reject) => {
    AsyncStorage.getItem('@iBANStore:session_token').then(session_token => fn(session_token, resolve, reject))
  })
}

export function list() {
  return withSessionToken((session_token, resolve, reject) => {
    fetch(
      `${ baseUrl }/api/v1/saving_questions?session_token=${session_token}`,
      {
        method: 'GET'
      }
    )
      .then(response => parse_response(resolve, reject, response))
      .catch((error) => reject(error));
  })
}

export function answer(saving_answer_id, option) {
  return withSessionToken((session_token, resolve, reject) => {
    fetch(
      `${ baseUrl }/api/v1/saving_questions?session_token=${session_token}`,
      {
        method: 'PUT',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({saving_answer_id, option})
      }
    )
      .then(response => parse_response(resolve, reject, response))
      .catch((error) => reject(error));
  })
}

export function finish(navigation) {
  return withSessionToken((session_token, resolve, reject) => {
    fetch(
      `${ baseUrl }/api/v1/saving_questions?session_token=${session_token}`,
      {
        method: 'DELETE'
      }
    )
      .then(response => parse_response(resolve, reject, response, navigation))
      .catch((error) => reject(error));
  })
}