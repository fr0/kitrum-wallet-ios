import {
  AsyncStorage,
  Alert
} from 'react-native';
import { baseUrl } from './variables'
import Contacts from 'react-native-contacts'
import {handleRequestError} from './variables'

export function check(pin) {
  return new Promise((resolve, reject) => {

    fetch(
      `${ baseUrl }/api/v1/pin`,
      {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({pin})
      }
    )
      .then(response => {
        if(response.status === 200){
          resolve(response)
        }else{
          reject(response)
        }
      })
      .catch((error) => {
        reject(error)
      });
  })
}