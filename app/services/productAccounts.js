import {
  AsyncStorage
} from 'react-native';
import { baseUrl } from './variables'

export function list() {
  return new Promise((resolve, reject) => {
    AsyncStorage.getItem('@iBANStore:session_token').then((session_token) => {
      fetch(
        `${ baseUrl }/api/v1/product_accounts?session_token=${session_token}`,
        {
          method: 'GET'
        }
      )
        .then(response => {
          if(response.status === 200) {
            response = JSON.parse(response['_bodyText']);
            resolve(response['product_accounts'])
          }else{
            reject(response)
          }
        })
        .catch((error) => {
          reject(error)
        });
    })
  });
}

export function upsert(account) {
  return new Promise((resolve, reject) => {
    AsyncStorage.getItem('@iBANStore:session_token').then((session_token) => {
      fetch(
        `${ baseUrl }/api/v1/product_accounts?session_token=${session_token}`,
        {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(account)
        }
      )
        .then(response => {
          if(response.status === 200) {
            response = JSON.parse(response['_bodyText']);
            resolve(response)
          }else{
            reject(response)
          }
        })
        .catch((error) => {
          reject(error)
        });
    })
  });
}