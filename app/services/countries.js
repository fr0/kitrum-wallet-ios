import {
  AsyncStorage
} from 'react-native';
import { baseUrl } from './variables'

export function list() {
  return new Promise((resolve, reject) => {
    fetch(
      `${ baseUrl }/api/v1/countries`,
      {
        method: 'GET'
      }
    )
      .then(response => {
        if(response.status === 200) {
          response = JSON.parse(response['_bodyText']);
          resolve(response['countries'])
        }else{
          reject(response)
        }
      })
      .catch((error) => {
        reject(error)
      });
  });
}