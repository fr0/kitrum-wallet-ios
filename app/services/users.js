import {
  AsyncStorage,
  Alert
} from 'react-native';
import { baseUrl } from './variables'
import Contacts from 'react-native-contacts'
import {handleRequestError} from './variables'
import moment from 'moment';

export function profile(navigation, options = {}){
  return new Promise((resolve, reject) => {
    AsyncStorage.getItem('@iBANStore:session_token').then(session_token => {

      if(!session_token){
        reject({})
        return
      }

      fetch(
        `${ baseUrl }/api/v1/profile`,
        {
          method: 'GET',
          headers: {
            'Session-Token': session_token
          }
        }
      )
        .then(response => {
          data = JSON.parse(response['_bodyText']);

          if(data.date_of_birth){
            data.date_of_birth = moment(data.date_of_birth, "YYYY-MM-DD")
          }else{
            data.date_of_birth = moment()
          }


          if(response.status === 200 || (options.noRedirect && response.status === 203)){
            AsyncStorage.multiSet(
              [
                ['@iBANStore:current_user', JSON.stringify(data)],
              ]
            ).then(error => {

            });

            resolve(data)
          }else{
            handleRequestError(response, navigation)
            reject(response)
          }
        })
        .catch((error) => {
          reject(error)
        });
    })
  })
}

export function inDatabase(email){
  return new Promise((resolve, reject) => {
    fetch(
      `${ baseUrl }/api/v1/users/in_database?email=${ email }`,
      {
        method: 'GET'
      }
    )
      .then(response => {
        data = JSON.parse(response['_bodyText']);

        if(response.status === 200){
          resolve(data)
        }else{
          reject(response)
        }
      })
      .catch((error) => {
        reject(error)
      });
    })
}

export function setAdditional(user, navigation){
  return new Promise((resolve, reject) => {
    AsyncStorage.getItem('@iBANStore:session_token').then(session_token => {

      let {address1, post_code, city, country, dni_number, date_of_birth, coupone_number } = user

      fetch(
        `${ baseUrl }/api/v1/signup/additional`,
        {
          method: 'POST',
          headers: {
            'Session-Token': session_token
          },
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({address1, post_code, city, country_id: country.id, dni_number, date_of_birth, coupone_number })
        }
      )
        .then(response => {
          data = JSON.parse(response['_bodyText']);

          if(response.status === 200) {
            AsyncStorage.multiSet(
              [
                ['@iBANStore:current_user', JSON.stringify(data)],
              ]
            ).then(error => {

            });

            resolve(response)
          }else if(response.status == 422){
            resolve(response)
          }else{
            handleRequestError(response, navigation)
            reject(response)
          }
        })
        .catch((error) => {
          reject(error)
        });
    })
  })
}

export function updateProfile(user, navigation){
  return new Promise((resolve, reject) => {
    AsyncStorage.getItem('@iBANStore:session_token').then(session_token => {

      let {address1, post_code, city, country, dni_number, date_of_birth, coupone_number } = user

      fetch(
        `${ baseUrl }/api/v1/users`,
        {
          method: 'PUT',
          headers: {
            'Session-Token': session_token
          },
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({address1, post_code, city, country_id: country.id, dni_number, date_of_birth, coupone_number })
        }
      )
        .then(response => {
          resolve(response)
        })
        .catch((error) => {
          reject(error)
        });
    })
  })
}

export function signup(user, navigation) {
  return new Promise((resolve, reject) => {

    fetch(
      `${ baseUrl }/api/v1/signup`,
      {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(user)
      }
    )
      .then(response => {
        if(response.status === 203) {
          data = JSON.parse(response['_bodyText']);

          AsyncStorage.multiSet(
            [
              ['@iBANStore:session_token', data.session_token],
              ['@iBANStore:current_user', JSON.stringify(data.user)],
            ]
          ).then(error => {

          });

          handleRequestError(response, navigation)
          resolve(response)
        }else{
          handleRequestError(response, navigation)
          reject(response)
        }
      })
      .catch((error) => {
        reject(error)
      });
  })
}

export function setPin(pin, navigation) {
  return new Promise((resolve, reject) => {

    fetch(
      `${ baseUrl }/api/v1/signup/pin`,
      {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({pin})
      }
    )
      .then(response => {
        if(response.status === 203) {
          handleRequestError(response, navigation)
          resolve(response)
        }else{
          handleRequestError(response, navigation)
          reject(response)
        }
      })
      .catch((error) => {
        reject(error)
      });
  })
}

export function setCheckify({checkifyResponse, document_front, document_back}, navigation) {
  return new Promise((resolve, reject) => {

    fetch(
      `${ baseUrl }/api/v1/signup/checkify`,
      {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          checkify_response: JSON.stringify(checkifyResponse),
          document_front,
          document_back,
        })
      }
    )
      .then(response => {
        if(response.status === 203) {
          handleRequestError(response, navigation)
          resolve(response)
        }else{
          handleRequestError(response, navigation)
          reject(response)
        }
      })
      .catch((error) => {
        reject(error)
      });
  })
}

export function setFitnance(fitnance_request_code, navigation) {
  return new Promise((resolve, reject) => {

    fetch(
      `${ baseUrl }/api/v1/signup/fitnance`,
      {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          fitnance_request_code
        })
      }
    )
      .then(response => {
        if(response.status === 200) {
          resolve(response)
        }else if(response.status === 203) {
          handleRequestError(response, navigation)
          reject(response)
        }else{
          handleRequestError(response, navigation)
          reject(response)
        }
      })
      .catch((error) => {
        reject(error)
      });
  })
}

export function setIbanAccount(account, navigation) {
  return new Promise((resolve, reject) => {
    AsyncStorage.getItem('@iBANStore:session_token').then((session_token) => {
      fetch(
        `${ baseUrl }/api/v1/signup/iban_account`,
        {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Session-Token': session_token
          },
          body: JSON.stringify(account)
        }
      )
        .then(response => {
          if(response.status === 203) {
            handleRequestError(response, navigation)
            resolve(response)
          }else{
            handleRequestError(response, navigation)
            reject(response)
          }
        })
        .catch((error) => {
          reject(error)
        });
    })
  });
}

export function setSavings(step, navigation, values) {
  return new Promise((resolve, reject) => {
    AsyncStorage.getItem('@iBANStore:session_token').then((session_token) => {
      fetch(
        `${ baseUrl }/api/v1/savings/${ step }`,
        {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Session-Token': session_token
          },
          body: JSON.stringify({saving: {...values, ok: true}})
        }
      )
        .then(response => {
          if(response.status === 203) {
            handleRequestError(response, navigation)
            resolve(response)
          }else{
            handleRequestError(response, navigation)
            reject(response)
          }
        })
        .catch((error) => {
          reject(error)
        });
    })
  });
}

export function getStatements(account_id, date) {
  return new Promise((resolve, reject) => {
    AsyncStorage.getItem('@iBANStore:session_token').then((session_token) => {
      fetch(
        `${ baseUrl }/api/v1/statements/?account_id=${ account_id }&date=${date}`,
        {
          method: 'GET',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Session-Token': session_token
          }
        }
      )
        .then(response =>{
          data = JSON.parse(response['_bodyInit'])
          resolve(data)
        })
        .catch(error => {
          reject(error)
        })
    })
  })
}