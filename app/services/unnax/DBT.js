import {api_id, api_code, unnaxUrl, unnaxCallbackUrl, Base64} from '../variables'
import Alert from 'react-native'

export function init(account, lemonwayId) {

  console.log("======================================================SEND INIT")

  console.log('signature: ',  `Unnax ${ Base64.encode(`${ api_id }:${ api_code }`) }` )
  console.log('body init', JSON.stringify({
    order_code: new Date().getTime().toString(),
    amount: account.balance,
    concept: lemonwayId,
    currency: account.currency
  }))
  console.log("\n\n\n")
  console.log(lemonwayId)
  console.log("\n\n\n")
  console.log('url',  `${ unnaxUrl }/payment/transfer/lockstep/init/`)
  console.log('header', 'Authorization:', `Unnax ${ Base64.encode(`${ api_id }:${ api_code }`) }`)

  return new Promise((resolve, reject) => {
    fetch(
      `${ unnaxUrl }/payment/transfer/lockstep/init/`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Unnax ${ Base64.encode(`${ api_id }:${ api_code }`) }`
        },
        body: JSON.stringify({
          order_code: new Date().getTime().toString(),
          amount: account.balance,
          //---------------------------------------------------------------
          // concept: 'IBANON-tester2',                                    //|
          concept: lemonwayId,                                    //|
          //---------------------------------------------------------------
          currency: account.currency
        })
      }
    )
      .then((response) => {

        let data = JSON.parse(response['_bodyText'])

        console.log("======================================================SUCCESS")
        console.log(response.status)
        console.log(response)

        if(response.status == 200){
          resolve(data)
        }else{
          if(data['message']){
            reject(data['message'])
          }else{
            reject(Object.values(data['data']).join('. '))
          }
        }

      })
      .catch((error) => {
        console.log("======================================================ERROR")
        console.log(error)
        reject(JSON.stringify(error))
      });
  });
}

export function banks(sid) {

  console.log('---------------------------------------------------------------------------BANKS LIST')
  console.log('url', `${ unnaxUrl }/payment/transfer/lockstep/banks/?sid=${sid}`)
  return new Promise((resolve, reject) => {

    fetch(
      `${ unnaxUrl }/payment/transfer/lockstep/banks/?sid=${sid}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        }
      }
    )
      .then((response) => {

        let data = JSON.parse(response['_bodyText'])

        console.log("======================================================SUCCESS")
        console.log(response.status)
        console.log(JSON.stringify(data))
        console.log('------------------------------------------------------------------------BANKS LIST END')

        if(response.status == 200){
          resolve(data)
        }else{
          if(data['message']){
            reject(data['message'])
          }else{
            reject(Object.values(data['data']).join('. '))
          }
        }

      })
      .catch((error) => {
        console.log("======================================================ERROR")
        console.log(error)
        reject(JSON.stringify(error))
      });
  })
}


export function login({ values, bank, sid }){

  let parameters = {parameters: values, bank_id: bank.id}

  console.log("======================================================SEND LOGIN")
  console.log('url:', `${ unnaxUrl }/payment/transfer/lockstep/login/?sid=${ sid }`)
  console.log('parameters', JSON.stringify(parameters))

  return new Promise((resolve, reject) => {
    fetch(
      `${ unnaxUrl }/payment/transfer/lockstep/login/?sid=${ sid }`,
      {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(parameters)
      }
    )
      .then((response) => {
        // let data = response['_bodyText'] === '' ? {} : JSON.parse(response['_bodyText'])

        console.log("======================================================RESPONSE")
        console.log(response)
        console.log("======================================================RESPONSE")

        resolve(response)
      })
      .catch((error) => {
        console.log("======================================================ERROR")
        reject(error)
      });
  })
}

export function loginStatus(sid) {
console.log('---------------------------------------------------------------------------LOGIN STATUS')
  console.log('url',  `${ unnaxUrl }/payment/transfer/lockstep/login/?sid=${ sid }`)
  return new Promise((resolve, reject) => {
    fetch(
      `${ unnaxUrl }/payment/transfer/lockstep/login/?sid=${ sid }`,
      {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        }
      }
    )
      .then(response => {
        if(response.status == 200 || response.status == 202){
          let data = JSON.parse(response['_bodyText'])
          resolve({data: data, status: response.status})
        }else {
          let data = JSON.parse(response['_bodyText'])
          reject(data.message)
        }
        console.log('----------------------____________---------------------LOGIN STATUS SUCCESS')
        console.log(response)
        console.log('----------------------____________---------------------LOGIN STATUS SUCCESS')

      })
      .catch(error => {
        console.log('----------------------____________---------------------LOGIN STATUS ERROR')
        console.log(error)
        console.log('----------------------____________---------------------LOGIN STATUS ERROR')
        reject(error)
      })
  })
}

export function selectAccount(sid, { id } ) {
  let parameters = {account_id: id}


  console.log('fitnance account -----|||||||||||||||||||-------->   SELECT ACCOUNT')

  console.log('url', `${ unnaxUrl }/payment/transfer/lockstep/select_account/?sid=${ sid }`)
  console.log(JSON.stringify(parameters))

  return new Promise((resolve, reject) => {
    fetch(
      `${ unnaxUrl }/payment/transfer/lockstep/select_account/?sid=${ sid }`,
      {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(parameters)
      }
    )
      .then(response => {
        console.log('SELECT ACCOUNT SUCCESS------SELECT ACCOUNT SUCCESS-------SELECT ACCOUNT SUCCESS')
        console.log('response--------------->>>>>>>', response, '<<<<<<<-------response')
        console.log('SELECT ACCOUNT SUCCESS------SELECT ACCOUNT SUCCESS-------SELECT ACCOUNT SUCCESS')
        resolve(response)
      })
      .catch(error => {
        console.log('SELECT ACCOUNT ERROR|||||||SELECT ACCOUNT ERROR||||||||SELECT ACCOUNT ERROR')
        console.log('error ->>>>>>>>', error, '<<<<<<- error')
        console.log('SELECT ACCOUNT ERROR|||||||SELECT ACCOUNT ERROR||||||||SELECT ACCOUNT ERROR')
        reject(error)
      })
  })

}

export function selectAccountStatus(sid) {

  console.log('------------<><><><><><><><><>>>>> ACCOUNT SELECT STATUS')
  console.log('url', `${ unnaxUrl }/payment/transfer/lockstep/select_account/?sid=${ sid }`)


  return new Promise((resolve, reject) => {
    fetch(
      `${ unnaxUrl }/payment/transfer/lockstep/select_account/?sid=${ sid }`,
      {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        }
      }
    )
      .then(response => {
        console.log('SELECT ACCOUNT STATUS SUCCESS-=-=-=-=-=-=-=-SELECT ACCOUNT STATUS SUCCESS-=-=-=-=-=-=-=-=-=-=-SELECT ACCOUNT STATUS SUCCESS')
        console.log('response-=-=-=-=-=-=-=->', response, '<-=-=-=-=-=-=-=-=-response')
        console.log('SELECT ACCOUNT STATUS SUCCESS-=-=-=-=-=-=-=-SELECT ACCOUNT STATUS SUCCESS-=-=-=-=-=-=-=-=-=-=-SELECT ACCOUNT STATUS SUCCESS')
        if(response.status == 200 || response.status == 202){
          let data = JSON.parse(response['_bodyText'])
          resolve({data: data, status: response.status})
        }else {
          let data = JSON.parse(response['_bodyText'])
          reject(data.message)
        }
      })
      .catch(error => {
        console.log('SELECT ACCOUNT STATUS ERROR-=-=-=-=-=-=-=-SELECT ACCOUNT STATUS ERROR-=-=-=-=-=-=-=-=-=-=-SELECT ACCOUNT STATUS ERROR')
        console.log('error-=-=-=-=-=-=-=-=>', error, '<=-=-=-=-=-=error')
        console.log('SELECT ACCOUNT STATUS ERROR-=-=-=-=-=-=-=-SELECT ACCOUNT STATUS ERROR-=-=-=-=-=-=-=-=-=-=-SELECT ACCOUNT STATUS ERROR')
        reject(error)
      })
  })
}

export function sign(values, sid) {
  let parameters = {parameters: values}

  console.log("=================================SIGN====================================")
  console.log('url:', `${ unnaxUrl }/payment/transfer/lockstep/sign/?sid=${ sid }`)
  console.log('parameters', JSON.stringify(parameters))

  return new Promise((resolve, reject) => {
    fetch(
      `${ unnaxUrl }/payment/transfer/lockstep/sign/?sid=${ sid }`,
      {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(parameters)
      }
    )
      .then((response) => {
        console.log("=============================SIGN==========RESPONSE===============START")
        console.log(response)
        console.log("===============================SIGN========RESPONSE===============END")

        if(response.status == 200 || response.status == 202){
          resolve({data: response, status: response.status})
        }else {
          let parsedData = JSON.parse(response['_bodyText'])
          if(parsedData.data.sid){
            reject(parsedData.data.sid)
          }else if(parsedData.data.parameters){
            reject(parsedData.data.parameters)
          }else{
            reject(parsedData)
          }
        }
      })
      .catch((error) => {
        console.log("==============================SIGN============RESPONSE============ERROR")
        reject(error)
      });
  })
}

export function signStatus(sid) {

  console.log("=================================SIGN STATUS====================================")
  console.log('url:', `${ unnaxUrl }/payment/transfer/lockstep/sign/?sid=${ sid }`)

  return new Promise((resolve, reject) => {
    fetch(
      `${ unnaxUrl }/payment/transfer/lockstep/sign/?sid=${ sid }`,
      {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        }
      }
    )
      .then((response) => {
        console.log("=============================SIGN STATUS==========RESPONSE===============START")
        console.log(response)
        console.log("===============================SIGN STATUS========RESPONSE===============END")

        if(response.status == 200 || response.status == 202){
          let data = JSON.parse(response['_bodyText'])
          resolve({data: data, status: response.status})
        }else {
          let data = JSON.parse(response['_bodyText'])
          reject(data.message)
        }
      })
      .catch((error) => {
        console.log("==============================SIGN STATUS============RESPONSE============ERROR")
        reject(error)
      });
  })
}