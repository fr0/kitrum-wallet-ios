import {api_id, api_code, unnaxUrl, unnaxCallbackUrl, Base64} from '../variables'

export function init({request_code}) {

  console.log("======================================================SEND INIT")

  console.log('request code: ', request_code)
  console.log('signature: ',  `Unnax ${ Base64.encode(`${ api_id }:${ api_code }`) }` )

  return new Promise((resolve, reject) => {
    fetch(
      `${ unnaxUrl }/reader/lockstep/init/`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Unnax ${ Base64.encode(`${ api_id }:${ api_code }`) }`
        },
        body: JSON.stringify({
          request_code: request_code,
          'start_date': "2017-05-24"
        })
      }
    )
      .then((response) => {

        let data = JSON.parse(response['_bodyText'])

        console.log("======================================================SUCCESS")
        console.log(response.status)
        console.log(JSON.stringify(data))

        if(response.status == 200){
          resolve(data)
        }else{
          if(data['message']){
            reject(data['message'])
          }else{
            reject(Object.values(data['data']).join('. '))
          }
        }

      })
      .catch((error) => {
        console.log("======================================================ERROR")
        console.log(error)
        reject(JSON.stringify(error))
      });
  });
}

export function login({ values, bank, sid }){

  let parameters = {parameters: values, bank_id: bank.id}

  console.log("======================================================SEND LOGIN")
  console.log('sid:', sid)
  console.log('parameters', JSON.stringify(parameters))

  return new Promise((resolve, reject) => {
    fetch(
      `${ unnaxUrl }/reader/lockstep/login/?sid=${ sid }`,
      {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(parameters)
      }
    )
      .then((response) => {
        let data = response['_bodyText'] === '' ? {} : JSON.parse(response['_bodyText'])

        console.log("======================================================RESPONSE")
        console.log(response.status)
        console.log(JSON.stringify(data))

        if(response.status == 202) {
          resolve(data)
        }else{
          if(data['message']){
            reject(data['message'])
          }else{
            reject(Object.values(data['data']).join('. '))
          }
        }
      })
      .catch((error) => {
        console.log("======================================================ERROR")
        reject(error)
      });
  })
}

export function status({sid}){

  return new Promise((resolve, reject) => {
    console.log("======================================================SEND STATUS")
    console.log('sid:', sid)


    fetch(
      `${ unnaxUrl }/reader/lockstep/login/?sid=${ sid }`,
      {
        method: 'GET'
      }
    )
      .then((response) => {
        let data = JSON.parse(response['_bodyText'])

        console.log("======================================================RESPONSE")
        console.log(response.status)
        console.log(JSON.stringify(data))

        if(response.status == 200){
          resolve(data)
        }else if(response.status == 202){
          resolve({retry: true})
        }else{
          if(data['message']){
            reject(data['message'])
          }else{
            reject(Object.values(data['data']).join('. '))
          }
        }

      })
      .catch((error) => {
        reject(JSON.stringify(error))
      });
  })
}

export function complete({sid}){

  console.log("======================================================SEND COMPLETE")
  console.log('signature: ',  `Unnax ${ Base64.encode(`${ api_id }:${ api_code }`) }` )

  return new Promise((resolve, reject) => {
    fetch(
      `${ unnaxUrl }/reader/lockstep/complete/?sid=${ sid }`,
      {
        method: 'POST',
        headers: {
          'Authorization': `Unnax ${ Base64.encode(`${ api_id }:${ api_code }`) }`
        }
      }
    )
      .then((response) => {

        console.log("======================================================SUCCESS")
        console.log(response.status)
        console.log('--------', JSON.stringify(response))

        if(response.status == 200){
          resolve(data)
        }else{
          let data = JSON.parse(response['_bodyText'])

          if(data['message']){
            reject(data['message'])
          }else{
            reject(Object.values(data['data']).join('. '))
          }
        }
      })
      .catch((error) => {
        console.log("======================================================ERROR")
        console.log(error)
        reject(JSON.stringify(error))
      });
  });
}