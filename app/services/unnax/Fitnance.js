import {api_id, api_code, oldUnnaxUrl} from '../variables'

export function statements({sid, startOfMonth, endOfMonth, iban}) {

  return new Promise((resolve, reject) => {
    let random = Math.random().toString(36).substring(7)
    let sha1 = require('sha1')

    let body = `merchant_id=${ api_id }&merchant_signature=${ sha1(api_id + random + api_code) }&random=${ random }`

    if(startOfMonth){
      body += `&created_on=${ startOfMonth }`
    }

    if(endOfMonth){
      body += `&created_on=${ endOfMonth }`
    }

    if(iban){
      body += `&accounts=${ iban }`
    }

    fetch(
      `${ oldUnnaxUrl }/fitnance/statements?sid=${ sid }`,
      {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        body
      }
    )
      .then((response) => response.json())
      .then(response => {
        resolve(response)
      })
      .catch((error) => {
        reject(error)
      });
  });
}