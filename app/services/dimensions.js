import {
  Alert
} from 'react-native';
import Dimensions from 'Dimensions';

const originalScreenWidth = 414; // iPhone 7 plus
const originalScreenHeight = 736; // iPhone 7 plus

export function deviceHeight() {
  return Dimensions.get('window').height
}

export function deviceWidth() {
  return Dimensions.get('window').width
}

export function relativeHeight(originalSize){
  return originalSize * deviceHeight() / originalScreenHeight;
}

export function relativeWidth(originalSize){
  return originalSize * deviceWidth() / originalScreenWidth;
}