import {
  AsyncStorage,
  Alert
} from 'react-native';
import { NavigationActions } from 'react-navigation'

// export const baseUrl = 'http://localhost:3000'
export const baseUrl = 'http://iban-prod-lb-1434161395.eu-west-2.elb.amazonaws.com'
// export const api_id = 'io3471'                                  // test
// export const api_code = 'pafifowtpftahwvb'                      //  test
// export const unnaxUrl = 'https://integration.unnax.com/api/v2'  // test
// export const unnaxUrl = 'https://www.unnax.com/api/v3'
// export const api_id = 'io2298'
// export const api_code = 'tjyztqdipcqeacdr'
export const oldUnnaxUrl = 'https://www.unnax.com/api/v2'
export const unnaxUrl = 'https://www.unnax.com/api/v3'             // new
export const api_id = 'io9091'                                     // new
export const api_code = 'bldkwdaupqjdumrs'                         // new

export const unnaxCallbackUrl = 'http://31.131.20.12:98/unnax'

export function handleRequestError(error, navigation){

  // console.log(`-------------------HANDLE REQUEST ERROR---------${ error.status }-`);
  // console.log(JSON.stringify(error));

  switch(error.status){
    case 401:
      AsyncStorage.removeItem('@iBANStore:session_token').then(error => {
        const resetAction = NavigationActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({ routeName: 'Login'})
          ]
        })
        navigation.dispatch(resetAction)
      });
      break;
    case 203:
      let data = JSON.parse(error['_bodyText'])
      let routeName = ''

      switch(data['registration_step']){
        case 'pin_code':
          routeName = 'PinCode'
          break;
        case 'kyc':
          routeName = 'KYC'
          break;
        case 'bank_account':
          routeName = 'BankAccount'
          break;
        case 'risk_questionnaire':
          routeName = 'RiskQuestionnaire'
          break;
        case 'saving_questionnaire':
          routeName = 'SavingQuestionnaireIntro'
          break;
        case 'additional':
          routeName = 'Profile'
          break;
        case 'iban_account':
          routeName = 'AddIBANAccount'
          break;
        case 'finished':
          routeName = 'Wallet'
          break;
        case 'savings1':
          routeName = 'Savings1'
          break;
        case 'savings2':
          routeName = 'Savings2'
          break;
        case 'savings3':
          routeName = 'Savings3'
          break;
        case 'savings4':
          routeName = 'Savings4'
          break;
        case 'savings5':
          routeName = 'Savings5'
          break;
        case 'savings6':
          routeName = 'Savings6'
          break;
        case 'savings7':
          routeName = 'Savings7'
          break;
        case 'savings8':
          routeName = 'Savings8'
          break;
        case 'savings9':
          routeName = 'Savings9'
          break;
        case 'savings10':
          routeName = 'Savings10'
          break;
        case 'savings11':
          routeName = 'Savings11'
          break;
        case 'savings12':
          routeName = 'Savings12'
          break;
        default:
          routeName = 'PinCode'
      }

      navigation.push(routeName, {signupFlow: true})
      //
      // const resetAction = NavigationActions.reset({
      //   index: 0,
      //   actions: [
      //     NavigationActions.navigate({
      //       routeName,
      //       params: {signupFlow: true}
      //     })
      //   ]
      // })
      // navigation.dispatch(resetAction)
      break;
    default:

  }
}

export const Base64 = {
  characters: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=" ,

  encode: function( string )
  {
    var characters = Base64.characters;
    var result     = '';

    var i = 0;
    do {
      var a = string.charCodeAt(i++);
      var b = string.charCodeAt(i++);
      var c = string.charCodeAt(i++);

      a = a ? a : 0;
      b = b ? b : 0;
      c = c ? c : 0;

      var b1 = ( a >> 2 ) & 0x3F;
      var b2 = ( ( a & 0x3 ) << 4 ) | ( ( b >> 4 ) & 0xF );
      var b3 = ( ( b & 0xF ) << 2 ) | ( ( c >> 6 ) & 0x3 );
      var b4 = c & 0x3F;

      if( ! b ) {
        b3 = b4 = 64;
      } else if( ! c ) {
        b4 = 64;
      }

      result += Base64.characters.charAt( b1 ) + Base64.characters.charAt( b2 ) + Base64.characters.charAt( b3 ) + Base64.characters.charAt( b4 );

    } while ( i < string.length );

    return result;
  } ,

  decode: function( string )
  {
    var characters = Base64.characters;
    var result     = '';

    var i = 0;
    do {
      var b1 = Base64.characters.indexOf( string.charAt(i++) );
      var b2 = Base64.characters.indexOf( string.charAt(i++) );
      var b3 = Base64.characters.indexOf( string.charAt(i++) );
      var b4 = Base64.characters.indexOf( string.charAt(i++) );

      var a = ( ( b1 & 0x3F ) << 2 ) | ( ( b2 >> 4 ) & 0x3 );
      var b = ( ( b2 & 0xF  ) << 4 ) | ( ( b3 >> 2 ) & 0xF );
      var c = ( ( b3 & 0x3  ) << 6 ) | ( b4 & 0x3F );

      result += String.fromCharCode(a) + (b?String.fromCharCode(b):'') + (c?String.fromCharCode(c):'');

    } while( i < string.length );

    return result;
  }
};