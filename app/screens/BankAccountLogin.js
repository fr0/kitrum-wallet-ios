import React, { Component } from 'react';
import {
  Image,
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  ScrollView,
  KeyboardAvoidingView,
  Alert,
  StatusBar
} from 'react-native';

import SplashScreen from 'react-native-splash-screen';
import IbanStepper from '../common/stepper';
import IbanDropdownSelect from '../common/dropdownSelect';
import IbanTextInput from '../common/TextInput';
import FitnanceField from '../common/FitnanceField';
import IbanGreenRoundedButton from '../common/greenRoundedButton'
import NavBackButton from '../common/NavBackButton';

import IbanBankIcon from '../common/BankIcon';

import {unnaxUrl} from '../services/variables'

import { relativeWidth } from './../services/dimensions'
import { login, status } from '../services/unnax/ReaderLockstep'

export default class BankAccountLogin extends Component<{}> {

  state = {
    open: false,
    bank: {
      parameters: []
    },
    signatures: [],
    accept: false,
    values: {},
    clearButtonVisible: false
  }

  constructor(props){
    super(props);
    SplashScreen.hide();
  }

  componentWillMount(){
    const { bank, sid, step2 } = this.props.navigation.state.params;

    this.setState({bank, sid, step2})
  }

  _submit(){
    const { values, bank, sid, loading } = this.state;
    const {goBack} = this.props.navigation

    if(this.loading){
      return
    }

    this.setState({loading: true})

    login({values, bank, sid})
      .then(response => {
        this._checkStatus()
      })
      .catch((error) => {
        Alert.alert(error)
      });
  }

  _checkStatus(){
    let { values, sid, bank, step2 } = this.state;
    const { navigation, goBack } = this.props;
    let { request_code } = this.props.navigation.state.params

    setTimeout(
      () => {
        status({sid})
          .then(response => {
            if(response.retry){
              this._checkStatus()
            }else{
              this.setState({loading: false}, () => {
                if(response.step2){
                  navigation.push('BankAccountLogin', {
                    bank: {
                      ...bank,
                      name: `${ bank.name } (paso 2)`,
                      parameters: response.parameters
                    },
                    sid,
                    request_code,
                    step2: true
                  })
                }else{
                  navigation.push('BankAccountComplete', {bank, sid, request_code})
                }
              })
            }
          })
          .catch(error => {
            Alert.alert(
              error,
              '',
              [
                {
                  text: 'OK',
                  onPress: () => {
                    if(step2){
                      this.setState({loading: false}, () => {goBack()})
                    }else{
                      this.setState({loading: false})
                    }
                  }
                },
              ],
              { cancelable: false }
            )
          })
      },
      3000
    )
  }

  _clear(){
    const { clear } = this.state
    this.setState({clear: !clear})
  }

  render() {

    let {navigate} = this.props.navigation
    let { bank, signatures, accept, values, loading, clearButtonVisible, clear, step2 } = this.state;

    // bank.parameters = [
    //   {
    //     type: "password",
    //     name:"key",
    //     fields:[
    //       {min_length:0, label:"Ingrese1", type:"text", name:"",max_length:1,required_by:[]},
    //       {min_length:1, label:"Ingrese2", type:"text", name:"2",max_length:1,required_by:[]},
    //       {min_length:1, label:"Ingrese3", type:"text", name:"3",max_length:1,required_by:[]},
    //       {min_length:1, label:"Ingrese4", type:"text", name:"",max_length:0,required_by:[]},
    //       {min_length:0, label:"Ingrese5", type:"text", name:"5",max_length:1,required_by:[]},
    //       {min_length:0, label:"Ingrese6", type:"text", name:"",max_length:0,required_by:[]}
    //     ]
    //   }
    // ]
    return (
      <KeyboardAvoidingView
        behavior="padding"
      >
        <StatusBar
          barStyle="light-content"
        />

        <ScrollView
          style={styles.scroll}
        >
          <View
            style={ styles.main }
          >

            <IbanStepper
              style={styles.stepper }
              circles={3}
              current={2}
            />

            <Text style={styles.header}>Introduzca sus datos</Text>

            <View style={styles.bankView}>
              <IbanBankIcon
                bankId={bank['id']}
              />
              <Text style={styles.bankViewText}>{ bank.name }</Text>
            </View>

            {
              bank.parameters.map( (parameter, index) => {
                return (
                  <FitnanceField
                    key={ index }
                    parameter={ parameter }
                    onChange={(val) => this.setState({values: val})}
                    values={values}
                    onClearButtonNeeded={() => this.setState({clearButtonVisible: true})}
                    clear={clear}
                  />
                )
              })
            }

            <View
              style={styles.checkboxContainer}
            >
              <Text
                style={styles.accept}
              >
                En iBAN, tus credenciales son anónimas.
              </Text>
            </View>

            <IbanGreenRoundedButton
              value={`Entrar`}
              style={styles.button}
              onPress={() => this._submit()}
              loading={loading}
            ></IbanGreenRoundedButton>

            {
              clearButtonVisible ?
                <IbanGreenRoundedButton
                  value='Borrar'
                  style={styles.clearButton}
                  onPress={() => this._clear()}
                  textStyle={styles.clearButtonText}
                ></IbanGreenRoundedButton>
                : null
            }

            <View
              style={styles.links}
            >

              <TouchableWithoutFeedback
                onPress={() => navigate('FitnanceSecurityTerms')}
              >
                <View
                  style={styles.securityLinkWrapper}
                >
                  <Image
                    style={styles.lockIcon}
                    source={require('../images/icons/lock.png')}
                  ></Image>
                  <Text
                    style={styles.securityLink}
                  >
                    Seguridad
                  </Text>
                </View>
              </TouchableWithoutFeedback>
            </View>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}

BankAccountLogin.navigationOptions = ({ navigation }) => {
  return {
    title: 'Cuenta Bancaria',
    headerTitleStyle: {color: 'white', fontWeight: '400'},
    headerStyle: {backgroundColor: '#4CBA8D'},
    headerLeft: (
      <NavBackButton
        title='Volver'
        onPress={() => {
          navigation.goBack();
        }}
      />
    ),
    gesturesEnabled: false
  }
};

const styles = StyleSheet.create({
  scroll: {
    minHeight: '100%'
  },
  main: {
    paddingBottom: 30,
    alignItems: 'center',
    backgroundColor: 'white',
    minHeight: '100%',
    paddingRight: 20,
    paddingLeft: 20,
    minHeight: '100%'
  },
  header: {
    fontSize: relativeWidth(22),
    marginTop: relativeWidth(52),
    width: '100%'
  },
  stepper: {

  },
  button: {
    width: '100%',
    marginBottom: 0,
    marginTop: relativeWidth(38),
    borderRadius: 4
  },
  clearButton: {
    width: '100%',
    marginBottom: 0,
    marginTop: relativeWidth(10),
    borderRadius: 4,
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: '#D8D8D8'
  },
  clearButtonText: {
    color: '#D8D8D8'
  },
  text: {
    fontSize: 12,
    color: 'gray',
    marginTop: 80,
    textAlign: 'center'
  },
  links: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%'
  },
  backLink: {
    color: 'rgb(92,200,156)',
    fontSize: 16,
    marginTop: 20
  },
  backLinkWrapper: {

  },
  lockIcon: {

  },
  securityLink: {
    color: '#4CBA8D',
    fontSize: relativeWidth(14),
    textDecorationLine: "underline",
    textDecorationStyle: "solid",
    textDecorationColor: '#4CBA8D',
  },
  securityLinkWrapper: {
    width: '100%',
    justifyContent: 'flex-end',
    flexDirection: 'row',
    marginTop: 16
  },
  checkboxContainer: {
    width: '100%',
    marginTop: relativeWidth(12),
  },
  accept: {
    color: 'black',
    fontSize: relativeWidth(13),
    marginTop: -4,
  },
  bankView: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: '100%',
    marginTop: relativeWidth(47),
    marginBottom: relativeWidth(47)
  },
  bankViewText: {
    fontSize: relativeWidth(18),
    marginTop: relativeWidth(2),
    width: '80%',
    marginLeft: relativeWidth(10)
  }
});
