import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  StatusBar,
  ScrollView,
  Modal,
  Image,
  Keyboard,
  Alert
} from 'react-native';
import { relativeWidth } from './../../services/dimensions'
import { baseUrl } from './../../services/variables'
import IoniconsIcon from 'react-native-vector-icons/Ionicons'
import IbanGreenRoundedButton from '../../common/greenRoundedButton'
import Slider from '../../common/Slider'
import { MaskService } from 'react-native-masked-text'
import { setSavings } from './../../services/users'

export default class RiskQuestionnaire extends Component<{}> {

  constructor(props){
    super(props)
    this.state = {
      loading: false,
      value: [
        {from: 0, to: 1635, value: 279.45},
        {from: 0, to: 1635, value: 279.45},
        {from: 0, to: 1635, value: 279.45},
        {from: 0, to: 1635, value: 279.45}
      ]
    }

    this.submit = this.submit.bind(this)
  }

  static navigationOptions = { header: null, gesturesEnabled: false };

  handleChange = i => newValue => {
    let {value} = this.state
    value[i - 1].value = newValue
    this.setState({value})
  }

  submit(){
    const {navigation} = this.props

    Alert.alert(
      'Ingresos medios anuales',
      'Here is a message where we can put absolutely anything we want.',
      [
        {
          text: 'Entendido',
          onPress: () => {
            this.setState({loading: true})
            setSavings(2, navigation)
              .then(response =>{
                
              })
              .catch(error =>{
                this.setState({loading: false})
              })
          }
        },
      ]
    )



  }

  formatCurrency(value) {
    let masked = MaskService.toMask('money', value, {
      unit: '',
      separator: ',',
      delimiter: '.',
    })
    
    return masked.substring(0, masked.length - 3);
  }

  renderBlock(i) {
    const {value} = this.state 

    let text = ''
    switch(i){
      case 1:
        text = 'Ingresos medios anuales'
        break;
      case 2:
        text = 'Gastos medios anuales'
        break;
      case 3:
        text = '¿Cuál es el total de sus deudas?'
        break;
      case 4:
        text = '¿Cuál es el total de sus deudas?'
        break;
    }

    return (
      <View
        style={styles.block}
      >
        <View style={styles.textContainer}>
          <Text style={styles.text}>{text}</Text>
          <IoniconsIcon
            style={styles.infoIcon}
            name="ios-information-circle-outline"
            color="#4CBA8D"
          />
        </View>

        <View style={styles.pricesContainer}>
          <Text style={styles.fromPrice}>{this.formatCurrency(value[i - 1].from)} €</Text>
          <Text style={styles.toPrice}>{this.formatCurrency(value[i - 1].to)} €</Text>
        </View>
        
        <Slider
          maxValue={1653}
          value={value[i - 1].value}
          onChange={this.handleChange(i)}
          style={styles.slider}
        /> 
      </View>
    )
  }

  render() {
    const { loading } = this.state

    return (
      <ScrollView
        style={styles.container}
        keyboardShouldPersistTaps='always'
      >
        <StatusBar barStyle="dark-content"/>

        <View style={styles.header}>
          <Text style={styles.heading}>Defina de forma precisa los ahorros{"\n"}que tiene ahora</Text>
        </View>

        {this.renderBlock(1)}
        {this.renderBlock(2)}
        {this.renderBlock(3)}
        {this.renderBlock(4)}

        <IbanGreenRoundedButton
          value='Continuar'
          onPress={this.submit}
          loading={loading}
          style={styles.button}
        ></IbanGreenRoundedButton>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
 
  container: {
    backgroundColor: 'white',
    minHeight: '100%'
  },
  heading: {
    color: '#4CBA8D',
    fontSize: relativeWidth(23),
    marginTop: relativeWidth(41),
    textAlign: 'center'
  },
  button: {
    marginTop: relativeWidth(45),
    marginLeft: relativeWidth(40),
    marginRight: relativeWidth(40),
    marginBottom: relativeWidth(50),
  },
  header: {
    shadowOffset: {width: 0, height: relativeWidth(2)},
    shadowOpacity: 0.1,
    shadowRadius: relativeWidth(2),
    height: relativeWidth(150)
  },

  block: {
    marginTop: relativeWidth(24),
    marginLeft: relativeWidth(10),
    marginRight: relativeWidth(10),
  },
  textContainer: {
    flexDirection: 'row'
  },
  text: {
    fontSize: relativeWidth(19)
  },
  infoIcon: {
    marginLeft: relativeWidth(4),
    marginTop: relativeWidth(-3),
    fontSize: relativeWidth(30),
    lineHeight: relativeWidth(30),
    height: relativeWidth(30)
  },

  pricesContainer: {
    marginBottom: relativeWidth(10)
  },
  fromPrice: {
    color: 'gray'
  },
  toPrice: {
    color: 'gray',
    position: 'absolute',
    right: 0
  },
  slider: {
    marginBottom: relativeWidth(12)
  }
});