import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  ScrollView,
  Image,
  Alert,
  TouchableWithoutFeedback,
  Dimensions
} from 'react-native';
import { relativeWidth } from './../../services/dimensions'
import { baseUrl } from './../../services/variables'
import { setSavings } from './../../services/users'
import IbanGreenRoundedButton from '../../common/greenRoundedButton'
import FeatherIcon from 'react-native-vector-icons/Feather'
import Pdf from 'react-native-pdf';

export default class Savings extends Component {

  constructor(props){
    super(props)
    this.state = {
      loading: false,
      termsAccepted: false
    }

    this.submit = this.submit.bind(this)
  }

  static navigationOptions = { header: null, gesturesEnabled: false };

  submit(){
    const {navigation} = this.props

    this.setState({loading: true})
    setSavings(12, navigation)
      .then(response => {
        
      })
      .catch(error =>{
        this.setState({loading: false})
      })
  }

  render() {
    const { loading, termsAccepted } = this.state

    return (
      <View
        style={s.container}
        keyboardShouldPersistTaps='always'
      >
        <StatusBar barStyle="light-content"/>

        <View style={s.header}>
          <Text style={s.heading}>
            Antes de enviar el documento, debes aceptar los términos y condiciones.
          </Text>

          <FeatherIcon
            style={s.checkIcon}
            name="check-circle"
          />
          <FeatherIcon
            style={s.closeIcon}
            name="x"
          />
        </View>
        
        <View style={s.body}>
          <Pdf
            source={{uri: `${ baseUrl }/api/v1/terms?type=signup`, cache: true}}
            onLoadComplete={(numberOfPages,filePath)=>{
               
            }}
            onPageChanged={(page,numberOfPages)=>{
                
            }}
            onError={(error)=>{
                
            }}
            style={s.pdf}
            fitPolicy={0}
          />
        </View>

        <View style={s.footer}>
          <View style={s.checkboxContainer}>
            <TouchableWithoutFeedback
              onPress={() => this.setState({termsAccepted: !termsAccepted})}
            >
              { termsAccepted ? 
              <FeatherIcon
                style={s.checkbox}
                name="check-circle"
              />
              :
              <FeatherIcon
                style={s.checkbox}
                name="circle"
              />
              }
            </TouchableWithoutFeedback>
            <Text style={s.checkboxText}>Acepto los Terminos y condiciones</Text>
          </View>
          
          <IbanGreenRoundedButton
            value='FIRMAR'
            onPress={this.submit}
            loading={loading}
            style={s.button}
            textStyle={s.buttonText}
          ></IbanGreenRoundedButton>
        </View>
        
      </View>
    );
  }
}

const s = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    height: '100%',
    backgroundColor: '#EFEFF4'
  },
  header: {
    backgroundColor: '#339966',
    shadowOffset: {width: 0, height: 4},
    shadowOpacity: 0.1,
    shadowRadius: 4,
    paddingLeft: relativeWidth(15),
    paddingRight: relativeWidth(15),

    alignItems: 'center'
  },
  heading: {
    color: 'white',
    fontSize: relativeWidth(12.2),
    marginTop: relativeWidth(34),
    marginBottom: relativeWidth(9),
    marginRight: relativeWidth(43),
    marginLeft: relativeWidth(30),
  },
  checkIcon: {
    position: 'absolute',
    color: 'white',
    top: relativeWidth(34),
    left: relativeWidth(10),
    fontSize: relativeWidth(24)
  },
  closeIcon: {
    position: 'absolute',
    color: 'white',
    top: relativeWidth(30),
    right: relativeWidth(10),
    fontSize: relativeWidth(34)
  },
  
  button: {
    marginTop: relativeWidth(35),
    marginLeft: relativeWidth(40),
    marginRight: relativeWidth(40),
    marginBottom: relativeWidth(15),
    backgroundColor: 'white'
  },
  buttonText: {
    color: "#33CC99",
  },
  body: {
    position: 'absolute',
    top: relativeWidth(82),
    bottom: relativeWidth(180),
    left: relativeWidth(10),
    right: relativeWidth(10),
    shadowOffset: {width: 0, height: 0},
    shadowOpacity: .3,
    shadowRadius: 4,
  },
  pdf: {
  },

  footer: {
    backgroundColor: '#339966',
    position: 'absolute',
    width: '100%',
    bottom: 0,
    height: relativeWidth(170)
  },
  checkboxContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: relativeWidth(17)
  },
  checkboxText: {
    color: 'white',
    marginLeft: 10
  },
  checkbox: {
    color: 'white',
    fontSize: relativeWidth(30)
  }
});