import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  StatusBar,
  ScrollView,
  Modal,
  Image,
  Keyboard,
  Alert,
  Animated,
  Easing,
  TextInput
} from 'react-native';
import { relativeWidth } from './../../services/dimensions'
import { baseUrl } from './../../services/variables'
import { setSavings } from './../../services/users'
import FeatherIcon from 'react-native-vector-icons/Feather'
import IoniconsIcon from 'react-native-vector-icons/Ionicons'
import IbanGreenRoundedButton from '../../common/greenRoundedButton'
import { TextInputMask } from 'react-native-masked-text'

export default class RiskQuestionnaire extends Component<{}> {

  constructor(props){
    super(props)
    this.state = {

      loading: false,
      openedBlockIndex: null,

      block1Completed: true,
      block2Completed: false,
      block3Completed: false,
      block4Completed: false,

      chevron1Rotation: new Animated.Value(0),
      chevron2Rotation: new Animated.Value(0),
      chevron3Rotation: new Animated.Value(0),
      chevron4Rotation: new Animated.Value(0),
      chevron5Rotation: new Animated.Value(0),

      block1Height: new Animated.Value(0),
      block2Height: new Animated.Value(0),
      block3Height: new Animated.Value(0),
      block4Height: new Animated.Value(0),
      block5Height: new Animated.Value(0),
    }

    this.iconPaths = [
      require('./icons/icon1.png'),
      require('./icons/icon2.png'),
      require('./icons/icon3.png'),
      require('./icons/icon4.png'),
      require('./icons/icon5.png'),
    ]
    this.greenIconPaths = [
      require('./icons/icon1-green.png'),
      require('./icons/icon2-green.png'),
      require('./icons/icon3-green.png'),
      require('./icons/icon4-green.png'),
      require('./icons/icon5-green.png'),
    ]

    this.submit = this.submit.bind(this)
  }

  static navigationOptions = { header: null, gesturesEnabled: false };

  componentDidMount() {
    this.openBlock(2)
  }

  handleChange = key => value => {
    this.setState({[key]: value})
  }

  submit(){
    const {navigation} = this.props

    this.setState({loading: true})
    setSavings(1, navigation)
      .then(response =>{
        
      })
      .catch(error =>{
        this.setState({loading: false})
      })
  }

  openBlock(i) {
    const {openedBlockIndex} = this.state
    let animations = [
      Animated.timing(
        this.state[`chevron${i}Rotation`],
        {
          toValue: 1,
          duration: 300,
          easing: Easing.linear
        }
      ),
      Animated.timing(
        this.state[`block${i}Height`],
        {
          toValue: 1,
          duration: 300,
          easing: Easing.linear
        }
      )
    ]


    if(openedBlockIndex != null && openedBlockIndex > 0 && openedBlockIndex <= 5){
      animations.push(
        this.state[`block${openedBlockIndex}Completed`] ?
          null
          :
          Animated.timing(
            this.state[`chevron${openedBlockIndex}Rotation`],
            {
              toValue: 0,
              duration: 300,
              easing: Easing.linear
            }
          ),
        Animated.timing(
          this.state[`block${openedBlockIndex}Height`],
          {
            toValue: 0,
            duration: 300,
            easing: Easing.linear
          }
        )
      )
    }

    Animated.parallel(animations).start()

    this.setState({openedBlockIndex: openedBlockIndex == i ? 0 : i })
  }

  renderIcon(i) {
    const {openedBlockIndex} = this.state

    return (
      <View style={styles.headingIconContainer}>
        {this.state[`block${i}Completed`] && openedBlockIndex != i  ?
          <Image
            style={styles.icon}
            source={this.iconPaths[i-1]}
          />
          :
          <Image
            style={styles.icon}
            source={this.greenIconPaths[i-1]}
          />
        }
      </View>
    )
  }

  renderHeader(i) {
    const {openedBlockIndex} = this.state
    let text = ''

    switch(i){
      case 1:
        text = "Cantidad en  depósitos y ahorros\na la vista."
        break;
      case 2:
        text = "Cantidad en cuentas corrientes"
        break;
      case 3:
        text = "Cantidad de fondos de inversión y\nproductos a medio plazo."
        break;
      case 4:
        text = "Cantidad en Planes de jubilación y\nproductos a largo plazo."
        break;
      case 5:
        text = "Cantidad en Otras inversiones"
        break;
    }

    return (
      <TouchableWithoutFeedback onPress={() => this.openBlock(i)}>
        <View
          style={styles.collapseableHeading}
        >
          {this.renderIcon(i)}
          <View style={styles.headingTextContainer}>
            <Text
              style={[
                styles.headingText,
                {
                  color: this.state[`block${i}Completed`] && openedBlockIndex != i ? 'white' : 'black'
                }
              ]}
            >
              {text}
            </Text>
          </View>

          {openedBlockIndex != i && this.state[`block${i}Completed`] ?
            <View style={[styles.headingCheck, styles.headingChevronContainer]}>
              <IoniconsIcon
                style={styles.headingCheckIcon}
                name="ios-checkmark-circle"
                color="white"
              />
              <Text style={styles.headingCheckText}>Editar</Text>
            </View>
            :
            <Animated.View
              style={[
                styles.headingChevronContainer,
                {
                  transform: [{
                    rotate: this.state[`chevron${i}Rotation`].interpolate({
                      inputRange: [0, 1],
                      outputRange: ['0deg', '180deg']
                    })
                  }],
                }
              ]}
            >
              <FeatherIcon
                style={styles.headingChevron}
                name="chevron-down"
                color="gray"
              />
            </Animated.View>
          }
        </View>
      </TouchableWithoutFeedback>
    )
  }

  renderBlock(i) {
    const {openedBlockIndex} = this.state

    return (
      <View
        style={[
          styles.collapseable,
          {
            borderColor: openedBlockIndex == i ? '#4CBA8D' : 'transparent',
            backgroundColor: this.state[`block${i}Completed`] && openedBlockIndex != i ? '#4CBA8D' : 'white'
          }
        ]}
      >
        {this.renderHeader(i)}

        <Animated.View
          style={[
            styles.collapseableBody,
            openedBlockIndex == i ? null : styles.collapseableBodyOpen,
            {
              height: this.state[`block${i}Height`].interpolate({
                inputRange: [0, 1],
                outputRange: [0, relativeWidth(100)]
              })
            }
          ]}
        >

          <View style={styles.bodyItem}>
            <View style={styles.bodyItemCell1}>
              <Text style={styles.bodyItemCellText}>Saldo de</Text>
            </View>
            <View style={styles.bodyItemSeparator}>

            </View>
            <View style={styles.bodyItemCell2}>
              <Text style={styles.bodyItemCellText}>Interés </Text>
              <Text style={styles.bodyItemCellTextSup}>(TAE)</Text>
            </View>
          </View>

          <View style={styles.bodyItem}>
            <View style={styles.bodyItemCell1}>
            <View
                style={styles.inputContainer}
              >
                <TextInputMask
                  type={'money'}
                  options={{
                    unit: '',
                    separator: ',',
                    delimiter: '.'
                  }}
                  style={styles.input}
                  onChangeText={this.handleChange(`balance${i}`)}
                  value={this.state[`balance${i}`]}
                />
                <Text style={styles.inputText}>€</Text>
              </View>
            </View>
            <View style={styles.bodyItemSeparator}>
              <Text style={styles.bodyItemCellText}>al</Text>
            </View>
            <View style={styles.bodyItemCell2}>
              <View
                style={styles.inputContainer}
              >
                <TextInputMask
                  type={'money'}
                  options={{
                    unit: '',
                    separator: '.',
                    delimiter: ','
                  }}
                  style={styles.input}
                  onChangeText={this.handleChange(`target${i}`)}
                  value={this.state[`target${i}`]}
                />
                <Text style={styles.inputText}>€</Text>
              </View>
            </View>
          </View>

          
        </Animated.View>
      </View>
    )
  }

  render() {
    const { loading } = this.state

    return (
      <ScrollView
        style={styles.container}
        keyboardShouldPersistTaps='always'
      >
        <StatusBar barStyle="dark-content"/>

        <View style={styles.header}>

          <Text style={styles.heading}>Defina de forma precisa los ahorros{"\n"}que tiene ahora</Text>
        </View>

        {this.renderBlock(1)}
        {this.renderBlock(2)}
        {this.renderBlock(3)}
        {this.renderBlock(4)}
        {this.renderBlock(5)}

        <IbanGreenRoundedButton
          value='Continuar'
          onPress={this.submit}
          loading={loading}
          style={styles.button}
        ></IbanGreenRoundedButton>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    minHeight: '100%'
  },
  heading: {
    color: '#4CBA8D',
    fontSize: relativeWidth(23),
    marginTop: relativeWidth(41),
    textAlign: 'center'
  },
  button: {
    marginTop: relativeWidth(45),
    marginLeft: relativeWidth(40),
    marginRight: relativeWidth(40),
    marginBottom: relativeWidth(50)
  },
  header: {
    shadowOffset: {width: 0, height: relativeWidth(2)},
    shadowOpacity: 0.1,
    shadowRadius: relativeWidth(2),
    height: relativeWidth(150)
  },
  icon: {

  },

  collapseable: {
    borderWidth: 1.1,
    borderRadius: relativeWidth(5),

    shadowOffset: {width: 0, height: 0},
    shadowOpacity: 0.2,
    shadowRadius: 2,

    marginTop: relativeWidth(19),
    marginBottom: relativeWidth(7),
    marginLeft: relativeWidth(15),
    marginRight: relativeWidth(15),
  },
  collapseableHeading: {
    // height: relativeWidth(59.5),

    marginLeft: relativeWidth(15),
    marginRight: relativeWidth(15),
    marginTop: relativeWidth(8),
    marginBottom: relativeWidth(8),
  },
  headingTextContainer: {
    marginLeft: relativeWidth(40),
    justifyContent: 'center',
    paddingTop: relativeWidth(11.2),
    paddingBottom: relativeWidth(11.2),
  },
  headingText: {

  },
  headingChevronContainer: {
    position: 'absolute',
    top: 0,
    right: 0,
    height: '100%',
    justifyContent: 'center',
  },
  headingChevron: {
    fontSize: relativeWidth(26)
  },
  headingCheck: {
    alignItems: 'center'
  },
  headingCheckText: {
    fontSize: relativeWidth(14),
    color: 'white'
  },
  headingCheckIcon: {
    fontSize: relativeWidth(24)
  },
  headingIconContainer: {
    position: 'absolute',
    height: '100%',
    justifyContent: 'center',
    width: relativeWidth(40),
    top: 0,
    left: 0
  },
  collapseableBody: {
    borderTopWidth: 1,
    borderColor: 'lightgray',

    overflow: 'hidden',
    marginLeft: relativeWidth(15),
    marginRight: relativeWidth(15),
  },
  collapseableBodyOpen: {
    borderTopWidth: 0,
  },

  bodyItem: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: relativeWidth(7)
  },
  bodyItemCell1: {
    marginLeft: '6%',
    width: '34%',
    alignItems: 'flex-start',
    justifyContent: 'center',
    flexDirection: 'row'
  },
  bodyItemCell2: {
    marginRight: '6%',
    width: '34%',
    alignItems: 'flex-start',
    justifyContent: 'center',
    flexDirection: 'row'
  },
  bodyItemCellText: {
    fontSize: relativeWidth(18)
  },
  bodyItemCellTextSup: {
    fontSize: relativeWidth(10),
    flexDirection: 'row'
  },
  bodyItemSeparator: {
    width: '20%',
    alignItems: 'center',
    justifyContent: 'center'
  },

  inputContainer: {
    width: '100%',
    height: 30,
    borderWidth: 1,
    borderColor: '#4CBA8D',
    borderRadius: 4,
    flexDirection: 'row',

    alignItems: 'center',
    justifyContent: 'center',
  },
  inputText: {
    color: 'gray',
    marginRight: 2
  },
  input: {
    width: '90%',
    height: 30,
    textAlign: 'center',
    color: '#4CBA8D',
    borderRadius: 4
  }
});