import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  StatusBar,
  ScrollView,
  Modal,
  Image,
  Keyboard,
  Alert,
  Dimensions
} from 'react-native';
import { relativeWidth as rw } from './../../services/dimensions'
import { baseUrl } from './../../services/variables'
import IoniconsIcon from 'react-native-vector-icons/Ionicons'
import IbanGreenRoundedButton from '../../common/greenRoundedButton'
import Slider from '../../common/Slider'
import { MaskService } from 'react-native-masked-text'
import { setSavings } from './../../services/users'

import Svg, {
  Line,
  Path,
  Polygon,
  Ellipse,
  Polyline,
  Rect,
  Symbol,
  G,
  Text as SVGText
} from 'react-native-svg';

const MyStatusBar = ({backgroundColor, ...props}) => (
  <View style={[styles.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);

export default class Savings extends Component<{}> {

  constructor(props){
    super(props)
    this.state = {
      loading: false,
      value: []
    }

    this.submit = this.submit.bind(this);

    this.deviceWidth = Dimensions.get('window').width;
    this.triangleRadius = rw(205);
    this.circleRadius = rw(100);
  }

  handleTriangleChange(triangleName) {

    const {value} = this.state
    
    if(value.includes(triangleName)){
      let index = value.indexOf(triangleName)
      value.splice(index, 1)
    }else{
      value.push(triangleName)
    }

    if(value.length > 2){
      value.shift()
    }

    this.setState({value})
  }

  static navigationOptions = { header: null, gesturesEnabled: false };

  submit(){
    const {navigation} = this.props
    const { value } = this.state
    let values = {}
    this.setState({loading: true})

    value.forEach(i => { values[i] = true} )

    setSavings(3, navigation, values)
      .then(response => {
        
      })
      .catch(error =>{
        this.setState({loading: false})
      })
  };

  translate({x, y}) {
    return {x: x + this.deviceWidth/2, y: y + rw(230)};
  };

  rotate({x, y}, angle){
    return {
      x: x * Math.cos(angle) + y * Math.sin(angle),
      y: y * Math.cos(angle) - x * Math.sin(angle)
    }
  };

  render() {
    const { loading, value } = this.state;

    const trianglePoint = { x: 0, y: -this.triangleRadius };
    const circlePoint = { x: 0, y: -this.circleRadius };

    const triangle = [
      this.translate(this.rotate(trianglePoint, 0)),
      this.translate(this.rotate(trianglePoint, -Math.PI*2/3)),
      this.translate(this.rotate(trianglePoint, Math.PI*2/3))
    ];

      const seguridadCircle      = this.translate(this.rotate(circlePoint, Math.PI*2/3));
      const rentabilidadCircle     = this.translate(this.rotate(circlePoint, 0));
      const liquidezCircle  = this.translate(this.rotate(circlePoint, -Math.PI*2/3));

    return (
      <View>
        <MyStatusBar backgroundColor="#3C9570" barStyle="light-content" />
        <ScrollView
          style={styles.container}
          keyboardShouldPersistTaps='always'
        >

          <Text style={styles.heading}>Triangulo de inversión</Text>
          <Text style={styles.heading2}>Selecciona en las variables que deseas</Text>

          <View style={styles.legends}>
            <View style={[styles.legend, {backgroundColor: '#6699FF'}]}>
              <Text style={styles.legendText}>Liquidez</Text>
            </View>
            <View style={[styles.legend, {backgroundColor: '#E2526F'}]}>
              <Text style={styles.legendText}>Seguridad</Text>
            </View>
            <View style={[styles.legend, {backgroundColor: '#4CBA8D'}]}>
              <Text style={styles.legendText}>Rentabilidad</Text>
            </View>
          </View>

          <Svg
            height={rw(350) + rw(50)}
            width={this.deviceWidth}
            style={styles.triangle}
          >

            <Ellipse
              cx={seguridadCircle.x}
              cy={seguridadCircle.y}
              rx={this.triangleRadius - this.circleRadius}
              ry={this.triangleRadius - this.circleRadius}
              fill='#E2526F'
              fillOpacity={value.includes('investment_security') ? 0.55 : 0}
              onPressIn={() => this.handleTriangleChange('investment_security')}
            />
            <Ellipse
              cx={rentabilidadCircle.x}
              cy={rentabilidadCircle.y}
              rx={this.triangleRadius - this.circleRadius}
              ry={this.triangleRadius - this.circleRadius}
              fill='#4CBA8D'
              fillOpacity={value.includes('investment_profitability') ? .55 : 0}
              onPressIn={() => this.handleTriangleChange('investment_profitability')}
            />
            <Ellipse
              cx={liquidezCircle.x}
              cy={liquidezCircle.y}
              rx={this.triangleRadius - this.circleRadius}
              ry={this.triangleRadius - this.circleRadius}
              fill='#6699FF'
              fillOpacity={value.includes('investment_liquidity') ? .55 : 0}
              onPressIn={() => this.handleTriangleChange('investment_liquidity')}
            />
            <Polygon
              points={triangle.map(({x, y}) => `${x},${y}`).join(' ')}
              fill={"rgb(250,250,250)"}
              stroke="#000000"
              strokeOpacity={.15}
              strokeWidth="2"
              fillOpacity={.6}
            />
            <SVGText
              fill='#E2526F'
              stroke='#E2526F'
              fillOpacity={.6}
              strokeOpacity={.5}
              fontSize="40"
              fontWeight='bold'
              x={seguridadCircle.x + rw(25)}
              y={seguridadCircle.y - rw(15)}
              textAnchor="middle"
            >
              S
            </SVGText>
            <SVGText
              fill='#4CBA8D'
              stroke='#4CBA8D'
              fillOpacity={.6}
              strokeOpacity={.5}
              fontSize="40"
              fontWeight='bold'
              x={rentabilidadCircle.x}
              y={rentabilidadCircle.y + rw(25)}
              textAnchor="middle"
            >
              R
            </SVGText>
            <SVGText
              fill='#6699FF'
              stroke='#6699FF'
              fillOpacity={.6}
              strokeOpacity={.5}
              fontSize="40"
              fontWeight='bold'
              x={liquidezCircle.x - rw(25)}
              y={liquidezCircle.y - rw(15)}
              textAnchor="middle"
            >
              L
            </SVGText>
          </Svg>

          <Text style={styles.heading3}>Solo puedes seleccionar dos</Text>

          <IbanGreenRoundedButton
            value='Continuar'
            onPress={this.submit}
            loading={loading}
            style={styles.button}
          ></IbanGreenRoundedButton>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    minHeight: '100%'
  },
  heading: {
    color: '#4CBA8D',
    fontSize: rw(23),
    marginTop: rw(54),
    textAlign: 'center'
  },
  heading2: {
    fontSize: rw(14),
    marginTop: rw(11),
    textAlign: 'center'
  },
  heading3: {
    fontSize: rw(12),
    marginTop: rw(12),
    textAlign: 'center',
    color: 'gray'
  },
  button: {
    marginTop: rw(25),
    marginLeft: rw(40),
    marginRight: rw(40),
  },

  legends: {
    flexDirection: 'row',
    marginTop: rw(30),
    justifyContent: 'space-around'
  },
  legend: {
    width: '33%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: rw(2),
    height: rw(28),
    width: rw(100)
  },
  legendText: {
    fontSize: rw(12),
    color: 'white',
    textAlign: 'center',
    fontWeight: 'bold',
  },
  triangle: {
    marginTop: rw(10)
  },
  statusBar: {
    height: 20
  },
});