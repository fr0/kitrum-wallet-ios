import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  StatusBar,
  ScrollView,
  Modal,
  Image,
  Keyboard,
  Alert,
  Dimensions
} from 'react-native';
import { relativeWidth } from './../../services/dimensions'
import { baseUrl } from './../../services/variables'
import IoniconsIcon from 'react-native-vector-icons/Ionicons'
import IbanGreenRoundedButton from '../../common/greenRoundedButton'
import Slider from '../../common/Slider'
import { MaskService } from 'react-native-masked-text'
import { setSavings } from './../../services/users'
import { PieChart } from 'react-native-svg-charts'

export default class Savings extends Component<{}> {

  constructor(props){
    super(props)
    this.state = {
      loading: false,
      data: []
    }

    this.submit = this.submit.bind(this)
  }

  static navigationOptions = { header: null, gesturesEnabled: false };

  submit(){
    const {navigation} = this.props

    this.setState({loading: true})
    setSavings(4, navigation)
      .then(response => {
        
      })
      .catch(error =>{
        this.setState({loading: false})
      })
  }

  componentDidMount() {
    this.setState({
      data: [
        {
          color: '#48A74C',
          value: 40
        },
        {
          color: '#1A5A1E',
          value: 24
        },
        {
          color: '#00DB70',
          value: 9
        },
        {
          color: '#64E5A6',
          value: 9
        },
        {
          color: '#9DCC9F',
          value: 8
        }
      ]
    })
  }

  render() {
    const { data, loading } = this.state 

    const pieData = data.map(({color, value}, index) => (
      {
        value,
        svg: {
            fill: color
        },
        key: `pie-${index}`
      }
    ))

    return (
      <ScrollView
        style={styles.container}
        keyboardShouldPersistTaps='always'
      >
        <StatusBar barStyle="dark-content"/>

        <Text style={styles.heading}>Distribución y rentabilidad óptima{'\n'}según su perfil</Text>

        <View style={styles.pieContainer}>
          <PieChart
              style={styles.pie}
              data={ pieData }
              innerRadius={"0%"}
              outerRadius={"100%"}
              animated={true}
              animationDuration={500}
              padAngle={0}
          />
          <View style={styles.pieLegend}>
            <Text style={styles.pieLegendText}>Rentabilidad mediade portafolio es</Text>
            <Text style={styles.pieLegendPercentage}>4,4%</Text>
          </View>
        </View>

        <View style={styles.heading2Container}>
          <Text style={styles.heading2}>DATOS</Text>
        </View>

       
        <View style={styles.legend}>
          <View style={[styles.legendSquare, {backgroundColor: '#48A74C'}]}></View>
          <Text style={styles.legendText}>Cantidad en depósitos y ahorros</Text>
          <Text style={styles.legendPercentage}>40%</Text>
        </View>
        <View style={styles.legend}>
          <View style={[styles.legendSquare, {backgroundColor: '#1A5A1E'}]}></View>
          <Text style={styles.legendText}>Cantidad en cuentas corrientes</Text>
          <Text style={styles.legendPercentage}>24%</Text>
        </View>
        <View style={styles.legend}>
          <View style={[styles.legendSquare, {backgroundColor: '#64E5A6'}]}></View>
          <Text style={styles.legendText}>Cantidad en productos a medio plazo</Text>
          <Text style={styles.legendPercentage}>9%</Text>
        </View>
        <View style={styles.legend}>
          <View style={[styles.legendSquare, {backgroundColor: '#64E5A6'}]}></View>
          <Text style={styles.legendText}>Cantidad en producto a largo plazo</Text>
          <Text style={styles.legendPercentage}>9%</Text>
        </View>
        <View style={styles.legend}>
          <View style={[styles.legendSquare, {backgroundColor: '#9DCC9F'}]}></View>
          <Text style={styles.legendText}>Cantidad en otras inversiones</Text>
          <Text style={styles.legendPercentage}>8%</Text>
        </View>
      
        <IbanGreenRoundedButton
          value='Continuar'
          onPress={this.submit}
          loading={loading}
          style={styles.button}
        ></IbanGreenRoundedButton>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    minHeight: '100%',
    paddingLeft: relativeWidth(18),
    paddingRight: relativeWidth(18),
  },
  pieContainer: {
    marginTop: relativeWidth(27)
  },
  pie: {
    height: relativeWidth(195),
    width: relativeWidth(195)
  },
  pieLegend: {
    position: 'absolute',
    top: relativeWidth(30),
    right: 0,
    width: relativeWidth(160),
    height: relativeWidth(135),
    shadowOffset: {width: 0, height: 0},
    shadowOpacity: 0.2,
    shadowRadius: 5,
    borderRadius: relativeWidth(8)
  },
  pieLegendText: {
    marginTop: relativeWidth(20),
    textAlign: 'center'
  },
  pieLegendPercentage: {
    color: '#4CBA8D',
    fontSize: relativeWidth(36),
    marginTop: relativeWidth(20),
    fontWeight: 'bold',
    textAlign: 'center'
  },
  heading: {
    color: '#4CBA8D',
    fontSize: relativeWidth(23),
    marginTop: relativeWidth(41),
    textAlign: 'center'
  },
  heading2Container: {
    marginTop: relativeWidth(26),
    marginLeft: relativeWidth(40),
    marginRight: relativeWidth(40),
    borderTopWidth: 1,
    borderColor: 'lightgray',
    marginBottom: relativeWidth(8),
    alignItems: 'center'
  },
  heading2: {
    fontSize: relativeWidth(16),
    textAlign: 'center',
    color: 'lightgray',
    fontWeight: 'bold',
    marginTop: relativeWidth(-10),
    width: relativeWidth(80),
  },
  button: {
    marginTop: relativeWidth(62),
    marginLeft: relativeWidth(40),
    marginRight: relativeWidth(40),
    marginBottom: relativeWidth(40),
  },
  legend: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: .5,
    borderColor: '#707070',
    paddingTop: relativeWidth(12.8),
    paddingBottom: relativeWidth(12.8),
  },
  legendSquare: {
    width: relativeWidth(10),
    height: relativeWidth(18),
    borderRadius: relativeWidth(2)
  },
  legendPercentage: {
    position: 'absolute',
    fontSize: relativeWidth(17),
    fontWeight: 'bold',
    color: 'rgba(1,1,1,.7)',
    right: 0,
    height: '100%'
  },
  legendText: {
    fontSize: relativeWidth(12),
    marginLeft: relativeWidth(5),
    color: 'rgba(1,1,1,.66)'
  }
});