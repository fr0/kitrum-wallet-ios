import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  ScrollView,
  Image,
  Alert,
  Slider
} from 'react-native';
import { relativeWidth } from './../../services/dimensions'
import { baseUrl } from './../../services/variables'
import { setSavings } from './../../services/users'
import IbanGreenRoundedButton from '../../common/greenRoundedButton'
import IbanButtonGroup from '../../common/buttonGroup';
import IoniconsIcon from 'react-native-vector-icons/Ionicons'

import CheckBox from 'react-native-checkbox';
import { Dropdown } from 'react-native-material-dropdown';

export default class Savings extends Component {

  constructor(props){
    super(props)
    this.state = {
      loading: false,
      scenario: 'expected'
    }

    this.submit = this.submit.bind(this)
  }

  static navigationOptions = { header: null, gesturesEnabled: false };

  submit(){
    const {navigation} = this.props

    this.setState({loading: true})
    setSavings(9, navigation)
      .then(response => {
        
      })
      .catch(error =>{
        this.setState({loading: false})
      })
  }

  _handleChanges = (key, value) => {
   
    this.setState({
      [key]: value
    })
  }

  render() {
    const { loading, scenario, duration } = this.state

    const durations = [
      {label: '10 años'},
      {label: '20 años'},
      {label: '30 años'},
    ]

    return (
      <ScrollView
        style={s.container}
        keyboardShouldPersistTaps='always'
      >
        <StatusBar barStyle="dark-content"/>

        <View style={s.header}>
          <Text style={s.heading}>Tu plan a largo plazo</Text>
          <Text style={s.heading2}>Puedes usar iBAN Wallet durante 1 día, 1 año o toda la cida, porque no tenemos permanecia, sin embargo, nuestros productos están diseñados para el ahorro a largo plazo.</Text>
          <Text style={s.heading3}>Mira tú mismo los diferentes escenarios:</Text>
        
          <IbanButtonGroup
            style={s.buttonGroup}
            textStyle={{color: '#00CC99'}}
            activeTextStyle={{color: 'white'}}
            itemStyle={{borderColor: '#00CC99', paddingTop: relativeWidth(8), paddingBottom: relativeWidth(8)}}
            items={[
              {value: 'Pesimista', action: () => this.setState({scenario: 'pesimistic'}), active: scenario == 'pesimistic' },
              {value: 'Esperado', action: () => this.setState({scenario: 'expected'}), active: scenario == 'expected'},
              {value: 'Optimista', action: () => this.setState({scenario: 'optimistic'}), active: scenario == 'optimistic'}
            ]}
          />

          <View style={s.prices}>
            <View style={[s.price, s.priceLeft]}>
              <Text style={[s.priceValue, s.priceValueLeft]}>160.000</Text>
              <Text style={[s.priceDelta, s.priceDeltaLeft]}>+30,000€</Text>
            </View>
            <View style={s.priceDivider}>
              <Text style={s.priceDividerText}>y</Text>
            </View>
            <View style={[s.price, s.priceRight]}>
              <Text style={[s.priceValue, s.priceValueRight]}>160.000€</Text>
              <Text style={[s.priceDelta, s.priceDeltaRight]}>+138,000€</Text>
            </View>
          </View>

          <View style={s.greenContainer}>
            <View style={s.greenBorder}>
              <Text style={s.green}>¿Cómo calculamos estos datos?</Text>
            </View>
            <IoniconsIcon
              style={s.infoIcon}
              name="ios-information-circle-outline"
              color="gray"
            />
          </View>
        </View>
        
        <View style={s.body}>
          <Text style={s.heading4}>Arrastra para cambiar la duración prevista del Plan y verás como a largo plazo los escenarios son siempre positivos.</Text>
        
          <View style={s.durationContainer}>
            <Text style={s.durationText}>Duración prevista:</Text>
            <Dropdown
              containerStyle={s.duration}
              label=''
              data={durations}
              value={duration}
              onChangeText={ (value, index, data) => this.setState({duration: data[index].label})}
              error={''}
              textColor='#4CBA8D'
              baseColor='#4CBA8D'
              itemColor='#4CBA8D'
            />
          </View>

          <Slider
            style={s.slider}
            minimumTrackTintColor='#4CBA8D'
          ></Slider>
        </View>
        
        <IbanGreenRoundedButton
          value='Entendido'
          onPress={this.submit}
          loading={loading}
          style={s.button}
        ></IbanGreenRoundedButton>
      </ScrollView>
    );
  }
}

const s = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    minHeight: '100%',
  },
  header: {
    shadowOffset: {width: 0, height: 4},
    shadowOpacity: 0.1,
    shadowRadius: 4,
    paddingLeft: relativeWidth(15),
    paddingRight: relativeWidth(15),
  },
  heading: {
    color: '#4CBA8D',
    fontSize: relativeWidth(23),
    marginTop: relativeWidth(41),
    textAlign: 'center'
  },
  heading2: {
    color: 'rgba(1,1,1,.66)',
    fontSize: relativeWidth(16),
    marginTop: relativeWidth(27),
    lineHeight: relativeWidth(24),
    textAlign: 'center'
  },
  heading3: {
    color: 'rgba(1,1,1,.66)',
    fontSize: relativeWidth(16),
    marginTop: relativeWidth(44),
    textAlign: 'center'
  },
  heading4: {
    color: 'rgba(1,1,1,.76)',
    fontSize: relativeWidth(16.5),
    marginTop: relativeWidth(9),
    lineHeight: relativeWidth(22),
    textAlign: 'center'
  },
  buttonGroup: {
    marginTop: relativeWidth(33),
    marginBottom: relativeWidth(0),
  },
  button: {
    marginTop: relativeWidth(15),
    marginLeft: relativeWidth(40),
    marginRight: relativeWidth(40),
    marginBottom: relativeWidth(40),
  },
  body: {
    padding: relativeWidth(15)
  },

  prices: {
    flexDirection: 'row'
  },
  price: {
    marginTop: relativeWidth(11),
    width: '45%'
  },
  priceLeft: {
    
  },
  priceRight: {
    
  },
  priceValue: {
    color: 'rgba(1,1,1,.57)',
    fontSize: relativeWidth(30)
  },
  priceValueLeft: {
    textAlign: 'right'
  },
  priceValueRight: {
    textAlign: 'left'
  },
  priceDelta: {
    color: '#339966',
    marginTop: relativeWidth(9),
    fontSize: relativeWidth(14),
  },
  priceDeltaLeft: {
    textAlign: 'right'
  },
  priceDeltaRight: {
    textAlign: 'left'
  },
  priceDivider: {
    width: '10%',
    marginTop: relativeWidth(18),
  },
  priceDividerText: {
    fontSize: relativeWidth(20),
    color: '#95989A',
    textAlign: 'center'
  },

  greenContainer: {
    marginTop: relativeWidth(19),
    marginBottom: relativeWidth(32),
    alignItems: 'center'
  },
  greenBorder: {
    borderBottomWidth: 1,
    borderColor: '#4CBA8D',
    width: relativeWidth(270)
  },
  green: {
    color: '#4CBA8D',
    textAlign: 'center',
    fontSize: relativeWidth(18),
  },
  infoIcon: {
    position: 'absolute',
    top: 0,
    right: relativeWidth(10),
    fontSize: relativeWidth(30),
    height: relativeWidth(30)
  },

  durationContainer: {
    marginTop: relativeWidth(-6),
    marginLeft: relativeWidth(20),
    marginRight: relativeWidth(20),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  duration: {
    width: relativeWidth(110),
    marginTop: relativeWidth(-20),
    marginLeft: relativeWidth(15)
  },
  durationText: {
    fontSize: relativeWidth(16),
    fontWeight: '400'
  },
  slider: {
    marginTop: relativeWidth(6)
  }
});