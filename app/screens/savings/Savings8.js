import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  ScrollView,
  Image,
  Alert
} from 'react-native';
import { relativeWidth } from './../../services/dimensions'
import { baseUrl } from './../../services/variables'
import { setSavings } from './../../services/users'
import IbanGreenRoundedButton from '../../common/greenRoundedButton'

import IbanTextInput from '../../common/TextInput';
import CheckBox from 'react-native-checkbox';

export default class Savings extends Component {

  constructor(props){
    super(props)
    this.state = {
      loading: false,
      val1: '',
      val2: '',
    }

    this.submit = this.submit.bind(this)
  }

  static navigationOptions = { header: null, gesturesEnabled: false };

  submit(){
    const {navigation} = this.props

    this.setState({loading: true})
    setSavings(8, navigation)
      .then(response => {
        
      })
      .catch(error =>{
        this.setState({loading: false})
      })
  }

  _handleChanges = (key, value) => {
   
    this.setState({
      [key]: value
    })
  }

  render() {
    const { loading, val1, val2, checked } = this.state

    return (
      <ScrollView
        style={styles.container}
        keyboardShouldPersistTaps='always'
      >
        <StatusBar barStyle="dark-content"/>

        <View style={styles.header}>
          <Text style={styles.heading}>¿Cuánto te gustaría aportar a tu plan?</Text>
          <Text style={styles.heading2}>Puedes empezar con solo 50€ y podrás ajustar tu aportación mensual cuando quieras, sin penalizaciones.</Text>
        </View>
        
        <View style={styles.body}>
          <IbanTextInput
            label='Para empezar'
            value={val1}
            onChangeText={ v => this._handleChanges('val1', v) }
            style={styles.field}
          />

          <View style={styles.blueContainer}>
            <Text style={styles.blue}>Quiero traspasar mi plan de pensiones</Text>
          </View>

          <IbanTextInput
            label='Y mensualmente quiero poner'
            value={val2}
            onChangeText={ v => this._handleChanges('val2', v) }
            style={styles.field}
          />

          <CheckBox
            containerStyle={styles.checkbox}
            labelStyle={styles.checkboxText}
            label={'Por ahora solo quiero poner mensualidad'}
            checked={checked}
            onChange={() => this._handleChanges('checked', !checked)}
            checkboxStyle={styles.acceptCheckbox}
            uncheckedImage={require('../../images/icons/checkbox-outline.png')}
            checkedImage={require('../../images/icons/checkbox-checked.png')}
          />

          <View style={styles.greenContainer}>
            <Text style={styles.green}>Aportarías 2.200€ el primer año, podrás ajustar estas cantidades más adelante.</Text>
          </View>
        </View>
        
        <IbanGreenRoundedButton
          value='Entendido'
          onPress={this.submit}
          loading={loading}
          style={styles.button}
        ></IbanGreenRoundedButton>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    minHeight: '100%',
  },
  header: {
    shadowOffset: {width: 0, height: 4},
    shadowOpacity: 0.1,
    shadowRadius: 4,
    height: relativeWidth(212)
  },
  heading: {
    color: '#4CBA8D',
    fontSize: relativeWidth(23),
    marginTop: relativeWidth(41),
    textAlign: 'center'
  },
  heading2: {
    fontSize: relativeWidth(18),
    marginTop: relativeWidth(34),
    lineHeight: relativeWidth(23.7),
    textAlign: 'center'
  },
  button: {
    marginTop: relativeWidth(15),
    marginLeft: relativeWidth(40),
    marginRight: relativeWidth(40),
    marginBottom: relativeWidth(40),
  },
  body: {
    padding: relativeWidth(15)
  },
  blueContainer: {
    borderBottomWidth: 1,
    borderColor: "#3399CC",
    paddingBottom: relativeWidth(8.5),
    marginTop: relativeWidth(20),
    marginBottom: relativeWidth(30),
  },
  blue: {
    fontSize: relativeWidth(18),
    color: "#3399CC"
  },
  greenContainer: {
    marginTop: relativeWidth(15),
    backgroundColor: '#339966',
    borderRadius: 5,
    padding: relativeWidth(18)
  },
  green: {
    color: 'white',
    textAlign: 'center',
    lineHeight: relativeWidth(22)
  },

  checkbox: {
    marginTop: relativeWidth(30)
  },
  checkboxText: {
    fontSize: relativeWidth(14),
    color: 'gray'
  }
});