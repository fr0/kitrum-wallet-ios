import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  ScrollView,
  Image,
  Animated,
  Easing
} from 'react-native';
import { relativeWidth as rw } from './../../services/dimensions'
import { baseUrl } from './../../services/variables'
import { setSavings } from './../../services/users'
import IbanGreenRoundedButton from '../../common/greenRoundedButton'
import LinearGradient from 'react-native-linear-gradient'

export default class Savings extends Component {

  constructor(props){
    super(props)
    this.state = {
      loading: false,
      value: [],
      chartHeight: new Animated.Value(0),
      cellHeight: new Animated.Value(0)
    }
    this.submit = this.submit.bind(this)
  }

  static navigationOptions = { header: null, gesturesEnabled: false };

  submit(){
    const {navigation} = this.props

    this.setState({loading: true})
    setSavings(7, navigation)
      .then(response => {
        
      })
      .catch(error =>{
        this.setState({loading: false})
      })
  }

  componentDidMount() {
    this.showChart()
  }

  showChart() {
    let animations = [
      Animated.timing(
        this.state.chartHeight,
        {
          toValue: 1,
          duration: 700,
          easing: Easing.out(Easing.ease)
        }
      ),
      Animated.timing(
        this.state.cellHeight,
        {
          toValue: 1,
          duration: 900,
          easing: Easing.out(Easing.ease)
        }
      )
    ]

    Animated.parallel(animations).start()
  }

  render() {
    const { loading, value, cellHeight } = this.state

    let Cell = ({title, price, gray}) => {
      return (
        <Animated.View 
          style={[
            s.cell,
            {
              height: cellHeight.interpolate({
                inputRange: [0, 1],
                outputRange: [rw(0), rw(73)]
              })
            }
          ]}>     
            <Text style={[s.cellTitle, gray ? s.cellGray : null]}>{ title }</Text>  
            <Text style={[s.cellPrice, gray ? s.cellGray : null]}>{ price }</Text>  
          </Animated.View>
      )
    }

    return (
      <ScrollView
        style={s.container}
        keyboardShouldPersistTaps='always'
      >
        <StatusBar barStyle="dark-content"/>

        <Text style={s.heading}>El producto perfecto no existe…</Text>

        <View style={s.chartShadow}>
          <View style={s.chartContainer}>
            <Animated.View
              style={s.greenArea}
            />
            <Animated.View 
              style={[
                s.subtractionEllipse,
                {
                  height: this.state.chartHeight.interpolate({
                    inputRange: [0, 1],
                    outputRange: [rw(800), rw(400)]
                  }),
                  width: this.state.chartHeight.interpolate({
                    inputRange: [0, 1],
                    outputRange: [rw(800), rw(400)]
                  })
                }
              ]}/>

              <View style={s.columnsContainer}>
                <View style={s.column}>
                  <LinearGradient 
                    colors={['rgba(1,1,1,.05)', 'rgba(1,1,1,.4)','rgba(1,1,1,.5)']} 
                    locations={[0.3, 0.9, 1]}
                    style={s.gradient}
                  />
                  <Cell 
                    title="Cuentas\ncorrientes"
                    price="1.824,00€"
                  />
                  <Cell/>
                </View>
                <View style={s.column}>
                  <LinearGradient 
                    colors={['rgba(1,1,1,.05)', 'rgba(1,1,1,.4)','rgba(1,1,1,.5)']} 
                    locations={[0.3, 0.9, 1]}
                    style={s.gradient}
                  />
                  <Cell
                    title={`Depósitos y\nahorros`}
                    price="1.824,00€"
                  />
                  <Cell
                    title={`Otras\ninversiones`}
                    price="1.824,00€"
                    gray={true}
                  />
                  <Cell/>
                </View>
                <View style={s.column}>
                  <LinearGradient 
                    colors={['rgba(1,1,1,.05)', 'rgba(1,1,1,.4)','rgba(1,1,1,.5)']} 
                    locations={[0.3, 0.9, 1]}
                    style={s.gradient}
                  />
                  <Cell
                    title={`Productos\na largo plazo`}
                    price="1.824,00€"
                  />
                  <Cell
                    title={`Productos a\nmedio plazo`}
                    gray={true}
                  />
                  <Cell/>
                  <Cell/>
                </View>
              </View>
          </View>

        </View>

        <Text style={s.heading2}>Pero sí la estrategia perfecta!!</Text>

        <IbanGreenRoundedButton
          value='Entendido'
          onPress={this.submit}
          loading={loading}
          style={s.button}
        ></IbanGreenRoundedButton>
      </ScrollView>
    );
  }
}

const s = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    minHeight: '100%',
  },
  chart: {
    position: 'absolute',
    top: 0,
    width: '100%'
  },
  heading: {
    color: '#4CBA8D',
    fontSize: rw(23),
    marginTop: rw(37),
    textAlign: 'center'
  },
  heading2: {
    color: '#4CBA8D',
    fontSize: rw(23),
    marginTop: rw(30),
    textAlign: 'center'
  },
  button: {
    marginTop: rw(40),
    marginLeft: rw(40),
    marginRight: rw(40),
    marginBottom: rw(40),
  },

  chartShadow: {
    shadowOffset: {width: 0, height: 5},
    shadowOpacity: 0.1,
    shadowRadius: 2,
  },
  chartContainer: {
    alignItems: 'center',
    marginTop: rw(17),
    height: rw(474),
    overflow: 'hidden'
  },
  subtractionEllipse: {
    position: 'absolute',
    top: rw(-236),
    left: rw(-180),
    borderRadius: 400,
    transform: [
      {scaleX: 2}
    ],
    backgroundColor: 'white'
  },
  greenArea: {
    backgroundColor: '#B0F0D0',
    width: '100%',
    height: '100%',
    position: 'absolute'
  },

  columnsContainer: {
    flexDirection: 'row',
    position: 'absolute',
    bottom: rw(27),
    justifyContent: 'space-between'
  },
  column: {
    width: '31%',
    flexDirection: 'column',
    alignSelf: 'flex-end',

    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.3,
    shadowRadius: 4,
  },
  cell: {
    borderBottomWidth: 1,
    borderColor: 'rgba(1,1,1,.2)',
    padding: rw(10),
    overflow: 'hidden'
  },
  gradient: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  cellTitle: {
    color: 'rgba(1,1,1,.7)',
    fontSize: rw(13),
    backgroundColor: 'transparent'
  },
  cellPrice: {
    marginTop: rw(3),
    color: '#339966',
    fontSize: rw(18),
    backgroundColor: 'transparent'
  },
  cellGray: {
    color: '#95989A'
  }
});