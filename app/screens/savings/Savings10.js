import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  ScrollView,
  Image,
  Alert,
  Slider
} from 'react-native';
import { relativeWidth } from './../../services/dimensions'
import { baseUrl } from './../../services/variables'
import { setSavings } from './../../services/users'
import IbanGreenRoundedButton from '../../common/greenRoundedButton'
import IoniconsIcon from 'react-native-vector-icons/Ionicons'
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome'

export default class Savings extends Component {

  constructor(props){
    super(props)
    this.state = {
      loading: false,
      scenario: 'expected'
    }

    this.submit = this.submit.bind(this)
  }

  static navigationOptions = { header: null, gesturesEnabled: false };

  submit(){
    const {navigation} = this.props

    this.setState({loading: true})
    setSavings(10, navigation)
      .then(response => {
        
      })
      .catch(error =>{
        this.setState({loading: false})
      })
  }

  _handleChanges = (key, value) => {
   
    this.setState({
      [key]: value
    })
  }

  render() {
    const { loading, scenario, duration } = this.state

    const durations = [
      {label: '10 años'},
      {label: '20 años'},
      {label: '30 años'},
    ]

    return (
      <ScrollView
        style={s.container}
        keyboardShouldPersistTaps='always'
      >
        <StatusBar barStyle="dark-content"/>

        <View style={s.header}>
          <Image
            style={s.logo}
            source={require('./images/logo.png')}
          />
          <Image
            style={s.pig}
            source={require('./images/pig.png')}
          />
          <Text style={s.heading}>Plan de ahorro</Text>
        </View>
        
        <View style={s.body}>
       
        <View style={s.block}>
            <View style={s.blockContent}>
              <Text style={s.blockHeading}>Aportaciones</Text>
              <Text style={s.blockDescription}>
                10.000€ para empezar / 1.000€ al mes{'\n'}
                Primera mensualidad: 09/2017</Text>
            </View>
            <View style={s.blockIconContainer}>
              <Image
                style={[s.blockIcon, {height: relativeWidth(40)}]}
                source={require('./images/euro.png')}
              />
            </View>
            <View style={s.blockEditIconContainer}>
              <FontAwesomeIcon
                style={s.blockEditIcon}
                name="edit"
              />
            </View>
          </View>

          <View style={s.block}>
            <View style={s.blockContent}>
              <Text style={s.blockHeading}>Configuración del plan</Text>
              <Text style={s.blockDescription}>5 / 5 Atrevido</Text>
            </View>
            <View style={s.blockIconContainer}>
              <Image
                style={s.blockIcon}
                source={require('./images/calendar.png')}
              />
            </View>
            <View style={s.blockEditIconContainer}>
              <FontAwesomeIcon
                style={s.blockEditIcon}
                name="edit"
              />
            </View>
          </View>
          <View style={s.block}>
            <View style={[s.blockContent, {borderBottomWidth: 0}]}>
              <Text style={s.blockHeading}>Coste del producto</Text>
              <View style={s.blockDescriptionContainer}>
                <Text style={s.blockDescription}>0,46 anual en concepto de seguro</Text>
                <IoniconsIcon
                  style={s.infoIcon}
                  name="ios-information-circle-outline"
                />
              </View>
              <View style={s.blockDescriptionContainer}>
                <Text style={s.blockDescription}>0,69 anual sobre tus ahorros</Text>
                <IoniconsIcon
                  style={s.infoIcon}
                  name="ios-information-circle-outline"
                />
              </View>
            </View>
            <View style={s.blockIconContainer}>
              <Image
                style={s.blockIcon}
                source={require('./images/list.png')}
              />
            </View>
          </View>
        </View>
        
        <IbanGreenRoundedButton
          value='Entendido'
          onPress={this.submit}
          loading={loading}
          style={s.button}
        ></IbanGreenRoundedButton>
      </ScrollView>
    );
  }
}

const s = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    minHeight: '100%',
  },
  header: {
    backgroundColor: '#339966',
    shadowOffset: {width: 0, height: 4},
    shadowOpacity: 0.1,
    shadowRadius: 4,
    paddingLeft: relativeWidth(15),
    paddingRight: relativeWidth(15),

    alignItems: 'center',
    marginBottom: relativeWidth(0)
  },
  heading: {
    color: 'white',
    fontSize: relativeWidth(34),
    marginTop: relativeWidth(43),
    marginBottom: relativeWidth(30),
    textAlign: 'center'
  },
  
  button: {
    marginTop: relativeWidth(15),
    marginLeft: relativeWidth(40),
    marginRight: relativeWidth(40),
    marginBottom: relativeWidth(40),
  },
  body: {
    padding: relativeWidth(15)
  },
  pig: {
    marginTop: relativeWidth(56),
    width: relativeWidth(350),
    height: relativeWidth(95),
  },
  logo: {
    marginTop: relativeWidth(25),
    width: relativeWidth(150),
    height: relativeWidth(20),
  },
  infoIcon: {
    fontSize: relativeWidth(30),
  
    color: "#007AFF",
    marginLeft: relativeWidth(30),
    marginTop: relativeWidth(-7)
  },

  block: {
    
  },
  blockContent: {
    marginLeft: relativeWidth(50),
    marginRight: relativeWidth(50),
    borderBottomWidth: 1,
    borderColor: 'lightgray',
    paddingBottom: relativeWidth(24),
    marginTop: relativeWidth(10)
  },
  blockHeading: {
    fontSize: relativeWidth(17),
    marginBottom: relativeWidth(7),
    marginTop: relativeWidth(6)
  },
  blockDescriptionContainer: {
    flexDirection: 'row',
  },
  blockDescription: {
    fontSize: relativeWidth(13),
    color: 'rgba(1,1,1,.6)',
  },
  blockIconContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: relativeWidth(50),
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  blockIcon: {
    width: relativeWidth(30),
    height: relativeWidth(30),
  },
  blockEditIconContainer: {
    position: 'absolute',
    top: 0,
    right: 0,
    height: '100%',
    width: relativeWidth(50),
    justifyContent: 'center',
    alignItems: 'center'
  },
  blockEditIcon: {
    color: '#33CC99',
    fontSize: relativeWidth(26)
  }
});