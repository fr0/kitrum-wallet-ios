import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  StatusBar,
  ScrollView,
  Keyboard,
  Alert,
  Dimensions
} from 'react-native';
import { relativeWidth } from './../../services/dimensions'
import { baseUrl } from './../../services/variables'
import IbanGreenRoundedButton from '../../common/greenRoundedButton'
import { setSavings } from './../../services/users'
import Icon from 'react-native-vector-icons/Feather'

import { StackedAreaChart, XAxis, YAxis, Grid } from 'react-native-svg-charts'
import * as shape from 'd3-shape'

export default class Savings extends Component {

  constructor(props){
    super(props)
    this.state = {
      loading: false,
      value: []
    }

    this.submit = this.submit.bind(this)
  }

  static navigationOptions = { header: null, gesturesEnabled: false };

  submit(){
    const {navigation} = this.props

    this.setState({loading: true})
    setSavings(6, navigation)
      .then(response => {
        
      })
      .catch(error =>{
        this.setState({loading: false})
      })
  }

  render() {
    const { loading } = this.state

    let data = [
      {
        age: 41,
        actual: 0,
        optimal: 0
      },
      {
        age: 46,
        actual: 10,
        optimal: 20
      },
      {
        age: 51,
        actual: 20,
        optimal: 100,
      },
      {
        age: 56,
        actual: 30,
        optimal: 80,
      },
      {
        age: 61,
        actual: 50,
        optimal: 160,
      },
      {
        age: 65,
        actual: 90,
        optimal: 200
      }
    ]

    data.forEach(i => i.optimal -= i.actual)

    const maxValue = data[data.length - 1].optimal
    const colors = [ '#1F8FE8', 'rgb(49, 153, 102, 0.25)' ]
    const keys   = [ 'actual', 'optimal' ]
    const xAxisKeys = [ 'age' ]

    return (
      <ScrollView
        style={styles.container}
        keyboardShouldPersistTaps='always'
      >
        <StatusBar barStyle="dark-content"/>

        <Text style={styles.heading}>Esta gráfica con sus datos, según 5{'\n'}productos</Text>

        <View style={styles.calcContainer}>
          <View style={styles.calcBlock}>
            <View style={styles.calcArrowContainer}>
              <Icon name="arrow-up" size={14} color="rgba(31,143,232,.38)" />
              <Text style={styles.calcPercentage}>11%</Text>
            </View>
            <Text style={[styles.calcPrice, {color: '#1F8FE8'}]}>114.900€</Text>
            <Text style={styles.calcDescription}>Capital final{'\n'}situación actual</Text>
            <View style={styles.calcDivider}/>
          </View>
          <View style={styles.calcBlock}>
            <View style={styles.calcArrowContainer}>
              <Icon name="arrow-up" size={14} color="rgba(51,153,102,.38)" />
              <Text style={styles.calcPercentage}>20%</Text>
            </View>
            <Text style={[styles.calcPrice, {color: '#339966'}]}>114.900€</Text>
            <Text style={styles.calcDescription}>Capital final{'\n'}situación óptima</Text>
          </View>
        </View>

        <View style={styles.chartShadow}>
          <View style={styles.chartContainer}>
            <StackedAreaChart
              style={ { flex: 1 } }
              contentInset={ { top: 10, bottom: 0 } }
              data={ data }
              keys={ keys }
              colors={ colors }
              curve={ shape.curveBasis }
              numberOfTicks={maxValue/25}
            >
              <Grid
                svg={{strokeDasharray: '4,4'}}
              />
            </StackedAreaChart>
            <YAxis
              numberOfTicks={maxValue/25}
              style={ { position: 'absolute', top: 0, bottom: 0, left: 10 }}
              data={ StackedAreaChart.extractDataPoints(data, keys) }
              contentInset={ { top: 0, bottom: 10 } }
              formatLabel={ value => `${value}€` }
              svg={ {
                fontSize: 12,
                fill: 'rgba(1,1,1,.6)',
                alignmentBaseline: 'baseline',
                baselineShift: '-45',
              } }
            />
          </View>
        </View>

        <XAxis
          numberOfTicks={maxValue/25}
          style={styles.chartXAxis}
          data={ data }
          xAccessor={({item}) => item.age}
          contentInset={ { top: 0, bottom: 10 } }
          formatLabel={ value => value }
          svg={ {
            fontSize: 12,
            fill: 'rgba(1,1,1,.6)'
          } }
        />
        <XAxis
          numberOfTicks={maxValue/25}
          style={styles.chartXAxis}
          data={ data }
          xAccessor={({item}) => item.age}
          contentInset={ { top: 0, bottom: 10 } }
          formatLabel={ value => `Años` }
          svg={ {
            fontSize: 12,
            fill: 'rgba(1,1,1,.6)'
          } }
        />

        <IbanGreenRoundedButton
          value='Continuar'
          onPress={this.submit}
          loading={loading}
          style={styles.button}
        ></IbanGreenRoundedButton>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    minHeight: '100%',
  },
  chartShadow: {
    shadowOffset: {width: 0, height: 0},
    shadowOpacity: 0.2,
    shadowRadius: 3,
    marginTop: relativeWidth(38),
    borderRadius: relativeWidth(4),
    marginLeft: relativeWidth(15),
    marginRight: relativeWidth(15),
    marginBottom: relativeWidth(10),
  },
  chartXAxis: {
    marginLeft: relativeWidth(15),
    marginRight: relativeWidth(15),
  },
  chartContainer: {
    height: relativeWidth(280),
    borderRadius: relativeWidth(4),
    flexDirection: 'row',
    backgroundColor: '#DDEDE5',
    overflow: 'hidden'
  },
  heading: {
    color: '#4CBA8D',
    fontSize: relativeWidth(23),
    marginTop: relativeWidth(41),
    textAlign: 'center'
  },
  button: {
    marginTop: relativeWidth(26),
    marginLeft: relativeWidth(40),
    marginRight: relativeWidth(40),
    marginBottom: relativeWidth(40),
  },

  legendContainer: {
    position: 'absolute',
    top: 0,
    left: relativeWidth(10),
  },
  legend: {
    color: 'gray',
    fontSize: relativeWidth(13),
    marginTop: relativeWidth(10),
    marginBottom: relativeWidth(20.6),
    backgroundColor: 'transparent'
  },

  calcContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: relativeWidth(46)
  },
  calcBlock: {
    width: '50%',
    alignItems: 'center'
  },
  calcArrowContainer: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  calcPercentage: {
    color: '#737373',
    fontWeight: 'bold'
  },
  calcPrice: {
    marginTop: relativeWidth(14),
    fontSize: relativeWidth(35),
    fontWeight: 'bold'
  },
  calcDescription: {
    textAlign: 'center',
    fontSize: relativeWidth(13),
    color: '#737373',
    marginTop: relativeWidth(13)
  },
  calcDivider: {
    position: 'absolute',
    borderLeftWidth: 1,
    borderColor: 'lightgray',
    right: 1,
    top: relativeWidth(30),
    height: relativeWidth(70)
  }
});