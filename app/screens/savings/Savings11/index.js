import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  StatusBar,
  ScrollView,
  Modal,
  Image,
  Keyboard,
  Alert,
  Dimensions
} from 'react-native';
import { relativeWidth } from './../../../services/dimensions'
import InvestorClassButton from '../../../common/InvestorClassButton'

export default class Savings extends Component {

  constructor(props){
    super(props)
    this.state = {
    }

    this.submit = this.submit.bind(this)
  }

  static navigationOptions = { header: null, gesturesEnabled: false };

  submit(investorType){
    const {navigation} = this.props

    navigation.push('InvestorType', {investorType: investorType})

  }

  componentDidMount() {
  }

  render() {

    return (
      <View style={styles.container}>
        <StatusBar
          barStyle="dark-content"
        />
        <Image
          style={ styles.icon }
          source={require('../../savings/images/save-money.png')}
        ></Image>
        <Text style={styles.title}>
          Clase de inversor
        </Text>
        <Text style={styles.text}>
          Para poder acceder a la aplicación, por favor selecciona el perfil que mejor te describe.
        </Text>
        <Text style={[styles.text, {color: 'rgb(51,153,102)'}]}>
          ¿Porqué pedimos eso?
        </Text>
        <InvestorClassButton
          value='Inversor con patrimonio'
          onPress={() => this.submit('patrimonio')}
        />
        <InvestorClassButton
          value='Inversor sofisticado'
          onPress={() => this.submit('sofisticado')}
        />
        <InvestorClassButton
          value='Inversor no cualificado'
          onPress={() => this.submit('cualificado')}
        />
      </View>


    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    minHeight: '100%',
    paddingLeft: relativeWidth(18),
    paddingRight: relativeWidth(18),
    marginTop: relativeWidth(20)
  },
  icon: {
    alignSelf: 'center',
    marginTop: relativeWidth(65)
  },
  title: {
    alignSelf: 'center',
    marginTop: relativeWidth(10),
    marginBottom: relativeWidth(5),
    fontSize: relativeWidth(31),
    letterSpacing: relativeWidth(2)
  },
  text: {
    alignSelf: 'center',
    textAlign: 'center',
    marginLeft: relativeWidth(15),
    marginRight: relativeWidth(15),
    marginBottom: relativeWidth(40),
    fontSize: relativeWidth(16),
  }
});