import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  Image,
  Alert,
  Dimensions,
  TouchableWithoutFeedback
} from 'react-native';
import Pdf from 'react-native-pdf'
import IBanCheckBox from '../../../common/checkbox';
import { relativeWidth } from './../../../services/dimensions'
import InvestorClassButton from '../../../common/InvestorClassButton'
import { setSavings } from './../../../services/users'
import {baseUrl} from "../../../services/variables";

export default class InvestorType extends Component {

  constructor(props){
    super(props)
    this.state = {
      loading: false,
    }

    this.submit = this.submit.bind(this)
  }

  static navigationOptions = { header: null, gesturesEnabled: false };

  submit(){
    const {navigation} = this.props

    this.setState({loading: true})
    setSavings(11, navigation)
      .then(response => {
      })
      .catch(error =>{
      })
  }

  _handleBack(){
    const { navigation } = this.props
    navigation.goBack()
  }

  componentDidMount() {
  }

  _handleChanges = (key, value) => {

    this.setState({
      [key]: value
    })
  }

  render() {
    const { data, loading, checked } = this.state
    const { investorType } = this.props.navigation.state.params


    return (
      <View style={{backgroundColor: 'white'}}>
      <View style={styles.container}>
        <StatusBar
          barStyle="dark-content"
        />
        <TouchableWithoutFeedback
          onPress={() => this._handleBack()}
        >
          <Image
            style={styles.backArrow}
            source={require('../../../images/icons/back-arrow.png')}
          />
        </TouchableWithoutFeedback>
        <Image
          style={ styles.icon }
          source={require('../../savings/images/save-money.png')}
        />
        <Text style={styles.title}>
          Inversor con patrimonio
        </Text>
        <View style={styles.body}>
        <Pdf
          source={{uri: `${ baseUrl }/api/v1/terms?type=${investorType}`, cache: true}}
          onLoadComplete={(numberOfPages,filePath)=>{

          }}
          onPageChanged={(page,numberOfPages)=>{

          }}
          onError={(error)=>{

          }}
          style={styles.pdf}
          fitPolicy={0}
        />
        </View>
        <View>
          <IBanCheckBox
            label={'Acepto que las inversiones con las que se relacionan las promociones pueden exponerme a un riesgo significativo de perder todo el dinero u otra propiedad invertida.'}
          />
          <IBanCheckBox
            label={'Soy consciente de que tengo la posibilidad de pedir consejo a una persona autorizada y especializada en asesorar sobre valores no realizables.'}
            containerStyle={{marginTop: relativeWidth(35)}}
          />
        </View>

        <InvestorClassButton
          value='Firmar'
          onPress={this.submit}
          loading={loading}
          style={{marginTop: relativeWidth(40)}}
        />
      </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    minHeight: '100%',
    marginTop: relativeWidth(50)
  },
  icon: {
    alignSelf: 'center',
  },
  title: {
    alignSelf: 'center',
    marginTop: relativeWidth(10),
    marginBottom: relativeWidth(15),
    fontSize: relativeWidth(31),
    letterSpacing: relativeWidth(2)
  },
  body: {
    flex: 1,
    maxHeight: relativeWidth(300),
    justifyContent: 'flex-start',
    minWidth: Dimensions.get('window').width,
    paddingRight: relativeWidth(0),
    paddingLeft: relativeWidth(0)
  },
  pdf: {
    maxHeight: relativeWidth(300),
    minWidth: '100%'
  },
  backArrow: {
    marginLeft: relativeWidth(25)
  }
});