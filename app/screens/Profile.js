import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableWithoutFeedback,
  StatusBar,
  ScrollView,
  KeyboardAvoidingView,
  Alert,
  AsyncStorage,
  TouchableOpacity,
  Keyboard
} from 'react-native';
import IbanGreenRoundedButton from '../common/greenRoundedButton';
import IbanBlueLink from '../common/blueLink';
import IbanButtonGroup from '../common/buttonGroup';
import IbanTextInput from '../common/TextInput';
import IbanFileInput from '../common/FileInput';

import KYCsuccessAlert from './signup/KYCsuccessAlert';
import KYCsuccessPrompt from './signup/KYCsuccessPrompt';
import KYCerrorPrompt from './signup/KYCerrorPrompt';
import KYCerror from './signup/KYCerror';

import { Dropdown } from 'react-native-material-dropdown';
import { DatePickerDialog } from 'react-native-datepicker-dialog'

import moment from 'moment';
import { baseUrl } from './../services/variables'
import { NavigationActions, StackActions } from 'react-navigation'

import { relativeWidth } from './../services/dimensions'
import { profile, updateProfile, setAdditional } from './../services/users'
import { list as getCountries } from './../services/countries'

export default class Profile extends Component {

  state = {
    alertScreen: '',
    user: {
      address1: '',
      address2: '',
      date_of_birth: moment().subtract('18', 'Y'),
      country: {}
    },
    validationErrors: {},
    loading: false,
    countries: []
  }

  static navigationOptions = { header: null, gesturesEnabled: false };

  componentDidMount(){

    const { navigation } = this.props;
    const signupFlow = this.props.navigation.state && this.props.navigation.state.params
      ?
      this.props.navigation.state.params.signupFlow
      :
      false

    profile(navigation, {noRedirect: true})
      .then(user => {
        this.setState({user, signupFlow})
      })
      .catch(error => {

      })

    this._retrieveCountries()
  }

  _openDateOfBirthDialog = () => {
    let { user, validationErrors } = this.state;

    this.refs.DatePickerDialog.open({
      date: user.date_of_birth.toDate(),
    });
  }

  _handleChanges(key, value){
    let {user, validationErrors} = this.state;
    user[key] = value;

    this.setState({
      user,
      validationErrors: {
        ...validationErrors,
        [key]: null
      }
    })
  }

  _retrieveCountries(){
    getCountries().then(countries => {
      countries = countries.map(c => {
        return {
          value: c.name,
          name: c.name,
          id: c.id
        }
      })
      this.setState({countries})
    })
  }

  render() {
    const { user, loading, validationErrors, signupFlow, countries } = this.state

    return (
          <View
            style={styles.form}
          >

            <View
              style={ styles.main }
            >
              <IbanTextInput
                label="Dirección 1"
                value={user.address1}
                onChangeText={ val => this._handleChanges('address1', val) }
                error={ (validationErrors.address1 || []).join('. ')}
              />

              <IbanTextInput
                label="Dirección 2 (opcional)"
                value={user.address2}
                onChangeText={ val => this._handleChanges('address2', val) }
              />

              <IbanTextInput
                label="Código postal"
                value={user.post_code}
                onChangeText={ val => this._handleChanges('post_code', val) }
                error={ (validationErrors.post_code || []).join('. ')}
              />

              <IbanTextInput
                label="Ciudad"
                value={user.city}
                onChangeText={ val => this._handleChanges('city', val) }
                error={ (validationErrors.city || []).join('. ')}
              />

              <Dropdown
                label='País'
                data={countries}
                value={user.country.name}
                valueExtractor={({value}) => value.name}
                labelExtractor={({value}) => value.name}
                onChangeText={ (value, index, data) => this._handleChanges('country', data[index]) }
                error={ (validationErrors.country || []).join('. ')}
              />

              <IbanTextInput
                label="Fecha nacimiento"
                value={user.date_of_birth ? moment(user.date_of_birth).format('DD/MMM/YYYY') : '' }
                onChangeText={ val => this._handleChanges('date_of_birth', new Date()) }
                onPress={() => this._openDateOfBirthDialog()}
                error={ (validationErrors.date_of_birth || []).join('. ')}
              />

              {/*<IbanTextInput*/}
                {/*label="Código del cupón"*/}
                {/*value={user.coupone_number}*/}
                {/*onChangeText={ val => this._handleChanges('coupone_number', val) }*/}
                {/*error={ (validationErrors.coupone_number || []).join('. ')}*/}
              {/*/>*/}

              <IbanGreenRoundedButton
                value='Continuar'
                style={styles.button}
                onPress={() => this.submit()}
                loading={loading}
              ></IbanGreenRoundedButton>

              {signupFlow ? null :
                <IbanGreenRoundedButton
                  value='Logout'
                  style={styles.button}
                  onPress={() => this.logout()}
                ></IbanGreenRoundedButton>
              }

              <DatePickerDialog
                ref="DatePickerDialog"
                onDatePicked={(val) => this._handleChanges('date_of_birth', val) }
                okLabel="Ok"
                cancelLabel="Cancel"
              />

            </View>
          </View>
    );
  }

  submit(){
    Keyboard.dismiss();

    let {navigation, setAlert} = this.props
    let {user, loading} = this.state

    let update = this.props.navigation.state && this.props.navigation.state.params && this.props.navigation.state.params.signupFlow ? setAdditional : updateProfile

    if(loading){
      return
    }

    this.setState({loading: true})
    update(user, navigation)
      .then(response => {
        this.setState({loading: false})

        data = JSON.parse(response['_bodyText']);

        if(response.status === 200){
          setAlert('successPrompt')
        }else{
          if(response.status == 422){
            this.setState({
              validationErrors: data.validation_errors
            });
          }
        }
      })
      .catch((error) => {
        this.setState({loading: false})

      });
  }

  logout(){
    Keyboard.dismiss();

    const { navigate } = this.props.navigation;

    AsyncStorage.getItem('@iBANStore:session_token').then((session_token) => {
      fetch(
        `${ baseUrl }/api/v1/logout`,
        {
          method: 'DELETE',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            session_token
          })
        }
      )
        .then(response => {
          this._dropSessionAndResetNavStack()
        })
        .catch((error) => {
          console.error(error);
          this._dropSessionAndResetNavStack()
        });
    })
  }

  _dropSessionAndResetNavStack(){
    AsyncStorage.removeItem('@iBANStore:session_token').then(error => {
      const resetAction = StackActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({ routeName: 'Login'})
        ]
      })
      this.props.navigation.dispatch(resetAction)
    });
  }
}

const styles = StyleSheet.create({
  main: {
    paddingBottom: 30
  },
  form: {
    flex: 1,
    padding: 10,
    backgroundColor: 'white'
  }
});