import React, { Component } from 'react';
import {
  StyleSheet,
  WebView,
} from 'react-native';

import {baseUrl} from '../services/variables'
import { relativeWidth } from './../services/dimensions'
import NavBackButton from '../common/NavBackButton';

export default class WebLogin extends Component<{}> {

  constructor(props){
    super(props)
  }

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Login',
      headerTitleStyle: {color: 'white', fontWeight: '400'},
      headerStyle: {backgroundColor: '#339966'},
      headerLeft: (
        <NavBackButton
          title='Volver'
          onPress={() => {
            navigation.goBack();
          }}
        />
      ),
      gesturesEnabled: false
    }
  };

  render() {
    let {email, password} = this.props.navigation.state.params

    return (
      <WebView
        style={styles.webView}
        source={{uri: `https://secure.myibanwallet.com/`}}
        injectedJavaScript={`
          usuario.value = '${ email }';
          password.value = '${ password }'
        `}
      >

      </WebView>
    );
  }


}

const styles = StyleSheet.create({
  webView: {
    height: '100%',
    width: '100%'
  },
  navText: {
    color: 'white',
    fontSize: relativeWidth(18),
    paddingLeft: relativeWidth(18)
  },
});
