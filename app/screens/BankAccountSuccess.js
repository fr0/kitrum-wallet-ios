import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  Image,
  Alert
} from 'react-native';
import { NavigationActions } from 'react-navigation'
import { relativeWidth } from './../services/dimensions'
import { setFitnance } from './../services/users'

export default class BankAccountSuccess extends Component<{}> {

  constructor(props){
    super(props);

    this.state = {
      secondHeaderVisible: false,
      loading: false
    }
  }

  componentDidMount(){
    setTimeout(() => {
      this.setState({secondHeaderVisible: true})
    }
    , 2000)
  }

  _close(){
    const {secondHeaderVisible} = this.state

    const { navigation } = this.props;

    if(!secondHeaderVisible){
      return
    }

    if(secondHeaderVisible){
      if(this.state.loading){
        return
      }
      setFitnance(null, navigation).then(res => {
        navigation.navigate('Wallet')
      })
        .catch(error => {
        })
    }
  }

  render() {
    const {secondHeaderVisible} = this.state

    return (
      <TouchableWithoutFeedback onPress={() => this._close()}>
        <View style={ styles.main }>
          <View style={styles.text}>
            <Text style={styles.header}>¡Éxito!</Text>
            {
              secondHeaderVisible ?
                <Text style={styles.header2}>EL PROCESO SE HA{'\n'}COMPLETADO{'\n'}CORRECTAMENTE</Text>
                : null
            }
          </View>

          <Image
            style={ styles.closeIcon }
            source={require('../images/icons/close.png')}
          ></Image>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

BankAccountSuccess.navigationOptions = { header: null, gesturesEnabled: false };

const styles = StyleSheet.create({
  main: {
    alignItems: 'center',
    justifyContent: 'space-around',
    backgroundColor: '#4CBA8D',
    height: '100%',
    width: '100%'
  },
  header: {
    textAlign: 'center',
    color: 'white',
    fontSize: relativeWidth(35),
  },
  header2: {
    textAlign: 'center',
    color: 'white',
    fontSize: relativeWidth(18),
    marginTop: relativeWidth(18)
  },
  text: {

  },
  closeIcon: {
    position: 'absolute',
    top: relativeWidth(40),
    right: relativeWidth(17),
    height: relativeWidth(22),
    width: relativeWidth(22),
  }
});
