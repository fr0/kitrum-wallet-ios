import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableWithoutFeedback,
  StatusBar,
  ScrollView,
  KeyboardAvoidingView,
  Alert,
  AsyncStorage,
  TouchableOpacity,
  Keyboard,
  RefreshControl
} from 'react-native';

import { relativeWidth } from './../../services/dimensions'
import { list as getContacts } from './../../services/contacts'
import { profile, getStatements } from './../../services/users'
import { statements } from './../../services/unnax/Fitnance'
import NavBackButton from '../../common/NavBackButton';
import { handleRequestError, baseUrl, unnaxUrl, oldUnnaxUrl, api_id, api_code } from './../../services/variables'

import moment from 'moment';

export default class AccountDetails extends Component {

  constructor(props){
    super(props)

    this.state = {
      contacts: [],
      selectedMonth: {},
      history: [],
      loading: false,
      timeValues: [],
      first_deposit_date: null
    }

    this._reloadContacts = this._reloadContacts.bind(this)
    this._handleRefresh = this._handleRefresh.bind(this)
  }

  componentDidMount(){
    this._reloadContacts()
    const { card } = this.props
      let { first_deposit_date } = this.props.card;
    const { selectedMonth } = this.state

      let dateStart = first_deposit_date ? moment(first_deposit_date, 'YYYY-MM-DD') : moment();
      let dateEnd = moment();
      let timeValues = [];

      while (dateEnd > dateStart) {
        timeValues.push({
          year: dateStart.format('YY'),
          month: dateStart.format('MMM').toUpperCase(),
          moment: dateStart.clone()
        });
        dateStart.add(1,'month');
      }

      this.setState({
          first_deposit_date,
          timeValues,
          selectedMonth: timeValues[timeValues.length - 1]
      },
        () => {
          getStatements(card.iban, this.state.selectedMonth.moment.format('YYYY-MM-DD'))
            .then(response => {
              this.setState({history: response.statements})
            })
            .catch(error => {
              Alert.alert(JSON.stringify(error))
            })
        }
      )
  }

  _retrieveHistory(){
    let { selectedMonth } = this.state
    let { card } = this.props;

    if(!selectedMonth || !selectedMonth.moment){
      return
    }

    // const startOfMonth = selectedMonth.moment.startOf('month').format('YYYY-MM-DD');
    // const endOfMonth   = selectedMonth.moment.endOf('month').format('YYYY-MM-DD');

    this.setState({loading: true})

    getStatements(card.iban, selectedMonth.moment.format('YYYY-MM-DD'))
      .then(response => {
        this.setState({history: response.statements, loading: false})
      })
      .catch(error => {
        Alert.alert(JSON.stringify(error))
        this.setState({loading: false})
      })
  }

  _reloadContacts(){
    let {navigation} = this.props

    getContacts().then(contacts => {
      this.setState({contacts})
    })
      .catch(error => handleRequestError(error, navigation))
  }

  _formatPrice(price){
    price = price/100.0

    if(!price){
      price = 0
    }
    return price.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,').replace(/^(-)?/, '$1 €')
  }

  _handleRefresh() {
    this._retrieveHistory()
  }

  handleChangeMonth = month => event => {
    let { selectedMonth } = this.state

    if(selectedMonth.month == month.month && selectedMonth.year == month.year){
      return
    }

    this.setState({selectedMonth: month}, this._retrieveHistory.bind(this))
  }

  _goBack() {
    const {loading} = this.state

    if(loading){
      return
    }

    let { onSelectTab } = this.props;
    onSelectTab('wallet')
  }

  render() {

    let { navigation: {navigate}, card } = this.props;
    const { selectedMonth, contacts, history, loading, timeValues } = this.state;

    return (
      <KeyboardAvoidingView
        style={styles.container}
        behavior="padding"
        style={ styles.main }
      >
        <View
          style={ styles.header }
        >
          <StatusBar
            barStyle="light-content"
          />

          <View style={styles.headerButtons}>
            <NavBackButton
              title=''
              onPress={() => this._goBack()}
              style={styles.navBackButton}
            />
            <View style={ styles.headerButton }>
              <Text style={ styles.headerButtonText }>{ card.title }</Text>
            </View>
            <View style={ styles.headerButton }>
              <Text style={ styles.headerButtonText }></Text>
              {/*<Text style={ styles.headerButtonText }>Editar</Text>*/}
            </View>
          </View>
          <View style={styles.headerContent}>
            <Text style={styles.headerTitleSmall}>Balance Disponible:</Text>
            <Text style={styles.headerTitleAmount}>{ card.balance }</Text>
          </View>
          <View style={styles.headerSubtitleContent}>
            <View style={styles.headerSubtitle}>
              <Text style={styles.headerSubtitleSmall}>Balance Total:</Text>
              <Text style={styles.headerSubtitleAmount}>{ card.balance }</Text>
            </View>
            <View style={styles.headerSubtitle}>
              <Text style={styles.headerSubtitleSmall}>Balance en Tránsito:</Text>
              <Text style={styles.headerSubtitleAmount}>0 €</Text>
            </View>
          </View>
        </View>

        <View style={styles.monthsContainer}>
          <ScrollView
            horizontal={true}
            contentContainerStyle={styles.monthsContainerScroll}
            style={{height: '100%'}}
            showsHorizontalScrollIndicator={false}
            ref="monthsMenu"
          >
            {timeValues.map((item, index) =>
              <TouchableWithoutFeedback
                key={index}
                onPress={this.handleChangeMonth(item)}
              >
                <View style={[styles.month, selectedMonth.month == item.month && selectedMonth.year == item.year ? styles.monthActive : null]}>
                  <Text style={[styles.monthTitle, selectedMonth.month == item.month && selectedMonth.year == item.year ? styles.monthTitleActive : null]}>
                    {item.month} {moment().format('YY') == item.year ? null : `'${ item.year }`}
                  </Text>
                </View>
              </TouchableWithoutFeedback>
            )}
          </ScrollView>
        </View>

        <ScrollView
          keyboardShouldPersistTaps='always'
          style={styles.scroll}
          refreshControl={
            <RefreshControl
              refreshing={loading}
              onRefresh={this._handleRefresh}
            />
          }
        >
          <Text style={[styles.sectionTitle]}>Enviar dinero</Text>

          <View style={contactsStyles.contactsContainer}>
            <ScrollView
              horizontal={true}
              contentContainerStyle={contactsStyles.contactsContainerScroll}
              style={{height: '100%'}}
            >

              <TouchableWithoutFeedback onPress={() => navigate('SelectContact', {onSelect: this._reloadContacts})}>
                <View style={[contactsStyles.contact, {borderStyle: 'dashed', borderColor: '#DCE6F0', borderWidth: 2}]}>
                  <View style={contactsStyles.avatarContainer}>
                    <Image
                      source={require('../../images/contacts/add-new.png')}
                    />
                  </View>
                  <Text style={contactsStyles.contactName}>Añadir{"\n"}contacto</Text>
                </View>
              </TouchableWithoutFeedback>

              {
                contacts.map((contact, index) =>
                  <TouchableWithoutFeedback
                    key={index}
                    onPress={() => navigate('SendMoney')}
                  >
                    <View style={contactsStyles.contact}>
                      <View style={contactsStyles.avatarContainer}>
                        <Image
                          style={ contactsStyles.avatar }
                          source={{uri: `${baseUrl}${contact.avatar.url}`}}
                        />
                      </View>
                      <Text style={contactsStyles.contactName}>
                        {contact.first_name}{"\n"}{contact.last_name}
                      </Text>
                    </View>
                  </TouchableWithoutFeedback>
                )
              }

            </ScrollView>
          </View>

          <Text style={[styles.sectionTitle]}>Historial</Text>

          <View style={historyStyles.list}>
            {
              history.length == 0 ?
                <Text
                  style={styles.blankText}
                >¡Tu banco no nos ha devuelto datos de transacción! Por ahora, no se podrá consultar esta información!</Text>
                : null
            }

            {
              history.map((item, index) => (
                <View
                  key={index}
                  style={historyStyles.item}
                >
                  <View style={historyStyles.logoContainer}>
                    {
                      Math.random() >= .7 ?
                        <Image
                          style={ historyStyles.logo }
                          source={require('../../images/contacts/logo3.png')}
                        />
                      :
                      Math.random() >= .5 ?
                        <Image
                          style={ historyStyles.logo }
                          source={require('../../images/contacts/logo1.png')}
                        />
                      :
                        <Image
                          style={ historyStyles.logo }
                          source={require('../../images/contacts/logo2.png')}
                        />
                    }

                  </View>
                  <View style={historyStyles.container}>
                    <Text
                      style={historyStyles.title}
                      ellipsizeMode="tail"
                      numberOfLines={1}
                    >
                      { item.concepts.length > 0 ? item.concepts[0].split(';')[0] : '' }</Text>
                    <Text style={historyStyles.date}>{ moment(item.deposit_date).format('DD-MM-YYYY') }</Text>
                  </View>
                  <View style={historyStyles.priceContainer}>
                    <Text
                      style={[
                        historyStyles.price,
                        item.amount > 0 ? historyStyles.priceGreen : null
                      ]}
                    >
                      { this._formatPrice(item.amount) }
                    </Text>
                    <Text style={historyStyles.priceArrow}>></Text>
                  </View>
                </View>
              ))
            }
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}

const historyStyles = StyleSheet.create({
  list: {
    marginTop: relativeWidth(13)
  },
  item: {
    height: relativeWidth(54),
    paddingLeft: relativeWidth(64),
    backgroundColor: 'white',
    marginLeft: relativeWidth(21),
    marginRight: relativeWidth(21),
    borderRadius: relativeWidth(6),
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    marginBottom: relativeWidth(15),
  },
  container: {
    width: '65%'
  },
  priceContainer: {
    flexDirection: 'row'
  },
  title: {
    fontSize: relativeWidth(14),
  },
  date: {
    color: 'gray',
    fontSize: relativeWidth(11.5),
  },
  price: {
    color: '#EE5A55',
    fontSize: relativeWidth(15),
    marginRight: relativeWidth(10)
  },
  priceGreen: {
    color: '#2CC197',
  },
  priceArrow: {
    color: 'gray',
    fontSize: relativeWidth(15),
    marginRight: relativeWidth(10)
  },
  logoContainer: {
    height:  relativeWidth(54),
    width: relativeWidth(54),
    alignItems: 'center',
    justifyContent: 'space-around',
    position: 'absolute',
    top: 0,
    left: 0,
  },
  logo: {

  }
});

const contactsStyles = StyleSheet.create({
  contactsContainer: {
    height: relativeWidth(122)
  },
  contactsContainerScroll: {
    height: '100%',
    flexDirection: 'row',
    alignItems: 'center',
  },
  contact: {
    height: relativeWidth(122),
    width: relativeWidth(100),
    backgroundColor: 'white',
    marginLeft: relativeWidth(11),
    paddingTop: relativeWidth(13),
    borderRadius: relativeWidth(6),
    alignItems: 'center',
  },
  contactName: {
    color: 'gray',
    backgroundColor: 'transparent',
    fontSize: relativeWidth(13),
    marginTop: relativeWidth(5),
    textAlign: 'center'
  },
  avatarContainer: {
    height: relativeWidth(56),
    width: relativeWidth(56),
    alignItems: 'center',
    justifyContent: 'space-around',
    borderRadius: relativeWidth(100),
    overflow: 'hidden',
  },
  avatar: {
    width: '100%',
    height: '100%',
    opacity: .4
  },
});

const styles = StyleSheet.create({
  main: {
    paddingBottom: 30,
    height: '100%',
    backgroundColor: '#F6F9FC',
  },
  header: {
    backgroundColor: '#339966',
    paddingTop: relativeWidth(28),
    paddingLeft: relativeWidth(20),
    paddingRight: relativeWidth(20),
    height: relativeWidth(239),
  },
  headerButtons: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: relativeWidth(50)
  },
  navBackButton: {},
  headerButtonText: {
    color: 'white',
    fontSize: relativeWidth(19),
    marginTop: relativeWidth(8),
  },
  headerContent: {
    marginTop: relativeWidth(3),
  },
  headerTitleSmall: {
    color: 'white',
    fontSize: relativeWidth(14.5)
  },
  headerTitleAmount: {
    color: 'white',
    fontSize: relativeWidth(42),
    lineHeight: relativeWidth(42)
  },
  headerSubtitleContent: {
    flexDirection: 'row',
    marginTop: relativeWidth(19)
  },
  headerSubtitle: {
    width: '50%'
  },
  headerSubtitleSmall: {
    color: 'white',
    fontSize: relativeWidth(14.5)
  },
  headerSubtitleAmount: {
    color: 'white',
    fontSize: relativeWidth(24),
    lineHeight: relativeWidth(24)
  },
  scroll: {

  },
  sectionTitle: {
    fontSize: relativeWidth(23),
    opacity: 0.8,
    marginBottom: relativeWidth(16),
    marginTop: relativeWidth(21),
    marginLeft: relativeWidth(21),
    color: '#1a1a1a',
    backgroundColor: 'transparent'
  },
  monthsContainer: {
    shadowOpacity: 0.26,
    shadowRadius: relativeWidth(5),
    shadowColor: 'black',
    shadowOffset: {height: 0, width: 0},
    height: relativeWidth(70)
  },
  monthsContainerScroll: {
    height: '100%',
    flexDirection: 'row',
    alignItems: 'center',
  },
  month: {
    marginLeft: relativeWidth(17),
    marginRight: relativeWidth(17),

    paddingTop: relativeWidth(8),
    paddingBottom: relativeWidth(8),
    paddingLeft: relativeWidth(20),
    paddingRight: relativeWidth(20),

    borderRadius: relativeWidth(20),
  },
  monthActive: {
    backgroundColor: '#00CC99',

    shadowOpacity: 0.16,
    shadowRadius: relativeWidth(2),
    shadowColor: 'black',
    shadowOffset: {height: 2, width: 0},
  },
  monthTitle: {
    color: 'gray',
    backgroundColor: 'transparent'
  },
  monthTitleActive: {
    color: 'white',
  },
  blankText: {
    color: 'gray',
    fontSize: relativeWidth(11.5),
    textAlign: 'center',
    margin: 20
  },

});