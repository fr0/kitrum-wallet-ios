import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableWithoutFeedback,
  StatusBar,
  ScrollView,
  KeyboardAvoidingView,
  Alert,
  AsyncStorage,
  TouchableOpacity,
  Keyboard
} from 'react-native';

import { relativeWidth } from './../../services/dimensions'
import IbanGreenRoundedButton from '../../common/greenRoundedButton';
import IbanBlueLink from '../../common/blueLink';
import IbanTextInput from '../../common/TextInput';
import IbanCardIOInput from '../../common/CardIOInput';

export default class AddCard extends Component<{}> {

  constructor(props){
    super(props)
    this.state = {
      card: {},
      validationErrors: {}
    }
  }

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Agregar Tarjeta',
      headerTitleStyle: {color: 'white', fontWeight: '400'},
      headerStyle: {backgroundColor: '#339966'},
      headerLeft: (
        <TouchableWithoutFeedback
          onPress={() => navigation.goBack()}
        >
          <View>
            <Text style={styles.navText}>Cancelar</Text>
          </View>
        </TouchableWithoutFeedback>
      ),
      gesturesEnabled: false
    }
  };

  componentDidMount(){

  }

  _handleChanges(key, value){
    let { card, validationErrors } = this.state;
    this.setState({
      card: {
        ...card,
        [key]: value
      },
      validationErrors: {
        ...validationErrors,
        [key]: null
      }
    })
  }

  _handleCardSelect(card){
    this.setState({
      card: {
        ...card,
        post_code: (card.postalCode || '').toString(),
        exp_month: (card.expiryMonth || '').toString(),
        exp_year: (card.expiryYear || '').toString(),
        cvv: (card.cvv || '').toString(),
        number: (card.cardNumber || '').toString(),
        name: (card.cardholderName || '').toString() || (card.cardType || '').toString()
      }
    })
  }

  _submit(){

  }

  render() {

    const { loading, card, validationErrors } = this.state
    let { navigation } = this.props

    return (
      <KeyboardAvoidingView
        style={styles.container}
        behavior="padding"
      >
        <ScrollView
          keyboardShouldPersistTaps='always'
          style={styles.scroll}
        >
          <View style={styles.main}>

            <Text style={styles.header}>Nombre en la Tarjeta</Text>

            <IbanTextInput
              label='Escríbe el Nombre'
              value={card.name}
              onChangeText={ val => this._handleChanges('name', val) }
              error={validationErrors.name || ''}
              style={styles.field}
            />

            <Text style={styles.header}>Número en la Tarjeta</Text>

            <IbanCardIOInput
              navigation={navigation}
              label='Escríbe el Número'
              value={card.number}
              onChangeText={ val => this._handleChanges('number', val) }
              error={validationErrors.number || ''}
              style={styles.field}
              onCardSelect={ card => this._handleCardSelect(card) }
            />

            <Text style={styles.header}>Fecha de Expiración</Text>

            <View style={styles.expirationContainer}>
              <View style={styles.expirationContainerM}>
                <IbanTextInput
                  label='MM'
                  value={card.exp_month}
                  onChangeText={ val => this._handleChanges('exp_month', val) }
                  error={validationErrors.exp_month || ''}
                  style={styles.field}
                />
              </View>

              <View style={styles.expirationContainerY}>
                <IbanTextInput
                  label='YY'
                  value={card.exp_year}
                  onChangeText={ val => this._handleChanges('exp_year', val) }
                  error={validationErrors.exp_year || ''}
                  style={styles.field}
                />
              </View>

              <View style={styles.expirationContainerCVV}>
                <IbanTextInput
                  label='CVV'
                  value={card.cvv}
                  onChangeText={ val => this._handleChanges('cvv', val) }
                  error={validationErrors.cvv || ''}
                  style={styles.field}
                />
              </View>
            </View>

            <Text style={styles.header}>País</Text>

            <IbanTextInput
              label='Selecciona tu País'
              value={card.country}
              onChangeText={ val => this._handleChanges('country', val) }
              error={validationErrors.country || ''}
              style={styles.field}
            />

            <Text style={styles.header}>Código Postal</Text>

            <IbanTextInput
              label='Código Postal'
              value={card.post_code}
              onChangeText={ val => this._handleChanges('post_code', val) }
              error={validationErrors.post_code || ''}
              style={styles.field}
            />

            <IbanGreenRoundedButton
              onPress={ () => this._submit() }
              value='AGREGAR'
              loading={loading}
              style={styles.button}
            ></IbanGreenRoundedButton>

            <Text
              style={styles.grayText}
            >
              Al agregar, estás aceptando nuestros
            </Text>

            <IbanBlueLink
              underline={true}
              uppercase={true}
              value='TÉRMINOS DE SERVICIO Y POLÍTICA DE PRIVACIDAD'
              style={styles.blueText}
              onPress={() => navigation.navigate('Web', {uri: "https://myibanwallet.com/terminos"})}
            ></IbanBlueLink>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  main: {
    minHeight: '100%',
    paddingBottom: 30
  },
  scroll: {
    paddingLeft: relativeWidth(10),
    paddingRight: relativeWidth(10),
    backgroundColor: 'white',
  },
  navText: {
    color: 'white',
    fontSize: relativeWidth(18),
    paddingLeft: relativeWidth(18)
  },
  grayText: {
    fontSize: relativeWidth(12),
    color: '#95989A',
    marginTop: relativeWidth(22),
    textAlign: 'center'
  },
  blueText: {
    marginTop: relativeWidth(8)
  },
  header: {
    fontSize: relativeWidth(18),
    marginTop: relativeWidth(35),
    fontWeight: '400'
  },
  field: {
    marginTop: relativeWidth(-5),
    marginBottom: relativeWidth(-2)
  },
  expirationContainer: {
    flexDirection: 'row'
  },
  expirationContainerM: {
    width: '25%',
    paddingRight: relativeWidth(10),
  },
  expirationContainerY: {
    width: '25%',
    paddingRight: relativeWidth(10),
    paddingLeft: relativeWidth(10),
  },
  expirationContainerCVV: {
    width: '50%',
    paddingLeft: relativeWidth(10),
  }
});