import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableWithoutFeedback,
  StatusBar,
  ScrollView,
  KeyboardAvoidingView,
  Alert,
  AsyncStorage,
  TouchableOpacity,
  Keyboard,
  RefreshControl,
  ActionSheetIOS
} from 'react-native';

import { relativeWidth } from './../../services/dimensions'
import { handleRequestError } from './../../services/variables'
import { list as getProductAccounts } from './../../services/productAccounts'
import { profile } from './../../services/users'
import IbanBankIcon from '../../common/BankIcon';

export default class Wallet extends Component<{}> {

  constructor(props){
    super(props)

    this.state = {
      productAccounts: [],
      fitnanceAccounts: [],
      refreshing: false
    }

    this._showActionSheet = this._showActionSheet.bind(this)
  }

  static navigationOptions = { header: null, gesturesEnabled: false };

  componentDidMount(){
    let {navigation} = this.props
    this._retrieveProductAccounts();
    this._retrieveFitnanceAccounts();
  }

  _retrieveFitnanceAccounts(){
    let {onFirstCard, navigation} = this.props

    profile(navigation)
      .then(user => {
        this.setState({fitnanceAccounts: user.fitnance_accounts || []})


        if(user.fitnance_accounts.length > 0){
          card = user.fitnance_accounts[0]

          onFirstCard({
            title: card.name,
            balance: `${ this._formatPrice(card.balance) } ${ this._formatCurrency(card.currency || 'EUR') }`,
            iban: card.id,
            first_deposit_date: card.first_deposit_date
          })
        }
      })
      .catch(error => {

      })
  }

  _retrieveProductAccounts(){
    let {navigation} = this.props
    this.setState({refreshing: true});

    getProductAccounts()
      .then(productAccounts => {
        this.setState({productAccounts})
        this.setState({refreshing: false});
      })
      .catch(error => {
        this.setState({refreshing: false});
        handleRequestError(error, navigation)
      })
  }

  _formatPrice(price){
    price = price/100.0
    
    if(!price){
      price = 0
    }
    return price.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')
  }

  _formatCurrency(currency){
    switch(currency){
      case 'USD':
        return '$'
      case 'EUR':
        return '€'
      case 'GBP':
        return '£'
    }
  }

  _formatId(num){
    var s = num + "";
    while (s.length < 6) s = "0" + s;
    return s;
  }

  _onRefresh() {
    this._retrieveProductAccounts();
    this._retrieveFitnanceAccounts()
  }

  _showActionSheet(){

    const { navigation } = this.props;

    ActionSheetIOS.showActionSheetWithOptions({
        options: ['Cuenta iBAN', 'Tarjeta', 'Cuenta Bancaria', 'Cancel'],
        cancelButtonIndex: 3,
      },
      (buttonIndex) => {
        if (buttonIndex === 1) { navigation.push('AddCard') }
        else if (buttonIndex === 0) { navigation.push('Products') }
        else if (buttonIndex === 2) { navigation.push('BankAccount') }
      });
  }

  render() {

    const {productAccounts, fitnanceAccounts} = this.state

    const data = [
      {
        title: 'Cuentas iBAN',
        cards: productAccounts.map(account => ({
          title: 'iBAN Cuenta',
          number: this._formatId(account.id),
          balance: `${ this._formatPrice(account.balance) } ${ this._formatCurrency(account.currency) }`,
          image: <View style={[styles.cardIconContainer, {borderColor: '#148042'}]} ><Image source={require('../../images/icons/dashboard/cards/iban-one.png')} /></View>
        }))
      },
      // {
      //   title: 'Tarjetas de Crédito',
      //   cards: [
      //     {
      //       title: 'Visa',
      //       number: '**** 0817',
      //       balance: '20.000,00 €',
      //       image: <View style={[styles.cardIconContainer, {borderColor: '#2566AF'}]} ><Image source={require('../../images/icons/dashboard/cards/visa.png')} /></View>
      //     },
      //     {
      //       title: 'Mastercard',
      //       number: '**** 2356',
      //       balance: '10.000,00 €',
      //       image: <View style={[styles.cardIconContainer, {borderColor: '#E9B040'}]} ><Image source={require('../../images/icons/dashboard/cards/mastercard.png')} /></View>
      //     }
      //   ]
      // },
      {
        title: 'Cuentas Bancarias',
        cards: fitnanceAccounts.map(account => ({
          title: account.name,
          first_deposit_date: account.first_deposit_date,
          number: account.id,
          balance: `${ this._formatPrice(account.balance) } ${ this._formatCurrency(account.currency || 'EUR') }`,
          image: <View style={[styles.cardIconContainer, {borderColor: '#148042'}]} >
            <IbanBankIcon
              bankId={account.bank_id}
              style={styles.accountIcon}
            />
          </View>
        }))
      }
    ]

    const { onSelectTab, navigation } = this.props

    return (
      <KeyboardAvoidingView
        behavior="padding"
      >
        <View
          style={ styles.main }
        >
          <View
            style={ styles.header }
          >
            <StatusBar
              barStyle="light-content"
            />
            <View style={styles.headerButtons}>
              <TouchableWithoutFeedback
                onPress={() => navigation.push('Profile')}
              >
                <Image
                  style={ styles.headerButton }
                  source={require('../../images/icons/dashboard/user.png')}
                />
              </TouchableWithoutFeedback>
              <TouchableWithoutFeedback onPress={this._showActionSheet}>
                <Image
                  style={ styles.headerButton }
                  source={require('../../images/icons/dashboard/plus.png')}
                />
              </TouchableWithoutFeedback>
            </View>
            <Text style={styles.headerTitle}>Wallet</Text>
          </View>

          <ScrollView
            keyboardShouldPersistTaps='always'
            style={styles.scroll}
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this._onRefresh.bind(this)}
              />
            }
          >
            <View style={styles.form}>
              {
                data.map((i, index) =>
                  <View key={index}>
                    <Text style={[styles.sectionTitle, index == 0 ? {marginTop: relativeWidth(10)} : null]}>{i.title}</Text>
                    <View style={styles.cardContainer}>
                      {
                        i.cards.map((card, card_index) =>
                          <TouchableWithoutFeedback
                            onPress={() => onSelectTab('account_details', {card: {
                              title: card.title,
                              balance: card.balance,
                              iban: card.number,
                              first_deposit_date: card.first_deposit_date
                            }})}
                            key={`${index} ${card_index}`}
                          >
                            <View
                              style={[styles.card, card_index == i.cards.length - 1 ? {borderBottomWidth: 0} : null]}
                            >
                              <Text style={styles.cardTitle}>{card.title}</Text>
                              <Text style={styles.cardNumber}>{card.number}</Text>
                              <Text style={styles.balanceTitle}>Balance total:</Text>
                              <Text style={styles.balance}>{card.balance}</Text>
                              {card.image}
                            </View>
                          </TouchableWithoutFeedback>
                        )
                      }
                    </View>
                  </View>
                )
              }
            </View>

          </ScrollView>
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  main: {
    height: '100%'
  },
  header: {
    backgroundColor: '#339966',
    paddingTop: relativeWidth(32),
    paddingLeft: relativeWidth(20),
    paddingRight: relativeWidth(20),
    height: relativeWidth(129),
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%'
  },
  headerButtons: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  form: {
    flex: 1,
    paddingLeft: relativeWidth(17),
    paddingRight: relativeWidth(17),
    backgroundColor: 'white',
    minHeight: '100%',
    paddingBottom: relativeWidth(70),
  },
  headerTitle: {
    color: 'white',
    fontSize: relativeWidth(37),
    fontWeight: 'bold',
    marginTop: relativeWidth(13),
  },
  scroll: {
    backgroundColor: 'white',
    marginTop: relativeWidth(129),
    height: '100%'
  },
  sectionTitle: {
    fontSize: relativeWidth(24),
    opacity: 0.8,
    marginBottom: relativeWidth(26),
    marginTop: relativeWidth(33),
  },
  cardContainer: {
    borderRadius: relativeWidth(5),
    shadowOpacity: 0.16,
    shadowRadius: relativeWidth(8),
    shadowColor: 'black',
    shadowOffset: { height: 0, width: 0 },
  },
  card: {
    borderColor: '#95989A',
    borderBottomWidth: relativeWidth(0.5),
    paddingLeft: relativeWidth(97.5),
    height: relativeWidth(89),
  },
  cardIconContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    height: relativeWidth(89 - 2 * 19),
    width: relativeWidth(97.5),
    justifyContent: 'center',
    alignItems: 'center',
    borderLeftWidth: 3,
    marginTop: relativeWidth(19),
  },
  cardIcon: {

  },
  cardTitle: {
    marginTop: relativeWidth(26),
    fontSize: relativeWidth(16),
  },
  cardNumber: {
    marginTop: relativeWidth(5),
    fontSize: relativeWidth(12),
    color: "#8E8E93"
  },
  balanceTitle: {
    position: 'absolute',
    top: relativeWidth(21),
    right: relativeWidth(15),
    marginTop: relativeWidth(5),
    fontSize: relativeWidth(12),
    color: "#8E8E93",
  },
  balance: {
    position: 'absolute',
    top: relativeWidth(41),
    right: relativeWidth(15),
    marginTop: relativeWidth(6),
    fontSize: relativeWidth(14),
    color: "#339966",
  },
  accountIcon: {
    width: relativeWidth(54),
    height: relativeWidth(54),
    backgroundColor: 'rgba(0, 0, 0, .08)',
    borderRadius: relativeWidth(27),
  }
});