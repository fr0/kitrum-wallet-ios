import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableWithoutFeedback,
  StatusBar,
  ScrollView,
  Alert,
  Keyboard
} from 'react-native';
import NavBackButton from '../../common/NavBackButton';

import { relativeWidth } from './../../services/dimensions'

import Contacts from 'react-native-contacts'
import { handleRequestError } from './../../services/variables'
import { upsert } from './../../services/contacts'
import { list as getContacts } from './../../services/contacts'

export default class SelectContact extends Component<{}> {

  constructor(props){
    super(props)

    this.state = {
      contacts: [],
      savedContacts: []
    }
  }

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Agregar contacto',
      headerTitleStyle: {color: 'white', fontWeight: '400'},
      headerStyle: {backgroundColor: '#339966'},
      headerLeft: (
        <NavBackButton
          title=''
          onPress={() => {
            navigation.goBack();
          }}
        />
      ),
      headerRight: (
        <Image
          style={ styles.searchIcon }
          source={require('./../../images/icons/search-white.png')}
        />
      ),
      gesturesEnabled: false
    }
  };

  componentDidMount(){
    let {navigation} = this.props

    Contacts.checkPermission( (err, permission) => {
      if(permission === 'undefined'){
        this._getSystemContacts()
      }
      if(permission === 'authorized'){
        this._getSystemContacts()
      }
      if(permission === 'denied'){
        Alert.alert('To use this feature please go to your settings and enable permissions to contacts for iBAN Wallet')
        navigation.goBack()
      }
    })

    getContacts().then(savedContacts => {
      this.setState({savedContacts})
    })
      .catch(error => handleRequestError(error, navigation))
  }

  _getSystemContacts() {
    let {navigation} = this.props

    Contacts.getAll( (err, contacts) => {
      if(err && err.type === 'permissionDenied'){
        navigation.goBack()
      }else{
        if(contacts){
          this.setState({contacts})
        }
      }
    })
  }

  _saveContactPrompt(contact){
    Alert.alert(
      `Are you sure you want to add ${ contact.familyName} ${ contact.givenName } to iBAN contacts ?`,
      '',
      [
        {text: 'Cancel', style: 'cancel'},
        {text: 'OK', onPress: () => this._saveContact(contact)},
      ],
      { cancelable: false }
    )
  }

  _saveContact(contact){
    let {navigation} = this.props
    let onSelect = navigation.state && navigation.state.params ?  navigation.state.params.onSelect : null

    upsert(contact).then(() => {
      navigation.goBack()
      if(onSelect){
        onSelect()
      }
    })
      .catch(error => handleRequestError(error, navigation))
  }

  render() {

    const { contacts, savedContacts } = this.state
    let { navigation: {navigate} } = this.props

    return (
      <ScrollView
        keyboardShouldPersistTaps='always'
        style={styles.scroll}
      >
        <View style={styles.main}>
          {contacts.map((contact, index) => {
            let isSaved = savedContacts.map(c => c.phone_number).includes(contact.phoneNumbers[0].number)

            return <TouchableWithoutFeedback
              key={index}
              onPress={() => isSaved ? null : this._saveContactPrompt(contact)}
            >
              <View
                style={styles.contactContainer}
              >
                <Text style={styles.name}>{contact.familyName} {contact.givenName}</Text>
                <Text style={styles.number}>{contact.phoneNumbers[0].number}</Text>
                <View style={styles.avatarContainer}>
                  {contact.hasThumbnail ?
                    <Image
                      style={styles.avatar}
                      source={{uri: contact.thumbnailPath}}
                    />
                    :
                    <Image
                      style={styles.missingAvatar}
                      source={require('../../images/contacts/no-avatar.png')}
                    />
                  }
                </View>
                {isSaved ?
                  <Image
                    style={styles.ibanLogo}
                    source={require('../../images/logo-small.png')}
                  />
                : null}
              </View>
            </TouchableWithoutFeedback>
          })}
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  main: {
    minHeight: '100%',
    paddingBottom: 30
  },
  scroll: {
    paddingLeft: relativeWidth(15),
    paddingRight: relativeWidth(15),
    backgroundColor: 'white',
  },
  searchIcon: {
    width: relativeWidth(20),
    height: relativeWidth(20),
    marginRight: relativeWidth(20),
  },
  avatarContainer: {
    backgroundColor: '#EBEFF2',
    borderRadius: relativeWidth(100),
    width: relativeWidth(47.5),
    height: relativeWidth(47.5),
    alignItems: 'center',
    justifyContent: 'space-around',

    position: 'absolute',
    left: 0,
    top: relativeWidth(11),
    overflow: 'hidden'
  },
  avatar: {
    width: '100%',
    height: '100%',
  },
  missingAvatar: {
    width: '35%',
    height: '35%',
  },
  ibanLogo: {
    position: 'absolute',
    right: 0,
    top: relativeWidth(21)
  },
  contactContainer: {
    height: relativeWidth(74),
    paddingLeft: relativeWidth(60),
  },
  name: {
    fontSize: relativeWidth(18),
    marginTop: relativeWidth(13),
  },
  number: {
    fontSize: relativeWidth(12),
    marginTop: relativeWidth(5),
    color: 'gray'
  }
});