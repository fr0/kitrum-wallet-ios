import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableWithoutFeedback,
  StatusBar,
  ScrollView,
  KeyboardAvoidingView,
  Alert,
  AsyncStorage,
  TouchableOpacity,
  Keyboard
} from 'react-native';

import Dimensions from 'Dimensions';

import { relativeWidth } from './../../services/dimensions'
import { baseUrl, handleRequestError } from './../../services/variables'
import { all as getProductCategories } from './../../services/productCategories'
import IbanBlueLink from '../../common/blueLink';

export default class Products extends Component<{}> {

  constructor(props){
    super(props)

    this.state = {
      cards: [],
      validationErrors: {}
    }
  }

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Productos iBAN',
      headerTitleStyle: {color: 'white', fontWeight: '400'},
      headerStyle: {backgroundColor: '#339966'},
      headerLeft: (
        <TouchableWithoutFeedback
          onPress={() => navigation.goBack()}
        >
          <View>
            <Text style={styles.navText}>Cancelar</Text>
          </View>
        </TouchableWithoutFeedback>
      ),
      gesturesEnabled: false
    }
  };

  componentDidMount(){
    let {navigation} = this.props

    getProductCategories().then(cards => {
      this.setState({
        cards: cards.map(card => {
          return {
            ...card,
            imageComponent: <Image style={ styles.cardBackground } source={{uri: `${baseUrl}${card.image}`}}/>
          }
        })
      })
    })
      .catch(error => handleRequestError(error, navigation))
  }

  render() {

    const { loading, validationErrors, cards } = this.state
    let { navigation: {navigate} } = this.props

    return (
      <KeyboardAvoidingView
        style={styles.container}
        behavior="padding"
      >
        <ScrollView
          keyboardShouldPersistTaps='always'
          style={styles.scroll}
        >
          <View style={styles.main}>

            <Text style={styles.header}>Selecciona Tu Producto iBAN</Text>

            {cards.map((card, index) =>
              <View
                key={index}
                style={styles.card}
              >
                { card.imageComponent }
                <View style={styles.cardBackgroundOverlay}/>
                <View style={styles.cardLeft}>
                  <Text style={styles.cardTitle}>{card.title}</Text>
                  <Text style={styles.cardDescription}>{card.description}</Text>
                </View>
                <View style={styles.cardRight}>
                  <TouchableWithoutFeedback
                    onPress={() => navigate('AddIBANAccount', {productCategory: card}) }
                  >
                    <View>
                      <Text style={styles.cardCreate}>Crear</Text>
                    </View>
                  </TouchableWithoutFeedback>
                </View>
              </View>
            )}

            <Text style={styles.grayText} >
              ¿Necesitas ayuda para elegir tu producto iBAN?
            </Text>

            <IbanBlueLink
              underline={true}
              uppercase={true}
              value='VISITA NUESTRA PÁGINA DE PRODUCTOS'
              style={styles.blueText}
            ></IbanBlueLink>

          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  main: {
    minHeight: '100%',
    paddingBottom: 30
  },
  scroll: {
    paddingLeft: relativeWidth(15),
    paddingRight: relativeWidth(15),
    backgroundColor: 'white',
  },
  navText: {
    color: 'white',
    fontSize: relativeWidth(18),
    paddingLeft: relativeWidth(18)
  },
  grayText: {
    fontSize: relativeWidth(12),
    color: '#95989A',
    marginTop: relativeWidth(22),
    textAlign: 'center'
  },
  blueText: {
    marginTop: relativeWidth(8)
  },
  header: {
    fontSize: relativeWidth(22),
    marginTop: relativeWidth(26),
    marginBottom: relativeWidth(27),
    fontWeight: '400'
  },
  card: {
    borderRadius: relativeWidth(5),
    shadowOpacity: 0.16,
    shadowRadius: relativeWidth(18),
    shadowColor: 'black',
    shadowOffset: { height: 0, width: 0 },
    height: relativeWidth(96.5),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: relativeWidth(27),
    overflow: 'hidden',
  },
  cardBackground: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    top: 0,
    left: 0
  },
  cardBackgroundOverlay: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    top: 0,
    left: 0,
    backgroundColor: 'rgba(0, 0, 0, .65)'
  },
  cardTitle: {
    backgroundColor: 'transparent',
    fontSize: relativeWidth(20),
    color: 'white',
    paddingLeft: relativeWidth(15),
  },
  cardDescription: {
    backgroundColor: 'transparent',
    fontSize: relativeWidth(12),
    color: 'white',
    paddingLeft: relativeWidth(15),
  },
  cardCreate: {
    backgroundColor: 'transparent',
    fontSize: relativeWidth(16),
    color: 'white',
    paddingRight: relativeWidth(15),
    paddingTop: relativeWidth(20),
    paddingBottom: relativeWidth(20),
    paddingLeft: relativeWidth(20),
  }
});