import React, { Component } from 'react';
import {
  Image,
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
  Alert
} from 'react-native';

import IbanStepper from '../common/stepper';
import IbanDropdownSelect from '../common/dropdownSelect';
import NavBackButton from '../common/NavBackButton';
import SplashScreen from 'react-native-splash-screen';

import {init} from '../services/unnax/ReaderLockstep'

import { relativeWidth } from './../services/dimensions'
import {setFitnance} from "../services/users";

export default class BankAccount extends Component {

  constructor(props){
    super(props);

    this.state = {
      open: false,
      banks: [],
      sid: '',
      outerClick: 0,
      request_code: `IBAN${ Math.floor(Date.now() / 1000) }`
    }
  }

  componentDidMount(){

    const { navigate, goBack } = this.props.navigation;
    const {request_code} = this.state

    setFitnance(request_code, this.props.navigation)
      .then(response => {

        init({request_code})
          .then(response => {

            this.setState({
              sid: response['sid'],
              banks: response['banks']
            })

            // let bank = response['data']['banks'].filter(i => i.name.toLowerCase().includes('null'))[0]
            // console.log(bank)
            // navigate('BankAccountLogin', {bank})
          })
          .catch(error => {
            this.setState({loading: false})
            Alert.alert(error)
          })
      })
      .catch(error => {

      })




  }

  render() {

    let { banks, sid, outerClick, request_code } = this.state;
    const { navigate, goBack } = this.props.navigation;

    return (
      <KeyboardAvoidingView
        behavior="padding"
      >
        <TouchableWithoutFeedback
          onPress={ () => this.setState({outerClick: outerClick + 1}) }
        >
          <View
            style={styles.main}
          >
            <IbanStepper
              style={styles.stepper }
              circles={3}
              current={1}
            />

            <Text style={styles.header}>Seleccionar su banco</Text>

            <Text
              style={styles.text}
            >
              Al selecccionar un banco, usted accepta y confirma que ha leido
              los <Text onTouchStart={() => navigate('FitnanceTerms') } style={styles.link_tos}>terminos y condiciones</Text>, que los entiende y que acepta que
              quedara vinculadopor ellos
            </Text>

            <IbanDropdownSelect
              outerClick={outerClick}
              style={styles.dropdown}
              list={banks}
              onSelect={(bank) => navigate('BankAccountLogin', {bank: bank, sid: sid, request_code})}
            />
          </View>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    );
  }
}

BankAccount.navigationOptions = ({ navigation }) => {
  return {
    title: 'Cuenta Bancaria',
    headerTitleStyle: {color: 'white', fontWeight: '400'},
    headerStyle: {backgroundColor: '#4CBA8D'},
    gesturesEnabled: false
  }
};

const styles = StyleSheet.create({
  main: {
    zIndex: 1,
    alignItems: 'center',
    backgroundColor: 'white',
    minHeight: '100%',
    paddingRight: relativeWidth(17),
    paddingLeft: relativeWidth(17)
  },
  header: {
    fontSize: relativeWidth(22),
    marginTop: relativeWidth(52),
    width: '100%'
  },
  stepper: {

  },
  dropdown: {
    position: 'absolute',
    top: relativeWidth(186),
    width: '100%'
  },
  text: {
    fontSize: relativeWidth(12.8),
    marginTop: relativeWidth(125),
    lineHeight: relativeWidth(18),
    padding: 0,
    width: '100%'
  },
  link_tos: {
    color: '#4CBA8D'
  },
});
