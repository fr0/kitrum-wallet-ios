import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableWithoutFeedback,
  StatusBar,
  AsyncStorage,
  Animated,
  Easing,
  Alert
} from 'react-native';

import TouchID from 'react-native-touch-id';

import { relativeWidth } from './../services/dimensions'
import { check } from './../services/pin'

export default class LockScreen extends Component<{}> {

  constructor(props) {
    super(props);

    this.state = {
      password: '',
      maskMarginLeft: new Animated.Value(0)
    }

    this._handleTouchID = this._handleTouchID.bind(this)
  }

  static navigationOptions = { header: null, gesturesEnabled: false };

  _handleNumber(number){
    let {password} = this.state
    let {onSuccess} = this.props

    password = `${ password }${ number }`

    this.setState({password})

    if(password.length >= 4){
      check(password)
        .then(() => onSuccess())
        .catch(() => this._resetPassword())
    }
  }

  _resetPassword(){
    Animated.timing(
      this.state.maskMarginLeft,
      {
        toValue: -80,
        duration: 100,
      }
    ).start();

    setTimeout(() => {
      this.setState({password: ''})

      Animated.timing(
        this.state.maskMarginLeft,
        {
          toValue: 0,
          duration: 400,
          easing: Easing.elastic(5)
        }
      ).start();
    }, 100)
  }

  _handleTouchID(){
    let {onSuccess} = this.props

    TouchID.authenticate('to unlock screen')
      .then(success => {
        onSuccess()
      })
      .catch(error => {

      });
  }

  render() {

    let { password } = this.state

    return (
      <View
        style={styles.main}
      >
        <View style={styles.logo}>
          <Image
            style={ styles.logoText }
            source={require('../images/img_iban_green.png')}
          />
          <Image
            style={ styles.logoImage }
            source={require('../images/logo-small.png')}
          />
        </View>

        <Animated.View style={[styles.mask, {marginLeft: this.state.maskMarginLeft}]}>
          { [...Array(4).keys()].map(i =>
            <View
              key={i}
              style={[styles.maskDot, password.length > i ? styles.maskDotActive : null]}
            />
          )}
        </Animated.View>

        <View style={styles.keyboard}>
          { [...Array(9).keys()].map(i => i + 1).map(i => {
            return (
              <TouchableWithoutFeedback
                key={i}
                onPress={() => this._handleNumber(i) }
              >
                <View style={styles.numberContainer}>
                  <Text style={styles.number}>
                    {i}
                  </Text>
                </View>
              </TouchableWithoutFeedback>
            )
          })}

          <TouchableWithoutFeedback>
            <View style={styles.numberContainer}>
              <Text style={[styles.number, styles.forgot]}>
                Forgot?
              </Text>
            </View>
          </TouchableWithoutFeedback>

          <TouchableWithoutFeedback
            onPress={() => this._handleNumber(0)}
          >
            <View style={styles.numberContainer}>
              <Text style={styles.number}>
                0
              </Text>
            </View>
          </TouchableWithoutFeedback>

          <TouchableWithoutFeedback
            onPress={this._handleTouchID}
          >
            <View style={styles.numberContainer}>
              <Image
                style={ styles.fingerprint }
                source={require('../images/ic_fingerprint_green.png')}
              />
            </View>
          </TouchableWithoutFeedback>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  main: {
    flexDirection: 'column',
    minHeight: '100%',
    backgroundColor: 'white'
  },
  keyboard: {
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  numberContainer: {
    width: '33.33%',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: relativeWidth(60),
  },
  number: {
    fontSize: relativeWidth(34),
    color: "#4CBA8D"
  },
  forgot: {
    fontSize: relativeWidth(14)
  },
  fingerprint: {
    width: relativeWidth(27),
    height: relativeWidth(27),
  },

  logo: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: relativeWidth(59),
    marginBottom: relativeWidth(67),
  },
  logoText: {
    width: relativeWidth(93),
    height: relativeWidth(15),
    marginRight: relativeWidth(6)
  },
  logoImage: {
    width: relativeWidth(35),
    height: relativeWidth(35),
  },

  mask: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: relativeWidth(158),
  },
  maskDot: {
    backgroundColor: 'lightgray',
    width: relativeWidth(12),
    height: relativeWidth(12),
    marginLeft: relativeWidth(15.5),
    marginRight: relativeWidth(15.5),

    borderRadius: relativeWidth(6)
  },
  maskDotActive: {
    backgroundColor: '#4CBA8D'
  }
});
