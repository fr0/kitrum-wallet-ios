import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableWithoutFeedback,
  StatusBar,
  ScrollView,
  KeyboardAvoidingView,
  Alert,
  AsyncStorage,
  TouchableOpacity,
  Keyboard,
  RefreshControl
} from 'react-native';

import { relativeWidth as rw} from './../../../services/dimensions'
import Progress from '../../../common/Progress'
import moment from 'moment';

export default class Investments extends Component {

  constructor(props){
    super(props)

    this.state = {
      investments: [],
      loading: false
    }

    this._handleRefresh = this._handleRefresh.bind(this)
  }

  componentDidMount() {
    this._retrieveInvestments()
  }

  _retrieveInvestments() {
    this.setState({loading: true})

    setTimeout(() => {
      this.setState({
        loading: false,
        investments: [
          {
            active: true,
            pending: false, 
            title: 'Macbook pro 2016',
            filledColor: '#33CC99',
            sliderColor: 'rgba(1,1,1,.14)',
            value: 15,
            delta: 126.12,
            interest: 6.2
          },
          {
            active: false,
            pending: true,
            title: 'Interfaz de audio apollo',
            filledColor: '#33CC99',
            sliderColor: '#EABA36',
            value: 46,
            delta: 126.12,
            interest: 6.2
          },
          {
            active: false,
            pending: false,
            title: 'Mesa de estudio',
            filledColor: 'rgba(21,146,230,.78)',
            sliderColor: 'transparent',
            value: 100,
            delta: 61.10
          },
          {
            active: false,
            pending: false,
            title: 'Mazda 3, 2013',
            filledColor: 'rgba(230,21,21,.64)',
            sliderColor: 'transparent',
            value: 100,
            delta: -11.01
          },
        ]
      })
    }, 500)
  }

  _formatPrice(price){
    price = price/100.0

    if(!price){
      price = 0
    }
    return price.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,').replace(/^(-)?/, '$1 €')
  }

  _handleRefresh() {
    this._retrieveInvestments()
  }

  render() {
    const { investments, loading } = this.state;

    return (
      <ScrollView
        keyboardShouldPersistTaps='always'
        style={s.scroll}
        refreshControl={
          <RefreshControl
            refreshing={loading}
            onRefresh={this._handleRefresh}
          />
        }
      >
        {
          investments.length == 0 ?
            <Text style={s.blankText}>
              ¡Tu banco no nos ha devuelto datos de transacción! Por ahora, no se podrá consultar esta información!
            </Text>
          : null
        }

        {
          investments.map((item, index) => (
            <View key={index} style={s.listItem}>
              { item.active ? 
                <View style={[s.label, s.activeLabel]}>
                  <Text style={[s.labelText, s.activeLabelText]}>Activo</Text>
                </View>
              : null }

              { item.pending ? 
                <View style={[s.label, s.pendingLabel]}>
                  <Text style={[s.labelText, s.pendingLabelText]}>Atraso</Text>
                </View>
              : null }

              <Text style={s.title}>{ item.title }</Text>
              <Progress 
                style={s.progress}
                filledColor={item.filledColor}
                sliderColor={item.sliderColor}
                value={item.value}
              />

              {item.value < 100 ? 
                <View style={s.table}>
                  <View style={[s.cell, s.firstCell]}>
                    <Text style={s.cellTitle}>{item.value} %</Text>
                    <Text style={s.cellDescription}>COMPLETADO</Text>
                  </View>
                  <View style={[s.cell, s.secondCell]}>
                    <Text style={s.cellTitle}>+{item.delta}</Text>
                    <Text style={s.cellDescription}>GANANCIA ESPERADA</Text>
                  </View>
                  <View style={[s.cell, s.thirdCell]}>
                    <Text style={s.cellTitle}>{item.interest}</Text>
                    <Text style={s.cellDescription}>INTERÉS</Text>
                  </View>
                </View>
              : 
                <View style={s.cellInfo}>
                  <Text 
                    style={[
                      s.cellTitle,
                      {color: item.delta < 0 ? 'red' : '#1592E6'}
                    ]}
                  >
                    {item.delta < 0 ? '-' : '+'}{Math.abs(item.delta)}€
                  </Text>
                  <Text style={s.cellDescription}>GANANCIA FINAL</Text>
                </View>
              }
            </View>
          ))
        }
        <View style={{height: 30}}/>
      </ScrollView>
    );
  }
}

const s = StyleSheet.create({
  scroll: {
    padding: rw(16),
    paddingBottom: 100,
    backgroundColor: '#F0F0F0',
    
    height: '100%'
  },
  blankText: {
    color: 'gray',
    fontSize: rw(11.5),
    textAlign: 'center',
    margin: 20
  },
  listItem: {
    backgroundColor: 'white',
    marginBottom: rw(16),
    borderRadius: 2,

    shadowOpacity: 0.16,
    shadowRadius: rw(2),
    shadowColor: 'black',
    shadowOffset: {height: 2, width: 0},
    paddingTop: rw(13),
    paddingLeft: rw(13),
    paddingRight: rw(13),
  },
  title: {
    color: 'rgba(1,1,1,.65)',
    fontSize: rw(26),
    backgroundColor: 'transparent'
  },
  label: {
    position: 'absolute',
    top: 0,
    right: rw(8),
    padding: rw(6),
    borderBottomLeftRadius: rw(3),
    borderBottomRightRadius: rw(3),
  },
  activeLabel: {
    backgroundColor: 'rgba(57, 183, 86, .25)'
  },
  pendingLabel: {
    backgroundColor: '#EABA36'
  },
  labelText: {
    fontSize: rw(13),
  },
  activeLabelText: {
    color: '#39B756',
  },
  pendingLabelText: {
    color: 'white',
  },
  progress: {
    marginTop: rw(17),
    marginBottom: rw(16.7),
  },

  table: {
    marginBottom: rw(8),
    flexDirection: 'row'
  },
  cell: {
    borderLeftWidth: 1,
    borderColor: 'lightgray',
    paddingLeft: rw(21.5)
  },
  firstCell: {
    borderLeftWidth: 0,
    paddingLeft: 0,
    width: '30%'
  },
  secondCell: {
    width: '40%'
  },
  thirdCell: {
    width: '30%'
  },
  cellTitle: {
    color: '#2CC096',
    fontSize: rw(22)
  },
  cellDescription: {
    fontSize: rw(9),
    color: 'rgba(1,1,1,.47)'
  },
  cellInfo: {
    position: 'absolute',
    top: rw(10),
    right: rw(10)
  }
})