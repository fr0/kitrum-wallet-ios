import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableWithoutFeedback,
  StatusBar,
  ScrollView,
  KeyboardAvoidingView,
  Alert,
  AsyncStorage,
  TouchableOpacity,
  Keyboard,
  RefreshControl
} from 'react-native';

import { relativeWidth as rw} from './../../../services/dimensions'
import Progress from '../../../common/Progress'
import Button from '../../../common/greenRoundedButton'
import moment from 'moment';

export default class Opportunities extends Component {

  constructor(props){
    super(props)

    this.state = {
      investments: [],
      loading: false
    }

    this._handleRefresh = this._handleRefresh.bind(this)
  }

  componentDidMount() {
    this._retrieveInvestments()
  }

  submit() {
    const {navigation} = this.props
    navigation.push('PortfolioAddBalance')
  }

  _retrieveInvestments() {
    this.setState({loading: true})

    setTimeout(() => {
      this.setState({
        loading: false,
        investments: [
          {
            active: true,
            pending: false, 
            title: 'Macbook pro 2016',
            filledColor: '#33CC99',
            sliderColor: 'rgba(1,1,1,.14)',
            value: 15,
            delta: 126.12,
            interest: 6.2
          },
          
        ]
      })
    }, 500)
  }

  _formatPrice(price){
    price = price/100.0

    if(!price){
      price = 0
    }
    return price.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,').replace(/^(-)?/, '$1 €')
  }

  _handleRefresh() {
    this._retrieveInvestments()
  }

  render() {
    const { investments, loading } = this.state;

    return (
      <ScrollView
        keyboardShouldPersistTaps='always'
        style={s.scroll}
        refreshControl={
          <RefreshControl
            refreshing={loading}
            onRefresh={this._handleRefresh}
          />
        }
      >
        <View style={s.block}>
          <Text style={s.title}>Caminadora gym</Text>

          <Image 
            source={require('../../Marketplace/images/marketplace.png')}
            style={s.image}
          />

          <View style={s.priceContainer}>
            <Text style={s.smallPrice}>Balance a pagar</Text>
            <Text style={s.price}>256.12</Text>
            <View style={s.labels}>
              <View style={[s.label]}>
                <Text style={s.labelText}>Estado</Text>
              </View>
              <View style={[s.label, s.labelActive]}>
                <Text style={[s.labelText, s.labelTextActive]}>Activo</Text>
              </View>
            </View>
          </View>

          <Progress 
            style={s.progress}
            filledColor="#33CC99"
            sliderColor="rgba(1,1,1,.14)"
            value={20}
          />

          <View style={s.table}>
            <View style={[s.cell, s.firstCell]}>
              <Text style={s.cellTitle}>Plazo</Text>
              <Text style={s.cellDescription}>4/48</Text>
            </View>
            <View style={[s.cell, s.secondCell]}>
              <Text style={s.cellTitle}>Monto a pagar</Text>
              <Text style={s.cellDescription}>4.123,91 €</Text>
            </View>
            <View style={[s.cell, s.thirdCell]}>
              <Text style={s.cellTitle}>Proximo pago</Text>
              <Text style={s.cellDescription}>16 dias</Text>
            </View>
          </View>
        </View>

        <Button
          value='PAGAR BALANCE AHORA'
          onPress={() => this.submit()}
          loading={loading}
          style={s.button}
        />
        
        <Text style={s.secondTitle}>Regístro de pagos</Text>

        {[1, 2].map((item, index) => 
          <View 
            style={[s.block, s.item]}
            key={index}
          >
            <Text style={s.itemNumber}>#{ index + 1 }</Text>
            <Text style={s.itemTitle}>Pago de cuota</Text>
            <Text style={s.itemDate}>17-08-2017</Text>
            <Text style={s.itemPrice}>- $34,21 ></Text>
          </View>
        )}
        
        <View style={{height: 40}}/>
      </ScrollView>
    );
  }
}

const s = StyleSheet.create({
  scroll: {
    padding: rw(16),
    paddingBottom: 100,
    backgroundColor: '#F0F0F0',
    
    height: '100%'
  },
  block: {
    backgroundColor: 'white',
    marginBottom: rw(16),
    borderRadius: 2,
    padding: rw(8),

    shadowOpacity: 0.16,
    shadowRadius: rw(2),
    shadowColor: 'black',
    shadowOffset: {height: 2, width: 0},
  },
  title: {
    color: 'rgba(1,1,1,.65)',
    fontSize: rw(19),
    marginTop: rw(12),
    marginBottom: rw(2),
    backgroundColor: 'transparent'
  },
  image: {
    marginLeft: '-2.3%',
    width: '104.6%',
    height: rw(186)
  },
  priceContainer: {
    marginTop: rw(10)
  },
  smallPrice: {
    color: '#95989A',
    fontSize: rw(12)
  },
  price: {
    color: '#33CC66',
    fontSize: rw(46)
  },
  labels: {
    position: 'absolute',
    top: 0,
    right: rw(20)
  },
  label: {
    paddingTop: rw(6),
    paddingBottom: rw(6),
    paddingLeft: rw(8),
    paddingRight: rw(8),
  },
  labelActive: {
    backgroundColor: 'rgba(57,183,86,.25)'
  },
  labelText: {
    textAlign: 'center',
    color: '#95989A',
    fontSize: rw(12)
  },
  labelTextActive: {
    color: '#39B756'
  },
  
  progress: {
    marginTop: rw(17),
    marginBottom: rw(16.7),
  },

  table: {
    borderTopWidth: 1,
    borderColor: 'lightgray',
    marginBottom: rw(8),
    flexDirection: 'row'
  },
  cell: {
    paddingLeft: rw(21.5)
  },
  firstCell: {
    paddingLeft: 0,
    width: '30%'
  },
  secondCell: {
    width: '40%'
  },
  thirdCell: {
    width: '30%'
  },
  cellTitle: {
    color: '#494949',
    fontSize: rw(12),
    marginTop: rw(16)
  },
  cellDescription: {
    fontSize: rw(18),
    color: 'rgba(1,1,1,.56)',
    fontWeight: 'bold'
  },

  itemNumber: {

  },
  itemTitle: {
    color: '#41A3E4'
  },
  itemDate: {
    color: '#9FA7B3'
  },
  itemPrice: {

  },
  button: {
    marginTop: 0,
    borderRadius: 3,
    marginLeft: rw(40),
    marginRight: rw(40)
  },
  item: {
    height: rw(50),
    paddingLeft: rw(60),
    paddingRight: rw(70)
  },
  itemNumber: {
    position: 'absolute',
    top: rw(10),
    left: rw(20),
    color: '#41A3E4',
    fontSize: rw(26),

    alignSelf: 'center'
  },
  itemTitle: {
    color: '#41A3E4',
    fontSize: rw(11),
    fontWeight: 'bold',
    marginTop: rw(3),
  },
  itemDate: {
    marginTop: rw(4),
    color: '#9FA7B3',
    fontSize: rw(10)
  },
  itemPrice: {
    position: 'absolute',
    top: rw(18),
    right: rw(8),
    fontSize: rw(11)
  },
  secondTitle: {
    marginTop: rw(32),
    marginBottom: rw(16),
    color: 'rgba(1,1,1,.44)',
    fontSize: rw(20)
  }
})