import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableWithoutFeedback,
  StatusBar,
  ScrollView,
  KeyboardAvoidingView,
  Alert,
  AsyncStorage,
  Modal,
  Keyboard
} from 'react-native';

import { relativeWidth } from './../../services/dimensions'
import IbanGreenRoundedButton from '../../common/greenRoundedButton'
import IbanBlueLink from '../../common/blueLink'
import IbanTextInput from '../../common/TextInput'
import BankAccountInput from '../../common/BankAccountInput'
import DisabledTextInput from '../../common/DisabledTextInput'
import ConfettiAlert from '../../common/ConfettiAlert'
import { Dropdown } from 'react-native-material-dropdown'

import { baseUrl, handleRequestError } from './../../services/variables'
import { setIbanAccount } from './../../services/users'
import { upsert } from './../../services/productAccounts'

export default class AddBalance extends Component {

  constructor(props){
    super(props)
    this.state = {
      account: {
        balance: '',
        currency: 'EUR'
      },
      user: {},
      validationErrors: {},
      modalOpened: false,
      successModalOpened: false,
      loading: false
    }
  }

  static navigationOptions = ({ navigation }) => {

    const signupFlow = navigation.state && navigation.state.params ? navigation.state.params.signupFlow : false

    return {
      title: 'Pagar balance',
      headerTitleStyle: {color: 'white', fontWeight: '400'},
      headerStyle: {backgroundColor: '#339966'},
      gesturesEnabled: false,
      headerLeft: signupFlow ? null : (
        <TouchableWithoutFeedback
          onPress={() => navigation.goBack()}
        >
          <View>
            <Text style={styles.navText}>Cancelar</Text>
          </View>
        </TouchableWithoutFeedback>
      ),
    }
  };

  componentDidMount(){

  }

  _handleChanges(key, value){
    let { account, validationErrors } = this.state;
    this.setState({
      account: {
        ...account,
        [key]: value
      },
      validationErrors: {
        ...validationErrors,
        [key]: null
      }
    })
  }

  _submit(){
    this.setState({successModalOpened: true})
  }

  _handleBankAccount(fitnance_account){
    let {account} = this.state

    this.setState({
      account: {
        ...account,
        fitnance_account
      },
      modalOpened: false
    })
  }

  render() {
    const { loading, account, validationErrors, modalOpened, successModalOpened } = this.state
    const {navigation} = this.props
    let curencies = [{value: 'EUR'}, {value: 'GBP'}, {value: 'USD'}];

    return (
      <KeyboardAvoidingView
        style={styles.container}
        behavior="padding"
      >
        <View style={styles.header}>
          <Image 
            style={ styles.headerImage } 
            source={require('../Marketplace/images/marketplace.png')}
          />
          <View style={styles.headerOverlay}/>
          <Text style={styles.headerTitle}>Caminadora{'\n'}gym</Text>
        </View>

        <ScrollView
          keyboardShouldPersistTaps='always'
          style={styles.scroll}
        >

          <View style={styles.main}>

            <View style={styles.currencyContainer}>
              <View style={styles.currencyContainer1}>
                <IbanTextInput
                  label='Monto'
                  value={account.balance}
                  onChangeText={ val => this._handleChanges('balance', val) }
                  style={styles.field}
                  keyboardType='numeric'
                  error={(validationErrors.balance || []).join('. ')}
                />
              </View>

              <View style={styles.currencyContainer2}>
                <Dropdown
                  label=''
                  data={curencies}
                  value={account.currency}
                  onChangeText={ val => this._handleChanges('currency', val) }
                  error={(validationErrors.currency || []).join(' ')}
                  fontSize={relativeWidth(16)}
                  labelFontSize={relativeWidth(12)}
                  labelHeight={relativeWidth(32)}
                  itemPadding={relativeWidth(8)}
                />
              </View>
            </View>

            <DisabledTextInput
              label='Cuenta a debitar'
              value={account.fitnance_account ? account.fitnance_account.name : ''}
              onChange={ val => this._handleChanges('account', val) }
              onTouch={ val => this.setState({modalOpened: true}) }
              error={(validationErrors.fitnance_account || []).join('. ')}
              style={styles.field}
            />

            <IbanGreenRoundedButton
              onPress={ () => this._submit() }
              value='PAGAR'
              loading={loading}
              style={styles.button}
            ></IbanGreenRoundedButton>

            <Text
              style={styles.grayText}
            >
              Al agregar, estás aceptando nuestros
            </Text>

            <IbanBlueLink
              underline={true}
              uppercase={true}
              value='TÉRMINOS DE SERVICIO Y POLÍTICA DE PRIVACIDAD'
              style={styles.blueText}
            ></IbanBlueLink>
          </View>
        </ScrollView>

        <Modal
          visible={modalOpened}
          animationType={'fade'}
          transparent={true}
        >
          <BankAccountInput
            onPress={() => this.setState({modalOpened: false})}
            onSelect={(account) => this._handleBankAccount(account)}
            navigation={navigation}
          />
        </Modal>

        <Modal
          visible={successModalOpened}
          animationType={'fade'}
          transparent={true}
        >
          <ConfettiAlert
            onClose={() => this.setState({successModalOpened: false}, () => navigation.goBack())}
          />
        </Modal>

      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  main: {
    paddingBottom: 30,
    paddingLeft: relativeWidth(10),
    paddingRight: relativeWidth(10),
  },
  scroll: {
    backgroundColor: 'white',
    height: '100%',
  },
  navText: {
    color: 'white',
    fontSize: relativeWidth(18),
    paddingLeft: relativeWidth(18)
  },
  grayText: {
    fontSize: relativeWidth(12),
    color: '#95989A',
    marginTop: relativeWidth(44),
    textAlign: 'center'
  },
  blueText: {
    marginTop: relativeWidth(8)
  },
  header: {
    height: relativeWidth(167)
  },
  headerImage: {
    position: 'absolute',
    width: '100%',
    height: '100%'
  },
  headerTitle: {
    fontSize: relativeWidth(40),
    marginTop: relativeWidth(40),
    paddingLeft: relativeWidth(10),
    backgroundColor: 'transparent',
    color: 'white',
    fontWeight: '300'
  },
  headerOverlay: {
    height: '100%',
    width: '100%',
    position: 'absolute',
    backgroundColor: 'rgba(1,1,1,.45)'
  },
  field: {
    marginTop: relativeWidth(24),
    marginBottom: relativeWidth(-2)
  },
  currencyContainer: {
    flexDirection: 'row'
  },
  currencyContainer1: {
    width: '75%',
    paddingRight: relativeWidth(10),
  },
  currencyContainer2: {
    width: '25%',
    paddingRight: relativeWidth(10),
    paddingLeft: relativeWidth(10),
    paddingTop: relativeWidth(20),
  },
  button: {
    marginTop: relativeWidth(123),
  }
});