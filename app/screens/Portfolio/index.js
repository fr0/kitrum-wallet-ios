import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableWithoutFeedback,
  StatusBar,
} from 'react-native';

import { relativeWidth as rw} from './../../services/dimensions'
import IbanButtonGroup from '../../common/buttonGroup'
import Summary from './Summary'
import Investments from './Investments'
import Opportunities from './Opportunities'

export default class Portfolio extends Component {

  constructor(props){
    super(props)

    this.state = {
      page: 'opportunities'
    }
  }

  static navigationOptions = { header: null, gesturesEnabled: false };

  render() {
    const {page} = this.state 
    const {navigation} = this.props 

    let pageComponent = null 

    switch(page){
      case 'summary':
        pageComponent = <Summary navigation={navigation}/>
        break
      case 'investments':
        pageComponent = <Investments navigation={navigation}/>
        break
      case 'opportunities':
        pageComponent = <Opportunities navigation={navigation}/>
        break
    }

    return (
      <View style={ s.main }>
        <View style={ s.header }>
          <StatusBar barStyle="light-content"/>
          
          <Text style={s.headerTitle}>Portafolio</Text>

          <IbanButtonGroup
            style={s.buttonGroup}
            textStyle={{color: 'white'}}
            activeTextStyle={{color: '#339966'}}
            itemStyle={s.tabItem}
            activeItemStyle={s.activeTabItem}
            items={[
              {
                value: 'Resumen', 
                action: () => this.setState({page: 'summary'}),
                active: page == 'summary'
              },
              {
                value: 'Inversiones', 
                action: () => this.setState({page: 'investments'}),
                active: page == 'investments'
              },
              {
                value: 'Oportunidades', 
                action: () => this.setState({page: 'opportunities'}),
                active: page == 'opportunities'
              }
            ]}
          />
        </View>
        
        <View style={s.body}>
          {pageComponent}
        </View>

      </View>
    );
  }
}

const s = StyleSheet.create({
  main: {
    height: '100%',
  },
  header: {
    backgroundColor: '#339966',
    paddingTop: rw(32),
    paddingLeft: rw(20),
    paddingRight: rw(20),
    height: rw(203),
    width: '100%'
  },
  headerTitle: {
    color: 'white',
    fontSize: rw(37),
    fontWeight: 'bold',
    marginTop: rw(41),
  },
  buttonGroup: {
    marginTop: rw(18),
    borderColor: 'red'
  },
  tabItem: {
    backgroundColor: '#339966',
    borderColor: 'white',
    paddingTop: rw(8), 
    paddingBottom: rw(8)
  },
  activeTabItem: {
    backgroundColor: 'white'
  },
  body: {
    flex: 1,
  }
});