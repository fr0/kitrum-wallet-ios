import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableWithoutFeedback,
  StatusBar,
  ScrollView,
  KeyboardAvoidingView,
  Alert,
  AsyncStorage,
  TouchableOpacity,
  Keyboard,
  RefreshControl
} from 'react-native';

import { relativeWidth as rw } from './../../../services/dimensions'
import { statements } from './../../../services/unnax/Fitnance'
import { profile } from './../../../services/users'

import moment from 'moment';

export default class Summary extends Component {

  constructor(props){
    super(props)

    this.state = {
      contacts: [],
      selectedMonth: {},
      history: [],
      loading: false,
      timeValues: [],
      first_deposit_date: null
    }

    this._handleRefresh = this._handleRefresh.bind(this)
  }

  componentDidMount(){

    let { selectedMonth } = this.state

    profile().then(({fitnance_sid, first_deposit_date}) => {
      let dateStart = first_deposit_date ? moment(first_deposit_date, 'YYYY-MM-DD') : moment();
      let dateEnd = moment();
      let timeValues = [];

      while (dateEnd > dateStart) {
        timeValues.push({
          year: dateStart.format('YY'),
          month: dateStart.format('MMM').toUpperCase(),
          moment: dateStart.clone()
        });
        dateStart.add(1,'month');
      }

      this.setState(
        {
          sid: fitnance_sid,
          first_deposit_date,
          timeValues,
          selectedMonth: timeValues[timeValues.length - 1]
        },
        () => {
          this._retrieveHistory()
        }
      )
    }).catch(error => {

      Alert.alert(JSON.stringify(error))
    })
  }

  _retrieveHistory(){

    let { sid, selectedMonth, timeValues } = this.state

    if(!selectedMonth || !selectedMonth.moment){
      return
    }

    const startOfMonth = selectedMonth.moment.startOf('month').format('YYYY-MM-DD');
    const endOfMonth   = selectedMonth.moment.endOf('month').format('YYYY-MM-DD');

    this.setState({loading: true})

    statements({sid, startOfMonth, endOfMonth})
      .then(response => {
        this.setState(
          {history: response['data']['details'].reverse()},
          () => {
              this.refs.monthsMenu.scrollToEnd({animated: true})
              setTimeout(() => {
                this.setState({loading: false})
              }, 1000)
          }
        )
      })
      .catch((error) => {
        this.setState({loading: false})
      });
  }

  _formatPrice(price){
    price = price/100.0

    if(!price){
      price = 0
    }
    return price.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,').replace(/^(-)?/, '$1 €')
  }

  _handleRefresh() {
    this._retrieveHistory()
  }

  handleChangeMonth = month => event => {
    let { selectedMonth } = this.state

    if(selectedMonth.month == month.month && selectedMonth.year == month.year){
      return
    }

    this.setState({selectedMonth: month}, this._retrieveHistory.bind(this))
  }

  _goBack() {
    const {loading} = this.state

    if(loading){
      return
    }

    let { onSelectTab } = this.props;
    onSelectTab('wallet')
  }

  render() {

    let { navigation: {navigate}, card } = this.props;
    const { selectedMonth, contacts, history, loading, timeValues } = this.state;

    return (
      <View style={s.container}>
        <View style={s.monthsContainer}>
          <ScrollView
            horizontal={true}
            contentContainerStyle={s.monthsContainerScroll}
            style={{height: '100%'}}
            showsHorizontalScrollIndicator={false}
            ref="monthsMenu"
          >
            {timeValues.map((item, index) =>
              <TouchableWithoutFeedback
                key={index}
                onPress={this.handleChangeMonth(item)}
              >
                <View style={[s.month, selectedMonth.month == item.month && selectedMonth.year == item.year ? s.monthActive : null]}>
                  <Text style={[s.monthTitle, selectedMonth.month == item.month && selectedMonth.year == item.year ? s.monthTitleActive : null]}>
                    {item.month} {moment().format('YY') == item.year ? null : `'${ item.year }`}
                  </Text>
                </View>
              </TouchableWithoutFeedback>
            )}
          </ScrollView>
        </View>

        <ScrollView
          keyboardShouldPersistTaps='always'
          style={s.scroll}
          refreshControl={
            <RefreshControl
              refreshing={loading}
              onRefresh={this._handleRefresh}
            />
          }
        >

          <Text style={[s.sectionTitle]}>Historial</Text>

          <View style={s.greenBlock}>
            <View style={s.greenItem}>
              <Text style={s.greenTitle}>Rendimiento</Text>
              <Text style={s.greenDesc}>+$16.21</Text>
            </View>
            <View style={s.greenItem}>
              <Text style={s.greenTitle}>Patrimonio</Text>
              <Text style={s.greenDesc}>$12.091,01</Text>
            </View>
            <View style={s.greenItem}>
              <Text style={s.greenTitle}>Posiciones</Text>
              <Text style={s.greenDesc}>15</Text>
            </View>
          </View>

          <View style={hs.list}>
            {
              history.length == 0 ?
                <Text
                  style={s.blankText}
                >¡Tu banco no nos ha devuelto datos de transacción! Por ahora, no se podrá consultar esta información!</Text>
                : null
            }

            {
              history.map((item, index) => (
                <View
                  key={index}
                  style={hs.item}
                >
                  <View style={hs.logoContainer}>
                    {
                      Math.random() >= .7 ?
                        <Image
                          style={ hs.logo }
                          source={require('../../../images/contacts/logo3.png')}
                        />
                      :
                      Math.random() >= .5 ?
                        <Image
                          style={ hs.logo }
                          source={require('../../../images/contacts/logo1.png')}
                        />
                      :
                        <Image
                          style={ hs.logo }
                          source={require('../../../images/contacts/logo2.png')}
                        />
                    }

                  </View>
                  <View style={hs.container}>
                    <Text
                      style={hs.title}
                      ellipsizeMode="tail"
                      numberOfLines={1}
                    >
                      { item.concepts.length > 0 ? item.concepts[0].split(';')[0] : '' }</Text>
                    <Text style={hs.date}>{ item.deposit_date }</Text>
                  </View>
                  <View style={hs.priceContainer}>
                    <Text
                      style={[
                        hs.price,
                        item.amount > 0 ? hs.priceGreen : null
                      ]}
                    >
                      { this._formatPrice(item.amount) }
                    </Text>
                    <Text style={hs.priceArrow}>></Text>
                  </View>
                </View>
              ))
            }
          </View>
        </ScrollView>
      </View>
    );
  }
}

const hs = StyleSheet.create({
  list: {
    marginTop: rw(13)
  },
  item: {
    height: rw(54),
    paddingLeft: rw(64),
    backgroundColor: 'white',
    marginLeft: rw(21),
    marginRight: rw(21),
    borderRadius: rw(6),
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    marginBottom: rw(15),
  },
  container: {
    width: '65%'
  },
  priceContainer: {
    flexDirection: 'row'
  },
  title: {
    fontSize: rw(14),
  },
  date: {
    color: 'gray',
    fontSize: rw(11.5),
  },
  price: {
    color: '#EE5A55',
    fontSize: rw(15),
    marginRight: rw(10)
  },
  priceGreen: {
    color: '#2CC197',
  },
  priceArrow: {
    color: 'gray',
    fontSize: rw(15),
    marginRight: rw(10)
  },
  logoContainer: {
    height:  rw(54),
    width: rw(54),
    alignItems: 'center',
    justifyContent: 'space-around',
    position: 'absolute',
    top: 0,
    left: 0,
  },
  logo: {

  }
});

const s = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    height: '100%',
  },
  scroll: {
    
  },
  sectionTitle: {
    fontSize: rw(23),
    opacity: 0.8,
    marginBottom: rw(16),
    marginTop: rw(21),
    marginLeft: rw(21),
    color: '#1a1a1a',
    backgroundColor: 'transparent'
  },
  monthsContainer: {
    shadowOpacity: 0.26,
    shadowRadius: rw(5),
    shadowColor: 'black',
    shadowOffset: {height: 0, width: 0},
    height: rw(70)
  },
  monthsContainerScroll: {
    height: '100%',
    flexDirection: 'row',
    alignItems: 'center',
  },
  month: {
    marginLeft: rw(17),
    marginRight: rw(17),

    paddingTop: rw(8),
    paddingBottom: rw(8),
    paddingLeft: rw(20),
    paddingRight: rw(20),

    borderRadius: rw(20),
  },
  monthActive: {
    backgroundColor: '#00CC99',

    shadowOpacity: 0.16,
    shadowRadius: rw(2),
    shadowColor: 'black',
    shadowOffset: {height: 2, width: 0},
  },
  monthTitle: {
    color: 'gray',
    backgroundColor: 'transparent'
  },
  monthTitleActive: {
    color: 'white',
  },
  blankText: {
    color: 'gray',
    fontSize: rw(11.5),
    textAlign: 'center',
    margin: 20
  },

  greenBlock: {
    backgroundColor: '#339966',
    flexDirection: 'row',
    paddingLeft: rw(15)
  },
  greenItem: {
    width: '33%'
  },
  greenTitle: {
    color: 'white',
    fontSize: rw(14),
    marginTop: rw(10)
  },
  greenDesc: {
    color: 'white',
    fontSize: rw(20),
    fontWeight: 'bold',
    marginTop: rw(3),
    marginBottom: rw(17)
  }
})