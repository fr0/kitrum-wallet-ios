import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableWithoutFeedback,
  StatusBar,
  ScrollView,
  AsyncStorage,
  Keyboard
} from 'react-native';
import { TextField } from 'react-native-material-textfield';
import IbanGreenRoundedButton from '../common/greenRoundedButton';
import IbanBlueLink from '../common/blueLink';
import IbanButtonGroup from '../common/buttonGroup';
import IbanTextInput from '../common/TextInput';

import { relativeWidth } from './../services/dimensions'
import { signup } from './../services/users'
import { baseUrl } from './../services/variables'

export default class Signup extends Component<{}> {

  state = {
    user: {
      full_name: '',
      password: '',
      email: '',
      phone_number: '',
      loading: false
    },
    validationErrors: {}
  }

  componentDidMount(){

  }

  submit(){
    Keyboard.dismiss();

    let {user, loading} = this.state;
    let {setAlert, navigation } = this.props;

    if(loading){
      return
    }

    this.setState({loading: true})

    signup(user, navigation)
      .then(response => {

      })
      .catch((response) => {
        this.setState({loading: false})

        if(response.status === 422) {
          response = JSON.parse(response['_bodyText']);

          this.setState({
            validationErrors: response.errors
          })
        }

      });
  }

  _handleChanges(key, value){
    let { user } = this.state;

    this.setState({
      user: {
        ...user,
        [key]: value
      }
    })
  }

  render() {
    const { navigate } = this.props.navigation;
    let { user, loading, validationErrors } = this.state;

    return (
      <View
        style={ styles.main }
      >
        <IbanTextInput
          label='Nombre y Apellido'
          value={ user.full_name }
          onChangeText={ (val) => this._handleChanges('full_name', val)}
          style={styles.field}
          error={ (validationErrors.full_name || []).join('. ') }
        />
        <IbanTextInput
          label='Correo electronico'
          value={ user.email }
          onChangeText={ (val) => this._handleChanges('email', val)}
          email={true}
          keyboardType="email-address"
          style={styles.field}
          error={ (validationErrors.email || []).join('. ') }
        />
        <IbanTextInput
          label='Contraseña'
          value={user.password}
          onChangeText={ (val) => this._handleChanges('password', val)}
          type='password'
          style={styles.field}
          error={ (validationErrors.password || []).join('. ') }
        />
        <IbanTextInput
          label='Teléfono'
          value={user.phone_number}
          onChangeText={ (val) => this._handleChanges('phone_number', val)}
          type='number'
          keyboardType="phone-pad"
          style={styles.field}
          error={ (validationErrors.phone_number || []).join('. ') }
        />
        <IbanGreenRoundedButton
          onPress={ () => this.submit() }
          value='Registrar'
          loading={loading}
        ></IbanGreenRoundedButton>

        <Text
          style={styles.grayText}
        >
          Al registrarte, estás aceptando nuestros
        </Text>

        <IbanBlueLink
          underline={true}
          uppercase={true}
          value='TÉRMINOS DE SERVICIO Y POLÍTICA DE PRIVACIDAD'
          style={styles.blueText}
          onPress={() => navigate('KYCTerms')}
        ></IbanBlueLink>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  main: {
    paddingBottom: 30
  },
  field: {
    marginTop: relativeWidth(16)
  },
  grayText: {
    fontSize: relativeWidth(13),
    color: '#95989A',
    marginTop: relativeWidth(27),
    textAlign: 'center'
  },
  blueText: {
    marginTop: relativeWidth(5)
  }
});
