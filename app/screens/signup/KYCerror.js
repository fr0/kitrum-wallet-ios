import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableWithoutFeedback,
  Animated
} from 'react-native';

import { relativeWidth } from './../../services/dimensions'

export default class KYCerror extends Component<{}> {

  state = {

  }

  componentDidMount(){

  }

  render() {

    let { uploads, progress } = this.state;

    return (
      <View
        style={styles.container}
      >
        <View
          style={styles.circle}
        >
          <Image
            style={ styles.crossIcon }
            source={require('../../images/cross.png')}
            onPress={ () => goBack() }
          />
        </View>
        <Text
          style={styles.message}
        >
          ¡No Aprobado!
          {"\n"}
          {"\n"}
          Vuelve a subir tu documento
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    height: relativeWidth(310),
    backgroundColor: 'rgba(0,0,0,.81)',
    transform: [{'translate':[0,0,1]}],
    alignItems: 'center',
    padding: 10
  },
  circle: {
    backgroundColor: '#E50101',
    marginTop: 40,
    width: 90,
    height: 90,
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center'
  },
  message: {
    fontSize: 16,
    color: 'white',
    marginTop: 20,
    textAlign: 'center'
  },
  crossIcon: {
    width: 39,
    height: 39,
    resizeMode: 'contain'
  }
});
