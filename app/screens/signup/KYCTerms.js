import React, { Component } from 'react';
import {
  StyleSheet,
  WebView,
} from 'react-native';

import {baseUrl} from '../../services/variables'
import { relativeWidth } from './../../services/dimensions'
import NavBackButton from '../../common/NavBackButton';

export default class KYCTerms extends Component<{}> {

  constructor(props){
    super(props)
  }

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Términos y condiciones',
      headerTitleStyle: {color: 'white', fontWeight: '400'},
      headerStyle: {backgroundColor: '#339966'},
      headerLeft: (
        <NavBackButton
          title='Volver'
          onPress={() => {
            navigation.goBack();
          }}
        />
      ),
      gesturesEnabled: false
    }
  };

  render() {
    return (
      <WebView
        style={styles.webView}
        source={{uri: `${ baseUrl}/api/v1/terms?type=kyc`}}
      >

      </WebView>
    );
  }


}

const styles = StyleSheet.create({
  webView: {
    height: '100%',
    width: '100%'
  },
  navText: {
    color: 'white',
    fontSize: relativeWidth(18),
    paddingLeft: relativeWidth(18)
  },
});
