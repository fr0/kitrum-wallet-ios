import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableWithoutFeedback,
  Animated
} from 'react-native';

export default class KYCsuccessPrompt extends Component<{}> {

  state = {

  }

  componentDidMount(){

  }

  render() {

    let { uploads, progress } = this.state;

    return (
      <TouchableWithoutFeedback
        onPress={this.props.onAccept}
      >

        <View
          style={styles.container}
        >
          <View
            style={styles.circle}
          >
            <Image
              style={ styles.checkmark }
              source={require('../../images/checkmark.png')}
              onPress={ () => goBack() }
            />
          </View>
          <Text
            style={styles.message}
          >
            ¡Aprobado!
            {"\n"}
            {"\n"}
            Continúe con el Proceso
          </Text>


          <View
            style={styles.button}
          >
            <Text
              style={styles.buttonText}
            >ACEPTAR</Text>
          </View>

        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    backgroundColor: 'rgba(0,0,0,.81)',
    transform: [{'translate':[0,0,1]}],
    alignItems: 'center',
    padding: 10
  },
  circle: {
    backgroundColor: '#01E576',
    marginTop: 200,
    width: 106,
    height: 106,
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center'
  },
  message: {
    fontSize: 20,
    color: 'white',
    marginTop: 20,
    textAlign: 'center'
  },
  checkmark: {
    width: 48,
    height: 48,
    resizeMode: 'contain'
  },
  button: {
    position: 'absolute',
    bottom: 50
  },
  buttonText: {
    color: '#01E576',
    fontSize: 17
  }
});
