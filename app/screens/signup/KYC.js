import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableWithoutFeedback,
  StatusBar,
  ScrollView,
  KeyboardAvoidingView,
  Alert
} from 'react-native';
import IbanGreenRoundedButton from '../../common/greenRoundedButton';
import IbanBlueLink from '../../common/blueLink';
import IbanButtonGroup from '../../common/buttonGroup';
import IbanTextInput from '../../common/TextInput';
import IbanFileInput from '../../common/FileInput';

import KYCprogress from './KYCprogress';
import KYCsuccessAlert from './KYCsuccessAlert';
import KYCsuccessPrompt from './KYCsuccessPrompt';
import KYCerrorPrompt from './KYCerrorPrompt';
import KYCerror from './KYCerror';

import { Dropdown } from 'react-native-material-dropdown';

import {api_id, api_code, oldUnnaxUrl} from '../../services/variables'
import { relativeWidth } from './../../services/dimensions'
import {setCheckify, profile} from './../../services/users'

export default class KYC extends Component<{}> {

  constructor(props){
    super(props)

    this.state = {
      customer: {
        number: '',
        document_type: 'DNI',
        document_number: '',
        document_front: '',
        document_back: ''
      },
      alertScreen: '',
      validationErrors: {},
      checkifyResponse: ''
    }

    this._finish = this._finish.bind(this)
  }

  static navigationOptions = { header: null, gesturesEnabled: false };

  componentDidMount(){
    let {navigation} = this.props
    let {customer} = this.state

    profile(navigation, {noRedirect: true})
      .then(user => {
        this.setState({
          customer: {
            ...customer,
            number: user.full_name
          }
        })
      })
  }

  _finish(){
    const {navigation} = this.props;
    let {checkifyResponse} = this.state;
    const {document_front, document_back} = this.state.customer

    setCheckify({checkifyResponse, document_front, document_back}, navigation)
      .then(response => {

      })
      .catch(error => {
        data = JSON.parse(error['_bodyText'])
        Alert.alert(data['errors'].join())
      })
  }

  render() {
    const { navigate, goBack } = this.props.navigation;
    const { navigation } = this.props;
    let { customer, alertScreen, validationErrors, loading } = this.state;

    let data = [{value: 'Pasaporte'}, {value: 'DNI'}];

    let alertScreenComponent = null;

    switch(alertScreen) {
      case 'upload':
        alertScreenComponent = (
          <KYCprogress
            onSuccess={() => {
              this.setState({
                alertScreen: Math.random() > 0.7 ? 'errorPrompt' : 'successAlert'
              })
            }}
            onError={() => {
              this.setState({
                alertScreen: 'errorAlert'
              })
            }}
            documents={[]}
          />
        )
        break;
      case 'successAlert':
        alertScreenComponent = (
          <KYCsuccessAlert/>
        )
        setTimeout(() => {
          this.setState({alertScreen: 'successPrompt'})
        }, 3000)
        break;
      case 'successPrompt':
        alertScreenComponent = <KYCsuccessPrompt onAccept={this._finish}/>
        break;
      case 'errorPrompt':
        alertScreenComponent = <KYCerrorPrompt onAccept={this._finish}/>
        break;
      case 'error':
        alertScreenComponent = (
          <KYCerror/>
        )
        break;
    }

    //<KYCerrorPrompt onAccept={() => this.setState({alertScreen: 'error'}) }/>

    return (
      <KeyboardAvoidingView
        style={styles.container}
        behavior="padding"
      >
        <ScrollView
          keyboardShouldPersistTaps='always'
        >
          <StatusBar
            barStyle="light-content"
          />

          <View
            style={ styles.header }
          >
            <Image
              style={ styles.headerImage }
              source={require('../../images/kyc-header.png')}
            ></Image>

            <View style={styles.headerKYC}>
              <Image
                style={ styles.backChevron }
                source={require('../../images/icons/chevron-left.png')}
                onPress={ () => goBack() }
              />
              <Text
                style={styles.headerKYCBack}
                onPress={ () => goBack() }
              >
                Atrás
              </Text>
              <Text style={styles.headerKYCTitle}>KYC</Text>
              <Text style={styles.headerKYCText}>
                Necesitamos que nuestros usuarios suban algunos
                documentos especificos para la comprobación de
                su identidad. Este requerimiento es conforme con
                la Prevención de Blanqueo de Capitales y la
                protección de los usuarios de la comunidad iBAN
                Wallet.
              </Text>
            </View>

          </View>

          <View
            style={styles.form}
          >
            <IbanTextInput
              label='Nombre'
              value={customer.number}
              onChangeText={ val => this._handleChanges('number', val) }
              error={validationErrors.number || ''}
              style={styles.field}
            />

            <Dropdown
              label='Tipo de documento'
              data={data}
              onChangeText={ val => this._handleChanges('document_type', val) }
              error={validationErrors.document_type || ''}
              fontSize={relativeWidth(16)}
              labelFontSize={relativeWidth(12)}
              labelHeight={relativeWidth(32)}
              itemPadding={relativeWidth(8)}
              value={customer.document_type}
            />

            <IbanTextInput
              label='Número de documento'
              value={customer.document_number}
              onChangeText={ val => this._handleChanges('document_number', val) }
              error={validationErrors.document_number || ''}
              style={styles.field2}
            />

            <Text style={styles.greenHeader}>Fotos del Documento</Text>

            <IbanFileInput
              label="Parte Frontal"
              navigation={navigation}
              onChange={ val => this._handleChanges('document_front', val) }
              error={validationErrors.document_front || ''}
              style={styles.fileInput}
            />

            {
              customer.document_type == 'DNI'
              ?
              <IbanFileInput
                label="Parte Trasera"
                navigation={navigation}
                onChange={ val => this._handleChanges('document_back', val) }
                error={validationErrors.document_back || ''}
                style={styles.fileInput}
              />
              :
              null
            }

            <IbanGreenRoundedButton
              onPress={ () => this._submit() }
              value='SUBIR DOCUMENTACIÓN'
              loading={loading}
              style={styles.button}
            ></IbanGreenRoundedButton>

            <Text
              style={styles.grayText}
            >
              Al registrarte, estás aceptando nuestros
            </Text>

            <IbanBlueLink
              underline={true}
              uppercase={true}
              value='TÉRMINOS DE SERVICIO Y POLÍTICA DE PRIVACIDAD'
              style={styles.blueText}
              onPress={() => navigate('KYCTerms')}
            ></IbanBlueLink>
          </View>
        </ScrollView>

        { alertScreenComponent }

      </KeyboardAvoidingView>
    );
  }

  _submit(){
    let { customer, validationErrors } = this.state;

    let errors = {
      ...validationErrors,
      document_type: customer.document_type ? null : "Please select.",
      number: customer.number ? null : "Please type.",
      document_number: customer.document_number ? null : "Please type.",
      document_front: customer.document_front ? null : "Please choose.",
      document_back: customer.document_type == 'DNI' && !customer.document_back ? "Please choose." : null
    }

    this.setState({
      validationErrors: errors
    })

    if(Object.values(errors).every(i => i == null)){
      if(this.loading){
        return
      }

      this.setState({loading: true})

      let sha1 = require('sha1');
      let random = Math.floor(Math.random() * 10000);

      let body = {
        random: random,
        image_front: customer.document_front,
        merchant_signature: sha1(random.toString() + api_code),
        merchant_id: api_id
      }

      if(customer.document_type == 'DNIE'){
        body.image_back = customer.document_back
      }

      // {"type":"default","status":200,"ok":true,"headers":{"map":{"strict-transport-security":["max-age=15768000"],"server":["nginx/1.10.3 (Ubuntu)"],"vary":["Accept-Language, Cookie"],"content-language":["en"],"content-type":["application/json"],"date":["Mon, 11 Dec 2017 12:16:40 GMT"]}},"url":"https://www.unnax.com/api/v2/checkify/check_id","_bodyInit":"{\"data\": {\"code\": 200, \"status\": \"Success\", \"request_id\": 29893}}","_bodyText":"{\"data\": {\"code\": 200, \"status\": \"Success\", \"request_id\": 29893}}"}
      // 2017-12-11 14:16:40.691961+0200 iban[37350:7521829] {"type":"default","status":200,"ok":true,"headers":{"map":{"strict-transport-security":["max-age=15768000"],"server":["nginx/1.10.3 (Ubuntu)"],"vary":["Accept-Language, Cookie"],"content-language":["en"],"content-type":["application/json"],"date":["Mon, 11 Dec 2017 12:16:40 GMT"]}},"url":"https://www.unnax.com/api/v2/checkify/check_id","_bodyInit":"{\"data\": {\"code\": 200, \"status\": \"Success\", \"request_id\": 29893}}","_bodyText":"{\"data\": {\"code\": 200, \"status\": \"Success\", \"request_id\": 29893}}"}

      fetch(
        `${oldUnnaxUrl}/checkify/check_id`,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          body: Object.entries(body).map(([k, v]) => `${k}=${v}`).join('&')
        }
      )
        .then(response => {
          let checkifyResponse = JSON.parse(response['_bodyInit'])

          this.setState({checkifyResponse})

          if(response.status === 200){
            this.setState({alertScreen: 'upload'})
          }else{
            Alert.alert(
              checkifyResponse.message,
              '',
              [
                {text: 'OK', onPress: () => this.setState({alertScreen: 'upload'}) },
              ],
              { cancelable: false }
            )
            // setAlert('errorPrompt');
          }

          this.setState({loading: false})
        })
        .catch((error) => {
          console.log('----------------------------ERROR----')
          console.log(JSON.stringify(error))
          console.log('--------------------------------')
          this.setState({loading: false})
        });
    }
    return ;
  }

  _handleChanges(key, value){
    let { customer, validationErrors } = this.state;
    this.setState({
      customer: {
        ...customer,
        [key]: value
      },
      validationErrors: {
        ...validationErrors,
        [key]: null
      }
    })
  }
}

const styles = StyleSheet.create({
  container: {
    height: '100%',
    width: '100%'
  },
  header: {
    height: relativeWidth(310)
  },
  headerImage: {
    width: '100%',
    height: '100%'
  },
  greenHeader: {
    color: 'rgb(92,200,156)',
    fontSize: relativeWidth(22),
    marginTop: relativeWidth(27)
  },
  headerKYC: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    paddingTop: 30,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 10,
    backgroundColor: 'transparent'
  },
  headerKYCBack: {
    color: 'white',
    fontSize: relativeWidth(18),
    position: 'absolute',
    top: relativeWidth(35),
    left: relativeWidth(35)
  },
  headerKYCTitle: {
    color: 'white',
    fontSize: relativeWidth(30),
    position: 'absolute',
    top: relativeWidth(95),
    left: relativeWidth(15)
  },
  headerKYCText: {
    color: 'white',
    fontSize: relativeWidth(15),
    position: 'absolute',
    top: relativeWidth(146),
    left: relativeWidth(15),
    lineHeight: relativeWidth(20.45)
  },
  headerButtons: {
    marginLeft: relativeWidth(20),
    marginRight: relativeWidth(20)
  },
  form: {
    flex: 1,
    padding: relativeWidth(10),
    backgroundColor: 'white',
    paddingBottom: relativeWidth(30)
  },
  backChevron: {
    position: 'absolute',
    top: relativeWidth(35),
    left: relativeWidth(15)
  },
  field: {
    marginTop: relativeWidth(17)
  },
  field2: {
    marginTop: relativeWidth(0)
  },
  fileInput: {
    marginTop: relativeWidth(23)
  },
  button: {
    marginTop: relativeWidth(40)
  },
  grayText: {
    fontSize: relativeWidth(12),
    color: '#95989A',
    marginTop: relativeWidth(22),
    textAlign: 'center'
  },
  blueText: {
    marginTop: relativeWidth(8)
  }
});
