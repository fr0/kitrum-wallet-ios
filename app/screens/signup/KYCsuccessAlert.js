import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableWithoutFeedback,
  Animated
} from 'react-native';

import { relativeWidth } from './../../services/dimensions'

export default class KYCsuccessAlert extends Component<{}> {

  state = {

  }

  componentDidMount(){

  }

  render() {

    let { uploads, progress } = this.state;

    return (
        <View
          style={styles.container}
        >
          <View
            style={styles.circle}
          >
            <Image
              style={ styles.backChevron }
              source={require('../../images/checkmark.png')}
              onPress={ () => goBack() }
            />
          </View>
          <Text
            style={styles.message}
          >
            ¡Enhorabuena!
          </Text>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    backgroundColor: 'rgba(0,0,0,.81)',
    transform: [{'translate':[0,0,1]}],
    alignItems: 'center',
    padding: 10
  },
  circle: {
    backgroundColor: '#01E576',
    marginTop: relativeWidth(211),
    width: relativeWidth(171),
    height: relativeWidth(171),
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center'
  },
  message: {
    fontSize: relativeWidth(22),
    color: 'white',
    marginTop: relativeWidth(30)
  }
});
