import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableWithoutFeedback,
  StatusBar,
  AsyncStorage,
  Animated,
  Easing,
  Alert
} from 'react-native';

import { relativeWidth } from './../../services/dimensions'
import { setPin } from './../../services/users'
import IbanGreenRoundedButton from '../../common/greenRoundedButton';
import NavBackButton from '../../common/NavBackButton';

export default class PinCode extends Component<{}> {

  constructor(props) {
    super(props);

    this.state = {
      pin: '',
      pinConfirmation: '',
      maskMarginLeft: new Animated.Value(0),
      loading: false,
      step: 'create'
    }

    this._submit = this._submit.bind(this)
  }

  static navigationOptions = { header: null, gesturesEnabled: false };

  _handleNumber(number){
    let {pin, pinConfirmation, step} = this.state

    if(step == 'create'){
      if(pin.length < 4){
        pin = `${ pin }${ number }`
        this.setState({pin})
      }
    }else if(step == 'confirm'){
      pinConfirmation = `${ pinConfirmation }${ number }`

      this.setState({pinConfirmation})

      if(pinConfirmation.length >= 4){
        if(pin == pinConfirmation){
          this.setState({step: 'congrat'})

          setTimeout(() => this._submit(), 2000)

        }else{
          this._resetPassword()
        }
      }
    }
  }

  _handleBackspace(){
    let {pin} = this.state

    pin = pin.split('')
    pin.pop()
    pin = pin.join('')

    this.setState({pin})
  }

  _submit(){
    const {pin} = this.state
    const {navigation} = this.props

    this.setState({loading: true})

    setPin(pin, navigation)
      .then(response => {

      })
      .catch(error => {
        data = JSON.parse(error['_bodyText'])
        Alert.alert(data['errors'].join())
        this.setState({loading: false})
      })
  }

  _resetPassword(){
    Animated.timing(
      this.state.maskMarginLeft,
      {
        toValue: -80,
        duration: 100,
      }
    ).start();

    setTimeout(() => {
      this.setState({pinConfirmation: ''})

      Animated.timing(
        this.state.maskMarginLeft,
        {
          toValue: 0,
          duration: 400,
          easing: Easing.elastic(5)
        }
      ).start();
    }, 100)
  }

  render() {

    let { pin, pinConfirmation, loading, step } = this.state

    return (
      <View
        style={styles.main}
      >
        <View style={styles.logo}>
          <Image
            style={ styles.logoText }
            source={require('../../images/img_iban_green.png')}
          />
          <Image
            style={ styles.logoImage }
            source={require('../../images/logo-small.png')}
          />
        </View>

        {
          step == 'congrat' ?
            <View style={styles.dynamicCongrat}>

              <Image
                style={{width: relativeWidth(70), height: relativeWidth(50) }}
                source={require('../signup/images/check.png')}
              ></Image>
            </View>
            :
            <View style={styles.dynamic}>

              <Animated.View style={[styles.mask, {marginLeft: this.state.maskMarginLeft}]}>
                { [...Array(4).keys()].map(i =>
                  <View
                    key={i}
                    style={[styles.maskDot, (step == 'confirm' ? pinConfirmation : pin).length > i ? styles.maskDotActive : null]}
                  />
                )}
              </Animated.View>

              {step == 'confirm' ?
                <Text style={styles.text}>Confirme PIN de protección</Text>
                :
                <Text style={styles.text}>Defina su PIN de protección</Text>
              }
            </View>
        }

        <View style={styles.keyboard}>
          { [...Array(9).keys()].map(i => i + 1).map(i => {
            return (
              <TouchableWithoutFeedback
                key={i}
                onPress={() => this._handleNumber(i) }
              >
                <View style={styles.numberContainer}>
                  <Text style={styles.number}>
                    {i}
                  </Text>
                </View>
              </TouchableWithoutFeedback>
            )
          })}

          <TouchableWithoutFeedback>
            <View style={styles.numberContainer}>

            </View>
          </TouchableWithoutFeedback>

          <TouchableWithoutFeedback
            onPress={() => this._handleNumber(0)}
          >
            <View style={styles.numberContainer}>
              <Text style={styles.number}>
                0
              </Text>
            </View>
          </TouchableWithoutFeedback>

          <TouchableWithoutFeedback
            onPress={() => this._handleBackspace()}
          >
            <View style={styles.numberContainer}>
              <Image
                style={ styles.fingerprint }
                source={require('../../images/icons/backspace.png')}
              />
            </View>
          </TouchableWithoutFeedback>

        </View>

        {step == 'create' ?
          <IbanGreenRoundedButton
            value='Salvar'
            onPress={() => this.setState({step: 'confirm'})}
            loading={loading}
            style={styles.button}
            disabled={pin.length < 4}
          ></IbanGreenRoundedButton>
          :
          null
        }

        {step == 'confirm' ?
          <View style={styles.back}>
            <NavBackButton
              onPress={() => this.setState({step: 'create'})}
              title={''}
              green={true}
            />
          </View>
          :
          null
        }

      </View>
    );
  }
}

const styles = StyleSheet.create({
  main: {
    padding: 10,
    flexDirection: 'column',
    minHeight: '100%',
    backgroundColor: 'white'
  },
  keyboard: {
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  numberContainer: {
    width: '33.33%',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: relativeWidth(60),
  },
  number: {
    fontSize: relativeWidth(34),
    color: "#4CBA8D"
  },
  forgot: {
    fontSize: relativeWidth(14)
  },
  fingerprint: {
    width: relativeWidth(27),
    height: relativeWidth(27),
  },

  logo: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: relativeWidth(59),
  },
  logoText: {
    width: relativeWidth(93),
    height: relativeWidth(15),
    marginRight: relativeWidth(6)
  },
  logoImage: {
    width: relativeWidth(35),
    height: relativeWidth(35),
  },

  mask: {
    marginTop: relativeWidth(67),
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: relativeWidth(20),
  },
  maskDot: {
    backgroundColor: 'lightgray',
    width: relativeWidth(12),
    height: relativeWidth(12),
    marginLeft: relativeWidth(15.5),
    marginRight: relativeWidth(15.5),

    borderRadius: relativeWidth(6)
  },
  maskDotActive: {
    backgroundColor: '#4CBA8D'
  },

  text: {
    textAlign: 'center',
    marginBottom: relativeWidth(20),
    fontSize: relativeWidth(22)
  },
  button: {
    marginTop: relativeWidth(-20)
  },

  back: {
    position: 'absolute',
    top: relativeWidth(22),
    left: relativeWidth(22),
  },

  dynamic: {
    height: relativeWidth(160),
  },
  dynamicCongrat: {
    height: relativeWidth(160),
    alignItems: 'center',
    justifyContent: 'space-around'
  }
});
