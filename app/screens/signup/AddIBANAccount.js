import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableWithoutFeedback,
  StatusBar,
  ScrollView,
  KeyboardAvoidingView,
  Alert,
  AsyncStorage,
  Modal,
  Keyboard
} from 'react-native';

import { relativeWidth } from './../../services/dimensions'
import IbanGreenRoundedButton from '../../common/greenRoundedButton';
import IbanBlueLink from '../../common/blueLink';
import IbanTextInput from '../../common/TextInput';
import BankAccountInput from '../../common/BankAccountInput';
import DisabledTextInput from '../../common/DisabledTextInput';
import { Dropdown } from 'react-native-material-dropdown';

import { baseUrl, handleRequestError } from './../../services/variables'
import { setIbanAccount } from './../../services/users'
import { upsert } from './../../services/productAccounts'

export default class AddIBANAccount extends Component {

  constructor(props){
    super(props)
    this.state = {
      account: {
        balance: '',
        currency: 'EUR'
      },
      bank: {
        parameters: [],
        id: 0,
        name: ''
      },
      user: {},
      validationErrors: {},
      modalOpened: false,
      loading: false
    }
  }

  static navigationOptions = ({ navigation }) => {

    const signupFlow = navigation.state && navigation.state.params ? navigation.state.params.signupFlow : false

    return {
      title: 'iBAN Cuenta',
      headerTitleStyle: {color: 'white', fontWeight: '400'},
      headerStyle: {backgroundColor: '#339966'},
      gesturesEnabled: false,
      headerLeft: signupFlow ? null : (
        <TouchableWithoutFeedback
          onPress={() => navigation.goBack()}
        >
          <View>
            <Text style={styles.navText}>Cancelar</Text>
          </View>
        </TouchableWithoutFeedback>
      ),
    }
  };

  componentDidMount(){

  }

  _handleChanges(key, value){
    let { account, validationErrors } = this.state;
    this.setState({
      account: {
        ...account,
        [key]: value
      },
      validationErrors: {
        ...validationErrors,
        [key]: null
      }
    })
  }



  _submit(){
    let {account, loading} = this.state
    let {navigation} = this.props

    Keyboard.dismiss();

    this.setState({loading: true})
    if(loading){
      return;
    }

    const signupFlow = navigation.state && navigation.state.params ? navigation.state.params.signupFlow : false
    let func = signupFlow ? setIbanAccount : upsert

    func(account, navigation)
      .then(response => {
        const { lemonway_id } = response
        navigation.push('BankAccountLoginIBAN', {account, lemonway_id})
        this.setState({loading: false})
      })
      .catch(error => {
        if(error.status == 422){
          data = JSON.parse(error['_bodyText']);

          this.setState({
            validationErrors: data.validation_errors
          })
        }
        this.setState({loading: false})
      })
  }

  _handleBankAccount(fitnance_account){
    let {account} = this.state
    this.setState({
      account: {
        ...account,
        fitnance_account
      },
      modalOpened: false
    })
  }

  render() {
    const { loading, account, validationErrors, modalOpened } = this.state

    let curencies = [{value: 'EUR'}, {value: 'GBP'}, {value: 'USD'}];

    return (
      <KeyboardAvoidingView
        style={styles.container}
        behavior="padding"
      >
        <View style={styles.header}>
          <Image style={ styles.headerImage } source={require('../../images/products/add-iban-account-header.png')}/>
          <Text style={styles.headerTitle}>iBAN Cuenta</Text>
          <Text style={styles.headerDescription}>Producto ideal para el ahorro.</Text>
        </View>

        <ScrollView
          keyboardShouldPersistTaps='always'
          style={styles.scroll}
        >

          <View style={styles.main}>

            <View style={styles.currencyContainer}>
              <View style={styles.currencyContainer1}>
                <IbanTextInput
                  label='Monto'
                  value={account.balance}
                  onChangeText={ val => this._handleChanges('balance', val) }
                  style={styles.field}
                  keyboardType='numeric'
                  error={(validationErrors.balance || []).join('. ')}
                />
              </View>

              <View style={styles.currencyContainer2}>
                <Dropdown
                  label=''
                  data={curencies}
                  value={account.currency}
                  onChangeText={ val => this._handleChanges('currency', val) }
                  error={(validationErrors.currency || []).join(' ')}
                  fontSize={relativeWidth(16)}
                  labelFontSize={relativeWidth(12)}
                  labelHeight={relativeWidth(32)}
                  itemPadding={relativeWidth(8)}
                />
              </View>
            </View>

            <DisabledTextInput
              label='Cuenta a debitar'
              value={account.fitnance_account ? account.fitnance_account.name : ''}
              onChange={ val => this._handleChanges('account', val) }
              onTouch={ val => this.setState({modalOpened: true}) }
              error={(validationErrors.fitnance_account || []).join('. ')}
              style={styles.field}
            />

            <IbanGreenRoundedButton
              onPress={ () => this._submit() }
              value='CREAR'
              loading={loading}
              style={styles.button}
            ></IbanGreenRoundedButton>

            <Text
              style={styles.grayText}
            >
              Al agregar, estás aceptando nuestros
            </Text>

            <IbanBlueLink
              underline={true}
              uppercase={true}
              value='TÉRMINOS DE SERVICIO Y POLÍTICA DE PRIVACIDAD'
              style={styles.blueText}
            ></IbanBlueLink>
          </View>
        </ScrollView>


        <Modal
          visible={modalOpened}
          animationType={'fade'}
          transparent={true}
        >
          <BankAccountInput
            onPress={() => this.setState({modalOpened: false})}
            onSelect={(account) => this._handleBankAccount(account)}
            navigation={this.props.navigation}
          />
        </Modal>


      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  main: {
    paddingBottom: 30,
    paddingLeft: relativeWidth(10),
    paddingRight: relativeWidth(10),
  },
  scroll: {
    backgroundColor: 'white',
    height: '100%',
  },
  navText: {
    color: 'white',
    fontSize: relativeWidth(18),
    paddingLeft: relativeWidth(18)
  },
  grayText: {
    fontSize: relativeWidth(12),
    color: '#95989A',
    marginTop: relativeWidth(44),
    textAlign: 'center'
  },
  blueText: {
    marginTop: relativeWidth(8)
  },
  header: {
    height: relativeWidth(167)
  },
  headerImage: {
    position: 'absolute',
    width: '100%',
    height: '100%'
  },
  headerTitle: {
    fontSize: relativeWidth(40),
    marginTop: relativeWidth(40),
    paddingLeft: relativeWidth(10),
    backgroundColor: 'transparent',
    color: 'white',
    fontWeight: '300'
  },
  headerDescription: {
    fontSize: relativeWidth(17),
    paddingLeft: relativeWidth(10),
    backgroundColor: 'transparent',
    color: 'white',
    marginTop: relativeWidth(8),
  },
  field: {
    marginTop: relativeWidth(24),
    marginBottom: relativeWidth(-2)
  },
  currencyContainer: {
    flexDirection: 'row'
  },
  currencyContainer1: {
    width: '75%',
    paddingRight: relativeWidth(10),
  },
  currencyContainer2: {
    width: '25%',
    paddingRight: relativeWidth(10),
    paddingLeft: relativeWidth(10),
    paddingTop: relativeWidth(20),
  },
  button: {
    marginTop: relativeWidth(123),
  }
});