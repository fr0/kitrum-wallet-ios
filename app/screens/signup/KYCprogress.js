import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableWithoutFeedback,
  PanResponder,
  Animated
} from 'react-native';
import * as Progress from 'react-native-progress';

import { relativeWidth } from './../../services/dimensions'

export default class KYCprogress extends Component<{}> {

  state = {
    number: '',
    name: '',
    uploads: [],
    progress: 0
  }


  componentDidMount(){
    let uploads = [{}, {}, {}, {}]

    uploads.forEach((upload, index) => {
      upload.index = index
      upload.progress = .0
      upload.fadeAnim = new Animated.Value(1);
      upload.widthAnim = new Animated.Value(relativeWidth(204) - (index * relativeWidth(13)));
      upload.heightAnim = new Animated.Value(relativeWidth(245) - (index * relativeWidth(21)));
      upload.topAnim = new Animated.Value(relativeWidth(245) + index * relativeWidth(9));
      upload.leftAnim = new Animated.Value(relativeWidth(35) + (index * relativeWidth(127)));
      upload.textMarginAnim = new Animated.Value(30 - (index * 5));
    })

    this.setState({
      uploads: uploads
    })

    uploads.forEach( (upload, index) => {
      setTimeout(() => {
        let { uploads } = this.state;
        uploads[index].progress = .2
        this.setState({
          uploads: uploads
        })
      }, 200 + (index * 1000))

      setTimeout(() => {
        let { uploads } = this.state;
        uploads[0].progress = .4
        this.setState({
          uploads: uploads
        })
      }, 400 + (index * 1000))

      setTimeout(() => {
        let { uploads } = this.state;
        uploads[index].progress = .6
        this.setState({
          uploads: uploads
        })
      }, 600 + (index * 1000))

      setTimeout(() => {
        let { uploads } = this.state;
        uploads[index].progress = .8
        this.setState({
          uploads: uploads
        })
      }, 800 + (index * 1000))

      setTimeout(() => {
        let { uploads } = this.state;
        uploads[index].progress = 1

        this.setState({
          uploads: uploads,
          progress: 25 * (index + 1)
        })

        animations = [];
        animations.push(
          Animated.timing(
            upload.fadeAnim,
            {
              toValue: 0,
              duration: 500,
            }
          )
        );

        uploads.forEach(item => {
          if(item.index > 0){
            item.index -= 1;
            animations.push(
              Animated.timing(
                item.widthAnim,
                {
                  toValue: 184 - (item.index * 10),
                  duration: 500,
                }
              )
            )
            animations.push(
              Animated.timing(
                item.heightAnim,
                {
                  toValue: 224 - (item.index * 29),
                  duration: 500,
                }
              )
            )
            animations.push(
              Animated.timing(
                item.topAnim,
                {
                  toValue: 205 + item.index * 13,
                  duration: 500,
                }
              )
            )
            animations.push(
              Animated.timing(
                item.leftAnim,
                {
                  toValue: 30 + (item.index * 100),
                  duration: 500,
                }
              )
            )
            animations.push(
              Animated.timing(
                item.textMarginAnim,
                {
                  toValue: 30 - (item.index * 5),
                  duration: 500,
                }
              )
            )
          }
        })

        Animated.parallel(animations).start()

        this.setState({
          uploads: uploads
        })

      }, 1000 + (index * 1000))
    })

    setTimeout(() => {
      this.props.onSuccess();
    }, 5000)
  }

  render() {

    let { uploads, progress } = this.state;

    return (
        <View
          style={styles.container}
        >
          <Text
            style={styles.header}
          >
            Esto puede tomar{"\n"}
            varios minutos
          </Text>

          <View
            style={styles.progress}
          >
            <View
              style={[styles.progressValue, {width: `${ progress }%`}]}
            >

            </View>
          </View>

          {
            uploads.map((upload, index) => {
              return (
                <Animated.View
                  key={index}
                  style={[
                    styles.card,
                    {
                      width: upload.widthAnim,
                      height: upload.heightAnim,
                      top: upload.topAnim,
                      left: upload.leftAnim,
                      opacity: upload.fadeAnim
                    }
                  ]}
                >
                  <Progress.Circle
                    size={40 - (upload.index * 5)}
                    color='#33CC99'
                    unfilledColor='#D5D5D5'
                    borderWidth={0}
                    progress={upload.progress}
                  />
                  <Animated.View
                    style={styles.animView}
                  >
                    <Text
                      style={[styles.cardText, {fontSize: 18 - (upload.index * 2)} ]}
                    >
                      Comprobación de{"\n"}Prevención de{"\n"}Blanqueo de{"\n"}capitales.
                    </Text>
                  </Animated.View>
                </Animated.View>
              )
            }).reverse()
          }
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    backgroundColor: 'rgba(0,0,0,.81)',
    transform: [{'translate':[0,0,1]}],
    alignItems: 'center',
    padding: relativeWidth(10)
  },
  header: {
    fontSize: relativeWidth(23),
    marginTop: relativeWidth(50),
    color: 'white',
    textAlign: 'center'
  },
  progress: {
    marginTop: relativeWidth(35),
    height: relativeWidth(3),
    width: '100%',
    backgroundColor: 'white',
    borderRadius: relativeWidth(2)
  },
  progressValue: {
    height: '100%',
    backgroundColor: '#33CC99'
  },
  card: {
    backgroundColor: 'white',
    borderRadius: relativeWidth(6),
    shadowOffset: {width: 0, height: relativeWidth(2)},
    shadowOpacity: 0.8,
    shadowRadius: relativeWidth(2),
    position: 'absolute',
    paddingTop: relativeWidth(30),
    alignItems: 'center'
  },
  cardText: {
    // textAlign: 'center'
  },
  animView: {
    marginTop: relativeWidth(40),
    width: '100%',
    paddingLeft: relativeWidth(17),
    paddingRight: relativeWidth(17)
  }
});
