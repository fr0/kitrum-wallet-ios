import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  ScrollView
} from 'react-native';

import IbanStepper from '../../common/stepper';
import IbanGreenRoundedButton from '../../common/greenRoundedButton';

import NavBackButton from '../../common/NavBackButton';
import IbanBankIcon from '../../common/BankIcon';

import { relativeWidth } from '.././../services/dimensions'

import { complete } from '../../services/unnax/DBT'

export default class BankAccountCompleteIBAN extends Component {

  constructor(props){
    super(props);

    this.state = {
      bank: {},
      loading: false
    }

    this._submit = this._submit.bind(this)
  }

  componentWillMount(){
    const { bank } = this.props.navigation.state.params

    this.setState({bank})

    this.completeReader()
  }

  completeReader() {
    const { sid, request_code } = this.props.navigation.state.params
    const { loading } = this.state

    if(this.loading){
      return
    }

    this.setState({loading: true})

    complete({sid, request_code})
      .then((response) => {
        this.setState({loading: false})
      })
      .catch((error) => {
        this.setState({loading: false})
        Alert.alert(error)
      });
  }

  _submit(){
    const { navigate, goBack, state } = this.props.navigation;
    const { bank, sid, request_code } = state.params

    navigate('BankAccountSuccess', {sid, request_code})
  }

  render() {

    let { bank, alertScreen, loading } = this.state;
    const { navigation } = this.props
    const { navigate, goBack } = navigation;

    let alertScreenComponent = null;

    return (
      <ScrollView>

        <View
          style={ styles.main }
        >

          <Text style={styles.bankName}>{bank.name.replace(' (paso 2)', '')}</Text>

          <IbanBankIcon
            bankId={bank.id}
            style={styles.bankIcon}
          />

          <IbanStepper
            style={styles.stepper }
            circles={3}
            current={3}
          />
          <Text style={styles.greenHeader}>¡Banco agregado exitosamente!</Text>

          <Text style={styles.grayLabel}>¿Qué desea hacer?</Text>

          <IbanGreenRoundedButton
            value='Terminar'
            style={styles.button}
            onPress={this._submit}
            loading={loading}
          ></IbanGreenRoundedButton>

          <IbanGreenRoundedButton
            value='Agregar otro banco'
            style={styles.buttonMore}
            textStyle={{color: 'gray'}}
            onPress={() => navigate('BankAccount')}
          ></IbanGreenRoundedButton>
        </View>
      </ScrollView>
    );
  }
}


BankAccountCompleteIBAN.navigationOptions = ({ navigation }) => {
  return {
    title: 'Cuenta Bancaria',
    headerTitleStyle: {color: 'white', fontWeight: '400'},
    headerStyle: {backgroundColor: '#4CBA8D'},
    headerLeft: (
      <NavBackButton
        title='Volver'
        onPress={() => {
          navigation.goBack();
        }}
      />
    ),
    gesturesEnabled: false
  }
}

const styles = StyleSheet.create({
  main: {
    paddingBottom: 30,
    alignItems: 'center',
    backgroundColor: 'white',
    minHeight: '100%',
    paddingRight: 20,
    paddingLeft: 20
  },
  greenHeader: {
    color: '#4CBA8D',
    fontSize: relativeWidth(24),
    marginTop: relativeWidth(50),
  },
  stepper: {
    marginTop: relativeWidth(25)
  },
  bankName: {
    fontSize: relativeWidth(23),
    marginTop: relativeWidth(29)
  },
  bankIcon: {
    height: relativeWidth(65),
    width: relativeWidth(70),
    marginTop: relativeWidth(15)
  },
  grayLabel: {
    color: '#8E8E93',
    fontSize: relativeWidth(14),
    fontWeight: '500',
    marginTop: relativeWidth(10)
  },
  button: {
    width: '100%',
    marginBottom: 0,
    marginTop: relativeWidth(50),
    borderRadius: 4
  },
  buttonMore: {
    width: '100%',
    marginBottom: 0,
    marginTop: relativeWidth(16),
    borderRadius: 4,
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: '#D8D8D8'
  }
});
