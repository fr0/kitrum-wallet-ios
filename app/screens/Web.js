import React, { Component } from 'react';
import {
  StyleSheet,
  WebView,
  TouchableWithoutFeedback,
  View,
  Text
} from 'react-native';
import { relativeWidth } from './../services/dimensions'

export default class Web extends Component<{}> {

  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.state.params.uri,
      headerTitleStyle: {color: 'white', fontWeight: '400'},
      headerStyle: {backgroundColor: '#339966'},
      headerLeft: (
        <TouchableWithoutFeedback
          onPress={() => navigation.goBack()}
        >
          <View>
            <Text style={styles.navText}>Cancelar</Text>
          </View>
        </TouchableWithoutFeedback>
      ),
      gesturesEnabled: false
    }
  };

  render() {

    const {uri} = this.props.navigation.state.params

    return (
      <WebView
        source={{uri}}
        style={styles.container}
      />
    );
  }


}

const styles = StyleSheet.create({
  container: {
    height: '100%',
    width: '100%'
  },
  navText: {
    color: 'white',
    fontSize: relativeWidth(18),
    paddingLeft: relativeWidth(18)
  },
});
