import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  StatusBar,
  ScrollView,
  Modal,
  Image
} from 'react-native';

import { relativeWidth } from './../../services/dimensions'
import { list } from './../../services/riskQuestions'
import Icon from 'react-native-vector-icons/Feather';
import IbanGreenRoundedButton from '../../common/greenRoundedButton';


export default class Result extends Component<{}> {

  constructor(props){
    super(props)
    this.state = {
      loading: false
    }

    this.submit = this.submit.bind(this)
  }

  static navigationOptions = { header: null, gesturesEnabled: false };

  componentDidMount(){

  }

  submit() {
    const {navigation} = this.props
    navigation.push('SavingQuestionnaire')
  }

  goBack() {
    const {navigation} = this.props
    const {number} = this.state
    if(number > 0){
      navigation.goBack()
    }
  }

  render() {
    const { loading } = this.state

    return (
      <View
        style={styles.container}
        behavior="padding"
      >
        <StatusBar barStyle="dark-content"/>

        <Text style={styles.heading}>¿Cuáles son sus objetivos de ahorro?</Text>

        <Image
          style={ styles.image }
          source={require('./images/saving_questionnaire_intro.png')}
        ></Image>

        <IbanGreenRoundedButton
          value='Continuar'
          onPress={() => this.submit()}
          loading={loading}
          style={styles.button}
        ></IbanGreenRoundedButton>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    minHeight: '100%'
  },
  heading: {
    color: '#4CBA8D',
    fontSize: relativeWidth(23),
    marginTop: relativeWidth(54),
    textAlign: 'center'
  },
  image: {
    marginTop: relativeWidth(86),
    width: '100%',
    height: relativeWidth(433)
  },
  button: {
    marginTop: relativeWidth(45),
    marginLeft: relativeWidth(40),
    marginRight: relativeWidth(40),
  }
});