import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  StatusBar,
  ScrollView,
  Modal,
  Image,
  Alert
} from 'react-native';

import { relativeWidth } from './../../services/dimensions'
import { baseUrl } from './../../services/variables'
import { list, answer, finish } from './../../services/savingQuestions'
import Spinner from '../../common/Spinner'
import Icon from 'react-native-vector-icons/Feather';


export default class RiskQuestionnaire extends Component {

  constructor(props){
    super(props)
    this.state = {
      questions: [],
      number: 0,
      loading: false
    }

    this.goBack = this.goBack.bind(this)
  }

  static navigationOptions = { header: null, gesturesEnabled: false };

  componentDidMount(){
    const {navigation} = this.props
    const { local, questions, number, userNumber } = navigation.state.params || {};

    if(local){
      this.setState({questions, number}, () => this.goToLastStep(userNumber))
    }else{
      list()
        .then(({questions, number}) => {
          this.setState({questions, number}, () => this.goToLastStep(number))
        })
        .catch()
    }
  }

  goToLastStep(lastQuestionNumber){
    const {navigation} = this.props
    const {number, questions} = this.state

    if(number < lastQuestionNumber){
      if(questions.length > number + 1){
        navigation.push('SavingQuestionnaire', {local: true, questions, number: number + 1, userNumber: lastQuestionNumber})
      }else{
        this.finish()
      }
    }
  }

  handleSelectOption = option => e => {
    const {navigation} = this.props
    const {questions, number, loading} = this.state

    if(loading){
      return
    }

    this.setState({loading: true})

    setTimeout(() => { // for better animation
      answer(questions[number].id, option)
        .then(response => {
          this.setState(
            {loading: false},
            () => {
              if(questions.length == number + 1){
                this.finish()
              }else{
                navigation.push('SavingQuestionnaire', {local: true, questions, number: number + 1})
              }
            }
          )
        })
        .catch()
    }, 1000)
  }

  finish(){
    const {navigation} = this.props

    finish(navigation)
      .then(() => {

      })
      .catch(() => {

      })
  }

  goBack() {
    const {navigation} = this.props
    const {number} = this.state
    if(number > 0){
      navigation.goBack()
    }
  }

  render() {
    const { loading, questions, number } = this.state

    let question = questions[number] || {}
    let {text} = question

    return (
      <View
        style={styles.container}
        behavior="padding"
      >
        <StatusBar barStyle="dark-content"/>

        <Text style={styles.heading}>{question.text}</Text>

        <View style={styles.legends}>
          <View style={styles.legend}>
            <View style={styles.legendLabel}>
              <Text style={styles.legendLabelDescription}>1</Text>
            </View>
            <Text style={styles.legendDescription}>NS / NC</Text>
          </View>
          <View style={styles.legend}>
            <View style={styles.legendLabel}>
              <Text style={styles.legendLabelDescription}>2</Text>
            </View>
            <Text style={styles.legendDescription}>No importante</Text>
          </View>
        </View>

        <View style={styles.legends}>
          <View style={styles.legend}>
            <View style={styles.legendLabel}>
              <Text style={styles.legendLabelDescription}>3</Text>
            </View>
            <Text style={styles.legendDescription}>importante</Text>
          </View>
          <View style={styles.legend}>
            <View style={styles.legendLabel}>
              <Text style={styles.legendLabelDescription}>4</Text>
            </View>
            <Text style={styles.legendDescription}>Muy importante</Text>
          </View>
        </View>

        <View
          keyboardShouldPersistTaps='always'
          style={styles.imageContainer}
        >
          <Image
            style={styles.image}
            source={{uri: `${ baseUrl }/${ question.image }` }}
          />

          <View
            style={styles.options}
          >
            {[1, 2 ,3, 4].map((option) =>
              <TouchableWithoutFeedback
                key={option}
                onPress={this.handleSelectOption(option)}
              >
                <View
                  style={styles.option}
                >
                  <Text style={styles.optionText}>{option}</Text>
                </View>
              </TouchableWithoutFeedback>
            )}
          </View>

        </View>

        <View style={styles.footer}>
          <TouchableWithoutFeedback
            onPress={this.goBack}
          >
            <View style={styles.footerIcon}>
              <Icon name="arrow-left" size={25} color="gray" />
            </View>
          </TouchableWithoutFeedback>
          <View style={styles.footerTextContainer}>
            <Text style={styles.footerText} >{number + 1} de {questions.length}</Text>
          </View>
        </View>

        <Modal
          visible={loading}
          animationType={'fade'}
          transparent={true}
        >
          <Spinner/>
        </Modal>


      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    minHeight: '100%'
  },
  heading: {
    color: '#4CBA8D',
    fontSize: relativeWidth(23),
    marginTop: relativeWidth(54),
    textAlign: 'center'
  },
  footer: {
    height: relativeWidth(70),
    position: 'absolute',
    bottom: 0,
    width: '100%',
  },
  footerIcon: {
    position: 'absolute',
    height: '100%',
    width: relativeWidth(60),
    justifyContent: 'center',
    alignItems: 'center'
  },
  footerTextContainer: {
    height: '100%',
    marginLeft: '20%',
    marginRight: '20%',

    justifyContent: 'center'
  },
  footerText: {
    textAlign: 'center',
    color: 'gray'
  },

  imageContainer: {
    position: 'absolute',
    bottom: relativeWidth(70),
    width: '100%',
    height: relativeWidth(433)
  },

  options: {
    width: '100%',
    height: '100%',
    paddingTop: relativeWidth(45),
    paddingBottom: relativeWidth(45),
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'flex-end',
  },

  image: {
    position: 'absolute',
    top: 0,
    width: '100%',
    height: '100%',
  },

  option: {
    backgroundColor: 'white',
    width: relativeWidth(50),
    height: relativeWidth(50),
    borderRadius: relativeWidth(25),
    marginRight: relativeWidth(18),

    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.4,

    justifyContent: 'center',
    alignItems: 'center'
  },
  optionText: {
    backgroundColor: 'transparent',
    color: '#4CBA8D',
    fontSize: relativeWidth(18)
  },

  legends: {
    flexDirection: 'row',
    marginTop: relativeWidth(16)
  },
  legend: {
    width: '50%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  legendLabel: {
    borderRadius: relativeWidth(4),
    backgroundColor: '#4CBA8D',
    width: relativeWidth(26),
    height: relativeWidth(26),
    alignItems: 'center',
    justifyContent: 'center'
  },
  legendLabelDescription: {
    color: 'white',
    fontSize: relativeWidth(21),
    backgroundColor: 'transparent'
  },
  legendDescription: {
    color: 'gray',
    fontSize: relativeWidth(12),
    marginLeft: relativeWidth(6),
    width: relativeWidth(100)
  }

});