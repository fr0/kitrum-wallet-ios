import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableWithoutFeedback,
  StatusBar,
  ScrollView,
  AsyncStorage,
  Keyboard,
  Linking
} from 'react-native';
import IbanGreenRoundedButton from '../common/greenRoundedButton';
import IbanBlueLink from '../common/blueLink';
import IbanButtonGroup from '../common/buttonGroup';
import IbanTextInput from '../common/TextInput';

import { relativeWidth } from './../services/dimensions'
import { login } from './../services/sessions'
import { inDatabase } from './../services/users'
import { baseUrl } from './../services/variables'

export default class Login extends Component<{}> {

  state = {
    email: '',
    password: '',
    loading: false
  }

  componentDidMount(){

  }

  _handleChanges = (key, value) => {
    this.setState({
      [key]: value
    })
  }

  render() {

    let { email, password, loading } = this.state;

    return (
      <View
        style={ styles.main }
      >
        <IbanTextInput
          label='Correo electronico'
          value={email}
          onChangeText={ email => this._handleChanges('email', email) }
          email={true}
          keyboardType="email-address"
          style={styles.field}
        />
        <IbanTextInput
          label='Contraseña'
          value={password}
          onChangeText={ password => this._handleChanges('password', password) }
          type='password'
          style={styles.field}
        />
        <IbanGreenRoundedButton
          value='Iniciar sesión'
          onPress={() => this.submit()}
          loading={loading}
          style={styles.button}
        ></IbanGreenRoundedButton>

        <IbanBlueLink
          style={styles.link}
          value='¿Olvidaste tu contraseña?'
        ></IbanBlueLink>
      </View>
    );
  }

  submit(){
    Keyboard.dismiss()

    let { email, password, loading } = this.state;

    if(loading){
      return
    }

    let { setAlert, navigation } = this.props;

    this.setState({loading: true})

    login(email, password, navigation)
      .then(response => {
        this.setState({loading: false})

        if(response.status === 200){
          setAlert('successAlert')

          setTimeout(() => {
            navigation.navigate('Wallet')
          }, 2000)
        }
      })
      .catch((error) => {
        inDatabase(email)
          .then((data) => {
            if(data.ok){
              navigation.navigate('WebLogin', {email, password})
            }else{

            }
            this.setState({loading: false})
          }).catch(() => {
            this.setState({loading: false})
            setAlert('errorPrompt')
          })
      });
  }
}

const styles = StyleSheet.create({
  main: {
    paddingBottom: 30
  },
  link: {

  },
  field: {
    marginTop: relativeWidth(16)
  },
  button: {
    marginTop: relativeWidth(34.5)
  }
});
