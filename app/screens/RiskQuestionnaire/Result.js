import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  StatusBar,
  ScrollView,
  Modal,
  ActivityIndicator,
  Alert
} from 'react-native';

import { relativeWidth } from './../../services/dimensions'
import { list } from './../../services/riskQuestions'
import Icon from 'react-native-vector-icons/Feather';
import Header from './Header'
import { finish, result } from './../../services/riskQuestions'


export default class Result extends Component<{}> {

  constructor(props){
    super(props)
    this.state = {
      loading: true,
      text: '',
      rating: 0
    }

    this.submit = this.submit.bind(this)
  }

  static navigationOptions = { header: null, gesturesEnabled: false };

  componentDidMount(){
    result()
      .then((response) => {
        this.setState({
          rating: response.rating,
          ratingName: response.rating_name,
          text: response.text,
          loading: false
        })
      })
      .catch(() => {

      })
  }

  submit() {
    const {navigation} = this.props

    this.setState({loading: true})
    finish(navigation)
      .then(() => {

      })
      .catch(() => {

      })
  }

  goBack() {
    const {navigation} = this.props
    const {number} = this.state
    if(number > 0){
      navigation.goBack()
    }
  }

  render() {
    const { loading, rating, text, ratingName } = this.state

    return (
      <View
        style={styles.container}
        behavior="padding"
      >
        <StatusBar barStyle="dark-content"/>

        <Header/>

        <View
          style={styles.body}
        >
          <Text style={styles.text}>Perfil de ahorrador</Text>
          <Text style={styles.score}>
            <Text style={styles.scoreColor}>{rating}</Text>
            {" "}/ 5
          </Text>

          <Text style={styles.text1}>{ratingName}</Text>
          <Text style={styles.text2}>{text}</Text>
          <Text style={styles.text3}>*Podrás volver a realizar el cuestionario más adelante</Text>

          <TouchableWithoutFeedback onPress={this.submit}>
            <View style={styles.button}>
              {
                loading
                  ?
                  <ActivityIndicator
                    color='#4CBA8D'
                    animating={true}
                    style={styles.buttonSpinner}
                  ></ActivityIndicator>
                  :
                  null
              }
              <Text style={styles.buttonText}>Continuar</Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    minHeight: '100%'
  },
  body: {
    backgroundColor: '#4CBA8D',
    position: 'absolute',
    width: '100%',
    top: relativeWidth(301),
    bottom: 0,
    paddingLeft: relativeWidth(20),
    paddingRight: relativeWidth(20),
  },
  navText: {
    color: 'white',
    fontSize: relativeWidth(18),
    paddingLeft: relativeWidth(18)
  },
  button: {
    marginTop: relativeWidth(123),
  },
  score: {
    fontSize: relativeWidth(50),
    color: 'white',
    textAlign: 'center',
    marginTop: relativeWidth(25)
  },
  scoreColor: {
    fontSize: relativeWidth(80),
    color: '#FFCC33'
  },
  text: {
    color: 'white',
    textAlign: 'center',
    marginTop: relativeWidth(24),
    fontSize: relativeWidth(19)
  },
  text1: {
    textAlign: 'center',
    color: 'white',
    fontSize: relativeWidth(15),
    marginTop: relativeWidth(-1)
  },
  text2: {
    textAlign: 'center',
    color: 'white',
    fontSize: relativeWidth(15),
    marginTop: relativeWidth(27)
  },
  text3: {
    textAlign: 'center',
    color: 'white',
    fontSize: relativeWidth(13),
    marginTop: relativeWidth(29)
  },

  button: {
    backgroundColor: 'white',
    borderRadius: relativeWidth(50),
    marginTop: relativeWidth(36),
    height: relativeWidth(60),
    justifyContent: 'center',
    alignItems: 'center',

    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.4,
    flexDirection: 'row',
    justifyContent: 'center'
  },
  buttonText: {
    color: "#4CBA8D",
    textAlign: 'center',
    fontSize: relativeWidth(23)
  },
  buttonSpinner: {
    marginRight: relativeWidth(10)
  }
});