import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  StatusBar,
  ScrollView,
  Modal,
  Alert
} from 'react-native';

import { relativeWidth } from './../../services/dimensions'
import { list, answer, result } from './../../services/riskQuestions'
import Icon from 'react-native-vector-icons/Feather';
import Spinner from '../../common/Spinner'
import Header from './Header'
import {finish} from "../../services/riskQuestions";


export default class RiskQuestionnaire extends Component {

  constructor(props){
    super(props)
    this.state = {
      questions: [],
      number: 0,
      loading: false
    }

    this.goBack = this.goBack.bind(this)
  }

  static navigationOptions = { header: null, gesturesEnabled: false };

  componentDidMount(){
    const {navigation} = this.props
    const { local, questions, number, userNumber } = navigation.state.params;
    if(local){
      this.setState({questions, number}, () => this.goToLastStep(userNumber))
    }else{
      list()
        .then(({questions, number}) => {
          this.setState({questions, number}, () => this.goToLastStep(number))
        })
        .catch()
    }
  }

  goToLastStep(lastQuestionNumber){
    const {navigation} = this.props
    const {number, questions} = this.state

    if(number < lastQuestionNumber){
      if(questions.length > number + 1){
        navigation.push('RiskQuestionnaire', {local: true, questions, number: number + 1, userNumber: lastQuestionNumber})
      }else{
        this.finish()
      }
    }
  }

  handleSelectOption = option => e => {
    const {navigation} = this.props
    const {questions, number, loading} = this.state

    if(loading){
      return
    }

    this.setState({loading: true})

    setTimeout(() => { // for better animation
      answer(questions[number].id, option.id)
        .then(response => {
          this.setState(
            {loading: false},
            () => {
              if(questions.length == number + 1){
                this.finish()
              }else{
                navigation.push('RiskQuestionnaire', {local: true, questions, number: number + 1})
              }
            }
          )
        })
        .catch()
    }, 1000)
  }

  finish(){
    const {navigation} = this.props

    navigation.push('RiskQuestionnaireResult')
  }
  goBack() {
    const {navigation} = this.props
    const {number} = this.state
    if(number > 0){
      navigation.goBack()
    }
  }

  render() {
    const { loading, questions, number } = this.state

    let question = questions[number] || {}
    let {text, options = []} = question

    return (
      <View
        style={styles.container}
        behavior="padding"
      >
        <StatusBar barStyle="dark-content"/>

        <Header/>

        <ScrollView
          keyboardShouldPersistTaps='always'
          style={styles.body}
        >
          <Text style={styles.question}>{text}</Text>
          {options.map((option, index) =>
            <TouchableWithoutFeedback
              key={index}
              onPress={this.handleSelectOption(option)}
            >
              <View
                style={styles.option}
              >
                <Text style={styles.optionText}>{option.text}</Text>
              </View>
            </TouchableWithoutFeedback>
          )}
        </ScrollView>

        <View style={styles.footer}>
          <TouchableWithoutFeedback
            onPress={this.goBack}
          >
            <View style={styles.footerIcon}>
              <Icon name="arrow-left" size={25} color="white" />
            </View>
          </TouchableWithoutFeedback>
          <View style={styles.footerTextContainer}>
            <Text style={styles.footerText} >{number + 1} de {questions.length}</Text>
          </View>
        </View>

        <Modal
          visible={loading}
          animationType={'fade'}
          transparent={true}
        >
          <Spinner/>
        </Modal>


      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    minHeight: '100%'
  },
  body: {
    backgroundColor: '#4CBA8D',
    position: 'absolute',
    width: '100%',
    top: relativeWidth(301),
    bottom: relativeWidth(60),
    paddingLeft: relativeWidth(20),
    paddingRight: relativeWidth(20),
  },
  footer: {
    backgroundColor: '#4CBA8D',
    height: relativeWidth(60),
    position: 'absolute',
    bottom: 0,
    width: '100%',
  },
  navText: {
    color: 'white',
    fontSize: relativeWidth(18),
    paddingLeft: relativeWidth(18)
  },
  button: {
    marginTop: relativeWidth(123),
  },
  footerIcon: {
    position: 'absolute',
    height: relativeWidth(60),
    width: relativeWidth(60),
    justifyContent: 'center',
    alignItems: 'center'
  },
  footerTextContainer: {
    height: relativeWidth(60),
    marginLeft: '20%',
    marginRight: '20%',

    justifyContent: 'center'
  },
  footerText: {
    textAlign: 'center',
    color: 'white'
  },

  question: {
    color: 'white',
    textAlign: 'center',
    fontSize: relativeWidth(18.5),
    marginTop: relativeWidth(23),
    marginBottom: relativeWidth(30.5),
  },
  option: {
    backgroundColor: 'white',
    marginBottom: relativeWidth(20),
    borderRadius: relativeWidth(30),
    paddingRight: relativeWidth(45),
    paddingLeft: relativeWidth(45),
    paddingTop: relativeWidth(8),
    paddingBottom: relativeWidth(8),

    minHeight: relativeWidth(53),
    justifyContent: 'center'
  },
  optionText: {
    color: '#4CBA8D',
    textAlign: 'center',
    fontSize: relativeWidth(14.5)
  }
});