import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  AsyncStorage
} from 'react-native';

import { relativeWidth } from './../../services/dimensions'


export default class Header extends Component<{}> {

  constructor(props){
    super(props)
    this.state = {
      userName: ''
    }
  }

  componentDidMount(){
    AsyncStorage.getItem('@iBANStore:current_user').then(user => {
      const {full_name} = JSON.parse(user);
      this.setState({userName: full_name})
    })
  }

  render() {
    const {userName} = this.state

    return (
      <View style={styles.header}>
        <Text style={styles.headerTitle}>{userName}, cuéntanos sobre ti</Text>
        <Text style={styles.headerSubtitle}>Para poder ofrecerte un plan personalizado primero necesitamos conocer tu perfil de ahorrador</Text>
        <Text style={styles.headerDescription}>No te lleverá más de 1 minuto.</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: 'white',
    height: relativeWidth(301),
  },
  headerTitle: {
    fontSize: relativeWidth(23),
    marginTop: relativeWidth(60),
    textAlign: 'center',
    backgroundColor: 'transparent',
    color: '#4CBA8D'
  },
  headerSubtitle: {
    fontSize: relativeWidth(19),
    marginTop: relativeWidth(45),
    textAlign: 'center',
    backgroundColor: 'transparent',
    color: 'rgba(1, 1, 1, .66)',
    lineHeight: relativeWidth(23.74),
  },
  headerDescription: {
    fontSize: relativeWidth(14),
    textAlign: 'center',
    backgroundColor: 'transparent',
    color: 'gray',
    marginTop: relativeWidth(26),
  }
});