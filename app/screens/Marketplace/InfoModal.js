import React, { Component } from 'react';

import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableWithoutFeedback,
  Alert
} from 'react-native';

import { relativeWidth as rw} from './../../services/dimensions'
import IbanGreenRoundedButton from '../../common/greenRoundedButton';
import FeatherIcon from 'react-native-vector-icons/Feather'

export default class ChoiceModal extends Component {

  constructor(props){
    super(props)
    this.state = {
      contacts: []
    }

    this.icons = {
      info: require('./images/modal/info.png'),
      rocket: require('./images/modal/rocket.png'),
      history: require('./images/modal/history.png')
    }
  }

  render() {

    let { onSubmit, title, description, loading, onClose, icon } = this.props;

    return (
      <View style={styles.body}>

        <TouchableWithoutFeedback onPress={onClose}>
          <FeatherIcon
            style={styles.topLeftIcon}
            name="x"
          />
        </TouchableWithoutFeedback>
       
        <TouchableWithoutFeedback onPress={onClose}>
          <View style={styles.topRightIcon}>
            <Text
              style={styles.topRightIconText}
            >
              Saltar
            </Text>
          </View>
        </TouchableWithoutFeedback>
        
        <View style={styles.imageContainer}>
          <Image
            style={[styles.image, icon == 'rocket' ? {marginTop: rw(-17), marginLeft: rw(6)} : 0]}
            source={this.icons[icon]}
          ></Image>
        </View>
       
        <Text
          style={styles.title}
        >
          {title}
        </Text>

        <Text
          style={styles.description}
        >
          {description}
        </Text>

        <IbanGreenRoundedButton
          value='Continuar'
          onPress={onSubmit}
          loading={loading}
          style={styles.button}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  body: {
    backgroundColor: 'white',
    height: '100%',
    width: '100%',

    alignItems: 'center',

    paddingLeft: rw(15),
    paddingRight: rw(15),
  },
  title: {
    marginTop: rw(41),
    fontSize: rw(19),
    fontWeight: '400',
    textAlign: 'center'
  },
  description: {
    color: '#8E8E93',
    textAlign: 'center',
    marginTop: rw(6),
    fontSize: rw(14)
  },
  button: {
    position: 'absolute',
    marginLeft: rw(15),
    marginRight: rw(15),
    bottom: rw(20),
    width: '100%'
  },
  imageContainer: {
    marginTop: rw(162),
    backgroundColor: '#339966',
    borderRadius: rw(103),
    width: rw(206),
    height: rw(206),
    alignItems: 'center',
    justifyContent: 'center',

    shadowOffset: {width: 0, height: 4},
    shadowOpacity: 0.2,
    shadowRadius: 4,
  },
  image: {
    height: rw(206)
  },
  topLeftIcon: {
    position: 'absolute',
    top: rw(31),
    left: rw(15),
    fontSize: rw(30)
  },
  topRightIcon: {
    position: 'absolute',
    top: rw(35),
    right: rw(15),
  },
  topRightIconText: {
    fontSize: rw(19)
  }
});
