import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  StatusBar,
  ScrollView,
  KeyboardAvoidingView,
  Alert,
  Keyboard,
  RefreshControl,
  TextInput,
  Modal
} from 'react-native';

import { relativeWidth as rw} from './../../services/dimensions'
import IbanGreenRoundedButton from '../../common/greenRoundedButton';
import FeatherIcon from 'react-native-vector-icons/Feather'

export default class Marketplace extends Component {

  constructor(props){
    super(props)

    this.state = {
      validationErrors: {},
      opportunity: {

      }
    }

    this.submit = this.submit.bind(this)
  }

  static navigationOptions = { header: null, gesturesEnabled: false };

  _handleChanges(k, v) {
    const {opportunity} = this.state
    this.setState({
      opportunity: {
        ...opportunity,
        [k]: v
      }
    })
  }

  submit() {
    const {navigation } = this.props
    navigation.navigate('MarketplaceFormStory')
  }

  render() {
    const { navigation } = this.props
    const { validationErrors, opportunity, loading} = this.state

    return (
      <KeyboardAvoidingView
        behavior="padding"
      >
        <View
          style={s.main}
        >
          <StatusBar barStyle="dark-content"/>

          <View style={s.topIconsContainer}>
            <TouchableWithoutFeedback 
              onPress={() => navigation.goBack()}
            >
              <FeatherIcon
                style={s.topLeftIcon}
                name="arrow-left"
              />
            </TouchableWithoutFeedback>
            <View style={s.progressContainer}>
              <View style={s.progress}></View>
            </View>
          </View>

          <Text style={s.title}>Historia</Text>
          <Text style={s.description}>(Video o imagen de lanzamiento)</Text>
        

          <ScrollView
            keyboardShouldPersistTaps='always'
            style={s.scroll}
          >

            <View style={s.field}>
              <Text style={s.fieldTitle}>
                Imagen de lanzamiento <Text style={s.fieldStar}>*</Text>
              </Text>
              <Text style={s.fieldDescription}>
                Suba una imagen para que aparezca en la parte superior de la página de su campaña. Resolución recomendada 695 x 460
              </Text>
              <View style={s.fieldInputContainer}>
                <View 
                  style={s.fieldButton}
                >
                  <Text style={s.fieldButtonText}>SELECCIONA TU IMAGEN</Text>
                </View>                
              </View>
              <Text style={s.fieldError}></Text>
            </View>

           
          </ScrollView>

          <IbanGreenRoundedButton
            value='Continuar'
            onPress={this.submit}
            loading={loading}
            style={s.button}
          />
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const s = StyleSheet.create({
  main: {
    height: '100%',
    backgroundColor: 'white',
  },
  scroll: {
    padding: rw(15),
    flex: 1
  },
  title: {
    textAlign: 'center',
    marginTop: rw(33),
    fontSize: rw(19),
  },
  description: {
    textAlign: 'center',
    color: 'rgba(1,1,1,.56)',
    fontSize: rw(16),
    marginBottom: rw(25)
  },

  button: {
    position: 'absolute',

    left: rw(15),
    right: rw(15),
    bottom: rw(20),
  },

  field: {
    marginTop: rw(12),
    marginBottom: rw(8),
  },
  fieldInputContainer: {
    marginTop: rw(7),
  },
  fieldTitle: {
    fontSize: rw(19),
  },
  fieldStar: {
    color: 'red'
  },
  fieldDescription: {
    marginTop: rw(4),
    color: '#818181',
    fontSize: rw(14),
    lineHeight: rw(20),
  },
  fieldInput: {
    marginTop: rw(14),
    fontSize: rw(16),
    flex: 1,
    borderWidth: 1,
    borderRadius: 3,
    borderColor: 'rgba(1,1,1,.17)',
    height: rw(43),
    paddingLeft: rw(10)
  },
  fieldButton: {
    marginTop: rw(14),
    backgroundColor: '#339966',
    height: rw(43),
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: rw(5),
    paddingRight: rw(5),
    shadowOffset: {width: 0, height: 4},
    shadowOpacity: 0.1,
    shadowRadius: 2,
    width: rw(220)
  },
  fieldButtonText: {
    width: 'auto',
    color: 'white',
    fontSize: rw(16)
  },
  fieldIcon: {
    color: '#339966',
    fontSize: rw(20),
    position: 'absolute',
    top: rw(9),
    left: rw(7)
  },
  fieldError: {
    marginTop: rw(5),
    fontSize: rw(12),
    color: 'rgba(1,1,1,.32)'
  },

  topIconsContainer: {
    alignItems: 'center',
    marginTop: rw(15),
    height: rw(30),
  },
  topLeftIcon: {
    position: 'absolute',
    top: rw(15),
    left: rw(15),
    color: 'rgba(1,1,1,.65)',
    fontSize: rw(30),
  },
  progressContainer: {
    overflow: 'hidden',
    borderRadius: rw(5),
    marginTop: rw(28),
    height: rw(4),
    width: rw(150),
    backgroundColor: '#EFEFF1'
  },
  progress: {
    height: '100%',
    width: '33%',
    position: 'absolute',
    top: 0,
    left: 0,
    backgroundColor: '#339966'
  }

});