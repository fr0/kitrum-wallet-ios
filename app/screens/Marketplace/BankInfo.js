import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  StatusBar,
  ScrollView,
  KeyboardAvoidingView,
  Alert,
  Keyboard,
  RefreshControl,
  TextInput,
  Modal
} from 'react-native';

import { relativeWidth as rw} from './../../services/dimensions'
import IbanGreenRoundedButton from '../../common/greenRoundedButton';
import FeatherIcon from 'react-native-vector-icons/Feather'
import { Dropdown } from 'react-native-material-dropdown'
import InfoModal from './InfoModal'

export default class Marketplace extends Component {

  constructor(props){
    super(props)

    this.state = {
      validationErrors: {},
      opportunity: {

      },
      modalOpened: false,
      modal2Opened: false
    }

    this.submit = this.submit.bind(this)
    this.openModal = this.openModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
    this.openModal2 = this.openModal2.bind(this)
    this.closeModal2 = this.closeModal2.bind(this)
  }

  static navigationOptions = { header: null, gesturesEnabled: false };

  _formatCurrency(currency){
    switch(currency){
      case 'USD':
        return '$'
      case 'EUR':
        return '€'
      case 'GBP':
        return '£'
    }
  }

  _handleChanges(k, v) {
    const {opportunity} = this.state
    this.setState({
      opportunity: {
        ...opportunity,
        [k]: v
      }
    })
  }

  submit() {
    const {navigation} = this.props
    this.setState(
      {modal2Opened: false},
      () => navigation.navigate('MarketplaceFormHistory')
    )
  }

  openModal() {
    this.setState({modalOpened: true})
  }

  closeModal() {
    this.setState({modalOpened: false})
  }

  openModal2() {
    this.setState({modalOpened: false, modal2Opened: true})
  }

  closeModal2() {
    this.setState({modal2Opened: false})
  }

  render() {
    const { navigation } = this.props
    const { validationErrors, opportunity, loading, modalOpened, modal2Opened } = this.state
    let data = [{value: 'EUR'}, {value: 'USD'}];

    return (
      <KeyboardAvoidingView
        behavior="padding"
      >
        <View
          style={s.main}
        >
          <StatusBar barStyle="dark-content"/>

          <View style={s.topIconsContainer}>
            <TouchableWithoutFeedback 
              onPress={() => navigation.goBack()}
            >
              <FeatherIcon
                style={s.topLeftIcon}
                name="arrow-left"
              />
            </TouchableWithoutFeedback>
            <View style={s.progressContainer}>
              <View style={s.progress}></View>
            </View>
          </View>

          <Text style={s.title}>Información Básica</Text>
        

          <ScrollView
            keyboardShouldPersistTaps='always'
            style={s.scroll}
          >
            <View style={s.field}>
              <Text style={s.fieldTitle}>
                Titulo de la Campaña <Text style={s.fieldStar}>*</Text>
              </Text>
              <Text style={s.fieldDescription}>
                ¿Cuál es el título de tu proyecto?
              </Text>
              <View style={s.fieldInputContainer}>
                <TextInput 
                  style={s.fieldInput}
                  keyboardType={'numeric'}
                  placeholder="El título es…"
                />                
              </View>
              <Text style={s.fieldError}></Text>
            </View>

            <View style={s.field}>
              <Text style={s.fieldTitle}>
              Imagen de Campaña <Text style={s.fieldStar}>*</Text>
              </Text>
              <Text style={s.fieldDescription}>
                Sube una imagen cuadrada que represente su campaña. La resolución recomendada es de 640 x 640, la resolución mínima es de 220 x 220.
              </Text>
              <View style={s.fieldInputContainer}>
                <View 
                  style={s.fieldButton}
                >
                  <Text style={s.fieldButtonText}>SELECCIONA TU IMAGEN</Text>
                </View>                
              </View>
              <Text style={s.fieldError}></Text>
            </View>

            <View style={s.field}>
              <Text style={s.fieldTitle}>
              Ubicación <Text style={s.fieldStar}>*</Text>
              </Text>
              <Text style={s.fieldDescription}>
                Elija la ubicación donde está ejecutando la campaña. Esta ubicación será visible en la página de su campaña para que su audiencia la vea.
              </Text>
              <View style={s.fieldInputContainer}>
                <TextInput 
                  style={s.fieldInput}
                  keyboardType={'numeric'}
                  placeholder="Ciudad"
                />   
                <TextInput 
                  style={s.fieldInput}
                  keyboardType={'numeric'}
                  placeholder="Pais"
                />                
              </View>
              <Text style={s.fieldError}></Text>
            </View>

            <View style={s.field}>
              <Text style={s.fieldTitle}>
              Categoria <Text style={s.fieldStar}>*</Text>
              </Text>
              <Text style={s.fieldDescription}>
                Para ayudar a los inversores a encontrar su oportunida, seleccione una categoría que mejor represente su proyecto.
              </Text>
              <View style={s.fieldInputContainer}>
                <Dropdown
                  containerStyle={s.fieldDropdown}
                  pickerStyle={s.fieldDropdownPicker}
                  label=""
                  data={data}
                  onChangeText={ val => this._handleChanges('document_type', val) }
                  error={validationErrors.document_type || ''}
                  fontSize={rw(16)}
                  labelFontSize={rw(12)}
                  labelHeight={rw(32)}
                  itemPadding={rw(8)}
                  value={opportunity.currency}
                  textColor='rgba(1,1,1,.17)'
                  itemColor='rgba(1,1,1,.17)'
                  renderBase={() => {
                    return (
                      <View style={dropdownStyles.base}>
                        <Text style={dropdownStyles.baseText}>
                          {opportunity.category ? opportunity.category : 'Seleccionar categoria'}
                        </Text>
                        <FeatherIcon
                          style={dropdownStyles.baseIcon}
                          name="chevron-down"
                        />
                      </View>
                    )
                  }}
                />            
              </View>
              <Text style={s.fieldError}></Text>
            </View>

            <View style={s.field}>
              <Text style={s.fieldTitle}>
              Tags <Text style={s.fieldStar}>*</Text>
              </Text>
              <Text style={s.fieldDescription}>
                Ingrese hasta cinco palabras clave que mejor describan su campaña. Estas etiquetas ayudarán con la organización y la visibilidad.
              </Text>
              <View style={s.fieldInputContainer}>
                <TextInput 
                  style={s.fieldInput}
                  keyboardType={'numeric'}
                  placeholder="Ingrese algunas etiquetas para su campaña."
                />                
              </View>
              <Text style={s.fieldError}></Text>
            </View>
           
            <View style={s.field}>
              <Text style={s.fieldTitle}>
              Duración de la campaña <Text style={s.fieldStar}>*</Text>
              </Text>
              <Text style={s.fieldDescription}>
                ¿Por cuántos días ejecutará su campaña? Puede ejecutar una campaña por cualquier cantidad de días, con un máximo de 60 días de duración.
              </Text>
              <View style={s.fieldInputContainer}>
                           
              </View>
              <Text style={s.fieldError}></Text>
            </View>

            <IbanGreenRoundedButton
              value='Continuar'
              onPress={this.openModal}
              loading={loading}
              style={s.button}
            />
          </ScrollView>

          <Modal
            visible={modalOpened}
            animationType={'fade'}
            transparent={true}
          >
            <InfoModal
              onSubmit={this.openModal2}
              onClose={this.closeModal}
              icon='rocket'
              title={"Lanza tu oportunidad antes\n13 de septiembre de 2018 8:59 p.m. PDT"}
              description="Inicia cuando esté listo antes de esta fecha. Las oportunidades solo pueden estar en borrador durante 6 meses antes de que caduquen y se eliminen de su cuenta. Ya no tendrás acceso a este borrador una vez que caduque."
              loading={loading}
            />
          </Modal>

          <Modal
            visible={modal2Opened}
            animationType={'fade'}
            transparent={true}
          >
            <InfoModal
              onSubmit={this.submit}
              onClose={this.closeModal2}
              icon='history'
              title={"Historia"}
              description="Escribe para qué se va a utilizar la financiación. Proporcionando información a los inversores del marketplace puede ayudarles a decidir si o no hacer una inversión."
              loading={loading}
            />
          </Modal>
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const dropdownStyles = StyleSheet.create({
  base: {
    justifyContent: 'center',
    paddingLeft: rw(8),
    borderWidth: 1,
    borderColor: 'rgba(1,1,1,.17)',
    borderRadius: 3,
    height: rw(43),
  },
  baseText: {
    color: 'rgba(1,1,1,.17)',
    fontSize: rw(18)
  },
  baseIcon: {
    color: 'gray',

    fontSize: rw(22),
    position: 'absolute',
    top: rw(10),
    right: rw(10),
  }
})

const s = StyleSheet.create({
  main: {
    height: '100%',
    backgroundColor: 'white',
  },
  scroll: {
    padding: rw(15),
    flex: 1
  },
  title: {
    textAlign: 'center',
    marginTop: rw(33),
    fontSize: rw(19),
  },

  button: {
    marginTop: rw(150),
    marginBottom: rw(50),
  },

  field: {
    marginTop: rw(12),
    marginBottom: rw(8),
  },
  fieldInputContainer: {
    marginTop: rw(7),
  },
  fieldTitle: {
    fontSize: rw(19),
  },
  fieldStar: {
    color: 'red'
  },
  fieldDescription: {
    marginTop: rw(4),
    color: '#818181',
    fontSize: rw(14),
    lineHeight: rw(20),
  },
  fieldInput: {
    marginTop: rw(14),
    fontSize: rw(16),
    flex: 1,
    borderWidth: 1,
    borderRadius: 3,
    borderColor: 'rgba(1,1,1,.17)',
    height: rw(43),
    paddingLeft: rw(10)
  },
  fieldButton: {
    marginTop: rw(14),
    backgroundColor: '#339966',
    height: rw(43),
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: rw(5),
    paddingRight: rw(5),
    shadowOffset: {width: 0, height: 4},
    shadowOpacity: 0.1,
    shadowRadius: 2,
    width: rw(220)
  },
  fieldButtonText: {
    width: 'auto',
    color: 'white',
    fontSize: rw(16)
  },
  fieldIcon: {
    color: '#339966',
    fontSize: rw(20),
    position: 'absolute',
    top: rw(9),
    left: rw(7)
  },
  fieldError: {
    marginTop: rw(5),
    fontSize: rw(12),
    color: 'rgba(1,1,1,.32)'
  },

  topIconsContainer: {
    alignItems: 'center',
    marginTop: rw(15),
    height: rw(30),
  },
  topLeftIcon: {
    position: 'absolute',
    top: rw(15),
    left: rw(15),
    color: 'rgba(1,1,1,.65)',
    fontSize: rw(30),
  },
  progressContainer: {
    overflow: 'hidden',
    borderRadius: rw(5),
    marginTop: rw(28),
    height: rw(4),
    width: rw(150),
    backgroundColor: '#EFEFF1'
  },
  progress: {
    height: '100%',
    width: '33%',
    position: 'absolute',
    top: 0,
    left: 0,
    backgroundColor: '#339966'
  }

});