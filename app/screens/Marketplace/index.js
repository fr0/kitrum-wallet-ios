import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableWithoutFeedback,
  StatusBar,
  ScrollView,
  KeyboardAvoidingView,
  Alert,
  AsyncStorage,
  TouchableOpacity,
  Keyboard,
  RefreshControl,
  TextInput
} from 'react-native';

import { relativeWidth } from './../../services/dimensions'
import { list as getProductAccounts } from './../../services/productAccounts'
import { profile } from './../../services/users'

import FeatherIcon from 'react-native-vector-icons/Feather'
import LinearGradient from 'react-native-linear-gradient'

export default class Marketplace extends Component {

  constructor(props){
    super(props)

    this.state = {
      productAccounts: [],
      fitnanceAccounts: [],
      refreshing: false
    }
  }

  static navigationOptions = { header: null, gesturesEnabled: false };

  componentDidMount(){
    let {navigation} = this.props
    this._retrieveProductAccounts();
    this._retrieveFitnanceAccounts();
  }

  _retrieveFitnanceAccounts(){
    let {onFirstCard, navigation} = this.props

    profile(navigation)
      .then(user => {
        this.setState({fitnanceAccounts: user.fitnance_accounts || []})


        if(user.fitnance_accounts.length > 0){
          card = user.fitnance_accounts[0]

          onFirstCard({
            title: card.name,
            balance: `${ this._formatPrice(card.balance) } ${ this._formatCurrency(card.currency || 'EUR') }`,
            iban: card.id,
            first_deposit_date: card.first_deposit_date
          })
        }
      })
      .catch(error => {

      })
  }

  _retrieveProductAccounts(){
    let {navigation} = this.props
    this.setState({refreshing: true});

    getProductAccounts()
      .then(productAccounts => {
        this.setState({productAccounts})
        this.setState({refreshing: false});
      })
      .catch(error => {
        this.setState({refreshing: false});
        handleRequestError(error, navigation)
      })
  }

  _formatPrice(price){
    price = price/100.0
    
    if(!price){
      price = 0
    }
    return price.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')
  }

  _formatCurrency(currency){
    switch(currency){
      case 'USD':
        return '$'
      case 'EUR':
        return '€'
      case 'GBP':
        return '£'
    }
  }

  _onRefresh() {
    this._retrieveProductAccounts();
  }

  render() {

    const { onSelectTab, navigation } = this.props

    return (
      <KeyboardAvoidingView
        behavior="padding"
      >
        <View
          style={s.main}
        >
          <StatusBar barStyle="dark-content"/>

          <View style={s.header}>
            <Text style={s.headerTitle}>Market Place</Text>

            <View style={s.filterInputContainer}>

              <TextInput
                style={s.filterInput}
                onChangeText={() => {}}
                value=''
                placeholder="Buscar"
              />

              <View style={s.filterInputIconContainer}>
                <FeatherIcon
                  style={s.filterInputIcon}
                  name="search"
                  color="gray"
                />
              </View>
              
              <View style={s.filterInputMicIconContainer}>
                <FeatherIcon
                  style={s.filterInputMicIcon}
                  name="mic"
                  color="gray"
                />
              </View>
              
            </View>

            <View style={s.topIconsContainer}>
              <TouchableWithoutFeedback
                onPress={() => navigation.navigate('MarketplaceForm')}
              >
                <FeatherIcon
                  style={s.topIcon}
                  name="file-plus"
                />
              </TouchableWithoutFeedback>
              <TouchableWithoutFeedback
                onPress={() => navigation.navigate('MarketplaceFilters')}
              >
                <FeatherIcon
                  style={s.topIcon}
                  name="filter"
                />
              </TouchableWithoutFeedback>
              
            </View>
          </View>

          <ScrollView
            keyboardShouldPersistTaps='always'
            style={s.scroll}
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this._onRefresh.bind(this)}
              />
            }
          >
            {[1, 2, 3].map((_, index) => 
              <TouchableWithoutFeedback
                key={index}  
                onPress={() => navigation.navigate('MarketplaceDrilldown')}
              >
                <View
                  style={s.card}
                >
                  <Image 
                    source={require('./images/marketplace.png')}
                    style={s.cardImage}
                  />
                  <View style={s.cardOverlay}></View>
                  <LinearGradient colors={['transparent', 'transparent', 'rgba(1,1,1,.8)']} style={s.cardGradient}/>
                  <View style={s.aLabel}>
                    <Text style={s.aLabelText}>A+</Text>
                  </View>
                  <View style={s.cardPriceContainer}>
                    <Text style={s.cardPrice}>+5,68 €</Text>
                    <View style={s.cardLabelContainer}>
                      <Text style={s.cardLabel}>Mensual</Text>
                    </View>
                  </View>

                  <Text style={s.cardPriceDescription}>6% por 48 Messes</Text>
                  <Text style={s.cardName}>PSS Meribel #2</Text>
                  <Text style={s.cardDescription}>Es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno</Text>
                </View>
              </TouchableWithoutFeedback>
            )}

            <View style={{height: 50}}></View>
          </ScrollView>
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const s = StyleSheet.create({
  main: {
    height: '100%'
  },
  form: {
    flex: 1,
    paddingLeft: relativeWidth(17),
    paddingRight: relativeWidth(17),
    backgroundColor: 'white',
    minHeight: '100%',
    paddingBottom: relativeWidth(70),
  },
  header: {
    height: relativeWidth(179),
    backgroundColor: 'white',
    padding: relativeWidth(15)
  },
  headerTitle: {
    color: 'rgba(1,1,1,.65)',
    fontSize: relativeWidth(37),
    fontWeight: 'bold',
    marginTop: relativeWidth(59),
  },
  scroll: {
    position: 'absolute',
    top: relativeWidth(179),
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'white',

    padding: relativeWidth(15)
  },
 
  card: {
    height: relativeWidth(186),
    borderRadius: relativeWidth(8),
    overflow: 'hidden',
    marginBottom: relativeWidth(17)
  },
  cardOverlay: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgba(1,1,1,.4)'
  },
  cardGradient: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  cardImage: {
    width: '100%',
    height: '100%',
    resizeMode: 'cover',

    position: 'absolute',
    top: 0
  },
  aLabel: {
    position: 'absolute',
    top: -6,
    right: -5,
    backgroundColor: '#33CC99',
    width: relativeWidth(56),
    height: relativeWidth(56),
    borderRadius: relativeWidth(28),
    justifyContent: 'center'
  },
  aLabelText: {
    color: 'white',
    textAlign: 'center',
    backgroundColor: 'transparent',
    fontSize: relativeWidth(20)
  },
  cardPriceContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: relativeWidth(18)
  },
  cardPrice: {
    backgroundColor: 'transparent',
    color: '#2CC096',
    fontSize: relativeWidth(30),
    marginLeft: relativeWidth(12),
    fontWeight: '500'
  },
  cardLabelContainer: {
    backgroundColor: 'rgba(1,1,1,.3)',
    borderRadius: 10,
    height: relativeWidth(20), 
    justifyContent: 'center',
    paddingLeft: relativeWidth(10),
    paddingRight: relativeWidth(10),
    marginLeft: relativeWidth(15)
  },
  cardLabel: {
    backgroundColor: 'transparent',
    color: 'white',
    fontSize: relativeWidth(12)
  },
  cardPriceDescription: {
    backgroundColor: 'transparent',
    color: 'white',
    fontSize: relativeWidth(10),
    marginTop: relativeWidth(6),
    marginLeft: relativeWidth(15),
    fontWeight: '600'
  },
  cardName:{
    backgroundColor: 'transparent',
    color: 'white',
    fontSize: relativeWidth(24),
    marginTop: relativeWidth(31),
    marginLeft: relativeWidth(15),
  },
  cardDescription: {
    backgroundColor: 'transparent',
    color: 'white',
    fontSize: relativeWidth(13),
    lineHeight: relativeWidth(17),
    marginTop: relativeWidth(0),
    marginLeft: relativeWidth(15),
  },
    
  topIconsContainer: {
    flexDirection: 'row',
    position: 'absolute',
    top: relativeWidth(35),
    right: relativeWidth(10),
  },
  topIcon: {
    color: '#33CC99',
    fontSize: relativeWidth(25),
    marginLeft: relativeWidth(20)
  },
  filterInputContainer: {
    marginTop: relativeWidth(14),
    backgroundColor: '#E8E9EA',
    borderRadius: relativeWidth(9),
    height: relativeWidth(40),
    justifyContent: 'center',
  },
  filterInput: {
    marginLeft: relativeWidth(35),
    marginRight: relativeWidth(35),
    height: '100%'
  },
  filterInputIconContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: relativeWidth(40),
    height: '100%',

    justifyContent: 'center',
    alignItems: 'center'
  },
  filterInputIcon: {
    color: 'gray',
    fontSize: relativeWidth(22)
  },
  filterInputMicIcon: {
    color: 'gray',
    fontSize: relativeWidth(22)
  },
  filterInputMicIconContainer: {
    position: 'absolute',
    top: 0,
    right: 0,
    width: relativeWidth(40),
    height: '100%',

    justifyContent: 'center',
    alignItems: 'center'
  }
});