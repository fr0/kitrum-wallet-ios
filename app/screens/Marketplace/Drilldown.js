import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableWithoutFeedback,
  StatusBar,
  ScrollView,
  KeyboardAvoidingView,
  Alert,
  AsyncStorage,
  TouchableOpacity,
  Keyboard,
  RefreshControl,
  TextInput
} from 'react-native';

import { relativeWidth } from './../../services/dimensions'
import { list as getProductAccounts } from './../../services/productAccounts'
import { profile } from './../../services/users'
import IbanGreenRoundedButton from '../../common/greenRoundedButton'
import Progress from '../../common/Progress'

import Ionicon from 'react-native-vector-icons/Ionicons'
import FeatherIcon from 'react-native-vector-icons/Feather'

export default class Marketplace extends Component {

  constructor(props){
    super(props)

    this.state = {
      productAccounts: [],
      fitnanceAccounts: [],
      refreshing: false
    }
  }

  static navigationOptions = { header: null, gesturesEnabled: false };

  componentDidMount(){
    let {navigation} = this.props
    this.submit()
  }

  _formatPrice(price){
    price = price/100.0
    
    if(!price){
      price = 0
    }
    return price.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')
  }

  _formatCurrency(currency){
    switch(currency){
      case 'USD':
        return '$'
      case 'EUR':
        return '€'
      case 'GBP':
        return '£'
    }
  }

  submit() {
    const {navigation} = this.props
    navigation.navigate('MarketplaceOpportunity')
  }

  _onRefresh() {

  }

  render() {

    const { onSelectTab, navigation, loading } = this.props

    return (
      <KeyboardAvoidingView
        behavior="padding"
      >
        <View
          style={s.main}
        >
          <StatusBar barStyle="dark-content"/>

          <View style={s.header}>

            <View style={s.topIconsContainer}>
              <TouchableWithoutFeedback 
                onPress={() => navigation.goBack()}
              >
                <FeatherIcon
                  style={s.topLeftIcon}
                  name="chevron-left"
                />
              </TouchableWithoutFeedback>
              <FeatherIcon
                style={s.topRightIcon}
                name="more-vertical"
              />
            </View>
          </View>

          <View style={s.cardContainer}>
            <View style={s.card}>
              <Image 
                source={require('./images/marketplace.png')}
                style={s.cardImage}
              />
              <View style={s.aLabel}>
                <Text style={s.aLabelText}>A+</Text>
              </View>
            </View>
          </View>

          <ScrollView
            keyboardShouldPersistTaps='always'
            style={s.scroll}
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this._onRefresh.bind(this)}
              />
            }
          >
            
            <Image 
              source={require('./images/avatar.png')}
              style={s.avatar}
            />
            <Text style={s.username}>ErnestoP</Text>
            <Text style={s.name}>PSS Meribel #2</Text>
            <Text style={s.description}>Es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500.</Text>
            <Text style={s.more}>Ver más</Text>

            <Progress
              style={s.progress}
              filledColor="#33CC99"
              sliderColor="rgba(1,1,1,.14)"
              value={20}
            />

            <View style={s.table}>
              <View style={[s.cell, {borderLeftWidth: 0, paddingLeft: 0}]}>
                <Text style={s.cellTitle}>15 %</Text>
                <Text style={s.cellDescription}>COMPLETADO</Text>
              </View>
              <View style={s.cell}>
                <Text style={s.cellTitle}>0.6 %</Text>
                <Text style={s.cellDescription}>INTERES</Text>
              </View>
              <View style={s.cell}>
                <Text style={s.cellTitle}>+5,68 €</Text>
                <Text style={s.cellDescription}>GANANCIA</Text>
              </View>
            </View>

            <View style={s.buttonsContainer}>
              <View style={s.heartContainer}>
                <Ionicon
                  style={s.heart}
                  name="ios-heart"
                />
              </View>
              
              <IbanGreenRoundedButton
                value='INVERTIR'
                onPress={() => this.submit()}
                loading={loading}
                style={s.button}
              ></IbanGreenRoundedButton>
            </View>
            
          </ScrollView>

          
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const s = StyleSheet.create({
  main: {
    height: '100%',
    backgroundColor: 'white'
  },
  form: {
    flex: 1,
    paddingLeft: relativeWidth(17),
    paddingRight: relativeWidth(17),
    backgroundColor: 'white',
    minHeight: '100%',
    paddingBottom: relativeWidth(70),
  },
  header: {
    backgroundColor: '#33CC99',
    height: relativeWidth(174),
    padding: relativeWidth(15)
  },
  scroll: {
    position: 'absolute',
    top: relativeWidth(276),
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'white',

    padding: relativeWidth(15)
  },
 
  cardContainer: {
    height: relativeWidth(185),
    borderRadius: relativeWidth(8),
    marginTop: relativeWidth(-85),
    marginLeft: relativeWidth(15),
    marginRight: relativeWidth(15),
    shadowOffset: {width: 0, height: 4},
    shadowOpacity: 0.1,
    shadowRadius: 4,
  },
  card: {
    height: '100%',
    borderRadius: relativeWidth(8),
    overflow: 'hidden',
  },
  cardImage: {
    width: '100%',
    height: '100%',
    resizeMode: 'cover',

    position: 'absolute',
    top: 0
  },
  aLabel: {
    position: 'absolute',
    top: -6,
    right: -5,
    backgroundColor: 'rgb(252,208,76)',
    width: relativeWidth(56),
    height: relativeWidth(56),
    borderRadius: relativeWidth(28),
    justifyContent: 'center'
  },
  aLabelText: {
    color: 'white',
    textAlign: 'center',
    backgroundColor: 'transparent',
    fontSize: relativeWidth(20)
  },

  avatar: {
    width: relativeWidth(61),
    height: relativeWidth(61),
    position: 'absolute',
    top: 0,
    left: 0,

    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.1,
    shadowRadius: 4,
  },
  username: {
    marginTop: relativeWidth(6),
    fontSize: relativeWidth(15),
    color: '#31A5E2',
    marginLeft: relativeWidth(77)
  },
  name:{
    color: 'rgba(1,1,1,.65)',
    fontSize: relativeWidth(32),
    marginTop: relativeWidth(0),

    marginLeft: relativeWidth(77)
  },
  more: {
    marginTop: relativeWidth(12),
    fontSize: relativeWidth(17),
    color: '#31A5E2'
  },
  description: {
    marginTop: relativeWidth(16),
    color: 'rgba(1,1,1,.72)',
    fontSize: relativeWidth(14),
    lineHeight: relativeWidth(19),
  },
    
  topIconsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: relativeWidth(15),
  },
  topLeftIcon: {
    color: 'white',
    fontSize: relativeWidth(35),
  },
  topRightIcon: {
    color: 'white',
    fontSize: relativeWidth(23),
    marginTop: relativeWidth(4)
  },
  table: {
    marginTop: relativeWidth(25),
    flexDirection: 'row'
  },
  cell: {
    width: '33.3%',
    borderLeftWidth: 1,
    borderColor: '#C7C7CC',
    paddingLeft: relativeWidth(15),
    paddingTop: relativeWidth(4),
    paddingBottom: relativeWidth(4),
  },
  cellTitle: {
    color: '#2CC096',
    fontSize: relativeWidth(30)
  },
  cellDescription: {
    marginTop: relativeWidth(4),
    color: 'rgba(1,1,1,.47)',
    fontSize: relativeWidth(14)
  },

  buttonsContainer: {
    marginTop: relativeWidth(14),
    flexDirection: 'row',
    alignItems: 'flex-end',
  },
  heartContainer: {
    width: relativeWidth(50),
    height: relativeWidth(53),
    backgroundColor: '#C7C7CC',

    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 3,
  },
  heart: {
    fontSize: relativeWidth(40),
    color: 'rgba(1,1,1,.24)',
  },
  button: {
    flex: 1,
    borderRadius: 3,
    marginLeft: relativeWidth(8)
  },

  progress: {
    marginTop: relativeWidth(34)
  }
});