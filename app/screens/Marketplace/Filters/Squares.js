import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  ActivityIndicator,
  Image,
  PanResponder,
  Animated,
  Easing,
  Alert
} from 'react-native';

import { relativeWidth as rw } from './../../../services/dimensions'

export default class Squares extends Component {

  constructor(props) {
    super(props)
    this.state = {}
  }
  
  handleChanges = index => () => {
    const {values, onChange} = this.props
    values[index].selected = !values[index].selected
    onChange(values)
  }

  render() {
    const {values} = this.props

    return (
      <View style={s.container}>
        {values.map((item, index) => (
          <TouchableWithoutFeedback key={index} onPress={this.handleChanges(index)}>
            <View style={[s.square, {backgroundColor: item.selected ? '#33CC99' : 'white'}]}>
              <Text style={[s.squareText, {color: item.selected ? 'white' : 'black'}]}>{item.label}</Text>
            </View>
          </TouchableWithoutFeedback>
        ))}
      </View>
    )
  }
}

const s = StyleSheet.create({
  container: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  square: {
    width: '25%',
    height: rw(83.5),
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderRightWidth: 1,
    borderColor: 'lightgray'
  },
  squareText: {
    fontSize: rw(26)
  }
});
