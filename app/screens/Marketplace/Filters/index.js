import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  StatusBar,
  ScrollView,
  KeyboardAvoidingView,
  Alert,
  Animated,
  Easing,
  Switch
} from 'react-native';

import { list as getCountries } from './../../../services/countries'
import { relativeWidth as rw } from './../../../services/dimensions'
import IbanGreenRoundedButton from '../../../common/greenRoundedButton'
import FeatherIcon from 'react-native-vector-icons/Feather'
import Slider from '../../../common/Slider2'
import Squares from './Squares'
import Dropdown from './Dropdown'

export default class Filters extends Component {

  constructor(props){
    super(props)
    this.state = {
      card: {},
      validationErrors: {},
      blocks: [
        {
          open: true,
          alwaysOpen: true, 
          title: 'Ganancia mensual',
          height: new Animated.Value(1), 
          type: 'slider', 
          value: 1,
          min: 0, 
          max: 1000, 
          sym: '€', 
          step: .01
        },
        {
          open: true,
          alwaysOpen: true, 
          title: 'Interés',
          height: new Animated.Value(1), 
          type: 'slider', 
          value: 1,
          min: 0, 
          max: 1,
          sym: '%',
          step: .01
        },
        {
          open: false, 
          alwaysOpen: false,
          title: 'Tipo de riesgo',
          height: new Animated.Value(0), 
          type: 'squares',
          values: [
            {label: 'A*', selected: false}, 
            {label: 'A', selected: false},
            {label: 'B', selected: false},
            {label: 'C', selected: false},
            {label: 'D', selected: false},
            {label: 'E', selected: false}
          ]
        },
        {
          open: false, 
          alwaysOpen: false, 
          title: 'País',             
          height: new Animated.Value(0), 
          type: 'dropdown', 
          values: []
        },
        {
          open: false, 
          alwaysOpen: false, 
          title: 'Divisa',           
          height: new Animated.Value(0), 
          type: 'dropdown', 
          values: ['USD', 'EUR']
        },
      ]
    }
  }

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Filtrar',
      headerTitleStyle: {color: 'white', fontWeight: '400'},
      headerStyle: {backgroundColor: '#339966'},
      headerLeft: (
        <TouchableWithoutFeedback
          onPress={() => navigation.goBack()}
        >
          <View>
            <Text style={s.navText}>Cancelar</Text>
          </View>
        </TouchableWithoutFeedback>
      ),
      headerRight: (
        <TouchableWithoutFeedback
          onPress={() => navigation.goBack()}
        >
          <View>
            <Text style={s.navText}>Descartar</Text>
          </View>
        </TouchableWithoutFeedback>
      ),
      gesturesEnabled: false
    }
  };

  componentDidMount(){
    let { blocks } = this.state
    getCountries().then(countries => {
      blocks[3].values = countries.map(c => c.name)
      this.setState({blocks})
    })
  }

  _handleChanges = (index, key) => value => {
    let { blocks } = this.state 
    blocks[index][key] = value 
    this.setState({blocks})
  }

  _submit(){

  }

  openBlock(index) {
    const { blocks } = this.state
    const block = blocks[index]

    if(block.alwaysOpen){
      return 
    }

    blocks[index].open = !blocks[index].open

    this.setState(
      {blocks},
      () => {
        Animated
          .timing(
            block.height,
            {
              toValue: block.open ? 1 : 0,
              duration: 300,
              easing: Easing.linear
            }
          )
          .start()
      }
    )
  }

  renderBlock(index) {
    const block = this.state.blocks[index]

    let element = null

    switch(block.type){
      case 'slider':
        element = <Slider
          min={block.min}
          max={block.max}
          sym={block.sym}
          step={block.step}
          value={block.value}
          onChange={this._handleChanges(index, 'value')}
        />
        break
      case 'squares':
        element = <Squares
          values={block.values}
          onChange={this._handleChanges(index, 'values')}
        />
        break
      case 'dropdown':
        element = <Dropdown
          values={block.values}
          value={block.value}
          onChange={this._handleChanges(index, 'value')}
        />
        break
    }

    return (
      <View style={s.block} key={index}>
        <TouchableWithoutFeedback onPress={() => this.openBlock(index)}>
          <Animated.View style={[
            s.blockTitle,
            {
              backgroundColor: block.height.interpolate({
                inputRange: [0, 1],
                outputRange: ['white', '#EFEFF4']
              })
            }
          ]}>
            <Text style={s.blockTitleText}>{block.title}</Text>
            {block.alwaysOpen ? 
              <FeatherIcon
                style={s.blockInfoIcon}
                name="info"
                color="gray"
              />
            :
              <Switch
                style={s.blockSwitch}
                value={block.open}
                onValueChange={() => this.openBlock(index)}
              />
            }
          </Animated.View>
        </TouchableWithoutFeedback>
        <Animated.View
            style={[
              s.blockBody,
              {
                height: block.height.interpolate({
                  inputRange: [0, 1],
                  outputRange: [0, rw(164)]
                })
              }
            ]}
          >
          {element}
        </Animated.View>
      </View>
    )
  }

  render() {

    const { loading, card, validationErrors, blocks } = this.state
    let { navigation } = this.props

    return (
      <KeyboardAvoidingView
        style={s.container}
        behavior="padding"
      >
        <ScrollView
          keyboardShouldPersistTaps='always'
          style={s.scroll}
        >
          <StatusBar barStyle="light-content"/>
          
          {blocks.map((block, index) => this.renderBlock(index))}

        </ScrollView>


        <IbanGreenRoundedButton
          onPress={ () => this._submit() }
          value='APLICAR FILTRO'
          loading={loading}
          style={s.button}
        ></IbanGreenRoundedButton>
      </KeyboardAvoidingView>
    );
  }
}

const s = StyleSheet.create({
  container: {
    height: '100%'
  },
  scroll: {
    backgroundColor: 'white',
    flex: 1,
    marginBottom: rw(50)
  },
  navText: {
    color: 'white',
    fontSize: rw(18),
    paddingLeft: rw(18),
    paddingRight: rw(18),
  },
  button: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    borderRadius: 0
  },
  block: {
    borderBottomWidth: rw(1),
    borderColor: 'lightgray',
  },
  blockTitle: {
    height: rw(50),
    backgroundColor: '#EFEFF4',
    paddingLeft: rw(15),
    justifyContent: 'center',
  },
  blockTitleText: {
    fontSize: rw(18)
  },
  blockInfoIcon: {
    position: 'absolute',
    top: rw(10),
    right: rw(10),
    color: '#007AFF',
    fontSize: rw(26)
  },
  blockSwitch: {
    position: 'absolute',
    top: rw(8),
    right: rw(10),
  },
  blockBody: {
    overflow: 'hidden'
  }
  
});