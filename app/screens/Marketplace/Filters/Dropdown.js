import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  ActivityIndicator,
  Image,
  PanResponder,
  Animated,
  Easing,
  Alert
} from 'react-native';

import { relativeWidth as rw } from './../../../services/dimensions'
import { Dropdown } from 'react-native-material-dropdown'

export default class FiltersDropdown extends Component {

  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    const {values, value, onChange} = this.props
    let data = values.map(v => ({value: v}))

    return (
      <View style={s.container}>
        <Dropdown
          containerStyle={s.dropdownContainer}
          pickerStyle={s.picker}
          label=""
          data={data}
          onChangeText={ val => onChange(val) }
          fontSize={rw(16)}
          labelFontSize={rw(12)}
          labelHeight={rw(32)}
          itemPadding={rw(8)}
          value={value}
          textColor='rgba(1,1,1,.17)'
          itemColor='rgba(1,1,1,.17)'
          renderBase={() => {
            return (
              <View style={s.base}>
                <Text style={s.baseText}>
                  {value ? value : 'Seleccionar categoria'}
                </Text>
                <Image
                  source={require('../images/icons/dropdown.png')}
                  style={s.baseIcon}
                />
              </View>
            )
          }}
        />   
      </View>
    )
  }
}

const s = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
    width: '100%'
  },
  dropdownContainer: {

  },
  base: {
    justifyContent: 'center',
    paddingLeft: rw(8),
    borderBottomWidth: 2,
    borderColor: 'rgba(1,1,1,.17)',
    borderRadius: 3,
    height: rw(43),
    borderColor: '#33CC99'
  },
  baseText: {
    color: 'rgba(1,1,1,.7)',
    fontSize: rw(30),
    marginRight: rw(30),
    marginLeft: rw(10),
  },
  baseIcon: {
    position: 'absolute',
    top: rw(10),
    right: rw(10),

    width: rw(15),
    height: rw(26),
  }
});