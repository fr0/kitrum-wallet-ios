import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableWithoutFeedback,
  StatusBar,
  ScrollView,
  KeyboardAvoidingView,
  Alert,
  AsyncStorage,
  TouchableOpacity,
  Keyboard,
  RefreshControl,
  TextInput
} from 'react-native';

import { relativeWidth as rw } from './../../services/dimensions'
import { profile } from './../../services/users'
import IbanGreenRoundedButton from '../../common/greenRoundedButton'
import Progress from '../../common/Progress'

import Ionicon from 'react-native-vector-icons/Ionicons'
import FeatherIcon from 'react-native-vector-icons/Feather'
import Slider from '../../common/Slider2'

export default class Opportunity extends Component {

  constructor(props){
    super(props)

    this.state = {
      opportunity:{
        price: 3
      },
      refreshing: false
    }
  }

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Opportunidad',
      headerTitleStyle: {color: 'white', fontWeight: '400'},
      headerStyle: {backgroundColor: '#33CC99'},
      headerLeft: (
        <TouchableWithoutFeedback
          onPress={() => navigation.goBack()}
        >
          <FeatherIcon
            style={s.topLeftIcon}
            name="chevron-left"
          />
        </TouchableWithoutFeedback>
      ),
      gesturesEnabled: false
    }
  };


  componentDidMount(){
    let {navigation} = this.props
  }

  _formatPrice(price){
    price = price/100.0
    
    if(!price){
      price = 0
    }
    return price.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')
  }

  _formatCurrency(currency){
    switch(currency){
      case 'USD':
        return '$'
      case 'EUR':
        return '€'
      case 'GBP':
        return '£'
    }
  }

  submit() {
    Alert.alert(
      'Confirmación de inversión',
      'Deleting this app will also delete its data.',
      [
        {text: 'Cancel', onPress: () => {}, style: 'cancel'},
        {text: 'Acceptar', onPress: () => {}}
      ],
      { cancelable: false }
    )
  }

  _handleChanges(k, v) {
    const {opportunity} = this.state
    this.setState({
      opportunity: {
        ...opportunity,
        [k]: v
      }
    })
  }

  render() {

    const { onSelectTab, navigation, loading } = this.props

    return (
      <KeyboardAvoidingView
        behavior="padding"
      >
        <View
          style={s.main}
        >
          <StatusBar barStyle="light-content"/>

          <ScrollView
            keyboardShouldPersistTaps='always'
            style={s.scroll}
          >
            <Text style={s.name}>PSS Meribel #2</Text>
           
            <Progress 
              style={s.progress}
              filledColor="#33CC99"
              sliderColor="rgba(1,1,1,.14)"
              value={20}
            />

            <View style={s.table}>
              <View style={[s.cell, {borderLeftWidth: 0, paddingLeft: 0}]}>
                <Text style={s.cellTitle}>15 %</Text>
                <Text style={s.cellDescription}>COMPLETADO</Text>
              </View>
              <View style={s.cell}>
                <Text style={s.cellTitle}>0.6 %</Text>
                <Text style={s.cellDescription}>INTERES</Text>
              </View>
              <View style={s.cell}>
                <Text style={s.cellTitle}>+5,68 €</Text>
                <Text style={s.cellDescription}>GANANCIA</Text>
              </View>
            </View>

            <View style={s.sliderContainer}>
              <Text style={s.sliderText}>Monto a invertir</Text>
              <Slider
                min={1}
                max={30}
                sym={'€'}
                step={.01}
                value={3}
                onChange={v => this._handleChanges('price', v)}
                color="#7FD9F2"
              />
            </View>
              
            <IbanGreenRoundedButton
              value='INVERTIR'
              onPress={() => this.submit()}
              loading={loading}
              style={s.button}
            ></IbanGreenRoundedButton>
            
          </ScrollView>

          
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const s = StyleSheet.create({
  main: {
    height: '100%',
    backgroundColor: 'white'
  },
  scroll: {
    flex: 1,
    backgroundColor: 'white',
    padding: rw(15)
  },
  name:{
    color: 'rgba(1,1,1,.65)',
    fontSize: rw(32),
    marginTop: rw(30),
  },
  
  topLeftIcon: {
    color: 'white',
    fontSize: rw(35),
  },
  table: {
    marginTop: rw(38),
    flexDirection: 'row'
  },
  cell: {
    width: '33.3%',
    borderLeftWidth: 1,
    borderColor: '#C7C7CC',
    paddingLeft: rw(15),
    paddingTop: rw(4),
    paddingBottom: rw(4),
  },
  cellTitle: {
    color: '#2CC096',
    fontSize: rw(30)
  },
  cellDescription: {
    marginTop: rw(4),
    color: 'rgba(1,1,1,.47)',
    fontSize: rw(14)
  },

  button: {
    flex: 1,
    borderRadius: 3,
    marginLeft: rw(8),
    marginTop: rw(50)
  },

  progress: {
    marginTop: rw(46)
  },
  sliderContainer: {
    marginTop: rw(58),
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.2,
    shadowRadius: 4,
    paddingBottom: rw(75)
  },
  sliderText: {
    marginTop: rw(38),
    fontSize: rw(20),
    textAlign: 'center'
  }
});