import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  StatusBar,
  ScrollView,
  KeyboardAvoidingView,
  Alert,
  Keyboard,
  RefreshControl,
  TextInput,
  Modal
} from 'react-native';

import { relativeWidth as rw} from './../../services/dimensions'
import IbanGreenRoundedButton from '../../common/greenRoundedButton';
import FeatherIcon from 'react-native-vector-icons/Feather'
import { Dropdown } from 'react-native-material-dropdown';
import InfoModal from './InfoModal'

export default class Marketplace extends Component {

  constructor(props){
    super(props)

    this.state = {
      validationErrors: {},
      opportunity: {
        currency: 'EUR'
      },
      modalOpened: false
    }

    this.openModal = this.openModal.bind(this)
    this.submit = this.submit.bind(this)
    this.closeModal = this.closeModal.bind(this)
  }

  static navigationOptions = { header: null, gesturesEnabled: false };

  _formatCurrency(currency){
    switch(currency){
      case 'USD':
        return '$'
      case 'EUR':
        return '€'
      case 'GBP':
        return '£'
    }
  }

  _handleChanges(k, v) {
    const {opportunity} = this.state
    this.setState({
      opportunity: {
        ...opportunity,
        [k]: v
      }
    })
  }

  submit() {
    const {navigation} = this.props 
    this.setState(
      {modalOpened: false},
      () => navigation.navigate('MarketplaceFormBankInfo')
    )
  }

  openModal() {
    this.setState({modalOpened: true})
  }

  closeModal() {
    this.setState({modalOpened: false})
  }

  render() {
    const { navigation } = this.props
    const { validationErrors, opportunity, loading, modalOpened } = this.state
    let data = [{value: 'EUR'}, {value: 'USD'}];

    return (
      <KeyboardAvoidingView
        behavior="padding"
      >
        <View
          style={s.main}
        >
          <StatusBar barStyle="dark-content"/>

          <View style={s.topIconsContainer}>
            <TouchableWithoutFeedback 
              onPress={() => navigation.goBack()}
            >
              <FeatherIcon
                style={s.topLeftIcon}
                name="arrow-left"
              />
            </TouchableWithoutFeedback>
          </View>

          <Text style={s.title}>Crea tu oportunidad</Text>
          <Text style={s.subtitle}>Crear oportunidades para proyectos o compras
personales</Text>
         

          <ScrollView
            keyboardShouldPersistTaps='always'
            style={s.scroll}
          >
            <View style={s.field}>
              <Text style={s.fieldTitle}>
                ¿Cuánto dinero necesitas para financiar{'\n'}tu oportunidad?
              </Text>

              <View style={s.fieldInputContainer}>
                <TextInput 
                  style={[s.fieldInput, s.numericFieldInput]}
                  keyboardType={'numeric'}
                />

                <View style={s.fieldDropdownContainer}>
                  <Dropdown
                    containerStyle={s.fieldDropdown}
                    pickerStyle={s.fieldDropdownPicker}
                    label=""
                    data={data}
                    onChangeText={ val => this._handleChanges('document_type', val) }
                    error={validationErrors.document_type || ''}
                    fontSize={rw(16)}
                    labelFontSize={rw(12)}
                    labelHeight={rw(32)}
                    itemPadding={rw(8)}
                    value={opportunity.currency}
                    textColor='white'
                    itemColor='white'
                    renderBase={() => {
                      return (
                        <View style={dropdownStyles.base}>
                          <Text style={dropdownStyles.baseText}>{opportunity.currency}</Text>
                          <FeatherIcon
                            style={dropdownStyles.baseIcon}
                            name="chevron-down"
                          />
                        </View>
                      )
                    }}
                  />
                </View>

                <Text style={s.fieldIcon}>€</Text>
              </View>
              
              <Text style={s.fieldError}>Mínimo 500 €.</Text>
            </View>

            <View style={s.field}>
              <Text style={s.fieldTitle}>
                ¿Cuál es el título de tu oportunidad?
              </Text>
              <View style={s.fieldInputContainer}>
                <TextInput 
                  style={s.fieldInput}
                  placeholder={`El título es…`}
                />
              </View>
              <Text style={s.fieldError}>50 Caracteres máximo.</Text>
            </View>

            <IbanGreenRoundedButton
              value='Crear oportunidad'
              onPress={this.openModal}
              loading={loading}
              style={s.button}
            />
          </ScrollView>

          <Modal
            visible={modalOpened}
            animationType={'fade'}
            transparent={true}
          >
            <InfoModal
              onSubmit={this.submit}
              onClose={this.closeModal}
              icon='info'
              title="Información Básica"
              description="La información básica es clave: muchos usuarios del marketplace seleccionan una oportunidad basado sobre la primera impresión que recibe al buscar por el marketplace."
              loading={loading}
            />
          </Modal>
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const dropdownStyles = StyleSheet.create({
  base: {
    justifyContent: 'center',
    height: '100%',
    paddingLeft: rw(8),
  },
  baseText: {
    color: 'white',
    fontSize: rw(18)
  },
  baseIcon: {
    color: 'white',

    fontSize: rw(22),
    position: 'absolute',
    top: rw(10),
    right: rw(10),
  }
})

const s = StyleSheet.create({
  main: {
    height: '100%',
    backgroundColor: 'white',

    padding: rw(15)
  },
  form: {
    flex: 1,
    paddingLeft: rw(17),
    paddingRight: rw(17),
    backgroundColor: 'white',
    minHeight: '100%',
    paddingBottom: rw(70),
  },
  scroll: {
    flex: 1
  },
  title: {
    marginTop: rw(12),
    fontSize: rw(38),
    fontWeight: 'bold',
    color: 'rgba(1,1,1,.65)'
  },
  subtitle: {
    marginTop: rw(11),
    fontSize: rw(17),
    lineHeight: rw(22)
  },
 
  topIconsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: rw(15),
  },
  topLeftIcon: {
    color: 'rgba(1,1,1,.65)',
    fontSize: rw(30),
  },

  button: {
    marginTop: rw(150)
  },

  field: {
    marginTop: rw(44),
  },
  fieldDropdownContainer: {
    width: rw(100)
  },
  fieldDropdown: {
    borderRadius: 3,
    backgroundColor: '#339966',
    marginLeft: rw(10),

    height: rw(43),
  },
  fieldDropdownPicker: {
    backgroundColor: '#339966'
  },
  fieldInputContainer: {
    marginTop: rw(7),
    flexDirection: 'row'
  },
  fieldTitle: {
    color: '#818181',
    fontSize: rw(14),
    lineHeight: rw(20),
  },
  fieldInput: {
    flex: 1,
    borderWidth: 1,
    borderRadius: 3,
    borderColor: 'rgba(1,1,1,.17)',
    height: rw(43),
    paddingLeft: rw(10)
  },
  fieldIcon: {
    color: '#339966',
    fontSize: rw(20),
    position: 'absolute',
    top: rw(9),
    left: rw(7)
  },
  numericFieldInput: {
    paddingLeft: rw(25)
  },
  fieldError: {
    marginTop: rw(5),
    fontSize: rw(12),
    color: 'rgba(1,1,1,.32)'
  }

});