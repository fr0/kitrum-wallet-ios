import React, { Component } from 'react';

import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  Image,
  Alert,
  ScrollView
} from 'react-native';
import IbanTextInput from '../../common/TextInput';

import { relativeWidth } from './../../services/dimensions'
import {api_id, api_code, unnaxUrl, unnaxCallbackUrl} from '../../services/variables'
import IbanBankIcon from '../../common/BankIcon';

export default class ChoiceModal extends Component<{}> {

  constructor(props){
    super(props)
    this.state = {
      contacts: []
    }
  }

  componentDidMount(){
    this.loadContacts()
  }

  loadContacts(){

  }

  render() {

    let { onPress, navigation: {navigate}, onBankSelect, onContactsSelect } = this.props;
    let { contacts } = this.state;

    return (
      <View style={[styles.overlay]}>

        <View style={styles.body}>
          <Text style={styles.title}>Seleccionar tipo de destinatario</Text>

          <View style={styles.container}>

            <TouchableWithoutFeedback
              onPress={() => onBankSelect()}
            >
              <View
                style={[styles.containerItem, {borderRightWidth: 1}]}
              >
                <Image
                  style={ styles.headerImage }
                  source={require('./images/ic-account-balance-24px.png')}
                ></Image>
                <Text
                  style={styles.containerText}
                >Cuenta bancaria</Text>
              </View>
            </TouchableWithoutFeedback>

            <TouchableWithoutFeedback
              onPress={() => onContactsSelect()}
            >
              <View
                style={[styles.containerItem]}
              >
                <Image
                  style={ styles.headerImage }
                  source={require('./images/ic_contacts.png')}
                ></Image>
                <Text
                  style={styles.containerText}
                >Contacto</Text>
              </View>
            </TouchableWithoutFeedback>

          </View>
        </View>

        <TouchableWithoutFeedback onPress={() => onPress()}>
          <View style={{height: relativeWidth(390)}}>

          </View>
        </TouchableWithoutFeedback>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  overlay: {
    flexDirection: 'column-reverse',
    backgroundColor: 'rgba(1, 1, 1, 0.7)',
    position: 'absolute',
    width: '100%',
    height: '100%',
    top: 0,
    left: 0,
    paddingLeft: relativeWidth(15),
    paddingRight: relativeWidth(15),
    paddingBottom: relativeWidth(40),
  },
  body: {
    backgroundColor: 'white',
    height: relativeWidth(214),
    borderRadius: relativeWidth(6),
    backgroundColor: '#F7FCFA'
  },
  title: {
    marginTop: relativeWidth(20),
    fontSize: relativeWidth(18),
    textAlign: 'center'
  },

  container: {
    marginTop: relativeWidth(49),
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  containerText: {
    color: 'rgb(159,167,179)',
    fontSize: relativeWidth(13),
    marginTop: relativeWidth(11),
  },
  containerItem: {
    width: '50%',
    alignItems: 'center',
    borderColor: 'rgba(159,167,179,.5)',
  },
  contactAvatar: {
    width: relativeWidth(54),
    height: relativeWidth(54),
    backgroundColor: 'rgba(0, 0, 0, .08)',
    borderRadius: relativeWidth(27),
    marginTop: relativeWidth(28)
  }
});
