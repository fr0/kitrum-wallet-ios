import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableWithoutFeedback,
  StatusBar,
  ScrollView,
  KeyboardAvoidingView,
  Alert,
  Modal,
  TextInput
} from 'react-native';

import { relativeWidth } from './../../services/dimensions'
import { baseUrl } from './../../services/variables'
import IbanBankIcon from '../../common/BankIcon';
import DisabledTextInput from '../../common/DisabledTextInput';
import SelectContact from './SelectContact';
import SelectBankAccount from './SelectBankAccount';
import ChoiceModal from './ChoiceModal';

import moment from 'moment';

export default class BankInput extends Component<{}> {

  constructor(props){
    super(props)

    this.state = {
      modalOpened: false,
      modalType: '',
    }

    this._onTouch = this._onTouch.bind(this)
  }

  componentWillUnmount(){
    this.setState({modalOpened: false})
  }

  _onTouch(){
    let {source} = this.props

    switch(source){
      case 'bankAccountsAndContacts':
        this.setState({
          modalOpened: true,
          modalType: 'choice'
        })
        break;
      case 'bankAccounts':
        this.setState({
          modalOpened: true,
          modalType: 'banks'
        })
        break;
      default:

    }
    // 'bankAccountsAndContacts'
    // 'bankAccounts'
    //
    // 'contacts'
    // 'banks'
    // 'choice'
  }

  _onChange(value, type){
    let {onChange} = this.props

    this.setState({modalOpened: false})
    onChange({
      ...value,
      type
    })
  }

  render() {

    let {error, label, value, navigation, source, onChange, exceptId} = this.props
    let {modalOpened, modalType} = this.state

    return (
      <View style={styles.container}>
        <Text style={[styles.sectionTitle, value ? null : styles.blankLabel]}>{ label } </Text>

        <View
          style={styles.accountField}
        >
          <DisabledTextInput
            label=''
            onTouch={this._onTouch}
            error={error || ''}
            style={styles.field}
          />

          {
            value ?
              value.type === 'contact' ?
                <Image
                  style={ styles.icon }
                  source={{uri: `${ baseUrl}${ value.avatar.url }`}}
                ></Image>
              :
                <IbanBankIcon
                  bankId={value.bank_id}
                  style={styles.icon}
                />
            : null
          }

          { value ?
              value.type === 'contact' ?
                <Text style={styles.accountFieldName}>{ `${value.first_name} ${ value.last_name}` }</Text>
              :
                <Text style={styles.accountFieldName}>{ value.name }</Text>
            : null
          }

          {value ?
            <Text style={styles.accountFieldNumber}>{ value.id }</Text>
            : null}

        </View>

        <Modal
          visible={modalOpened && modalType === 'contacts'}
          animationType={'fade'}
          transparent={true}
        >
          <SelectContact
            onPress={() => this.setState({modalOpened: false})}
            navigation={this.props.navigation}
            onSelect={v => this._onChange(v, 'contact')}
          />
        </Modal>

        <Modal
          visible={modalOpened && modalType === 'banks'}
          animationType={'fade'}
          transparent={true}
        >
          <SelectBankAccount
            onPress={() => this.setState({modalOpened: false})}
            navigation={this.props.navigation}
            onSelect={v => this._onChange(v, 'bank')}
            exceptId={exceptId}
          />
        </Modal>

        <Modal
          visible={modalOpened && modalType === 'choice'}
          animationType={'fade'}
          transparent={true}
        >
          <ChoiceModal
            onPress={() => this.setState({modalOpened: false})}
            navigation={this.props.navigation}
            onBankSelect={() => this.setState({modalOpened: true, modalType: 'banks'})}
            onContactsSelect={() => this.setState({modalOpened: true, modalType: 'contacts'})}
          />
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {

  },
  sectionTitle: {
    fontSize: relativeWidth(17),
    marginTop: relativeWidth(25),
  },
  field: {
    marginTop: relativeWidth(24),
    marginBottom: relativeWidth(-2)
  },
  button: {
    marginTop: relativeWidth(40)
  },

  accountField: {
    marginTop: relativeWidth(-14)
  },
  accountFieldIcon: {
    position: 'absolute',
    left: relativeWidth(5),
    top: relativeWidth(38),
  },
  accountFieldName: {
    position: 'absolute',
    left: relativeWidth(50),
    top: relativeWidth(35),
  },
  accountFieldNumber: {
    position: 'absolute',
    left: relativeWidth(50),
    top: relativeWidth(55),
    color: 'gray'
  },
  blank: {
    // marginTop: relativeWidth(-74),
  },
  
  blankLabel: {
    position: 'absolute',
    top: relativeWidth(18),
    fontSize: relativeWidth(16),
    color: 'rgba(0, 0, 0, .60)'
  },
  icon: {
    width: relativeWidth(40),
    height: relativeWidth(40),
    backgroundColor: 'rgba(0, 0, 0, .08)',
    borderRadius: relativeWidth(20),

    position: 'absolute',
    left: relativeWidth(5),
    top: relativeWidth(34),
  }
});