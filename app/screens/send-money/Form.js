import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableWithoutFeedback,
  StatusBar,
  ScrollView,
  KeyboardAvoidingView,
  Alert,
  Modal,
  TextInput
} from 'react-native';

import { relativeWidth } from './../../services/dimensions'
import { create } from './../../services/transactions'
import NavBackButton from '../../common/NavBackButton';
import IbanTextInput from '../../common/TextInput';
import ConfettiAlert from '../../common/ConfettiAlert';
import IbanGreenRoundedButton from '../../common/greenRoundedButton';
import IbanBankIcon from '../../common/BankIcon';
import BankInput from './BankInput';

export default class SendMoney extends Component {

  state = {
    menuItemIndex: 0,
    transaction: {
      amount: 0,
      from: null
    },
    validationErrors: {},
    modalOpened: false,
    successModalOpened: false
  }

  static navigationOptions = { header: null, gesturesEnabled: false };

  componentDidMount(){

  }

  componentWillUnmount(){
    this.setState({successModalOpened: false})
  }

  _handleChanges(k, v){
    let {transaction, validationErrors} = this.state

    if(k === 'amount' && v === ''){
      v = '0'
    }

    this.setState({
      transaction: {
        ...transaction,
        [k]: v
      },
      validationErrors: {
        ...validationErrors,
        [k]: null
      }
    })
  }

  _submit(){
    const {transaction} = this.state

    this.setState({loading: true})

    create(transaction)
      .then(() => {
        this.setState({
          successModalOpened: true,
          loading: false
        })
      })
      .catch(error => {
        this.setState({
          validationErrors: error.validation_errors,
          loading: false
        })
      })
  }

  render() {

    let { onSelectTab, navigation } = this.props;
    let {goBack} = navigation
    const { menuItemIndex, transaction, validationErrors, modalOpened, loading, successModalOpened } = this.state;

    return (
      <KeyboardAvoidingView
        style={styles.container}
        behavior="padding"
        style={ styles.main }
      >
        <View
          style={ styles.header }
        >
          <StatusBar
            barStyle="light-content"
          />

          <View style={styles.headerButtons}>
            <NavBackButton
              title=''
              style={[styles.navBackButton, styles.headerButton]}
              onPress={goBack}
            />
            <View style={ styles.headerButton }>
              <Text style={ styles.headerButtonText }>Enviar dinero</Text>
            </View>
            <View style={ styles.headerButton }>

            </View>
          </View>

          <View style={styles.amountContainer}>
            <Text
              adjustsFontSizeToFit={true}
              numberOfLines={1}
              style={styles.headerTitleAmount}
            >
              { `${transaction.amount} ${'$'}`}
            </Text>
            <TextInput
              style={styles.headerInputAmount}
              keyboardType={'numeric'}
              onChangeText={ val => this._handleChanges('amount', val) }
              returnKeyType='done'
            />
          </View>

        </View>

        <View style={ styles.menu }>
          <TouchableWithoutFeedback onPress={() => this.setState({menuItemIndex: 0}) }>
            <View style={[styles.menuItem, menuItemIndex === 0 ? styles.menuItemActive : null ]}>
              <Text style={styles.menuItemText}>Nueva</Text>
            </View>
          </TouchableWithoutFeedback>

          <TouchableWithoutFeedback onPress={() => this.setState({menuItemIndex: 1}) }>
            <View style={[styles.menuItem, menuItemIndex === 1 ? styles.menuItemActive : null ]}>
              <Text style={styles.menuItemText}>Reciente</Text>
            </View>
          </TouchableWithoutFeedback>
        </View>

        <ScrollView
          keyboardShouldPersistTaps='always'
          style={styles.scroll}
        >
          <View style={styles.form}>

            <BankInput
              label='Cuenta'
              value={transaction.from}
              onChange={ val => this._handleChanges('from', val) }
              error={(validationErrors.from || []).join('. ') }
              style={styles.field}
              navigation={navigation}
              source={'bankAccounts'}
              exceptId={ transaction.to ? transaction.to.id : null }
            />

            <BankInput
              label='Enviar a'
              value={transaction.to}
              onChange={ val => this._handleChanges('to', val) }
              error={(validationErrors.to || []).join('. ') }
              style={styles.field}
              navigation={navigation}
              source={'bankAccountsAndContacts'}
              exceptId={ transaction.from ? transaction.from.id : null }
            />

            <IbanTextInput
              label='Comentario o motivo'
              value={transaction.comment}
              onChangeText={ val => this._handleChanges('comment', val) }
              error={(validationErrors.comment || []).join('. ') }
              style={styles.field}
            />

            <IbanGreenRoundedButton
              onPress={ () => this._submit() }
              value='ENVIAR'
              loading={loading}
              style={styles.button}
            ></IbanGreenRoundedButton>

            <Modal
              visible={successModalOpened}
              animationType={'fade'}
              transparent={true}
            >
              <ConfettiAlert
                onClose={goBack}
              />
            </Modal>

          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  main: {
    paddingBottom: 30,
    height: '100%',
  },
  header: {
    backgroundColor: '#339966',
    paddingTop: relativeWidth(28),
    paddingLeft: relativeWidth(20),
    paddingRight: relativeWidth(20),
    height: relativeWidth(230),
  },
  menu: {
    backgroundColor: '#rgb(0, 132, 82)',
    height: relativeWidth(70),
    flexDirection: 'row',
    paddingLeft: relativeWidth(20),
    paddingRight: relativeWidth(20)
  },
  menuItem: {
    alignItems: 'center',
    justifyContent: 'space-around',
    paddingLeft: relativeWidth(20),
    paddingRight: relativeWidth(20),
  },
  menuItemActive: {
    borderBottomWidth: 3,
    borderColor: 'lightgreen'
  },
  menuItemText: {
    color: 'white',
    fontSize: relativeWidth(17),
  },
  navBackButton: {
    width: 20,
    height: 20,
  },
  headerButtonText: {
    color: 'white',
    fontSize: relativeWidth(19),
    marginTop: relativeWidth(8),
  },
  headerButtons: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: relativeWidth(50)
  },
  form: {
    flex: 1,
    backgroundColor: '#F6F9FC',
    minHeight: '100%',
    paddingLeft: relativeWidth(20),
    paddingRight: relativeWidth(20),
  },
  headerContent: {
    marginTop: relativeWidth(3),
  },
  headerTitleSmall: {
    color: 'white',
    fontSize: relativeWidth(14.5)
  },
  amountContainer: {
    marginTop: relativeWidth(32),
    height: relativeWidth(56),
    alignItems: 'center'
  },
  headerInputAmount: {
    color: 'white',
    textAlign: 'center',
    fontSize: relativeWidth(56),
    height: relativeWidth(56),
    opacity: 0,

    position: 'absolute',
    top: relativeWidth(17),
    width: '100%'
  },
  headerTitleAmount: {
    color: 'white',
    textAlign: 'center',
    height: relativeWidth(90),
    fontSize: relativeWidth(56),
    width: '90%',
    flex: 1,
    flexDirection: 'row'
  },
  scroll: {

  },
  sectionTitle: {
    fontSize: relativeWidth(17),
    marginTop: relativeWidth(25),
  },
  field: {
    marginTop: relativeWidth(24),
    marginBottom: relativeWidth(-2)
  },
  button: {
    marginTop: relativeWidth(40)
  },
});