import React, { Component } from 'react';

import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  Image,
  Alert,
  ScrollView
} from 'react-native';
import IbanTextInput from '../../common/TextInput';

import { relativeWidth } from './../../services/dimensions'
import {baseUrl} from '../../services/variables'
import {list} from '../../services/contacts'
import IbanBankIcon from '../../common/BankIcon';

export default class SendMoneySelectContact extends Component<{}> {

  constructor(props){
    super(props)
    this.state = {
      contacts: []
    }

    this._loadContacts = this._loadContacts.bind(this)
  }

  componentDidMount(){
    this._loadContacts()
  }

  _loadContacts(){

    list()
      .then(contacts => {
        this.setState({contacts})
      })
      .catch(() => {})
  }

  render() {

    let { onPress, onSelect, navigation: {navigate} } = this.props;
    let { contacts } = this.state;

    return (
      <View style={[styles.overlay]}>

        <View style={styles.body}>
          <Text style={styles.title}>Seleccionar Contacto</Text>

          <View style={styles.contacts}>
            <ScrollView
              horizontal={true}
            >
              {contacts.map((contact, index) =>
                <TouchableWithoutFeedback
                  key={index}
                  onPress={() => onSelect(contact)}
                >
                  <View
                    style={styles.contact}
                  >
                    <Image
                      style={ styles.contactAvatar }
                      source={{uri: `${ baseUrl}${ contact.avatar.url }`}}
                    ></Image>

                    <Text style={styles.accountTitle}>{ `${contact.first_name} ${ contact.last_name}`}</Text>
                  </View>
                </TouchableWithoutFeedback>
              )}
            </ScrollView>

          </View>

          <TouchableWithoutFeedback onPress={() => { onPress(); navigate('SelectContact') }}>
            <View style={styles.button}>
              <Text style={styles.buttonText}>+ Nuevo contacto</Text>
            </View>
          </TouchableWithoutFeedback>
        </View>

        <TouchableWithoutFeedback onPress={() => onPress()}>
          <View style={{height: relativeWidth(390)}}>

          </View>
        </TouchableWithoutFeedback>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  overlay: {
    flexDirection: 'column-reverse',
    backgroundColor: 'rgba(1, 1, 1, 0.7)',
    position: 'absolute',
    width: '100%',
    height: '100%',
    top: 0,
    left: 0,
    paddingLeft: relativeWidth(15),
    paddingRight: relativeWidth(15),
    paddingBottom: relativeWidth(40),
  },
  body: {
    backgroundColor: 'white',
    height: relativeWidth(282),
    borderRadius: relativeWidth(6),
    backgroundColor: '#F7FCFA'
  },
  title: {
    marginTop: relativeWidth(20),
    fontSize: relativeWidth(18),
    textAlign: 'center'
  },
  button: {
    paddingTop: relativeWidth(17),
    paddingBottom: relativeWidth(15),
    marginTop: relativeWidth(19),
    marginLeft: relativeWidth(20),
    marginRight: relativeWidth(20),
    alignItems: 'center',
    borderRadius: relativeWidth(6),
    backgroundColor: 'white'
  },
  buttonText: {
    fontSize: relativeWidth(18),
  },
  contacts: {
    borderBottomWidth: 1,
    borderColor: '#DCE6F0',
    height: relativeWidth(151),
  },
  contact: {
    width: 90,
    alignItems: 'center'
  },
  accountTitle: {
    textAlign: 'center',
    fontSize: relativeWidth(12),
    color: 'gray',
    marginTop: relativeWidth(10)
  },
  contactAvatar: {
    width: relativeWidth(54),
    height: relativeWidth(54),
    backgroundColor: 'rgba(0, 0, 0, .08)',
    borderRadius: relativeWidth(27),
    marginTop: relativeWidth(28)
  }
});
