import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableWithoutFeedback,
  StatusBar,
  ScrollView,
  KeyboardAvoidingView,
  AsyncStorage
} from 'react-native';
import IbanBlueLink from '../common/blueLink';
import IbanButtonGroup from '../common/buttonGroup';
import SplashScreen from 'react-native-splash-screen';

import Login from './../screens/Login';
import Signup from './../screens/Signup';
import KYC from './../screens/signup/KYC';

import KYCsuccessAlert from './../screens/signup/KYCsuccessAlert';
import KYCsuccessPrompt from './../screens/signup/KYCsuccessPrompt';
import KYCerrorPrompt from './../screens/signup/KYCerrorPrompt';
import KYCerror from './../screens/signup/KYCerror';

import { relativeWidth } from './../services/dimensions'
import { profile } from './../services/users'

export default class LoginLayout extends Component<{}> {

  constructor(props) {
    super(props);
    this.state = {
      screen: 'login',
      alertScreen: ''
    }

    SplashScreen.hide()
  }

  static navigationOptions = { header: null, gesturesEnabled: false };

  _navigateTo = (screen) => {
    this.setState({
      screen
    })
  }

  componentDidMount(){
    const { navigation } = this.props;

    profile(navigation)
      .then(response => {
        navigation.navigate('Wallet')
      })
      .catch(error => {})
  }

  render() {

    let { screen, alertScreen } = this.state;

    let currentScreen;
    let currentHeader;

    switch(screen){
      case 'signup':
        currentScreen = (
          <Signup
            navigation={this.props.navigation}
            navigate={ (page) => this._navigateTo(page) }
            setAlert={ (alertScreen) => this.setState({alertScreen}) }
          />
        )
        break;
      default:
        currentScreen = (
          <Login
            navigation={this.props.navigation}
            navigate={ (page) => this._navigateTo(page) }
            setAlert={ (alertScreen) => this.setState({alertScreen}) }
          />
        )
    }

    let alertScreenComponent = null;

    switch(alertScreen) {
      case 'successAlert':
        alertScreenComponent = (
          <KYCsuccessAlert/>
        )
        break;
      case 'errorPrompt':
        alertScreenComponent = (
          <KYCerrorPrompt
            onAccept={() => this.setState({alertScreen: ''}) }
          />
        )
        break;
    }

    return (
      <KeyboardAvoidingView
        behavior="padding"
      >
        <StatusBar
          barStyle="light-content"
        />

        <View
          style={ styles.main }
          keyboardShouldPersistTaps='always'
        >
          <View
            style={ styles.header }
          >
            <Image
              style={ styles.headerImage }
              source={require('../images/LoginBackground.png')}
            ></Image>

            <View style={styles.headerLogo}>
              <IbanButtonGroup
                style={styles.headerButtons}
                items={[
                  {value: 'Iniciar sesión', action: () => this._navigateTo('login'), active: screen == 'login' },
                  {value: 'Registrar', action: () => this._navigateTo('signup'), active: screen == 'signup'}
                ]}
              />
              <Image
                style={styles.ibanLogo}
                source={require('../images/img_logo.png')}
              />
              <Image
                style={styles.ibanLogoText}
                source={require('../images/img_logo_text.png')}
              />
            </View>

          </View>

          <ScrollView
            style={styles.form}
          >
            { currentScreen }
          </ScrollView>
        </View>

        { alertScreenComponent }

      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  main: {
    flexDirection: 'column',
    minHeight: '100%',
    backgroundColor: 'white'
  },
  header:{
    flex: 1,
    alignItems: 'center',
    height: relativeWidth(354),
    overflow: 'hidden'
  },
  form: {
    flex: 1,
    padding: 10
  },
  headerImage: {
    width: '100%',
    height: '110%'
  },
  headerLogo: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    alignItems: 'center'
  },
  headerButtons: {
    height: relativeWidth(35),
    marginTop: relativeWidth(53),
    marginRight: relativeWidth(15),
    marginLeft: relativeWidth(15),
  },
  ibanLogo: {
    marginTop: relativeWidth(40),
    height: relativeWidth(95),
    width: relativeWidth(95),
  },
  ibanLogoText: {
    marginTop: relativeWidth(17),
    height: relativeWidth(20),
    width: relativeWidth(150),
  }
});
