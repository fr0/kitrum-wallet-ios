import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableWithoutFeedback,
  StatusBar,
  ScrollView,
  KeyboardAvoidingView,
  AsyncStorage,
  Alert
} from 'react-native';
import SplashScreen from 'react-native-splash-screen';

import Profile from './../screens/Profile';

import KYCsuccessAlert from './../screens/signup/KYCsuccessAlert';
import KYCsuccessPrompt from './../screens/signup/KYCsuccessPrompt';
import KYCerrorPrompt from './../screens/signup/KYCerrorPrompt';
import KYCerror from './../screens/signup/KYCerror';

import { relativeWidth } from './../services/dimensions'

export default class LoginLayout extends Component {

  constructor(props) {
    super(props);
    this.state = {
      screen: 'login',
      alertScreen: '',
      signupFlow: false
    }

    SplashScreen.hide()
  }

  static navigationOptions = { header: null, gesturesEnabled: false };

  componentDidMount(){
    const signupFlow = this.props.navigation.state && this.props.navigation.state.params
      ?
      this.props.navigation.state.params.signupFlow
      :
      false

    this.setState({signupFlow})
  }

  render() {

    let { alertScreen, signupFlow } = this.state;
    let alertScreenComponent = null;

    switch(alertScreen) {
      case 'successPrompt':
        alertScreenComponent = (
          <KYCsuccessPrompt
            onAccept={() => this.setState({alertScreen: ''})}
          />
        )
        break;
      case 'errorPrompt':
        alertScreenComponent = (
          <KYCerrorPrompt
            onAccept={() => this.setState({alertScreen: ''}) }
          />
        )
        break;
    }

    return (
      <KeyboardAvoidingView
        behavior="padding"
      >
        <StatusBar
          barStyle="light-content"
        />

        <View
          style={ styles.main }
          keyboardShouldPersistTaps='always'
        >
          <View
            style={ styles.header }
          >
            <Image
              style={ styles.headerImage }
              source={require('../images/kyc-header.png')}
            ></Image>

            <View style={styles.headerKYC}>
              <Text style={styles.headerKYCTitle}>Tus datos</Text>
              <Text style={styles.headerKYCText}>
                Asegúrate de introducir los datos correctamente, ya que se utilizarán para verificar tu identidad.
              </Text>

              {signupFlow ? null :
                <TouchableWithoutFeedback onPress={() => goBack()}>
                  <View style={styles.headerKYCBackContainer}>
                    <Text style={styles.headerKYCBack}>
                      Saltar
                    </Text>
                  </View>
                </TouchableWithoutFeedback>
              }

            </View>

          </View>

          <ScrollView
            style={styles.form}
          >
            <Profile
              navigation={this.props.navigation}
              signupFlow={signupFlow}
              setAlert={ (alertScreen) => this.setState({alertScreen}) }
            />
          </ScrollView>
        </View>

        { alertScreenComponent }

      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  main: {
    flexDirection: 'column',
    minHeight: '100%',
    backgroundColor: 'white'
  },
  header: {
    height: relativeWidth(230)
  },
  headerImage: {
    width: '100%',
    height: '100%'
  },
  form: {
    flex: 1,
    padding: 10
  },
  headerLogo: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    alignItems: 'center'
  },
  headerKYC: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    paddingTop: relativeWidth(40),
    paddingLeft: relativeWidth(16.5),
    paddingRight: relativeWidth(16.5),
    backgroundColor: 'transparent'
  },
  headerKYCTitle: {
    color: 'white',
    fontSize: relativeWidth(30),
    marginTop: relativeWidth(60)
  },
  headerKYCText: {
    color: 'white',
    marginTop: relativeWidth(13),
    fontSize: relativeWidth(15),
    lineHeight: relativeWidth(21),
  },
  headerKYCBack: {
    color: 'white',
    fontSize: relativeWidth(18.2)
  },
  headerKYCBackContainer: {
    position: 'absolute',
    top: relativeWidth(35),
    right: relativeWidth(15)
  }
});
