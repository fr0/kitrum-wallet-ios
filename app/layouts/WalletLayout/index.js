import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableWithoutFeedback,
  StatusBar,
  ScrollView,
  KeyboardAvoidingView,
  AsyncStorage
} from 'react-native';
import IbanBlueLink from '../../common/blueLink';
import IbanButtonGroup from '../../common/buttonGroup';

import Wallet from './../../screens/wallet/Wallet'
import AccountDetails from './../../screens/wallet/AccountDetails'
import Marketplace from './../../screens/Marketplace'
import Portfolio from './../../screens/Portfolio'

import {relativeHeight, relativeWidth} from './../../services/dimensions'

export default class WalletLayout extends Component {

  constructor(props) {
    super(props);
    this.state = {
      screen: 'wallet',
      alertScreen: '',
      card: {}
    }

    this.icons = {
      wallet: require('./icons/wallet.png'),
      walletActive: require('./icons/walletActive.png'),
      marketplace: require('./icons/marketplace.png'),
      marketplaceActive: require('./icons/marketplaceActive.png'),
      portfolio: require('./icons/portfolio.png'),
      portfolioActive: require('./icons/portfolioActive.png'),
      more: require('./icons/more.png'),
      moreActive: require('./icons/moreActive.png'),
    }
  }

  static navigationOptions = { header: null, gesturesEnabled: false };

  _navigateTo = (screen) => {
    this.setState({screen})
  }

  _selectTab(screen, options = {}){
    this.setState({screen, card: options.card})
  }

  renderIcon(icon) {
    const {screen} = this.state
    let active = false

    switch(true) {
      case (icon == 'wallet' && ['wallet', 'account_details'].includes(screen)):
        active = true
        break
      case (icon == 'marketplace' && ['marketplace'].includes(screen)):
        active = true
        break
      case (icon == 'portfolio' && ['portfolio'].includes(screen)):
        active = true
        break
        case (icon == 'more' && ['more'].includes(screen)):
          active = true;
            break;
        default:
        active = false
        break
    }

    return (
      <View style={styles.iconContainer}>
        <Image
          style={styles.menuIcon}
          source={this.icons[`${icon}${ active ? 'Active' : ''}`]}
        />
      </View>
    )
  }

  render() {
    let { screen, alertScreen, card, firstCard } = this.state;

    let { navigation } = this.props;

    let currentScreen;
    let currentHeader;

    switch(screen){
      case 'wallet':
        currentScreen = (
          <Wallet
            navigation={navigation}
            onSelectTab={(tab, options) => this._selectTab(tab, options)}
            onFirstCard={(card) => this.setState({firstCard: card})}
          />
        )
        break;
      case 'account_details':
        currentScreen = (
          <AccountDetails
            navigation={navigation}
            onSelectTab={(tab, options) => this._selectTab(tab, options)}
            card={card}
          />
        )
        break;
      case 'marketplace':
        currentScreen = (
          <Marketplace
            navigation={navigation}
            onSelectTab={(tab, options) => this._selectTab(tab, options)}
          />
        )
        break
      case 'portfolio':
        currentScreen = (
          <Portfolio
            navigation={navigation}
          />
        )
        break;
      default:
        null
    }

    return (
      <View style={ styles.main }>
        <StatusBar barStyle="light-content"/>

        <View
          style={ styles.page }
          keyboardShouldPersistTaps='always'
        >
          { currentScreen }
        </View>

        <View style={ styles.footer }>

          <TouchableWithoutFeedback onPress={() => this._selectTab('wallet')}>
            <View style={styles.menuItem}>
              {this.renderIcon('wallet')}
              <Text style={[styles.menuText, screen == 'wallet' ? styles.menuTextActive : null]}>Wallet</Text>
            </View>
          </TouchableWithoutFeedback>

          <TouchableWithoutFeedback onPress={() => this._selectTab('portfolio')}>
            <View style={styles.menuItem}>
              {this.renderIcon('portfolio')}
              <Text style={[styles.menuText, screen == 'portfolio' ? styles.menuTextActive : null]}>Portafolio</Text>
            </View>
          </TouchableWithoutFeedback>

          <TouchableWithoutFeedback onPress={() => navigation.push('SendMoney')}>
            <View style={styles.menuItem}>
              <View style={styles.menuSuperItem}>
                <Image
                  style={ styles.menuIcon }
                  source={require('./icons/send.png')}
                />
              </View>
            </View>
          </TouchableWithoutFeedback>

          <TouchableWithoutFeedback onPress={() => this._selectTab('marketplace')}>
            <View style={styles.menuItem}>
              {this.renderIcon('marketplace')}
              <Text style={[styles.menuText, screen == 'marketplace' ? styles.menuTextActive : null]}>Marketplace</Text>
            </View>
          </TouchableWithoutFeedback>

          <TouchableWithoutFeedback onPress={() => {this._selectTab('more')}}>
            <View style={styles.menuItem}>
              {this.renderIcon('more')}
              <Text style={[styles.menuText, screen == 'more' ? styles.menuTextActive : null]}>More</Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  main: {
    height: '100%'
  },
  page: {
    height: '100%',
    flex: 1,
  },
  footer: {
    borderTopWidth: 1,
    borderColor: 'rgba(0, 0, 0, 0.3)',
    width: '100%',
    backgroundColor: '#F7F7F7',
    height: relativeWidth(55),
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  menuItem: {
    width: relativeWidth(68),
    alignItems: 'center',
    marginBottom: 13
  },
  iconContainer: {
    height: relativeWidth(45),
    alignItems: 'center',
    justifyContent: 'center',
  },
  menuIcon: {
    height: '100%',
    resizeMode: 'contain'
  },
  menuSuperItem: {
    backgroundColor: '#4CBA8D',
    width: relativeWidth(73),
    height: relativeWidth(73),
    borderRadius: 40,
    justifyContent: 'space-around',
    alignItems: 'center',
    marginTop: -38
  },
  menuText: {
    marginTop: relativeWidth(-6),
    fontSize: relativeWidth(10),
    color: "#8E8E93"
  },
  menuTextActive: {
    color: "#4CBA8D"
  }
});