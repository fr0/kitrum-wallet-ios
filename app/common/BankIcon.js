import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text
} from 'react-native';

export default class IbanBankIcon extends Component<{}> {

  state = {

  }

  logo(bankId){
    switch(parseInt(bankId)) {
      case -1:
        return require('../images/icons/dashboard/cards/iban-one.png')
        break;
      case 27:
        return require('../images/icons/banks/logo_Abanca.png')
        break;
      case 3:
        return require('../images/icons/banks/logo_Sabadell.png')
        break;
      case 16:
        return require('../images/icons/banks/logo_Sabadell.png')
        break;
      case 14:
        return require('../images/icons/banks/logo_BancoPopular.png')
        break;
      case 9:
        return require('../images/icons/banks/logo_Bankia.png')
        break;
      case 19:
        return require('../images/icons/banks/logo_Bankia.png')
        break;
      case 22:
        return require('../images/icons/banks/logo_Bakinter.png')
        break;
      case 21:
        return require('../images/icons/banks/logo_Bakinter.png')
        break;
      case 6:
        return require('../images/icons/banks/logo_BBVA.png')
        break;
      case 17:
        return require('../images/icons/banks/logo_BBVA.png')
        break;
      case 33:
        return require('../images/icons/banks/logo_BNP.png')
        break;
      case 32:
        return require('../images/icons/banks/logo_Cajamar.png')
        break;
      case 29:
        return require('../images/icons/banks/logo_Cajamar.png')
        break;
      case 30:
        return require('../images/icons/banks/logo_DeutscheBank.png')
        break;
      case 23:
        return require('../images/icons/banks/logo_Evo.png')
        break;
      case 10:
        return require('../images/icons/banks/logo_ibercaja.png')
        break;
      case 24:
        return require('../images/icons/banks/logo_ibercaja.png')
        break;
      case 35:
        return require('../images/icons/banks/logo_ING_Direct.png')
        break;
      case 11:
        return require('../images/icons/banks/logo_ING_Direct.png')
        break;
      case 20:
        return require('../images/icons/banks/logo_ING_Direct.png')
        break;
      case 1:
        return require('../images/icons/banks/logo_La_Caixa.png')
        break;
      case 15:
        return require('../images/icons/banks/logo_La_Caixa.png')
        break;
      case 34:
        return require('../images/icons/banks/logo_Liberbank.png')
        break;
      case 13:
        return require('../images/icons/banks/logo_Openbank.png')
        break;
      case 7:
        return require('../images/icons/banks/logo_Santander.png')
        break;
      case 18:
        return require('../images/icons/banks/logo_Santander.png')
        break;
      case 26:
        return require('../images/icons/banks/logo_Selfbank.png')
        break;
      case 25:
        return require('../images/icons/banks/logo_Unicaja.png')
        break;
      default:
        null
    }
  }

  render() {
    let { bankId, style } = this.props;

    return (
      <Image
        style={style}
        source={this.logo(bankId)}
      ></Image>
    );
  }
}

const styles = StyleSheet.create({

});