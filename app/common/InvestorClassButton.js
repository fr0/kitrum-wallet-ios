import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  ActivityIndicator
} from 'react-native';

import { relativeWidth } from './../services/dimensions'

export default class InvestorClassButton extends Component {

  render() {

    let { style, textStyle, onPress, loading, disabled, value } = this.props;

    return (
      <TouchableWithoutFeedback
        onPress={onPress}
      >
        <View
          style={[styles.container, {backgroundColor: disabled ? 'lightgray' : '#4CBA8D'}, style]}
        >
          <View
            style={styles.textContainer}
          >
            {
              loading
                ?
                <ActivityIndicator
                  color={textStyle && textStyle.color ? textStyle.color : 'white'}
                  animating={true}
                  style={styles.spinner}
                ></ActivityIndicator>
                :
                null
            }
            <Text
              style={[styles.text, textStyle]}
            >{value}</Text>
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
    paddingRight: 10,
    paddingLeft: 10,
    alignSelf: 'center',
    justifyContent: 'center',
    borderRadius: 7,
    height: relativeWidth(45),
    width: relativeWidth(300)
  },
  textContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  text: {
    color: 'white',
    fontSize: relativeWidth(19),
  },
  spinner: {
    marginRight: 10
  }
});
