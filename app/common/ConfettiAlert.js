import React, { Component } from 'react';

import {
  StyleSheet,
  Text,
  View,
  Image,
  Alert,
  Animated
} from 'react-native';

import { relativeWidth } from '../services/dimensions'
import IbanGreenRoundedButton from './greenRoundedButton'
import Confetti from './react-native-confetti/confettiView';

export default class ConfettiAlert extends Component {

  constructor(props){
    super(props)
    this.state = {
      buttonVisible: false,
      bodyHeight: new Animated.Value(relativeWidth(330))
    }

    this._accept = this._accept.bind(this)
  }

  componentDidMount(){

    if(this._confettiView) {
      this._confettiView.startConfetti();
    }

    setTimeout(() => {
      Animated.timing(
        this.state.bodyHeight,
        {
          toValue: relativeWidth(390),
          duration: 500,
        }
      ).start();
    }, 1500)

    setTimeout(() => {
      this.setState({
        buttonVisible: true
      })
    }, 2000)
  }

  componentWillUnmount(){
    if (this._confettiView){
      this._confettiView.stopConfetti();
    }
  }

  _accept(){
    let { onClose } = this.props;
    onClose()
  }

  render() {
    let { contacts, buttonVisible, bodyHeight } = this.state;

    return (
      <View style={[styles.overlay]}>

        <Animated.View style={[styles.body, {height: bodyHeight}]}>
          <View style={styles.circle}>
            <Image
              style={ styles.icon }
              source={require('../images/checkmark.png')}
            />
          </View>

          <View style={styles.confettiContainer}>
            <Confetti
              ref={(node) => this._confettiView = node}
              confettiCount={1000}
            />
          </View>

          {
            buttonVisible ?
              <View style={{width: '100%'}}>
                <Text style={styles.title}>¡Aprobado!</Text>
                <Text style={styles.subtitle}>Continúe con el Proceso</Text>
                <IbanGreenRoundedButton
                  value='ACEPTAR'
                  style={styles.button}
                  onPress={this._accept}
                ></IbanGreenRoundedButton>
              </View>
              :
              <Text style={styles.title}>¡Enhorabuena!</Text>
          }

        </Animated.View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  confettiContainer: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    top: -30
  },
  overlay: {
    backgroundColor: 'rgba(1, 1, 1, 0.7)',
    position: 'absolute',
    width: '100%',
    height: '100%',
    top: 0,
    left: 0,
    paddingLeft: relativeWidth(15),
    paddingRight: relativeWidth(15),
    justifyContent: 'center',

  },
  body: {
    borderRadius: relativeWidth(6),
    backgroundColor: '#F7FCFA',
    alignItems: 'center',
    paddingLeft: relativeWidth(15),
    paddingRight: relativeWidth(15),
    overflow: 'hidden'
  },
  title: {
    marginTop: relativeWidth(31),
    fontSize: relativeWidth(22),
    textAlign: 'center',
    backgroundColor: 'transparent'
  },
  subtitle: {
    marginTop: relativeWidth(13),
    fontSize: relativeWidth(22),
    textAlign: 'center',
    color: 'gray',
    backgroundColor: 'transparent'
  },
  buttonText: {
    fontSize: relativeWidth(18),
  },
  circle: {
    backgroundColor: '#01E576',
    marginTop: relativeWidth(53),
    width: relativeWidth(110),
    height: relativeWidth(110),
    borderRadius: 55,
    alignItems: 'center',
    justifyContent: 'center',

    shadowOpacity: 0.26,
    shadowRadius: relativeWidth(5),
    shadowColor: 'black',
    shadowOffset: {height: 1, width: 1},
  },
  icon: {
    width: relativeWidth(50),
    height: relativeWidth(40),
  },
  button: {
    width: '100%',
    marginBottom: 0,
    marginTop: relativeWidth(50),
    borderRadius: 4
  },
});
