import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  Image,
  Alert,
  Dimensions
} from 'react-native';
import IbanTextInput from './TextInput';
import { relativeWidth as rw} from './../services/dimensions'

export default class Progress extends Component {

  constructor(props) {
    super(props)
    this.state = {
      width: 0
    }

    this.handleOnLayout = this.handleOnLayout.bind(this)
  }

  handleOnLayout(e) {
    this.setState({width: e.nativeEvent.layout.width})
  }

  render() {
    let {width} = this.state
    let { style, filledColor, sliderColor, value } = this.props;

    return (
      <View 
        style={[s.container, style]}
        onLayout={this.handleOnLayout}
      >
        <View 
          style={[
            s.progress,
            {
              backgroundColor: filledColor,
              width: width * value / 100.0
            }
          ]}
        />
        <View 
          style={[
            s.slider, 
            {
              backgroundColor: sliderColor,
              left: (width * value / 100.0) - rw(19)
            }
          ]}></View>
      </View>
    );
  }
}

const s = StyleSheet.create({
  container: {
    marginTop: rw(20),
    backgroundColor: '#E0E0E2',
    height: rw(18),
    borderRadius: rw(9),
    overflow: 'hidden'
  },
  progress: {
    backgroundColor: '#33CC99',
    height: rw(20),
    position: 'absolute',
    top: 0,
    width: rw(120)
  },
  slider: {
    backgroundColor: 'rgba(1,1,1,.14)',
    width: rw(20),
    height: '100%',
    position: 'absolute',
    top: 0,
    left: rw(100)
  },
});
