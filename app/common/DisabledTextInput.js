import React, { Component } from 'react';

import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  Image,
  Alert
} from 'react-native';
import IbanTextInput from './TextInput';

import { relativeWidth } from '../services/dimensions'

export default class DisabledTextInput extends Component<{}> {

  constructor(props){
    super(props)
    this.state = {
      modalOpened: false
    }
  }

  render() {

    let { label, value, onTouch = () => {}, error, style } = this.props;
    const { modalOpened } = this.state

    return (
      <View
        style={[styles.container]}
      >

        <IbanTextInput
          label={label}
          value={value}
          error={error}
          style={[style, this.props.style]}
          clearButtonMode="never"
        />

        <Image
          style={ styles.chevronIcon }
          source={require('../images/icons/chevron-down.png')}
        />

        <TouchableWithoutFeedback
          onPress={() => onTouch()}
        >
          <View style={styles.touchableOverlay}>

          </View>
        </TouchableWithoutFeedback>

        {
          modalOpened ?
            <View style={styles.overlay}></View>
            : null
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {

  },
  chevronIcon: {
    position: 'absolute',
    top: relativeWidth(53),
    right: relativeWidth(10),
    width: relativeWidth(27),
    height: relativeWidth(27),
  },
  touchableOverlay: {
    position: 'absolute',
    width: '100%',
    height: '100%',
  },
  overlay: {
    backgroundColor: 'rgba(1, 1, 1, 0.3)',
    position: 'absolute',
    width: '100%',
    height: '100%',
    top: 0,
    left: 0
  }
});
