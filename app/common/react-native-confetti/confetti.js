import React, {Component} from 'react';
import {
  AppRegistry,
  StyleSheet,
  View,
  Animated,
  Dimensions
} from 'react-native';

let windowHeight = Dimensions.get('window').height;
let windowWidth = Dimensions.get('window').width;
import { relativeWidth } from './../../services/dimensions'

class Confetti extends Component {
  constructor(props) {
      super(props);
      this._yAnimation = new Animated.Value(0);
      this.color = this.randomColor(this.props.colors);
      this.left = this.randomValue(0, windowWidth);
  }

  componentWillMount() {
      let rotationOutput = this.randomValue(-220, 220) + 'deg';

      this._rotateAnimation = this._yAnimation.interpolate({
        inputRange: [this.randomValue(0, 180), windowHeight / 2, windowHeight],
        outputRange: ['0deg', rotationOutput, rotationOutput]
      });

      let xDistance = this.randomIntValue((windowWidth / 3 * -1), windowWidth / 3);
      this._xAnimation = this._yAnimation.interpolate({
        inputRange: [0, windowHeight],
        outputRange: [0, xDistance]
      });
  }

  componentDidMount() {
      let {duration, index} = this.props;
        Animated.timing(this._yAnimation, {
           duration: duration,
           toValue: windowHeight + 1.25,
           useNativeDriver: true
        }).start(this.props.onComplete);
  }

  getTransformStyle() {
      return {
         transform: [
           {translateY: this._yAnimation},
           {translateX: this._xAnimation},
           {rotate: this._rotateAnimation}
         ]
      }
  }

  getConfettiStyle() {
    return {
      height: relativeWidth(22),
      width: relativeWidth(10),
    }
  }

  randomValue(min, max) {
      return Math.random() * (max - min) + min;
  }

  randomIntValue(min, max) {
      return Math.floor(Math.random() * (max - min) + min);
  }

  randomColor(colors) {
      return colors[this.randomIntValue(0,colors.length)];
  }

  render() {
      let {left, ...otherProps} = this.props;
      return <Animated.View
        style={[
          styles.confetti,
          this.getConfettiStyle(),
          this.getTransformStyle(),
          {marginLeft: this.left, backgroundColor: this.color}
          ]}
        {...otherProps}
      />
  }
}

Confetti.defaultProps = {
    duration: 5000,
    colors: [
      "#FE547B",
      "#F1E189",
      "#B8DE6F",
      "#F39233",
      "#01C5C4"
    ]
}

const styles = StyleSheet.create({
  confetti: {
    position: 'absolute',
    marginTop: 0
  }
});

export default Confetti;
