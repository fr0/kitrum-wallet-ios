import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ActivityIndicator
} from 'react-native';

import { relativeWidth } from './../services/dimensions'

export default class Spinner extends Component<{}> {

  state = {

  }

  componentDidMount(){

  }

  render() {

    let { uploads, progress } = this.state;

    return (
        <View
          style={styles.container}
        >

          <Text
            style={styles.message}
          >
            Cargando{"\n"}
            Perfil de ahorrador
          </Text>

          <ActivityIndicator
            color='#4CBA8D'
            animating={true}
            style={styles.spinner}
            size={'large'}
          ></ActivityIndicator>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    backgroundColor: 'rgba(0,0,0,.81)',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10
  },
  message: {
    fontSize: relativeWidth(24),
    color: 'white',
    marginBottom: relativeWidth(60),
    textAlign: 'center'
  }
});
