import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  Linking
} from 'react-native';

import { relativeWidth } from './../services/dimensions'

export default class IbanBlueLink extends Component<{}> {

  constructor(props){
    super(props);
    this._onTouch = this._onTouch.bind(this)
  }

  _onTouch() {
    let { url, onPress } = this.props

    if(url){
      Linking.canOpenURL(url).then(supported => {
        if (!supported) {
          console.log('Can\'t handle url: ' + url);
        } else {
          return Linking.openURL(url);
        }
      }).catch(err => console.error('An error occurred', err));
    }else if(onPress){
      onPress()
    }
  }

  render() {
    return (
      <View
        style={[styles.container, this.props.style]}
      >
        <TouchableWithoutFeedback
          onPress={this._onTouch}
        >
          <View
            style={this.props.underline ? styles.underline : {}}
          >
            <Text
              style={styles.text}
            >{this.props.value}</Text>
          </View>
        </TouchableWithoutFeedback>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: relativeWidth(16),
    alignItems: 'center'
  },
  text: {
    color: '#3399CC',
    fontSize: relativeWidth(13),
  },
  underline: {
    borderBottomWidth: 1,
    borderColor: '#3399CC'
  }
});
