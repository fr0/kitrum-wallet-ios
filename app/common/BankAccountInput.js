import React, { Component } from 'react';

import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  Image,
  Alert,
  ScrollView
} from 'react-native';
import IbanTextInput from './TextInput';

import { relativeWidth } from '../services/dimensions'
import {api_id, api_code, unnaxUrl, unnaxCallbackUrl} from '../services/variables'
import IbanBankIcon from './BankIcon';
import { profile } from '../services/users'

export default class BankAccountInput extends Component {

  constructor(props){
    super(props)
    this.state = {
      user: {
        fitnance_accounts: [],
        fitnance_banks: []
      }
    }

  }

  componentDidMount(){
    this.loadBanks()
  }

  loadBanks(){
    const {navigation} = this.props

    profile(navigation, {noRedirect: true})
      .then(user => {
        this.setState({user})
      })
      .catch()
  }

  _onSelect(account){
    const {user} = this.state

    // let bank = user.fitnance_banks.find(bank => bank.id == account.bank_id)
    let customer = user.fitnance_customers.find(customer => customer.id == account.customers[0])

    // account.bankName = bank.name
    account.customerAddress = customer.address

    let { onSelect } = this.props;

    onSelect(account)
  }

  render() {

    let { onPress, navigation: {navigate} } = this.props;
    let { user } = this.state;

    return (
      <View style={[styles.overlay]}>

        <View style={styles.body}>
          <Text style={styles.title}>Seleccionar Cuenta</Text>

          <View style={styles.accounts}>
            <ScrollView
              horizontal={true}
            >
              {user.fitnance_accounts.map((account, index) =>
                <TouchableWithoutFeedback
                  onPress={() => this._onSelect(account)}
                  key={index}
                >
                  <View
                    style={styles.account}
                  >
                    <IbanBankIcon
                      bankId={account.bank_id}
                      style={styles.accountIcon}
                    />
                    <Text style={styles.accountTitle}>{account.name}</Text>
                  </View>
                </TouchableWithoutFeedback>
              )}
            </ScrollView>

          </View>

          <TouchableWithoutFeedback onPress={() => {onPress(); navigate('BankAccount')} }>
            <View style={styles.button}>
              <Text style={styles.buttonText}>+ Nueva cuenta</Text>
            </View>
          </TouchableWithoutFeedback>
        </View>

        <TouchableWithoutFeedback onPress={() => onPress()}>
          <View style={{height: relativeWidth(390)}}>

          </View>
        </TouchableWithoutFeedback>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  overlay: {
    flexDirection: 'column-reverse',
    backgroundColor: 'rgba(1, 1, 1, 0.7)',
    position: 'absolute',
    width: '100%',
    height: '100%',
    top: 0,
    left: 0,
    paddingLeft: relativeWidth(15),
    paddingRight: relativeWidth(15),
    paddingBottom: relativeWidth(40),
  },
  body: {
    backgroundColor: 'white',
    height: relativeWidth(282),
    borderRadius: relativeWidth(6),
    backgroundColor: '#F7FCFA'
  },
  title: {
    marginTop: relativeWidth(20),
    fontSize: relativeWidth(18),
    textAlign: 'center'
  },
  button: {
    paddingTop: relativeWidth(17),
    paddingBottom: relativeWidth(15),
    marginTop: relativeWidth(19),
    marginLeft: relativeWidth(20),
    marginRight: relativeWidth(20),
    alignItems: 'center',
    borderRadius: relativeWidth(6),
    backgroundColor: 'white'
  },
  buttonText: {
    fontSize: relativeWidth(18),
  },
  accounts: {
    borderBottomWidth: 1,
    borderColor: '#DCE6F0',
    height: relativeWidth(151),
  },
  account: {
    width: 90,
    alignItems: 'center'
  },
  accountTitle: {
    textAlign: 'center',
    fontSize: relativeWidth(12),
    color: 'gray',
    marginTop: relativeWidth(10)
  },
  accountIcon: {
    width: relativeWidth(54),
    height: relativeWidth(54),
    backgroundColor: 'rgba(0, 0, 0, .08)',
    borderRadius: relativeWidth(27),
    marginTop: relativeWidth(28)
  }
});
