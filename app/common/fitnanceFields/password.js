import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  TextInput,
} from 'react-native';

import { relativeWidth } from './../../services/dimensions'

export default class Password extends Component<{}> {

  constructor(props){
    super(props)

    let {parameter} = this.props

    let selectedIndex = parameter.fields.map((field, index) => {
      return {index, name: field.name}
    }).filter(i => i.name != '')[0].index

    this.state = {selectedIndex}
  }

  componentWillReceiveProps(nextProps){
    let {parameter} = nextProps

    if(Object.keys(nextProps.values).length === 0){
      let selectedIndex = parameter.fields.map((field, index) => {
        return {index, name: field.name}
      }).filter(i => i.name != '')[0].index

      this.setState({selectedIndex})
    }
  }

  _handleChanges = (value) => {
    const { onChange, values, parameter} = this.props;
    const { selectedIndex } = this.state;

    if(selectedIndex != null){

      let newIndexField = parameter.fields.map((field, index) => {
        return {index, name: field.name}
      }).filter(i => i.name != '' && i.index > selectedIndex)[0]

      if(newIndexField && newIndexField.index){
        this.setState({
          selectedIndex: newIndexField.index
        })
      }else{
        this.setState({
          selectedIndex: null
        })
      }


      onChange(parameter.fields[selectedIndex], value.toString());
    }
  }

  _select(index){
    this.setState({selectedIndex: index})
  }

  _renderField = (field, index) => {

    const { selectedIndex, errors } = this.state;
    const { values, parameter } = this.props;

    return (
      <TouchableWithoutFeedback
        key={index}
        onPress={() => field.name ? this._select(index) : null}
      >
        <View
          style={[styles.fieldContainer, selectedIndex == index ? styles.selectedFieldContainer : null, {width: `${100/parameter.fields.length}%`}]}
        >
          {
            field.name ?
              <Text style={styles.inputField}>{ values[field.name] }</Text>
              :
              <View style={styles.circle}></View>
          }
        </View>
      </TouchableWithoutFeedback>
    )
  }

  render() {
    const { values, parameter } = this.props
    const inputs = [...Array(10).keys()].sort(() => Math.random() - 0.5)

    return (
      <View
        style={[styles.container, this.props.style]}
      >
        <View
          style={styles.fieldsContainer}
        >
          {
            parameter.fields.map((field, index) => this._renderField(field, index))
          }
        </View>

        <View
          style={styles.inputContainer}
        >
          {
            inputs.filter((item, index) => index < 5).map(i =>
              <TouchableWithoutFeedback
                key={i}
                onPress={() => this._handleChanges(i)}
              >
                <View
                  style={styles.inputFieldContainer}
                >
                  <Text style={styles.inputField}>{i}</Text>
                </View>
              </TouchableWithoutFeedback>
            )
          }
        </View>
        <View
          style={styles.inputContainer}
        >
          {
            inputs.filter((item, index) => index >= 5).map(i =>
              <TouchableWithoutFeedback
                key={i}
                onPress={() => this._handleChanges(i)}
              >
                <View
                  style={styles.inputFieldContainer}
                >
                  <Text style={styles.inputField}>{i}</Text>
                </View>
              </TouchableWithoutFeedback>
            )
          }
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {

  },

  fieldsContainer: {
    width: '100%',
    flexDirection: 'row',
    borderLeftWidth: 1,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#D8D8D8',
    marginTop: -1,
    marginBottom: 20,
    height: relativeWidth(53),
  },
  fieldContainer: {
    height: '100%',
    borderRightWidth: 1,
    borderColor: '#D8D8D8',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  selectedFieldContainer: {
    borderWidth: 3,
    borderRightWidth: 3,
    borderColor: "#4CBA8D"
  },

  inputContainer: {
    width: '100%',
    flexDirection: 'row',
    borderLeftWidth: 1,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#D8D8D8',
    marginTop: -1,
    height: relativeWidth(40),
  },
  inputFieldContainer: {
    height: '100%',
    width: '20%',
    borderRightWidth: 1,
    borderColor: '#D8D8D8',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  inputField: {
    color: '#4CBA8D',
    fontSize: relativeWidth(22)
  },

  input: {
    height: '100%',
    padding: relativeWidth(10),
    fontSize: relativeWidth(16.5),
    textAlign: 'center'
  },
  error: {
    color: 'red',
    position: 'absolute',
    bottom: 0,
    right: 0,
    fontSize: relativeWidth(14)
  },

  circle: {
    backgroundColor: '#4CBA8D',
    width: relativeWidth(25),
    height: relativeWidth(25),
    borderRadius: relativeWidth(14),
  }
});
