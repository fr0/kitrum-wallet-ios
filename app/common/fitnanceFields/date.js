import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  TextInput,
} from 'react-native';

import { relativeWidth } from './../../services/dimensions'

export default class Date extends Component<{}> {

  state = {
    errors: {}
  }

  _handleChanges = (field, value) => {
    const { onChange, values } = this.props;

    onChange(field, value);

    this._validate(field, value);
  }

  _validate(field, value){
    let { errors } = this.state;

    if(!field.optional && value.length == 0){
      errors[field.name] = 'Campo requerido.'
    }else if(field.min_length && value.length < field.min_length){
      errors[field.name] = `Mínimo de ${ field.min_length } caracteres.`
    }else{
      errors[field.name] = ''
    }

    this.setState({errors})
  }

  _renderField = (field, index) => {

    const { selectedOptionIndex, errors } = this.state;
    const { values, parameter } = this.props;

    return (
      <View
        key={index}
        style={styles.dateFieldContainer}
      >

        <TextInput
          placeholder={field.label || field.name}
          placeholderTextColor="#8E8E93"
          value={values[field.name] || ''}
          onChangeText={ value => this._handleChanges(field, value) }
          maxLength={field.max_length}
          keyboardType='numeric'
          style={styles.input}
          selectionColor='#4CBA8D'
        />

        <Text
          style={styles.error}
        >
          {errors[field.name]}
        </Text>

      </View>
    )
  }

  render() {
    const { values, parameter } = this.props;

    return (
      <View
        style={[styles.dateContainer, this.props.style]}
      >
        {
          parameter.fields.map((field, index) => this._renderField(field, index))
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  dateContainer: {
    width: '100%',
    flexDirection: 'row',
    borderLeftWidth: 1,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#D8D8D8',
    marginTop: -1,
    height: relativeWidth(53),
  },
  fieldContainer: {
    height: '100%'
  },
  dateFieldContainer: {
    width: '33.33%',
    borderRightWidth: 1,
    borderColor: '#D8D8D8'
  },

  input: {
    height: '100%',
    padding: relativeWidth(10),
    fontSize: relativeWidth(16.5),
    textAlign: 'center'
  },
  error: {
    color: 'red',
    position: 'absolute',
    bottom: 0,
    right: 0,
    fontSize: relativeWidth(14)
  }
});
