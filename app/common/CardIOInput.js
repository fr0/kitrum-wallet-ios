import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  Image,
  Alert
} from 'react-native';
import IbanTextInput from './TextInput';
import { relativeWidth } from './../services/dimensions'

export default class IbanCardIOInput extends Component<{}> {

  _findCard(){
    let { navigation } = this.props;

    navigation.navigate('IbanCardIO', {didScanCard: this.didScanCard.bind(this)})
  }

  didScanCard(card){

    let { onCardSelect } = this.props;

    // {
    //post_code   "postalCode":null,
    //exp_month   "expiryMonth":1,
    //name   "cardType":"MasterCard",
    //   "redactedCardNumber":"••••••••••••8130",
    //exp_year   "expiryYear":2018,
    //   "scanned":true,
    //cvv   "cvv":null,
    //number   "cardNumber":"5168742211968130",
    //name   "cardholderName":null
    // }

    onCardSelect(card)
  }

  render() {

    let { label, value, onChangeText, error, style } = this.props;

    return (
      <View
        style={[styles.container, this.props.style]}
      >

        <IbanTextInput
          label={label}
          value={value}
          onChangeText={ (k, v) => onChangeText(k, v) }
          error={error}
          style={style}
          clearButtonMode="never"
        />

        <TouchableWithoutFeedback
          onPress={() => this._findCard()}
        >
          <View
            style={styles.cameraContainer}
          >
            <Image
              style={ styles.camera }
              source={require('../images/camera.png')}
            />
          </View>
        </TouchableWithoutFeedback>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {

  },
  cameraContainer: {
    width: relativeWidth(30),
    position: 'absolute',
    height: relativeWidth(30),
    top: relativeWidth(20),
    right: 0,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  camera: {

  },
});
