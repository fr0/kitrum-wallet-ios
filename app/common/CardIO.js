import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback
} from 'react-native';

import { CardIOView, CardIOUtilities } from 'react-native-awesome-card-io';
import { relativeWidth } from './../services/dimensions'

export default class IbanCardIO extends Component<{}> {

  static navigationOptions = { header: null, gesturesEnabled: false };

  componentWillMount() {
    CardIOUtilities.preload();
  }

  render() {
    let { navigation: { goBack, state: {params: {didScanCard}} } } = this.props;

    return (
      <View style={styles.container}>
        <CardIOView
          didScanCard={card => {didScanCard(card); goBack() }}
          style={styles.cardView}
        />

        <View
          style={styles.nav}
        >
          <TouchableWithoutFeedback
            onPress={() => goBack()}
          >
            <View>
              <Text style={styles.navText}>Cancelar</Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: 'black'
  },
  cardView: {
    width: '100%',
    height: '100%'
  },
  navText: {
    color: '#339966',
    fontSize: relativeWidth(18)
  },
  nav: {
    position: 'absolute',
    top: 30,
    right: 20
  }
});
