import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  ActivityIndicator,
  Image,
  PanResponder,
  Animated,
  Easing,
  Alert
} from 'react-native';

import { relativeWidth as rw } from './../../services/dimensions'

export default class Slider extends Component {

  constructor(props) {
    super(props)
    this.state = {}

    this.left = 0

    this.sliderWidth = 0
    this.shift = rw(10)
    this._previousLeft = 0

    pan: new Animated.ValueXY()

    this.panResponder = PanResponder.create({
      onStartShouldSetPanResponder    : () => true,
      
      onPanResponderMove              : (e, gestureState) => {
        this.left += gestureState.dx - this._previousLeft 

        if(this.left + this.shift <= 0){
          this.left = -this.shift
          this._previousLeft = 0
        }
        if(this.left + this.shift > this.sliderWidth){
          this.left = this.sliderWidth - this.shift
        }

        this._previousLeft = gestureState.dx
        this._updateNativeStyles()
      },
      onPanResponderTerminationRequest: (evt, gestureState) => true,
      onPanResponderRelease: this._handlePanResponderEnd.bind(this),
      onPanResponderTerminate: this._handlePanResponderEnd.bind(this),
    });

    this.handleOnLayout = this.handleOnLayout.bind(this)
  }

  componentDidMount() {
    this._updateNativeStyles()
  }

  handleOnLayout(e) {
    this.sliderWidth = e.nativeEvent.layout.width
  }
  
  _updateNativeStyles() {
    const {value, max, onChange} = this.props
    if(this.sliderWidth == 0){
      return
    }
    
    onChange((this.left + this.shift) * max / this.sliderWidth)

    this.refs.circle && this.refs.circle.setNativeProps({left: this.left - this.shift})
    this.refs.filled && this.refs.filled.setNativeProps({width: this.left < 0 ? 0 : this.left})
  }

  _handlePanResponderEnd(e, gestureState) {
    this._previousLeft = 0
  }
  
  render() {
    const {onChange, value, sym, color} = this.props

    return (
      <View 
        style={s.container}
        onLayout={this.handleOnLayout}
      >
        <View style={[s.titleContainer, {borderColor: color || '#33CC99'}]}>
          <Text style={s.title}>{`${value.toFixed(2)}`} {sym}</Text>
        </View>

        <View 
          style={s.sliderContainer}
          onLayout={this.handleOnLayout}
        >
          <View style={s.sliderLine}>
            <View style={[s.sliderLineFilled, {backgroundColor: color || "#33CC99"}]} ref='filled'/>
          </View>
          <Animated.View
            {...this.panResponder.panHandlers}
            style={s.circleContainer}
            ref='circle'
          >
            <View style={[s.circle, {borderColor: color || '#33CC99'}]}/>
            <Text 
              numberOfLines={1}
              style={s.circleText}>{`${value.toFixed(2)}`} {sym}</Text>
          </Animated.View>
          
        </View>
      </View>
    )
  }
}

const s = StyleSheet.create({
  container: {
    marginLeft: rw(30),
    marginRight: rw(30),
  },
  titleContainer: {
    alignSelf: 'center',
    marginTop: rw(14),
    borderBottomWidth: rw(3),
    paddingBottom: rw(8)
  },
  title: {
    backgroundColor: 'transparent',
    fontSize: rw(36),
    fontWeight: 'bold',
    color: 'rgba(1,1,1,.7)',
  },
  sliderContainer: {
    height: rw(12),
    width: '100%',
    marginTop: rw(30)
  },
  sliderLine: {
    borderRadius: rw(6),
    overflow: 'hidden',
    height: '100%',
    backgroundColor: 'rgba(1,1,1,.17)',
  },
  sliderLineFilled: {
    height: '100%',
  },
  circleContainer: {
    position: 'absolute',
    top: rw(-5),
    
    width: rw(40),
    height: rw(40),
    alignItems: 'center',
    justifyContent: 'center'
  },
  circleText: {
    textAlign: 'center',
    width: rw(200),
    backgroundColor: 'transparent'
  },
  circle: {
    width: rw(34),
    height: rw(34),
    backgroundColor: 'white',
    borderWidth: rw(4),
    borderRadius: rw(17),

    shadowOffset: {width: 0, height: rw(4)},
    shadowOpacity: 0.2,
    shadowRadius: rw(2),
  }
});
