import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  Image,
  Alert
} from 'react-native';
import { TextField } from './react-native-material-textfield';

export default class IbanTextInput extends Component<{}> {

  state = {
    patternError: '',
    minLengthError: '',
    maxLengthError: '',
    requiredError: '',
  }

  _handleBlur = () => {
    let { value } = this.props;

    this._validate(value)
  }

  _handleChange = (value) => {
    let { onChangeText } = this.props;
    onChangeText(value);
    this._validate(value);
  }

  _validate(value){
    let { minLength, maxLength, required, email } = this.props;

    this.setState({
      minLengthError: minLength && value.length < minLength ? `Min ${ minLength } characters.` : ''
    })

    this.setState({
      maxLengthError: maxLength && value.length > maxLength ? `Max ${ maxLength } characters.` : ''
    })

    this.setState({
      requiredError: required && value.length == 0 ? `Field is required.` : ''
    })
    this.setState({
      patternError: email && !(new RegExp('.*@.*\..*')).test(value) ? 'Invalid email.' : ''
    })
  }

  render() {

    let { label, value, onChangeText, type, error, onPress, email, keyboardType, clearButtonMode} = this.props;
    let { displayCloseButton, patternError, requiredError, minLengthError, maxLengthError } = this.state;

    return (
      <View
        style={[styles.container, this.props.style]}
      >
        <TextField
          label={ label }
          value={ value }
          onChangeText={val => this._handleChange(val) }
          baseColor={'#4CBA8D'}
          tintColor={'#4CBA8D'}
          onFocus={this._handleFocus}
          returnKeyType='done'
          secureTextEntry={type == 'password'}
          error={error || requiredError || patternError || maxLengthError || minLengthError}
          keyboardType={keyboardType || 'default'}
          clearButtonMode={clearButtonMode || "while-editing"}
          autoCapitalize='none'
        />
        {
          onPress
          ?
          <TouchableWithoutFeedback
            onPress={() => onPress()}
          >
            <View
              style={{position: 'absolute', width: '100%', height: '100%'}}
            ></View>
          </TouchableWithoutFeedback>
          :
          null
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    marginTop: 5
  },
  text: {
    color: 'rgb(81,152,199)',
    fontSize: 15,
  },
  closeIcon: {
    position: 'absolute',
    top: 20,
    right: 0,
    width: 50,
    height: 50,
    borderWidth: 1
  },
  closeImage: {
    top: 20,
    right: -30
  }
});
