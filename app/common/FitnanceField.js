import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  Image,
  Alert,
  PickerIOS,
  TextInput,
} from 'react-native';
import IbanTextInput from '../common/TextInput';
import Password from '../common/fitnanceFields/password';
import Date from '../common/fitnanceFields/date';
import Picker from 'react-native-picker';

import { relativeWidth } from './../services/dimensions'

export default class FitnanceField extends Component<{}> {

  state = {
    selectedOptionIndex: 0,
    errors: {}
  }

  _openOptionDialog = () => {

    const { parameter, values, onChange } = this.props;
    const { selectedOptionIndex } = this.state;

    let data = parameter.fields.map(item => item.label);
    let selectedItem = data[selectedOptionIndex];

    Picker.init({
      pickerTitleText: parameter.name,
      pickerData: data,
      selectedValue: [selectedItem],
      onPickerConfirm: data => {
        parameter.fields.forEach((field, index) => {
          if(field.label == data[0]){

            values[parameter.name] = field.name

            this.setState({
              selectedOptionIndex: index
            })
          }else{
            delete values[field.name]
          }
        });
        onChange(values);
      }
    });

    Picker.show();
  }

  componentWillUnmount(){
    Picker.hide();
  }

  componentDidMount() {
    const { parameter, onClearButtonNeeded, values, onChange } = this.props;

    if (parameter.type == 'password' && parameter.fields.length > 1) {
      onClearButtonNeeded()
    }

    if(parameter.type == 'option'){
      if(!values[parameter.name]){
        onChange({...values, [parameter.name]: parameter.fields[0].name})
      }
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.clear != this.props.clear) {
      this._clear()
      this.setState({clear: nextProps.clear})
    }
  }

  _clear(){
    const { parameter, onChange } = this.props;
    onChange({})
  }

  _handleChanges = (field, value) => {
    const { onChange, values } = this.props;

    onChange({
      ...values,
      [field.name]: value
    });

    this._validate(field, value);
  }

  _icon(parameterType, fieldName){
    if(fieldName == 'login_pseudonym' || fieldName == 'document' || fieldName == 'reference'){
      return require('../images/icons/bank-fields/document2.png')
    }

    if(fieldName == 'contract'){
      return require('../images/icons/bank-fields/document.png')
    }

    if(fieldName == 'company'){
      return require('../images/icons/bank-fields/case.png')
    }

    if(parameterType == 'password'){
      return require('../images/icons/bank-fields/key.png')
    }

    if(parameterType == 'option'){
      return require('../images/icons/bank-fields/document2.png')
    }

    if(parameterType == 'text'){
      return require('../images/icons/bank-fields/user.png')
    }

    return require('../images/icons/bank-fields/user.png')
  }

  _validate(field, value){
    let { errors } = this.state;

    if(!field.optional && value.length == 0){
      errors[field.name] = 'Campo requerido.'
    }else if(field.min_length && value.length < field.min_length){
      errors[field.name] = `Mínimo de ${ field.min_length } caracteres.`
    }else{
      errors[field.name] = ''
    }

    this.setState({errors})
  }

  _renderField = (field, index) => {

    const { selectedOptionIndex, errors } = this.state;
    const { values, parameter, onChange } = this.props;

    let containerClass = null;

    switch(parameter.type){
      case 'option':
        containerClass = 'optionFieldContainer'
        break;
      default:
        containerClass = 'fieldContainer'
    }

    if(parameter.type == 'option' && selectedOptionIndex != index){
      return null
    }

    return (
      <View
        key={index}
        style={styles[containerClass]}
      >

          <View
            style={styles.fieldIconContainer}
          >
            <Image
              style={styles.fieldIcon}
              source={this._icon(parameter.type, field.name)}
            ></Image>
          </View>

        <TextInput
          placeholder={field.label || field.name}
          placeholderTextColor="#8E8E93"
          value={values[field.name] || ''}
          onChangeText={ value => this._handleChanges(field, value) }
          maxLength={field.max_length}
          type={parameter.type}
          style={styles.input}
          selectionColor='#4CBA8D'
          autoCapitalize="none"
          returnKeyType="done"
        />

        <Text
          style={styles.error}
        >
          {errors[field.name]}
        </Text>

        {
          parameter.type == 'option'
            ?
            <TouchableWithoutFeedback
              onPress={this._openOptionDialog}
            >
              <Image
                style={styles.dropdownIcon}
                source={require('../images/icons/chevron-down.png')}
              ></Image>
            </TouchableWithoutFeedback>
            :
            null
        }

      </View>
    )
  }

  render() {
    const { values, parameter } = this.props;

    let containerClass = null;

    switch(parameter.type){
      default:
        containerClass = 'container'
    }

    // {
    //   "group_name":"Bankia",
    //   "group_id":9,
    //   "parameters":[
    //
    //     {
    //       "fields":[{"validator":"","required_by":[],"min_length":8,"label":"Documento","type":"text","name":"document","max_length":13,"optional":false}],
    //       "label":"Documento",
    //       "type":"text",
    //       "name":"document",
    //       "modes":["lockstep","auto","all"],"optional":false
    //     },
    //
    //   {
    //     "fields":[{"validator":"","required_by":[],"min_length":4,"label":"Contraseña","type":"text","name":"password","max_length":8,"optional":false}],
    //     "label":"Contraseña",
    //     "type":"password",
    //     "name":"password",
    //     "modes":["lockstep","auto","all"],"optional":false
    //   }
    //
    //   ],
    //
    //   "id":9,"prefixes":"2038","mode":1,"name":"Bankia","can_fast_transfer":false}


    // {
    //   "group_name":"ING Direct",
    //   "group_id":11,
    //   "parameters":[
    //     {
    //       "fields":[
    //         {"validator":"","required_by":[],"min_length":8,"label":"Documento","type":"text","name":"document","max_length":12,"optional":false}
    //       ],
    //       "label":"Documento",
    //       "type":"text",
    //       "name":"document",
    //       "modes":["lockstep","auto","all"],
    //       "optional":false
    //     },
    //
    //     {
    //       "fields":[
    //         {"validator":"","required_by":[],"min_length":2,"label":"DD","type":"day","name":"dob_dd","max_length":2,"optional":false},
    //         {"validator":"","required_by":[],"min_length":2,"label":"MM","type":"month","name":"dob_mm","max_length":2,"optional":false},
    //         {"validator":"","required_by":[],"min_length":4,"label":"AAAA","type":"year","name":"dob_yyyy","max_length":4,"optional":false}
    //       ],
    //       "label":"Fecha de nacimiento",
    //       "type":"date",
    //       "name":"date_of_birth",
    //       "modes":["lockstep","auto","all"],
    //       "optional":false
    //     },
    //     {
    //       "fields":[
    //         {"validator":"","required_by":[],"min_length":4,"label":"Contraseña","type":"text","name":"password","max_length":10,"optional":false}
    //       ],
    //       "label":"Contraseña",
    //       "type":"password",
    //       "name":"password",
    //       "modes":["lockstep","auto","all"],
    //       "optional":false
    //     },
    //     {"type":"password","name":"key","fields":[
    //       {"min_length":0,"label":"Ingrese el caracter de su contraseña presente en la posicion pedida","type":"text","name":"","max_length":0,"required_by":[]},
    //       {"min_length":1,"label":"Ingrese el caracter de su contraseña presente en la posicion pedida","type":"text","name":"2","max_length":1,"required_by":[]},
    //       {"min_length":1,"label":"Ingrese el caracter de su contraseña presente en la posicion pedida","type":"text","name":"3","max_length":1,"required_by":[]},
    //       {"min_length":1,"label":"Ingrese el caracter de su contraseña presente en la posicion pedida","type":"text","name":"4","max_length":1,"required_by":[]},
    //       {"min_length":0,"label":"Ingrese el caracter de su contraseña presente en la posicion pedida","type":"text","name":"","max_length":0,"required_by":[]},
    //       {"min_length":0,"label":"Ingrese el caracter de su contraseña presente en la posicion pedida","type":"text","name":"","max_length":0,"required_by":[]}]}],"success":true}}

    //   ],
    //   "id":11,
    //   "prefixes":"1465",
    //   "mode":1,
    //   "name":"ING Direct",
    //   "can_fast_transfer":false
    // }

    if(parameter.type == 'password' && parameter.fields.length > 1){
      return (
        <Password
          values={values}
          parameter={parameter}
          onChange={ (field, value) => this._handleChanges(field, value) }
        >

        </Password>
      )
    }else if(parameter.type == 'date'){
      return (
        <Date
          values={values}
          parameter={parameter}
          onChange={ (field, value) => this._handleChanges(field, value) }
        >

        </Date>
      )
    }

    return (
      <View
        style={[styles[containerClass], this.props.style]}
      >
        {
          parameter.fields.map((field, index) => this._renderField(field, index))
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    borderWidth: 1,
    borderColor: '#D8D8D8',
    marginTop: -1
  },
  fieldContainer: {
    height: relativeWidth(53),
  },
  optionFieldContainer: {
    paddingRight: 30,
    height: relativeWidth(53),
  },

  fieldIconContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    borderRightWidth: 1,
    borderColor: '#D8D8D8',
    width: relativeWidth(57),
    height: '100%',
    alignItems: 'center',
    justifyContent: 'space-around'
  },
  fieldIcon: {

  },
  dropdownIcon: {
    position: 'absolute',
    top: relativeWidth(0),
    right: relativeWidth(0),
    width: relativeWidth(50),
    height: relativeWidth(50),
    resizeMode: 'center'
  },
  input: {
    height: '100%',
    marginLeft: relativeWidth(57),
    padding: relativeWidth(10),
    fontSize: relativeWidth(16.5)
  },
  error: {
    color: 'red',
    position: 'absolute',
    bottom: 0,
    right: 0,
    fontSize: relativeWidth(14)
  }
});
