import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  Image,
  Alert
} from 'react-native';
var ImagePicker = require('react-native-image-picker');

import { relativeWidth } from './../services/dimensions'

export default class IbanFileInput extends Component<{}> {

  state = {

  }

  takePicture() {
    let { onChange } = this.props;
    let { navigate, goBack, label } = this.props.navigation;

    var options = {
      title: label,
      customButtons: [],
      storageOptions: {
        skipBackup: true,
        path: 'images'
      }
    };

    ImagePicker.launchCamera(options, (response) => {

      //     fileSize: 150304,
      //     data: '/9j/4AAQSkZJRgABAQAASABIAAD/
      //     timestamp: '2017-11-12T15:34:06Z',
      //     uri: 'file:///Users/michaelskubenych/Library/Developer/CoreSimulator/Devices/73F1D853-78A8-46F8-B13E.jpg',
      //     origURL: 'assets-library://asset/asset.JPG?id=D78023AC-B8BD-46AB-B056-409B8FC3FFB4&ext=JPG',
      //     isVertical: true,
      //     height: 2560,
      //     width: 1440,
      //     fileName: 'IMG_0007.JPG'

      if (response.didCancel) {

      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else {
        this.setState({fileName: response['fileName']})
        onChange(response['data'])
      }
    });
  }

  selectPicture() {
    let { onChange } = this.props;
    let { navigate, goBack, label } = this.props.navigation;

    var options = {
      title: label,
      customButtons: [],
      storageOptions: {
        skipBackup: true,
        path: 'images'
      }
    };

    ImagePicker.launchImageLibrary(options, (response) => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else {
        this.setState({fileName: response['fileName']})
        onChange(response['data'])
      }
    });
  }

  removePicture() {
    let { onChange } = this.props;
    this.setState({fileName: null})
    onChange(null)
  }

  render() {
    let { label, error, style } = this.props;
    let { image, fileName } = this.state;

    return (
      <View
        style={[styles.container, style]}
      >

        <Text
          style={styles.label}
        >
          { fileName || label }
        </Text>

        {
          fileName
          ?
            (
              <TouchableWithoutFeedback
                onPress={ () => this.removePicture() }
              >
                <View
                  style={styles.cancelContainer}
                >
                  <Image
                    style={ styles.cancel }
                    source={require('../images/cancel.png')}
                    onPress={ () => {} }
                  />
                </View>
              </TouchableWithoutFeedback>
            )
            :
            null
        }


        <TouchableWithoutFeedback
          onPress={() => this.selectPicture() }
        >
          <View
            style={styles.paperclipContainer}
          >
            <Image
              style={ styles.paperclip }
              source={require('../images/paperclip.png')}
              onPress={ () => {} }
            />
          </View>
        </TouchableWithoutFeedback>

        <TouchableWithoutFeedback
          onPress={() => this.takePicture()}
        >
          <View
            style={styles.cameraContainer}
          >
            <Image
              style={ styles.camera }
              source={require('../images/camera.png')}
            />
          </View>
        </TouchableWithoutFeedback>

        <Text
          style={styles.errorText}
        >
          {error}
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: relativeWidth(33),
    marginTop: relativeWidth(4),
    flex: 1,
    justifyContent: 'center',
  },
  label: {
    fontSize: relativeWidth(19)
  },
  paperclipContainer: {
    width: relativeWidth(30),
    position: 'absolute',
    height: relativeWidth(30),
    top: relativeWidth(-9),
    right: relativeWidth(40),
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  cameraContainer: {
    width: relativeWidth(30),
    position: 'absolute',
    height: relativeWidth(30),
    top: relativeWidth(-9),
    right: 0,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  cancelContainer: {
    width: relativeWidth(30),
    position: 'absolute',
    height: relativeWidth(30),
    top: relativeWidth(-9),
    right: relativeWidth(80),
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  paperclip: {

  },
  camera: {

  },
  cancel: {

  },
  errorText: {
    color: 'red',
    marginTop: 10,
    width: '50%'
  }
});
