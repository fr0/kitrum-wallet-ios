import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  ActivityIndicator,
  Image
} from 'react-native';

export default class NavBackButton extends Component<{}> {

  render() {

    let { onPress, title, green, style } = this.props;

    return (
      <TouchableWithoutFeedback
        onPress={ () => onPress() }
      >
        <View
          style={[styles.container, style]}
        >
          {green?
            <Image
              source={require('../images/icons/chevron-back-green.png')}
              style={styles.chevron}
            />
            :
            <Image
              source={require('../images/icons/chevron-left.png')}
              style={styles.chevron}
            />
          }
          <Text style={styles.text}>{title}</Text>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    paddingLeft: 25
  },
  chevron: {
    width: 13,
    height: 22,
    top: 10,
    left: 5,
    position: 'absolute'
  },
  text: {
    color: 'white',
    fontSize: 18,
  }
});
