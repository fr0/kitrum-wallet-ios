import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback
} from 'react-native';

import { relativeWidth as rw } from './../services/dimensions'

export default class IbanButtonGroup extends Component {

  render() {

    const { items, style, textStyle, activeTextStyle, itemStyle, activeItemStyle } = this.props;

    return (
      <View
        style={[styles.container, style]}
      >
        {
          items.map((item, index) => {
            return (
              <TouchableWithoutFeedback key={index} onPress={() => item.active ? null : item.action() }>
                <View
                  style={[
                    styles.item,
                    itemStyle,
                    index == items.length - 1 ? styles.endItem : null, 
                    index == 0 ? styles.startItem : null, 
                    item.active ? styles.activeItem : null, 
                    item.active ? activeItemStyle : null
                  ]}
                >
                  <Text
                    style={[styles.text, textStyle, item.active ? activeTextStyle : null]}
                  >{item.value}</Text>
                </View>
              </TouchableWithoutFeedback>
            )
          })
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: 'transparent',
    marginBottom: 20,
    justifyContent: 'center',
    alignItems: 'stretch',
    height: rw(32)
  },
  text: {
    color: 'white',
    fontSize: rw(14)
  },
  item: {
    flex: 10,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderRightWidth: 1,
    borderLeftWidth: 0,
    borderColor: 'white',

    paddingTop: rw(10),
    paddingBottom: rw(10),
    justifyContent: 'center',
    alignItems: 'center'
  },
  startItem: {
    borderTopLeftRadius: 4,
    borderBottomLeftRadius: 4,
    borderLeftWidth: 1,
  },
  endItem: {
    borderTopRightRadius: 4,
    borderBottomRightRadius: 4,
  },
  activeItem: {
    backgroundColor: '#00CC99'
  }
});
