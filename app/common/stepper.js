import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback
} from 'react-native';

import { relativeWidth } from './../services/dimensions'

export default class IbanStepper extends Component<{}> {

  render() {

    let { circles, current } = this.props;
    let width = (current * 100 / circles) - (100 / circles / 2);
    if(current == circles){
      width = 100;
    }


    return (
      <View
        style={[styles.container, this.props.style]}
      >
        <View
          style={[styles.progress, {width: `${ width }%`}]}
        />
        {
          Array(circles).fill().map((_, index) => {
            return <View
              key={index}
              style={[styles.circle, index + 1 > current ? {} : styles.circleActive]}
            >
              <Text
                style={styles.index}
              >
                {index + 1}
              </Text>
            </View>
          })
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: relativeWidth(55),
    alignItems: 'center',
    height: relativeWidth(12),
    width: '100%',
    backgroundColor: '#D4D4D4',
    flexDirection: 'row',
    justifyContent: 'space-around',
    borderRadius: relativeWidth(6)
  },
  circle: {
    backgroundColor: '#D4D4D4',
    height: relativeWidth(34),
    width: relativeWidth(34),
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center'
  },
  circleActive: {
    backgroundColor: '#4CBA8D',
  },
  progress: {
    position: 'absolute',
    top: 0,
    left: 0,
    height: relativeWidth(12),
    backgroundColor: '#4CBA8D',
    borderRadius: relativeWidth(6)
  },
  index: {
    color: 'white',
    fontSize: relativeWidth(16)
  }
});
