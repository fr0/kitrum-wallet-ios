import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  Image,
  Alert,
  Dimensions,
  TouchableHighlight
} from 'react-native';
import { relativeWidth as rw} from './../../services/dimensions'

export default class IBanCheckBox extends Component {

  constructor(props) {
    super(props)
    this.state = {
      checked: false
    }
  }

  _onCheck(){
    this.setState({
      checked: !this.state.checked
    })
  }


  render() {
    let { checked } = this.state
    let { label, containerStyle, iconStyle, labelStyle } = this.props;

    return (
      <View>
       <TouchableWithoutFeedback
         onPress={() => this._onCheck()}
       >
         <View style={[s.container, containerStyle]}>
           <Image
             style={[s.icon, iconStyle]}
             source={
               checked
                 ?
                 require('./icons/checkbox-checked-green.png')
                 :
                 require('./icons/checkbox-outline-green.png')
             }
           />
           <Text
             style={[s.label, labelStyle]}
           >
             {label}
           </Text>
         </View>
       </TouchableWithoutFeedback>
      </View>
    );
  }
}

const s = StyleSheet.create({
  container: {
    marginTop: rw(5),
    backgroundColor: 'white',
    flexDirection: 'row',
    maxWidth: Dimensions.get('window').width
  },
  label: {
    paddingLeft: rw(15),
    paddingRight: rw(15),
    width: Dimensions.get('window').width - rw(55),
    fontSize: rw(12),
    lineHeight: rw(17)
  },
  icon: {
    marginLeft: rw(25),
    marginTop: rw(5),
    height: rw(28),
    width: rw(28),
  },
});
