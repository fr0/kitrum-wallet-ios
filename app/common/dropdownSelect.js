import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  Image,
  Alert,
  ScrollView,
  TextInput
} from 'react-native';
import IbanBankIcon from '../common/BankIcon';

import { relativeWidth } from './../services/dimensions'

export default class IbanDropdownSelect extends Component<{}> {

  constructor(props){
    super(props);
    this.state = {
      open: false,
      filter: ''
    }
  }

  _toggle(){
    let { open } = this.state;

    this.setState({
      open: !open
    })
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.outerClick != this.props.outerClick){
      this.setState({open: false})
    }
  }

  componentDidMount(){

  }

  render() {

    let { open, filter } = this.state;
    let { list } = this.props;

    return (
      <View
        style={[styles.container, this.props.style]}
      >
        <TouchableWithoutFeedback
          onPress={() => this._toggle()}
        >
          {
            open
              ?
              <View
                style={styles.openHeader}
              >
                <TextInput
                  style={styles.input}
                  onChangeText={filter => this.setState({filter})}
                  value={filter}
                  autoCapitalize='none'
                  selectionColor='#4CBA8D'
                />
                <Image
                  style={ styles.searchIcon }
                  source={require('./../images/icons/search.png')}
                />
              </View>
              :
              <View
                style={styles.closedHeader}
              >
                <Text
                  style={styles.placeholder}
                >
                  Seleccionar su banco
                </Text>

                <Image
                  style={ styles.bankIcon }
                  source={require('./../images/icons/bank-icon.png')}
                />
                <View
                  style={styles.bankIconBorder}
                />

                <Image
                  style={ styles.arrowIcon }
                  source={require('./../images/icons/chevron-down.png')}
                />
              </View>
          }
        </TouchableWithoutFeedback>

        {
          open
          ?
            <ScrollView
              style={styles.dropdown}
              showsHorizontalScrollIndicator={true}
            >
              {
                list.filter(i => i.name.toLowerCase().includes(filter.toLowerCase())).map((item, index) => {
                  return (
                    <TouchableWithoutFeedback
                      key={index}
                      onPress={() => this.props.onSelect(item)}
                    >
                      <View
                        style={styles.dropdownItem}
                      >
                        <IbanBankIcon
                          bankId={item['id']}
                          style={styles.dropdownItemIcon}
                        />
                        <Text
                          style={styles.dropdownItemText}
                        >
                          {item.name}
                        </Text>
                      </View>
                    </TouchableWithoutFeedback>
                  )
                })
              }
            </ScrollView>
          :
            null
        }

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: relativeWidth(53),
  },
  dropdown: {
    maxHeight: relativeWidth(372),
    width: '100%',
    position: 'absolute',
    top: '100%',
    marginTop: -1,
    backgroundColor: 'white',
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#D8D8D8'
  },
  dropdownItem: {
    borderTopWidth: 1,
    borderTopColor: '#D8D8D8',
    padding: relativeWidth(16.6)
  },
  dropdownItemIcon: {
    position: 'absolute',
    top: 10,
    left: 10,
    height: 30,
    width: 30
  },
  dropdownItemText: {
    marginLeft: 30
  },

  openHeader: {
    height: '100%',
    borderWidth: 1,
    borderColor: '#D8D8D8',
  },
  searchIcon: {
    position: 'absolute',
    width: relativeWidth(20),
    height: relativeWidth(20),
    top: relativeWidth(16),
    right: relativeWidth(16),
  },
  input: {
    height: '100%',
    padding: 10
  },

  closedHeader: {
    height: '100%',
    borderWidth: 1,
    borderColor: '#D8D8D8',
  },
  bankIcon: {
    position: 'absolute',
    width: relativeWidth(24),
    height: relativeWidth(26),
    top: relativeWidth(11),
    left: relativeWidth(18),
  },
  placeholder: {
    fontSize: relativeWidth(16),
    color: '#8E8E93',
    paddingTop: relativeWidth(14),
    paddingBottom: relativeWidth(10),
    paddingLeft: relativeWidth(85)
  },
  arrowIcon: {
    position: 'absolute',
    width: relativeWidth(36),
    height: relativeWidth(12),
    top: relativeWidth(18),
    right: relativeWidth(16),
  },
  bankIconBorder: {
    height: '100%',
    position: 'absolute',
    left: relativeWidth(60),
    borderLeftWidth: 1,
    borderColor: '#D8D8D8'
  },
});