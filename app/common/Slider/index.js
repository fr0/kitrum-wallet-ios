import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  ActivityIndicator,
  Image,
  Dimensions,
  PanResponder,
  Animated,
  Easing,
  Alert
} from 'react-native';

import { TextInputMask } from 'react-native-masked-text'
import { relativeWidth } from './../../services/dimensions'

export default class Slider extends Component {

  constructor(props) {
    super(props)
    this.state = {}

    this.styles = {
      left: 0
    }

    this.sliderWidth = 0
    this.shift = relativeWidth(30)
    this._previousLeft = 0
    this.winWidth = Dimensions.get('window').width

    pan: new Animated.ValueXY()

    this.panResponder = PanResponder.create({
      onStartShouldSetPanResponder    : () => true,
      
      onPanResponderMove              : (e, gestureState) => {
        this.styles.left += gestureState.dx - this._previousLeft 

        if(this.styles.left + this.shift <= 0){
          this.styles.left = -this.shift
          this._previousLeft = 0
        }
        if(this.styles.left + this.shift > this.sliderWidth){
          this.styles.left = this.sliderWidth - this.shift
        }

        this._previousLeft = gestureState.dx
        this._updateNativeStyles()
      },
      onPanResponderTerminationRequest: (evt, gestureState) => true,
      onPanResponderRelease: this._handlePanResponderEnd.bind(this),
      onPanResponderTerminate: this._handlePanResponderEnd.bind(this),
    });

    this.handleOnLayout = this.handleOnLayout.bind(this)
  }

  componentDidMount() {
    this._updateNativeStyles()
  }

  handleOnLayout(e) {
    this.sliderWidth = e.nativeEvent.layout.width
  }
  
  _updateNativeStyles() {
    const {value, maxValue, onChange} = this.props

    onChange((this.styles.left + this.shift) * maxValue / this.sliderWidth)

    this.refs.triangle && this.refs.triangle.setNativeProps(this.styles);
    this.refs.filler && this.refs.filler.setNativeProps({left: this.styles.left + this.shift});
  }

  _handlePanResponderEnd(e, gestureState) {
    this._previousLeft = 0
  }
  
  render() {
    const {onChange, value, style} = this.props

    let images = []
    let imgWidth = 210
  
    for(let i = 0; i < Math.ceil(this.winWidth / imgWidth); i++){
      images.push((
        <Image key={i} source={require('./images/slider.png')} />
      ))
    }

    return (
      <View 
        style={[styles.container, style]}
        onLayout={this.handleOnLayout}
      >
        <View style={styles.containerMirror}/>
        <Animated.View
          style={styles.unfilledArea}
          ref='filler'
        >
          {images}
        </Animated.View>

        <View style={styles.inputContainer}>
          <TextInputMask
            type={'money'}
            options={{
              unit: '',
              separator: ',',
              delimiter: '.'
            }}
            style={styles.input}
            onChangeText={v => onChange(v)}
            value={value}
          />
          <Text style={styles.inputText}>€</Text>
        </View>

        <Animated.View
          {...this.panResponder.panHandlers}
          style={styles.triangleContainer}
          ref='triangle'
        >
          <View style={styles.triangle}/>
        </Animated.View>
        
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#339966',
    height: relativeWidth(89)
  },
  containerMirror: {
    position: 'absolute',
    bottom: 0,
    backgroundColor: 'rgba(1, 1, 1, 0.17)',
    height: relativeWidth(25),
    width: '100%'
  },
  unfilledArea: {
    height: '100%',
    width: '100%',
    position: 'absolute',
    top: 0,
    flex:1,
    flexDirection:'row'
  },
  inputContainer: {
    marginTop: relativeWidth(10),
    marginLeft: relativeWidth(10),
    backgroundColor: 'white',
    height: relativeWidth(40),
    width: relativeWidth(180),
    flexDirection: 'row'
  },
  input: {
    height: '100%',
    width: '80%',
    color: 'gray',
    fontSize: relativeWidth(30),
    paddingLeft: relativeWidth(10)
  },
  inputText: {
    color: 'gray',
    fontSize: relativeWidth(30)
  },
  triangleContainer: {
    position: 'absolute',
    top: relativeWidth(52),
    
    width: relativeWidth(60),
    height: relativeWidth(60),
    alignItems: 'center',
    justifyContent: 'center',

  },
  triangle: {
    width: 0,
    height: 0,
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderTopWidth: 0,
    borderRightWidth: relativeWidth(16),
    borderBottomWidth: relativeWidth(25),
    borderLeftWidth: relativeWidth(16),
    borderTopColor: 'transparent',
    borderRightColor: 'transparent',
    borderBottomColor: 'black',
    borderLeftColor: 'transparent',
  }
});
